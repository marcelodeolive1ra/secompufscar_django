from django.http import JsonResponse

from secompufscar2019.constants import EDICAO_ATUAL
from secompufscar_bd.models import Evento
from django.template.loader import render_to_string
from sendgrid.helpers.mail import *
from sendgrid.sendgrid import SendGridAPIClient

from secompufscar_django import settings


def get_evento_atual():
    return Evento.objects.get(edicao=EDICAO_ATUAL)


def criar_resposta_json_erro(mensagem, status_code):
    response = JsonResponse({
        'status': 'erro',
        'mensagem': mensagem
    })
    response.status_code = status_code
    return response


def get_email_pronto_para_envio(assunto, remetente, destinatario, template, contexto, nome_remetente=''):
    template_email_contato_renderizado = render_to_string(
        template_name=template, context=contexto)

    email_remetente = Email(email=remetente, name=(nome_remetente if nome_remetente != '' else 'SECOMP UFSCar 2018'))
    conteudo_email = Content("text/html", template_email_contato_renderizado)
    email_pronto_para_envio = Mail(from_email=email_remetente, subject=assunto, to_email=Email(email=destinatario),
                                   content=conteudo_email)
    return email_pronto_para_envio.get()


def _status_envio_email(resposta_sendgrid):
    return str(resposta_sendgrid.status_code)[0] == '2'  # Convenção do SendGrid: 2xx == sucesso


def enviar_email(corpo_email):
    sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
    resposta_sendgrid = sg.client.mail.send.post(request_body=corpo_email)
    return _status_envio_email(resposta_sendgrid)


def abreviar_sobrenome(sobrenome_completo, min_len):
    if len(sobrenome_completo) > min_len:
        sobrenomes = sobrenome_completo.split(' ')

        if len(sobrenomes[-1]) <= 1:
            sobrenomes = sobrenomes[:-1]

        if len(sobrenomes) > 1:
            sobrenome_abreviado = ''
            for i in range(0, len(sobrenomes) - 1):
                if sobrenomes[i].lower() not in ['de', 'da'] and len(sobrenomes[i]) > 1:
                    sobrenome_abreviado += sobrenomes[i][0] + '. '
                else:
                    sobrenome_abreviado += sobrenomes[i] + ' '
            sobrenome_abreviado += sobrenomes[-1]
        else:
            sobrenome_abreviado = sobrenomes[0]
        return sobrenome_abreviado
    else:
        return sobrenome_completo
