import datetime
import os
from django.db import models

DIRETORIAS =(
    ('SC','Sociocultural'),
    ('DM','Design e Marketing'),
    ('TI','Tecnologia de Informação'),
    ('JF','Jurídico-Financeira'),
    ('PA','Patrocínio'),
    ('CO','Conteúdo')
)
CURSOS =(
    ('BCC','Bacharelado em Ciência da Computação'),
    ('ENC','Eng. de Computação'),
    ('PPG','Pós Graduação'),
)

class ProcessoSeletivo(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=100, blank=False, default="")
    email = models.EmailField(max_length=100, blank=False, default="",unique=True)
    tel = models.CharField(max_length=20, blank=False, unique=True, default="")
    aprovado = models.BooleanField(default=False)
    primeira_opcao = models.CharField(blank=False, choices=DIRETORIAS, max_length=2)
    segunda_opcao = models.CharField(blank=False, choices=DIRETORIAS, max_length=2 )
    curso = models.CharField(max_length=3,choices=CURSOS, blank=False)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)
    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'Processo Seletivo')
        verbose_name_plural = (u'Processo Seletivo')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome
