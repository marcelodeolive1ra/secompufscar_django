from django.core.exceptions import ValidationError
from django.forms import ModelForm, Form, CharField, Textarea, TextInput, FileInput, PasswordInput, EmailInput
from django.template.defaultfilters import filesizeformat
from secompufscar2018.models import ConteudoMesaRedonda, ConteudoMiniCurso, ConteudoPalestra, MadDog
from secompufscar2019.models import ProcessoSeletivo

CONTENT_TYPES = ['image']
MAX_UPLOAD_SIZE = "2621440"


class LoginForm(Form):
    username = CharField(label=(u'Username'))
    password = CharField(label=(u'Password'), widget=PasswordInput(render_value=False))


def getMesaRedondaWidget():
    widgets = {
        'nome': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu nome...'
        }),
        'email': EmailInput(attrs={
        'required': True,
        'placeholder': 'Escreva seu email...'
        }),
        'tel': TextInput(attrs={
            'required': True,
            'placeholder': '(xx) xxxxx - xxxx'
        }),
        'profissao': TextInput(attrs={
            'required': True,
            'placeholder': 'Informe sua profissão...'
        }),
        'empresa': TextInput(attrs={
            'required': True,
            'placeholder': 'Informe a universidade/empresa...'
        }),
        'bio': Textarea(attrs={
            'required': True,
            'placeholder': 'Breve descrição biográfica, a ser utilizada na divulgação',
            'rows': 5
        }),
        'foto': FileInput(attrs={
            'required': True,
        }),
        'face': TextInput(attrs={
            'required': False,
            'placeholder': 'Facebook (opcional)'
        }),
        'twitter': TextInput(attrs={
            'required': False,
            'placeholder': 'Twitter (opcional)'
        }),
        'github': TextInput(attrs={
            'required': False,
            'placeholder': 'Github (opcional)'
        }),
        'linkedin': TextInput(attrs={
            'required': False,
            'placeholder': 'LinkedIn (opcional)'
        }),
        'cidade': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva a cidade...'
        }),
        'data_chegada': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'data_saida': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'obs': Textarea(attrs={
            'placeholder': 'Deixe aqui alguma observação ou informação adicional que julgar necessário..'
        }),
    }
    return widgets


def getPalestraWidget():
    widgets = {
        'nome': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu nome...'
        }),
        'tel': TextInput(attrs={
            'required': True,
            'placeholder': '(xx) xxxxx - xxxx'
        }),
        'profissao': TextInput(attrs={
            'required': True,
            'placeholder': 'Informe sua profissão...'
        }),
        'bio': Textarea(attrs={
            'required': True,
            'placeholder': 'Breve descrição biográfica, a ser utilizada na divulgação',
            'rows': 5
        }),
        'foto': FileInput(attrs={
            'required': True,
        }),
        'face': TextInput(attrs={
            'required': False,
            'placeholder': 'Facebook (opcional)'
        }),
        'twitter': TextInput(attrs={
            'required': False,
            'placeholder': 'Twitter (opcional)'
        }),
        'github': TextInput(attrs={
            'required': False,
            'placeholder': 'Github (opcional)'
        }),
        'linkedin': TextInput(attrs={
            'required': False,
            'placeholder': 'LinkedIn (opcional)'
        }),
        'cidade': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva a cidade...'
        }),
        'data_chegada': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'data_saida': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'obs': Textarea(attrs={
            'placeholder': 'Deixe aqui alguma observação ou informação adicional que julgar necessário..'
        }),
        'obs_palestra':Textarea(attrs={
        'placeholder': 'Deixe aqui alguma observação, requisito ou informação'
                       'adicional que julgar necessário',
        'rows': 3,
        'required': False,
        }),
        'palestra_titulo':TextInput(attrs={
        'placeholder': 'Escreva o título da palestra...',
        }),
        'palestra_descricao':Textarea(attrs={
        'placeholder': 'Breve descrição da palestra,'
                       'a ser utilizada na divulgação no site e em nossa página no Facebook',
        'rows': 10
        }),
        'recursos':Textarea(attrs={
        'placeholder': 'Durante o evento, a SECOMP disponibilizar um computador e projetor para as palestras. Caso queira utilizar seu próprio computador ou necessite de softwares específicos para a palestra, liste-os no campo abaixo (se necessário, liste também demais equipamentos que serão necessários)',
        'rows': 10
        }),
        'email': EmailInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu email...'
        }),
    }
    return widgets


def getMadDogWidget():
    widgets = {
        'nome': TextInput(attrs={
            'required': True,
            'placeholder': 'Enter your name'
        }),
        'tel': TextInput(attrs={
            'required': True,
            'placeholder': 'Enter your telephone'
        }),
        'profissao': TextInput(attrs={
            'required': True,
            'placeholder': 'Enter your profession'
        }),
        'empresa': TextInput(attrs={
            'required': True,
            'placeholder': 'Enter the name of the company/university you works'
        }),
        'bio': Textarea(attrs={
            'required': True,
            'placeholder': 'Enter a brief biography',
            'rows': 5
        }),
        'foto': FileInput(attrs={
            'required': True,
        }),
        'face': TextInput(attrs={
            'required': False,
            'placeholder': 'Facebook (optional)'
        }),
        'twitter': TextInput(attrs={
            'required': False,
            'placeholder': 'Twitter (optional)'
        }),
        'github': TextInput(attrs={
            'required': False,
            'placeholder': 'Github (optional)'
        }),
        'linkedin': TextInput(attrs={
            'required': False,
            'placeholder': 'LinkedIn (optional)'
        }),
        'cidade': TextInput(attrs={
            'required': True,
            'placeholder': 'Enter the city'
        }),
        'data_chegada': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'data_saida': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'obs': Textarea(attrs={
            'placeholder': 'Leave some note or additional information here'
        }),
        'obs_palestra':Textarea(attrs={
        'placeholder': 'Leave some note or additional information here',
        'rows': 3,
        'required': False,
        }),
        'palestra_titulo':TextInput(attrs={
        'placeholder': 'Enter the lecture title',
        }),
        'palestra_descricao':Textarea(attrs={
        'placeholder': 'Enter a brief description',
        'rows': 10
        }),
        'recursos':Textarea(attrs={
        'placeholder': 'During the event, SECOMP will provide a computer and projector for the lectures. If you want to use your own computer or need specific software for the lecture, list them in this field (if necessary, list other equipment as well)',
        'rows': 10
        }),
        'email': EmailInput(attrs={
            'required': True,
            'placeholder': 'Enter your e-mail'
        }),
    }
    return widgets


def getMiniCursoWidget():
    widgets = {
        'nome': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu nome...'
        }),
        'tel': TextInput(attrs={
            'required': True,
            'placeholder': '(xx) xxxxx - xxxx'
        }),
        'profissao': TextInput(attrs={
            'required': True,
            'placeholder': 'Informe sua profissão...'
        }),
        'bio': Textarea(attrs={
            'required': True,
            'placeholder': 'Breve descrição biográfica, a ser utilizada na divulgação',
            'rows': 5
        }),
        'foto': FileInput(attrs={
            'required': True,
        }),
        'face': TextInput(attrs={
            'required': False,
            'placeholder': 'Facebook (opcional)'
        }),
        'twitter': TextInput(attrs={
            'required': False,
            'placeholder': 'Twitter (opcional)'
        }),
        'github': TextInput(attrs={
            'required': False,
            'placeholder': 'Github (opcional)'
        }),
        'linkedin': TextInput(attrs={
            'required': False,
            'placeholder': 'LinkedIn (opcional)'
        }),
        'cidade': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva a cidade...'
        }),
        'data_chegada': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'data_saida': TextInput(attrs={
            'required': True,
            'type': 'Date'
        }),
        'curso_titulo': TextInput(attrs={
        'placeholder': 'Escreva o título do minicurso...',
        }),
        'curso_descricao': Textarea(attrs={
        'placeholder': 'Breve descrição do minicurso,a ser utilizada para divulgação no site e em nossa página do '
                       'Facebook',
        }),
        'tipo_dinamica': Textarea(attrs={
        'placeholder': 'Ex.: um computador/dispositivo por dupla'
        }),
        'prereqs': Textarea(attrs={
        'placeholder': 'Se houver, liste aqui os pré-requisitos recomendados aos participantes',
        'rows': 3
        }),
        'lab_hw': Textarea(attrs={
        'placeholder': 'Liste aqui os requisitos de hardware...',
        'rows': 4
        }),
        'lab_sw': Textarea(attrs={
        'placeholder': 'Liste aqui os requisitos de software...',
        'rows': 4
        }),
        'obs': Textarea(attrs={
        'placeholder': 'Deixe aqui alguma observação, requisito ou informação'
                       'adicional que julgar necessário',
        'rows': 3
        }),
        'email': EmailInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu email...'
        }),
    }
    return widgets


def getProcessoSeletivo():
    widgets = {
        'nome': TextInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu nome...'
        }),
        'tel': TextInput(attrs={
            'required': True,
            'placeholder': '(xx) xxxxx - xxxx'
        }),
        'email': EmailInput(attrs={
            'required': True,
            'placeholder': 'Escreva seu email...'
        }),
    }
    return widgets


class ConteudoMesaRedondaForm(ModelForm):
    class Meta:
        model = ConteudoMesaRedonda
        fields = '__all__'

        def __init__(self):
            self.instance = None
            self.cleaned_data = None


        def clean_foto(self):
            content = self.cleaned_data['foto']
            content_type = content.content_type.split('/')[0]
            if content_type in CONTENT_TYPES:
                if content._size > MAX_UPLOAD_SIZE:
                    raise ValidationError(('Please keep filesize under %s. Current filesize %s') % (
                    filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(content._size)))
            else:
                raise ValidationError('File type is not supported')
            return content

        widgets = getMesaRedondaWidget()


class ConteudoMiniCursoForm(ModelForm):
    class Meta:
        def __init__(self):
            self.instance = None
            self.cleaned_data = None

        model = ConteudoMiniCurso
        fields = '__all__'
        widget = getMiniCursoWidget()
        widgets = widget


class ConteudoPalestraForm(ModelForm):
    class Meta:
        model = ConteudoPalestra
        fields = '__all__'
        widget = getPalestraWidget()
        widgets = widget


class ConteudoMaddogForm(ModelForm):
    class Meta:
        model = MadDog
        fields = '__all__'
        widget = getMadDogWidget()
        widgets = widget


class ProcessoSeletivoForm(ModelForm):
    class Meta:
        def __init__(self):
            self.instance = None
            self.cleaned_data = None
        model = ProcessoSeletivo
        fields = '__all__'
        widget = getProcessoSeletivo()
        widgets = widget
