from secompufscar_bd.models import Evento

EDICAO_ATUAL = 10

TEMPLATE_ALL_FORMS = 'secompufscar2019/forms/all_forms.html'
TEMPLATE_INDEX = 'secompufscar2019/index.html'
TEMPLATE_CONTATO = 'secompufscar2019/contato.html'
ATIVIDADES = 'atividades'
PALESTRAS = 'palestras'
PALESTRAS_EMPRESARIAIS = 'palestras_empresariais'
MINICURSOS = 'minicursos'
WORKSHOPS = 'workshops'
MESAS_REDONDAS = 'mesas_redondas'
PROCESSOS_SELETIVOS = 'processos_seletivos'
DESAFIO_PROGRAMADORES = 'desafio_programadores'
PATROCINADORES = 'patrocinadores'
PATROCINADORES_DIAMANTE = 'patrocinadores_diamante'
PATROCINADORES_OURO = 'patrocinadores_ouro'
PATROCINADORES_PRATA = 'patrocinadores_prata'
PATROCINADORES_DESAFIO = 'patrocinadores_desafio'
APOIADORES = 'apoiadores'
AGORA = 'agora'
TIPO_ATIVIDADE_PROCESSO_SELETIVO = 'Processo Seletivo'
TIPO_ATIVIDADE_MESA_REDONDA = 'Mesa-Redonda'
TIPO_ATIVIDADE_WORKSHOP = 'Workshop'
TIPO_ATIVIDADE_MINICURSO = 'Minicurso'
TIPO_ATIVIDADE_PALESTRA_EMPRESARIAL = 'Palestra Empresarial'
TIPO_ATIVIDADE_PALESTRA = 'Palestra'
TEMPLATE_ATIVIDADES = 'secompufscar2019/programacao/atividades.html'
TEMPLATE_AREA_CONTEUDO = 'secompufscar2019/area_conteudo/area-conteudo-main.html'
TEMPLATE_FAQ = 'secompufscar2019/faq.html'
TEMPLATE_CARAVANAS = 'secompufscar2019/caravanas.html'
TEMPLATE_EQUPE = 'secompufscar2019/equipe.html'
TEMPLATE_APRESENTACAO = 'secompufscar2019/a_secomp/apresentacao.html'
TEMPLATE_SOBRE = 'secompufscar2019/a_secomp/asecomp.html'
TEMPLATE_PATROCINIO = 'secompufscar2019/patrocinio.html'
TEMPLATE_SOCIO = 'secompufscar2019/a_secomp/sociocultural.html'
TEMPLATE_CARABELLA = 'secompufscar2019/glados/enrichment.html'
TEMPLATE_400 = '400.html'
TEMPLATE_403 = '403.html'
TEMPLATE_404 = '404.html'
TEMPLATE_500 = '500.html'
TEMPLATE_RECAPTCHA = 'RECAPTCHA.html'
TEMPLATE_SUCESSO = 'erros/sucesso.html'
TEMPLATE_PROCESSO_SELETIVO = 'secompufscar2019/processo_seletivo.html'
TEMPLATE_PROGRAMACAO = 'secompufscar2019/programacao/programacao.html'
TEMPLATE_MINISTRANTES = 'secompufscar2019/programacao/ministrantes.html'
TEMPLATE_CRONOGRAMA = 'secompufscar2019/programacao/cronograma.html'
TEMPLATE_CTF = 'secompufscar2019/programacao/ctf.html'
TEMPLATE_EMAIL_DE_CONTATO = 'secompufscar2017/helpers/email_de_contato.html'
#Área - Conteudo
TEMPLATE_AREA_CONTEUDO_LOGIN = 'secompufscar2019/area_conteudo/login.html'
TEMPLATE_AREA_CONTEUDO_INFO = 'secompufscar2019/area_conteudo/info.html'
TEMPLATE_FORM = 'secompufscar2019/area_conteudo/forms_conteudo.html'
TEMPLATE_AREA_CONTEUDO_PROCESSO_SELETIVO = 'secompufscar2019/area_conteudo/processo_seletivo.html'
TEMPLATE_AREA_CONTEUDO_PROCESSO_SELETIVO_TEMP = 'secompufscar2019/ps_encerrado_temp.html'
TEMPLATE_MADDDOG = 'secompufscar2019/area_conteudo/maddog/form_maddog.html'
TEMPLATE_EMAIL_GENERICO_MADDOG = \
    'secompufscar2019/area_administrativa/templates_email/email_generico_maddog.html'

EMAIL_CONTATO_SECOMP = Evento.objects.get(edicao=EDICAO_ATUAL).email_destino_formulario_contato
EMAIL_ADMIN = 'gabriel@secompufscar.com.br'
EMAIL_CONTEUDO = 'conteudo@secompufscar.com.br'
EMAIL_SECOMP = 'contato@secompufscar.com.br'
EMAIL_PAGAMENTOS_SECOMP = 'pagamentos@secompufscar.com.br'
EMAIL_TI_SECOMP = 'ti@secompufscar.com.br'

TIPO_MESA_REDONDA = 0
TIPO_PALESTRA = 2
TIPO_MINICURSO = 1
TIPO_MADDOG = 3

GROUP_CONTEUDO = 'Conteudo'
