from PIL import Image
from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir("/home/olivato/Documentos/secompufscar_django/secompufscar2018/static/secompufscar2018/images/favicons") if isfile(join("/home/olivato/Documentos/secompufscar_django/secompufscar2018/static/secompufscar2018/images/favicons", f))]

for f in onlyfiles:
    try:
        img = Image.open(f)
        img = img.convert("RGBA")
        datas = img.getdata()

        newData = []
        for item in datas:
            if item[0] > 200 and item[1] > 200 and item[2] > 200:
                newData.append((255, 255, 255, 0))
            else:
                newData.append(item)

        img.putdata(newData)
        img.save(f, "PNG")
    except:
        pass