face = $('#face_ministrantes');
twitter = $('#twitter_ministrantes');
linkedin = $('#linkedin_ministrantes');
git = $('#github_ministrantes');
modal = $('.ui.large.modal');
foto_default = $('#foto_default').html();

function request_ministrante(tipo, id)
{
    url = window.location.protocol + '//' + window.location.host + "/api/ministrantes-single/" + id + "/" + tipo;
    $.get(url, function (data, status) {

        $('#nome_ministrante').html(data['ministrante'].nome);

        face.show();
        if ( data['ministrante'].face ) face.attr('href', data['ministrante'].face);
        else face.hide();

        git.show();
        if ( data['ministrante'].github ) git.attr('href', data['ministrante'].github);
        else git.hide();

        linkedin.show();
        if ( data['ministrante'].linkedin ) linkedin.attr('href', data['ministrante'].linkedin);
        else linkedin.hide();

        twitter.show();
        if ( data['ministrante'].twitter ) twitter.attr('href', data['ministrante'].twitter);
        else twitter.hide();

        profissao = data['ministrante'].profissao + ', ' + data['ministrante'].empresa;
        $('#profissao').html(profissao);

        $('#bio').html(data['ministrante'].bio);

        $('#foto').show();
        if ( data['ministrante'].foto ) $('#foto').attr('src', "/media/"+data['ministrante'].foto);
        else $('#foto').attr('src', foto_default);
        $('#description').show();
        $('#gif').hide();
        $('.image.content').show();
        modal.modal('refresh');
    })
}

function open_modal(id_tipo)
{
    temp = id_tipo.split('_');
    id = temp[0];
    tipo = temp[1];
    $('#gif').show();
    $('#nome_ministrante').html('');
    $('#profissao').html('');
    $('#bio').html('');
    $('#foto').hide();
    $('#description').hide();
    $('.image.content').hide();

    face.hide();
    git.hide();
    linkedin.hide();
    twitter.hide();

    request_ministrante(tipo, id);
    modal.modal('show');
}
