
$('input').removeAttr('required');
$('textarea').removeAttr('required');
$('select').removeAttr('required');
allowSubmit = false;
sucesso = $('#sucesso');
sucesso.hide();
erro = $('#erro');
erro.hide();
botao_enviar = $("#botao_enviar");
form = $('.ui.form');
campo_tel = $('#id_tel');
formulario_post = $('#post-form');
formulario_post.submit(upload);
function show_errors(data)
{
    msg = '';
    dict = JSON.parse(data);
    dict = dict['mensagem'];
    if(dict['nome'])
        msg += '<br>Nome:'+dict['nome'];
    if(dict['tel'])
        msg += '<br>Telefone:'+dict['tel'];
    if(dict['email'])
        msg += '<br>Email:'+dict['email'];
    if(dict['primeira_opcao'])
        msg += '<br>Primeira Opção:'+dict['primeira_opcao'];
    if(dict['segunda_opcao'])
        msg += '<br>Segunda Opção:'+dict['segunda_opcao'];
    if(dict['curso'])
        msg += '<br>Curso:'+dict['curso'];
    $("#msgs").html(msg)
}

function upload(event) {
    if(allowSubmit) {
        campo_tel.unmask();
        event.preventDefault();
        var data = new FormData(formulario_post.get(0));
        botao_enviar.addClass('loading');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                grecaptcha.reset();
                form.hide();
                erro.hide();
                sucesso.show(100);
                botao_enviar.removeClass('loading');
            },
            error: function (data) {
                grecaptcha.reset();
                erro.show(100);
                sucesso.hide();
                botao_enviar.removeClass('loading');
                show_errors(data.responseText);
            }
        });
    }
    return false;
}

$(document).on('keydown', '#id_tel', function (e) {
    var digit = e.key.replace(/\D/g, '');
    var value = $(this).val().replace(/\D/g, '');
    var size = value.concat(digit).length;
    $(this).mask((size <= 10) ? '(00)00000-0000' : '(00)00000-0000');
});
form
    .form({
        fields:{
            nome:{
                identifier: 'nome',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe o seu nome.'
                    }
                ]
            },
            tel:{
                identifier: 'tel',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe o seu telefone.'
                    }
                ]
            },
            email:{
                identifier: 'email',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe o seu email.'
                    }
                ]
            },
            primeira_opcao:{
                identifier: 'primeira_opcao',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe a sua primeira opção de diretoria.'
                    }
                ]
            },
            segunda_opcao:{
                identifier: 'segunda_opcao',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe a sua segunda opção de diretoria.'
                    }
                ]
            },
            curso:{
                identifier: 'curso',
                rules:[
                    {
                        type: 'empty',
                        prompt: 'Informe o seu curso.'
                    }
                ]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
    e.preventDefault();
});

function captcha_filled () {
    allowSubmit = true;
}
function captcha_expired () {
    allowSubmit = false;
}

$(document).on('keydown', '#id_tel', function (e) {
    var digit = e.key.replace(/\D/g, '');
    var value = $(this).val().replace(/\D/g, '');
    var size = value.concat(digit).length;
    $(this).mask((size <= 10) ? '(00)00000-0000' : '(00)00000-0000');
});

$('.ui.accordion').accordion();
