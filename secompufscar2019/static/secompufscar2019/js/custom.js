/* jQuery Pre loader
 -----------------------------------------------*/
$(window).on("load", function () {
    $(".preloader").fadeOut(1000); // set duration in brackets
});

/* HTML document is loaded. DOM is ready.
 -------------------------------------------*/
$(document).ready(function () {

    // /* Back to top
    //  -----------------------------------------------*/
    $(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$(".go-top").fadeIn();
		} else {
			$(".go-top").fadeOut();
		}
	});

	//Click event to scroll to top
	$(".go-top").click(function(){
		$("html, body").animate({scrollTop : 0},800);
		return false;
	});

    /* wow
     -------------------------------*/
    var wow = new WOW({mobile: true});
    wow.init();
});