allowSubmit = false;
captcha_warning = $('#captcha_warning');
captcha_warning.hide();
sucesso = $('#sucesso');
sucesso.hide();
erro = $('#erro');
erro.hide();
botao_enviar = $("#botao_enviar");
form = $('.ui.form');
campo_tel = $('#id_tel');
formulario_post = $('#post-form');
checkbox_locomocao = $( "#id_locomocao" );
checkbox_vaga_na_garagem = $( "#id_acomodacao" );
field_vaga = $("#vaga_garagem");
field_locomocao = $("#locomocao_tipo_modo");



//Remove o required default do HTML para adicionar nossos próprios via js
$('input').removeAttr('required');
$('textarea').removeAttr('required');
$('select').removeAttr('required');

$(function() {
    formulario_post.submit(upload);
});
function show_errors(data)
{
    msg = '';
    dict = JSON.parse(data);
    dict = dict['mensagem'];
    if(dict['nome'] !== undefined)
        msg += '<br>Name:'+dict['nome'];
    if(dict['email'])
        msg += '<br>Email:'+dict['email'];
    if(dict['tel'] !== undefined)
        msg += '<br>Phone Number:'+dict['tel'];
    if(dict['profissao'] !== undefined)
        msg += '<br>Profession:'+dict['profissao'];
    if(dict['empresa'] !== undefined)
        msg += '<br>Company/University:'+dict['empresa'];
    if(dict['bio'] !== undefined)
        msg += '<br>Brief biography:'+dict['bio'];
    if(dict['foto'] !== undefined)
        msg += '<br>Photo:'+dict['foto'];
    if(dict['github'] !== undefined)
        msg += '<br>Github:'+dict['github'];
    if(dict['face'] !== undefined)
        msg += '<br>Facebook:'+dict['face'];
    if(dict['twitter'] !== undefined)
        msg += '<br>Twitter:'+dict['twitter'];
    if(dict['linkedin'] !== undefined)
        msg += '<br>LinkedIn:'+dict['linkedin'];
    if(dict['tamanho_camiseta'] !== undefined)
        msg += '<br>T-Shirt Size:'+dict['tamanho_camiseta'];
    if(dict['cidade'] !== undefined)
        msg += '<br>City:'+dict['cidade'];
    if(dict['locomocao'] !== undefined)
        msg += '<br>Transport:'+dict['locomocao'];
    if(dict['locomocao_tipo'] !== undefined)
        msg += '<br>Type of Transport:'+dict['locomocao_tipo'];
    if(dict['data_chegada'] !== undefined)
        msg += '<br>Date of arrive:'+dict['data_chegada'];
    if(dict['data_saida'] !== undefined)
        msg += '<br>Date of departure:'+dict['data_saida'];
    if(dict['acomodacao'] !== undefined)
        msg += '<br>Accommodation:'+dict['acomodacao'];
    if(dict['vaga_acomodacao'] !== undefined)
        msg += '<br>Parking lot:'+dict['vaga_acomodacao'];
    if(dict['obs'] !== undefined)
        msg += '<br>Observation:'+dict['obs'];
    if(dict['palestra_titulo'] !== undefined)
        msg += '<br>Lecture\'s Title:'+dict['palestra_titulo'];
    if(dict['palestra_descricao'] !== undefined)
        msg += '<br>Lecture\'s Description:'+dict['palestra_descricao'];
    if(dict['recursos'] !== undefined)
        msg += '<br>Resources:'+dict['recursos'];
    if(dict['obs_palestra'] !== undefined)
        msg += '<br>Lecture\'s Observation:'+dict['obs_palestra'];
    $("#msgs").html(msg)
}

function upload(event) {
    if(allowSubmit) {
        campo_tel.unmask();
        event.preventDefault();
        var data = new FormData(formulario_post.get(0));
        botao_enviar.addClass('loading');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                grecaptcha.reset();
                form.hide();
                erro.hide();
                sucesso.show(100);
                botao_enviar.removeClass('loading');
            },
            error: function (data) {
                grecaptcha.reset();
                erro.show(100);
                sucesso.hide();
                botao_enviar.removeClass('loading');
                show_errors(data.responseText);
            }
        });
    }
    else {
        captcha_warning.show();
    }
    return false;
}


field_vaga.toggle(checkbox_vaga_na_garagem.checked);
field_locomocao.toggle(checkbox_locomocao.checked);
checkbox_locomocao.click(function() {
    field_locomocao.toggle(this.checked);
});

checkbox_vaga_na_garagem.click(function() {
    field_vaga.toggle(this.checked);
});

//Campos required criado por nós
form.form({
    fields: {
        nome: {
            identifier: 'nome',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter your name'
            }
            ]
        },
        tel: {
            identifier: 'tel',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter your telephone'
            }]
        },
        email: {
            identifier: 'email',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter your e-mail'
            }]
        },
        profissao: {
            identifier: 'profissao',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter your profession'
            }]
        },
        empresa: {
            identifier: 'empresa',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the name of the company/university you works'
            }]
        },
        bio: {
            identifier: 'bio',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter a brief biography'
            }]
        },
        tamanho_camiseta: {
            identifier: 'tamanho_camiseta',
            rules: [{
                type: 'empty',
                prompt: 'Please, select a T-Shirt size'
            }]
        },
        curso_descricao:{
            identifier: 'curso_descricao',
            rules: [{
                type: 'empty',
                prompt: 'Informe a descrição do minicurso'
            }]
        },
        tipo_dinamica:{
            identifier: 'tipo_dinamica',
            rules: [{
                type: 'empty',
                prompt: 'Informe o tipo de dinâmica'
            }]
        },
        lab_hw:{
            identifier: 'lab_hw',
            rules: [{
                type: 'empty',
                prompt: 'Informe os requisitos de hardware'
            }]
        },
        lab_sw:{
            identifier: 'lab_sw',
            rules: [{
                type: 'empty',
                prompt: 'Informe os requisitos de software'
            }]
        },
        cidade:{
            identifier: 'cidade',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the city'
            }]
        },
        locomocao_tipo:{
            identifier: 'locomocao_tipo',
            depends: 'locomocao',
            rules: [{
                type: 'empty',
                prompt: 'Please, select one'
            }]
        },
        data_chegada:{
            identifier: 'data_chegada',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the arrival date'
            }]
        },
        data_saida:{
            identifier: 'data_saida',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the departure date'
            }]
        },
        palestra_titulo: {
            identifier: 'palestra_titulo',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the lecture title'
            }]
        },
        palestra_descricao: {
            identifier: 'palestra_descricao',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the lecture description'
            }]
        },
        recursos: {
            identifier: 'recursos',
            rules: [{
                type: 'empty',
                prompt: 'Please, enter the necessary resources'
            }]
        }
    },inline: true,
    on: 'blur',
    onSuccess: function () {
        form.removeClass('success').removeClass('error');
    },
    onFailure: function () {
        window.scrollTo(0, 0);
        form.removeClass('success').removeClass('error');
    }
}).on('submit', function (e) {
    e.preventDefault();
});

function captcha_filled () {
    allowSubmit = true;
    captcha_warning.hide();
}
function captcha_expired () {
    allowSubmit = false;
    captcha_warning.show();
}

pessoal = $("#part_one");
minicurso = $("#info-minicurso");
palestra = $("#part_two");
locomocao = $("#part_three");
step1 = $("#one");
step2 = $("#two");
step3 = $("#three");

minicurso.hide();
palestra.hide();
locomocao.hide();

function setValidation1(){
    form.form({
        fields: {
            nome: {
                identifier: 'nome',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter your name'
                }
                ]
            },
            tel: {
                identifier: 'tel',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter your telephone'
                }]
            },
            email: {
                identifier: 'email',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter your e-mail'
                }]
            },
            profissao: {
                identifier: 'profissao',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter your profession'
                }]
            },
            empresa: {
                identifier: 'empresa',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the name of the company/university you works'
                }]
            },
            bio: {
                identifier: 'bio',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter a brief biography'
                }]
            },
            tamanho_camiseta: {
                identifier: 'tamanho_camiseta',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, select a T-Shirt size'
                }]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}


function setValidation2_2(){
    form.form({
        fields: {
            palestra_titulo: {
                identifier: 'palestra_titulo',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the lecture title'
                }]
            },
            palestra_descricao: {
                identifier: 'palestra_descricao',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the lecture description'
                }]
            },
            recursos: {
                identifier: 'recursos',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the necessary resources'
                }]
            }
        }
        ,inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function setValidation3(){
    form.form({
        fields:{
            cidade:{
                identifier: 'cidade',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the city'
                }]
            },
            locomocao_tipo:{
                identifier: 'locomocao_tipo',
                depends: 'locomocao',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, select one'
                }]
            },
            data_chegada:{
                identifier: 'data_chegada',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the arrival date'
                }]
            },
            data_saida:{
                identifier: 'data_saida',
                rules: [{
                    type: 'empty',
                    prompt: 'Please, enter the departure date'
                }]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function proxPessoal() {
    setValidation1();
    if(form.form('is valid')){
        pessoal.hide(500);
        palestra.show(500);
        step1.removeClass("active");
        step1.addClass("completed");
        step2.addClass("active");
        setValidation2_2();
    }
}

function anterior2(){
    palestra.hide(500);
    pessoal.show(500);
    step2.removeClass("active");
    step1.removeClass("completed");
    step1.addClass("active");
    setValidation1();
}

function proximo2(){
    if(form.form('is valid')) {
        palestra.hide(500);
        locomocao.show(500);
        step2.removeClass("active");
        step2.addClass("completed");
        step3.addClass("active");
        setValidation3();
    }
}

function anterior3(){
    locomocao.hide(500);
    palestra.show(500);
    step3.removeClass("active");
    step2.removeClass("completed");
    step2.addClass("active");
    setValidation2_2();
}



function isValid(){
    form.form('is valid');
}

var modal_edital = $('.ui.large.modal');
modal_edital.modal();
modal_edital.modal('attach events', '#botao_modal_edital');