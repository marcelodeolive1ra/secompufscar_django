allowSubmit = false;
formulario = $(".ui.form");
captcha_warning = $("#captcha_warning");
captcha_warning.hide();
botao_enviar = $('#botao_enviar_email');

function isFormularioValido(formulario) {
    return formulario.form('is valid', ['email', 'cidade', 'integrantes']);
}

function captcha_filled () {
    allowSubmit = true;
    captcha_warning.hide();
}

function captcha_expired () {
    allowSubmit = false;
    captcha_warning.show();
}

botao_enviar.on('click', function () {
        if (allowSubmit) {
            formulario.removeClass('success').removeClass('error');

            if (isFormularioValido(formulario)) {
                botao_enviar.addClass('loading');
                $.post("caravanas/enviar-email/", formulario.serialize())
                    .done(function () {
                        formulario.addClass('success');
                        botao_enviar.removeClass('loading');
                    }).fail(function () {
                    formulario.addClass('error');
                    botao_enviar.removeClass('loading');
                })
            }
        }
        else {
            captcha_warning.show();
        }
    }
);

$('.message .close').on('click', function () {
    formulario.removeClass('success').removeClass('error');
});

formulario.form({
    fields: {
        name: {
            identifier: 'email',
            rules: [
                {
                    type: 'email',
                    prompt: 'Digite um e-mail válido.'
                }
            ]
        },
        email: {
            identifier: 'cidade',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Informe a cidade de partida.'
                }
            ]
        },
        message: {
            identifier: 'integrantes',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Informe o nome dos integrantes.'
                }
            ]
        }
    },
    inline: true,
    on: 'blur',
    onSuccess: function () {
        formulario.removeClass('success').removeClass('error');
    },
    onFailure: function () {
        formulario.removeClass('success').removeClass('error');
    }
}).on('submit', function (e) {
    e.preventDefault();
    return false
});
