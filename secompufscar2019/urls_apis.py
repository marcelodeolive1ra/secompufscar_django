from django.urls import path

from secompufscar2019 import views_apis

urlpatterns = [
    path('ministrantes/',views_apis.api_area_conteudo_ministrantes, name='api-mesa-redonda'),
    path('atividades-app/',views_apis.atividades, name='api-atividades-app'),
    path('ministrantes-all/',views_apis.api_todos_ministrantes, name='api-ministrantes-all'),
    path('ministrantes-single/<int:ministrante>/<int:tipo>',views_apis.api_single_ministrantes, name='api-ministrantes-single'),
    path('atividades-single/<int:atividade>/',views_apis.api_single_atividades, name='api-atividades-single'),
    path('registra-presenca/<str:codigo>/<int:atividade_id>/',views_apis.registra_presenca, name='api-registra-presenca'),
    path('amanda-nude/<str:codigo>/',views_apis.manda_nude_from_inscricao, name='api-insformacoes-inscricao'),
    path('nojenta/',views_apis.api_dificil_do_app, name='api-antes-agora-depois-atividades'),
]
