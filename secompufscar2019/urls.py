import locale

from django.conf.urls import include
from django.urls import path

from secompufscar2019 import views

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
app_name = 'secompufscar2019'

urlpatterns = [
    path('', views.index, name='index'),
    path('faq', views.faq, name='faq'),
    path('patrocinio', views.patrocinio, name='patrocinio'),
    path('equipe', views.equipe, name='equipe'),
    path('a-secomp/', views.a_secomp, name='asecomp'),
    path('a-secomp/apresentacao/', views.apresentacao, name='apresentacao'),
    path('a-secomp/sociocultural/', views.sociocultural, name='sociocultural'),
    path('area-conteudo/',include('secompufscar2019.urls_area_conteudo')),
    path('recaptcha/', views.erro_recaptcha, name='recaptcha'),
    path('contato/enviar-email/', views.enviar_email_contato, name='enviar_email_contato'),
    path('404/', views.erro_404, name='404'),
    path('500/', views.erro_500, name='400')
]
