from django import template
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Min
from django.db.models.signals import post_save, post_delete, m2m_changed
from django.dispatch import receiver
from django.shortcuts import render
from django.utils import timezone
import requests
from secompufscar2017.forms import FormularioDeContato
from secompufscar2017.utilities import abreviar_sobrenome, criar_resposta_json_sucesso, criar_resposta_json_erro, get_email_pronto_para_envio, enviar_email
from secompufscar2017.views import is_sistema_em_producao, get_google_analytics_key, AGORA, _enviar_email_de_contato
from secompufscar2019.constants import *
from secompufscar2019.utilities import get_evento_atual
from secompufscar2019.forms import ProcessoSeletivoForm
from secompufscar2018.models import ConteudoMiniCurso, ConteudoPalestra, ConteudoMesaRedonda, MadDog
from secompufscar_bd.models import Diretoria, TipoAtividade
from secompufscar_django import settings

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
	group = Group.objects.get(name=group_name)
	return group in user.groups.all()


evento = get_evento_atual()


def _get_patrocinadores_por_cota(nome_cota):
	return evento.patrocinadores.filter(ativo_site=True, cota__nome=nome_cota).order_by('ordem_site')


def get_todos_ministrantes():
	minicurso = ConteudoMiniCurso.objects.all()
	palestrantes = ConteudoPalestra.objects.all()
	mesaredonda = ConteudoMesaRedonda.objects.all()
	maddog = MadDog.objects.all()

	ministrantes = []
	for m in minicurso:
		if m.nome not in [pessoas['nome'] for pessoas in ministrantes]:
			ministrantes.append(
				{'id': m.id,
				 'nome': m.nome,
				 'tipo': TIPO_MINICURSO,
				 'foto': str(m.foto)}
			)

	for m in mesaredonda:
		if m.nome not in [pessoas['nome'] for pessoas in ministrantes]:
			ministrantes.append(
				{'id': m.id,
				 'nome': m.nome,
				 'tipo': TIPO_MESA_REDONDA,
				 'foto': str(m.foto)}
			)

	for m in palestrantes:
		if m.nome not in [pessoas['nome'] for pessoas in ministrantes]:
			ministrantes.append(
				{'id': m.id,
				 'nome': m.nome,
				 'tipo': TIPO_PALESTRA,
				 'foto': str(m.foto)}
			)

	for m in maddog:
		ministrantes.append(
			{
				'id': m.id,
				'nome': m.nome,
				'tipo': TIPO_MADDOG,
				'foto': str(m.foto)
			}
		)

	return ministrantes

def _ajustar_tamanho_de_nome_para_pagina_equipe(membros):
	for membro in membros:
		sobrenome_completo = membro.participante.usuario.last_name
		membro.participante.usuario.last_name = abreviar_sobrenome(sobrenome_completo=sobrenome_completo, min_len=20)
	return membros


def _get_data_hora_inicio_evento():
	return evento.data_hora_inicio_evento


def _get_diretorias():
	return [{'nome': diretoria.nome,
			 'membros': _ajustar_tamanho_de_nome_para_pagina_equipe(evento.membros_equipe.filter(
				 diretoria=diretoria).order_by('-cargo__nome', 'participante__usuario__first_name',
											   'participante__usuario__last_name'))}
			for diretoria in Diretoria.objects.all()]


def _get_menus_site_ativos():
	return [menu.nome for menu in get_evento_atual().menus_site_ativos.filter(ativo=True)]


def _get_perguntas_frequentes():
	return evento.perguntas_frequentes.order_by('prioridade')

def _get_tipo_atividade(nome_tipo_atividade):
	try:
		return TipoAtividade.objects.get(nome=nome_tipo_atividade)
	except ObjectDoesNotExist:
		return None


def _get_atividades_por_tipo(nome_tipo_atividade):
	return evento.atividades.filter(ativa=True, tipo=_get_tipo_atividade(nome_tipo_atividade=nome_tipo_atividade)). \
		annotate(primeira_data=Min('horarios__data_hora_inicio')).order_by('primeira_data', 'titulo')


def TIPO_ATIVIDADE_(args):
	pass


def _get_dicionario_contexto():
	return {
		'producao': is_sistema_em_producao(),
		'google_analytics_key': get_google_analytics_key(),
		'patrocinadores': {
			'diamante': _get_patrocinadores_por_cota(nome_cota='Diamante'),
			'ouro': _get_patrocinadores_por_cota(nome_cota='Ouro'),
			'prata': _get_patrocinadores_por_cota(nome_cota='Prata'),
			'apoio': _get_patrocinadores_por_cota(nome_cota='Apoio'),
		},
		PALESTRAS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PALESTRA),
		PALESTRAS_EMPRESARIAIS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PALESTRA_EMPRESARIAL),
		MINICURSOS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_MINICURSO),
		WORKSHOPS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_WORKSHOP),
		MESAS_REDONDAS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_MESA_REDONDA),
		PROCESSOS_SELETIVOS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PROCESSO_SELETIVO),
		'diretorias': _get_diretorias(),
		'perguntas_frequentes': _get_perguntas_frequentes(),
		'menus_ativos': _get_menus_site_ativos(),
		'data_hora_inicio_evento': _get_data_hora_inicio_evento(),
		'evento': get_evento_atual(),
		'permissoes': {},
		'ministrantes': get_todos_ministrantes(),
		'form':None
	}


dicionario_contexto = _get_dicionario_contexto()


def atualizar_dicionario_permissoes():
	dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA'] = []
	for permissao in get_evento_atual().permissoes.all():
		dicionario_contexto['permissoes'][permissao.nome] = []
		for participante in permissao.usuarios_permitidos.all():
			dicionario_contexto['permissoes'][permissao.nome].append(participante.usuario)
			if participante.usuario not in dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA']:
				dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA'].append(participante.usuario)


def _atualizar_dicionario_contexto():
	dicionario_contexto.update(_get_dicionario_contexto())
	atualizar_dicionario_permissoes()


@receiver(post_save, sender=Evento)
@receiver(post_delete, sender=Evento)
def _atualizar_evento_dicionario_contexto(sender, **kwargs):
	_atualizar_dicionario_contexto()

m2m_changed.connect(_atualizar_evento_dicionario_contexto, sender=Evento.menus_site_ativos.through)
m2m_changed.connect(_atualizar_evento_dicionario_contexto, sender=Evento.tipos_atividades.through)
m2m_changed.connect(_atualizar_evento_dicionario_contexto, sender=Evento.membros_equipe.through)


def enviar_email_contato(request):
	if request.method == 'POST':
		formulario_contato = FormularioDeContato(request.POST)
		if formulario_contato.is_valid():
			nome = formulario_contato.cleaned_data['nome']
			email = formulario_contato.cleaned_data['email']
			mensagem = formulario_contato.cleaned_data['mensagem']
			recaptcha_response = request.POST.get('g-recaptcha-response')
			data = {
				'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
				'response': recaptcha_response
			}
			r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
			result = r.json()
			if result['success']:
				if _enviar_email_de_contato(nome=nome, email=email, mensagem=mensagem):
					return criar_resposta_json_sucesso('E-mail enviado com sucesso.')
				else:
					return criar_resposta_json_erro(mensagem='Erro no serviço de envio de e-mail.', status_code=503)
			else:
				return criar_resposta_json_erro(mensagem='ReCaptcha inválido.', status_code=503)
		else:
			return criar_resposta_json_erro(mensagem='Parâmetros inválidos.', status_code=400)
	else:
		return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)


def _enviar_email_de_caravanas(email, cidade, integrantes):
	contexto = {'nome': 'Um caboclo ai', 'email': email, 'mensagem': 'Cidade de partida: ' +  cidade + '\nLista de integrantes: ' + integrantes}
	assunto = 'Caravanas'
	destinatario = EMAIL_CONTATO_SECOMP if is_sistema_em_producao() else EMAIL_TI_SECOMP
	corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=email,
											  destinatario=destinatario, template=TEMPLATE_EMAIL_DE_CONTATO,
											  contexto=contexto, nome_remetente='Um caboclo ai')
	return enviar_email(corpo_email=corpo_email)


def enviar_email_caravana(request):
	if request.method == 'POST':
		form = request.POST
		email = form['email']
		cidade = form['cidade']
		integrantes = form['integrantes']
		recaptcha_response = request.POST.get('g-recaptcha-response')
		data = {
			'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
			'response': recaptcha_response
		}
		r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
		result = r.json()
		if result['success']:
			if _enviar_email_de_caravanas(email=email, cidade=cidade, integrantes=integrantes):
				return criar_resposta_json_sucesso('E-mail enviado com sucesso.')
			else:
				return criar_resposta_json_erro(mensagem='Erro no serviço de envio de e-mail.', status_code=503)
		else:
			return criar_resposta_json_erro(mensagem='ReCaptcha inválido.', status_code=503)
	else:
		return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)


def index(request):
	dicionario_contexto[AGORA] = timezone.now()
	dicionario_contexto['quantidade_inscritos'] = get_evento_atual().inscricoes.count()
	return render(request=request, template_name=TEMPLATE_INDEX, context=dicionario_contexto)


def faq(request):
	dicionario_contexto['form'] = FormularioDeContato()
	return render(request=request, template_name=TEMPLATE_FAQ, context=dicionario_contexto)

def equipe(request):
	if request.user.is_authenticated:
		username = request.user.first_name
		dicionario_contexto['easter'] = username
	else:
		dicionario_contexto['easter'] = "Caroline"

	return render(request=request, template_name=TEMPLATE_EQUPE, context=dicionario_contexto)


def patrocinio(request):
	return render(request=request, template_name=TEMPLATE_PATROCINIO, context=dicionario_contexto)


def a_secomp(request):
	return render(request=request, template_name=TEMPLATE_SOBRE, context=dicionario_contexto)


def apresentacao(request):
	return render(request=request, template_name=TEMPLATE_APRESENTACAO, context=dicionario_contexto)


# TODO: Implementar a página história
# def historia(request):
#     return render(request=request, template_name=TEMPLATE_HISTORIA, context=dicionario_contexto)


def sociocultural(request):
	return render(request=request, template_name=TEMPLATE_SOCIO, context=dicionario_contexto)


def erro_400(request,exception):
	response = render(request=request, template_name=TEMPLATE_400, context=dicionario_contexto)
	response.status_code = 400
	return response


def erro_403(request,exception):
	response = render(request=request, template_name=TEMPLATE_403, context=dicionario_contexto)
	response.status_code = 403
	return response


def erro_404(request):
	data = {}
	return render(request, TEMPLATE_404, data)


def erro_500(request):
	response = render(request=request, template_name=TEMPLATE_500, context=dicionario_contexto)
	response.status_code = 500
	return response

def erro_recaptcha(request):
	response = render(request=request, template_name=TEMPLATE_RECAPTCHA, context=dicionario_contexto)
	response.status_code = 500
	return response

def processo_seletivo(request):
	context = _get_dicionario_contexto()
	context['form'] = ProcessoSeletivoForm()
	return render(request=request, template_name=TEMPLATE_PROCESSO_SELETIVO, context=context)
