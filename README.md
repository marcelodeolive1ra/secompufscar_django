# Projeto Django para o site e sistema de inscrições da SECOMP UFSCar

Este repositório é um projeto Django do PyCharm (secompufscar_django). 
Reúne os sites 2015, 2016 e 2017, onde o de 2015 é meramente uma cópia estática do site que foi hospedado na época e os de 2016 e 2017 fazem uso dos recursos do Django.

## Dependências

+ Python 3.5.2+
+ Django 1.11.3
+ SendGrid 4.2.0
+ PayPal REST SDK 1.13
+ Pillow 4.2.1
+ Django AnyMail (SendGrid) 0.11
+ Raven 6.1.0
+ Classes CandyBarPdf417, CandyBarimagePdf417 e PatternPdf417 (já inclusas nos arquivos do projeto)
