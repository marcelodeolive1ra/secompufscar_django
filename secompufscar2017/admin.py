from django.contrib import admin
from .models import *

admin.site.register(Projeto)
admin.site.register(IntegranteProjeto)
admin.site.register(CaraterProjeto)