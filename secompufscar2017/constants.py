from secompufscar_bd.models import Evento
from django.conf import settings

ANO_ATUAL = "/2018/"

ATIVIDADES = 'atividades'
PALESTRAS = 'palestras'
PALESTRAS_EMPRESARIAIS = 'palestras_empresariais'
MINICURSOS = 'minicursos'
WORKSHOPS = 'workshops'
MESAS_REDONDAS = 'mesas_redondas'
PROCESSOS_SELETIVOS = 'processos_seletivos'
DESAFIO_PROGRAMADORES = 'desafio_programadores'
PATROCINADORES = 'patrocinadores'
PATROCINADORES_DIAMANTE = 'patrocinadores_diamante'
PATROCINADORES_OURO = 'patrocinadores_ouro'
PATROCINADORES_PRATA = 'patrocinadores_prata'
PATROCINADORES_DESAFIO = 'patrocinadores_desafio'
APOIADORES = 'apoiadores'
AGORA = 'agora'

EDICAO_ATUAL = 10

EMAIL_CONTATO_SECOMP = Evento.objects.get(edicao=EDICAO_ATUAL).email_destino_formulario_contato
EMAIL_ADMIN = 'marcelo@secompufscar.com.br'
EMAIL_CONTEUDO = 'conteudo@secompufscar.com.br'
EMAIL_SECOMP = 'contato@secompufscar.com.br'
EMAIL_PAGAMENTOS_SECOMP = 'financeiro@secompufscar.com.br'
EMAIL_TI_SECOMP = 'ti@secompufscar.com.br'

TIPO_ATIVIDADE_PROCESSO_SELETIVO = 'Processo Seletivo'
TIPO_ATIVIDADE_MESA_REDONDA = 'Mesa-Redonda'
TIPO_ATIVIDADE_WORKSHOP = 'Workshop'
TIPO_ATIVIDADE_MINICURSO = 'Minicurso'
TIPO_ATIVIDADE_PALESTRA_EMPRESARIAL = 'Palestra Empresarial'
TIPO_ATIVIDADE_PALESTRA = 'Palestra'

COTA_APOIO = 'Apoio'
COTA_PRATA = 'Prata'
COTA_OURO = 'Ouro'
COTA_DIAMANTE = 'Diamante'
COTA_DESAFIO_PROGRAMADORES = 'Desafio de Programadores'

TEMPLATE_INDEX = 'secompufscar2017/index.html'
TEMPLATE_A_SECOMP = 'secompufscar2017/a_secomp/asecomp.html'
TEMPLATE_APRESENTACAO = 'secompufscar2017/a_secomp/apresentacao.html'
TEMPLATE_SOCIOCULTURAL = 'secompufscar2017/a_secomp/sociocultural.html'
TEMPLATE_PROGRAMACAO = 'secompufscar2017/programacao/programacao.html'
TEMPLATE_ATIVIDADES = 'secompufscar2017/programacao/atividades.html'
TEMPLATE_PALESTRAS = 'secompufscar2017/programacao/palestras.html'
TEMPLATE_MESAS_REDONDAS = 'secompufscar2017/programacao/mesas_redondas.html'
TEMPLATE_GAME_NIGHT = 'secompufscar2017/programacao/game_night.html'
TEMPLATE_DESAFIO_DE_PROGRAMADORES = 'secompufscar2017/programacao/desafio_de_programadores.html'
TEMPLATE_CTF = 'secompufscar2017/programacao/ctf.html'
TEMPLATE_FUTEBOL_DE_ROBOS = 'secompufscar2017/programacao/futebol_de_robos.html'
TEMPLATE_MINICURSOS = 'secompufscar2017/programacao/minicursos.html'
TEMPLATE_WORKSHOPS = 'secompufscar2017/programacao/workshops.html'
TEMPLATE_CRONOGRAMA = 'secompufscar2017/programacao/cronograma.html'
TEMPLATE_PROCESSOS_SELETIVOS = 'secompufscar2017/programacao/processos_seletivos.html'
TEMPLATE_ENTERPRISE_DAY = 'secompufscar2017/programacao/enterprise_day.html'
TEMPLATE_FEIRA_DE_PROJETOS = 'secompufscar2017/programacao/feira_de_projetos.html'
TEMPLATE_CHURRASCO_DE_INTEGRACAO = 'secompufscar2017/programacao/churrasco.html'
TEMPLATE_PARCERIAS = 'secompufscar2017/programacao/parcerias.html'
TEMPLATE_PATROCINIO = 'secompufscar2017/patrocinio.html'
TEMPLATE_EQUIPE = 'secompufscar2017/equipe.html'
TEMPLATE_PACOTES = 'secompufscar2017/pacotes.html'
TEMPLATE_CONTATO = 'secompufscar2017/contato.html'
TEMPLATE_MENSAGEM_ENVIADA = 'secompufscar2017/helpers/mensagem_enviada.html'
TEMPLATE_MENSAGEM_NAO_ENVIADA = 'secompufscar2017/helpers/mensagem_nao_enviada.html'
TEMPLATE_ERRO_400 = 'secompufscar2017/erros/erro_400.html'
TEMPLATE_ERRO_403 = 'secompufscar2017/erros/erro_403.html'
TEMPLATE_ERRO_404 = 'secompufscar2017/erros/erro_404.html'
TEMPLATE_ERRO_500 = 'secompufscar2017/erros/erro_500.html'
TEMPLATE_EMAIL_DE_CONTATO = 'secompufscar2017/helpers/email_de_contato.html'
TEMPLATE_EMAIL_DE_CONTATO_SIMPLES = 'secompufscar2017/helpers/email_de_contato_simples.html'
TEMPLATE_EMAIL_PROJETO_RESPONSAVEL = 'secompufscar2017/helpers/email_projeto_para_responsavel.html'
TEMPLATE_EMAIL_PROJETO_ORGANIZACAO = 'secompufscar2017/helpers/email_projeto_para_organizacao.html'


TEMPLATE_LOGOUT_SUCESSO = 'secompufscar2017/area_do_participante/logout.html'
TEMPLATE_CADASTRO = 'secompufscar2017/area_do_participante/cadastro/cadastro.html'
TEMPLATE_CONFIRMACAO_EMAIL = 'secompufscar2017/area_do_participante/cadastro/confirmacao_de_email.html'
TEMPLATE_REENVIAR_EMAIL_CONFIRMACAO = 'secompufscar2017/area_do_participante/cadastro/reenviar_email_confirmacao.html'
TEMPLATE_CADASTRO_CONFIRMADO = 'secompufscar2017/area_do_participante/cadastro/cadastro_confirmado.html'
TEMPLATE_LINK_INVALIDO = 'secompufscar2017/area_do_participante/cadastro/link_invalido.html'
TEMPLATE_DASHBOARD = 'secompufscar2017/area_do_participante/dashboard.html'
TEMPLATE_INSCRICAO = 'secompufscar2017/area_do_participante/inscricao/inscricao.html'
TEMPLATE_INSCRICOES_NAO_DISPONIVEIS = 'secompufscar2017/area_do_participante/inscricoes_nao_disponiveis.html'
TEMPLATE_INSCRICAO_REALIZADA = 'secompufscar2017/area_do_participante/inscricao/inscricao_realizada.html'
TEMPLATE_COMPRAR_KIT = 'secompufscar2017/area_do_participante/inscricao/comprar_kit.html'
TEMPLATE_ENVIAR_COMPROVANTE = 'secompufscar2017/area_do_participante/inscricao/enviar_comprovante.html'
TEMPLATE_ENVIAR_COMPROVANTE_SUCESSO = 'secompufscar2017/area_do_participante/inscricao/envio_comprovante_sucesso.html'
TEMPLATE_KIT_RESERVADO = 'secompufscar2017/area_do_participante/inscricao/kit_reservado.html'
TEMPLATE_PAGAMENTO_PAYPAL_CONFIRMADO = \
    'secompufscar2017/area_do_participante/inscricao/pagamento_paypal_confirmado.html'
TEMPLATE_PAGAMENTO_PAYPAL_CANCELADO = \
    'secompufscar2017/area_do_participante/inscricao/pagamento_paypal_cancelado.html'
TEMPLATE_PAGAMENTO_PAYPAL_ERRO = \
    'secompufscar2017/area_do_participante/inscricao/pagamento_paypal_erro.html'


TEMPLATE_EMAIL_CONFIRMACAO_EMAIL = \
    'secompufscar2017/area_do_participante/cadastro/templates_email/email_confirmacao_cadastro.html'
TEMPLATE_EMAIL_INSCRICAO_REALIZADA = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_inscricao_realizada.html'
TEMPLATE_EMAIL_PAGAMENTO_PAYPAL_CONFIRMADO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_pagamento_paypal_confirmado.html'
TEMPLATE_EMAIL_KIT_RESERVADO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_kit_reservado.html'
TEMPLATE_EMAIL_COMPROVANTE_ENVIADO_SUCESSO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_comprovante_enviado_sucesso.html'
TEMPLATE_EMAIL_NOVO_COMPROVANTE_RECEBIDO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_novo_comprovante_recebido.html'
TEMPLATE_EMAIL_COMPROVANTE_APROVADO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_comprovante_aprovado.html'
TEMPLATE_EMAIL_COMPROVANTE_REJEITADO = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_comprovante_rejeitado.html'
TEMPLATE_EMAIL_COMPROVANTE_PAGAMENTO_PRESENCIAL = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_comprovante_pagamento_presencial.html'
TEMPLATE_EMAIL_CUPOM = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_cupom.html'
TEMPLATE_EMAIL_TROCA_CAMISETA = \
    'secompufscar2017/area_do_participante/inscricao/templates_email/email_troca_camiseta.html'

TEMPLATE_EMAIL_GENERICO = \
    'secompufscar2019/area_administrativa/templates_email/email_generico.html'


TEMPLATE_DASHBOARD_AREA_ADMINISTRATIVA = \
    'secompufscar2017/area_administrativa/dashboard.html'
TEMPLATE_VENDA_PRESENCIAL = \
    'secompufscar2017/area_administrativa/venda_presencial.html'


URL_LOGIN = ANO_ATUAL+'area-do-participante/login/'
URL_CONFIRMACAO_EMAIL = ANO_ATUAL+'area-do-participante/cadastro/confirmar-email/'
URL_CADASTRO_CONFIRMADO = ANO_ATUAL+'area-do-participante/cadastro/confirmado/'
URL_LINK_INVALIDO = ANO_ATUAL+'area-do-participante/cadastro/link-invalido/'
URL_DASHBOARD = ANO_ATUAL+'area-do-participante/dashboard/'
URL_INSCRICAO = ANO_ATUAL+'area-do-participante/inscricao/'
URL_INSCRICOES_NAO_DISPONIVEIS = ANO_ATUAL+'area-do-participante/inscricoes-nao-disponiveis/'
URL_INSCRICAO_REALIZADA = ANO_ATUAL+'area-do-participante/inscricao/inscricao-realizada/'
URL_COMPRAR_KIT = ANO_ATUAL+'area-do-participante/inscricao/comprar-kit/'
URL_KIT_RESERVADO = ANO_ATUAL+'area-do-participante/inscricao/comprar-kit/reservar-kit/reservado/'
URL_UPLOAD_COMPROVANTE_SUCESSO = \
    ANO_ATUAL+'area-do-participante/inscricao/comprar-kit/enviar-comprovante/upload-comprovante/sucesso/'

URL_RETORNO_PAYPAL = \
    'https://secompufscar.com.br'+ANO_ATUAL+'area-do-participante/inscricao/comprar-kit/pagamento-paypal/confirmado/'
URL_CANCELAMENTO_PAYPAL = \
    "https://secompufscar.com.br"+ANO_ATUAL+"area-do-participante/inscricao/comprar-kit/pagamento-paypal/cancelado/"


URL_DASHBOARD_AREA_ADMINISTRATIVA = ANO_ATUAL+'area-administrativa/dashboard/'
URL_ACESSO_PROIBIDO = '/403/'
URL_INTERNAL_SERVER_ERROR = '/500/'


PATH_COMPROVANTES = settings.MEDIA_ROOT + '/comprovantes/secompufscar2017/'
PATH_COMPROVANTES_RELATIVO = settings.MEDIA_URL + 'comprovantes/secompufscar2017/'


PERMISSAO_ADMIN = 'ADMIN'
PERMISSAO_NOTIFICACOES_APP = 'NOTIFICACOES_APP'
PERMISSAO_APROVAR_COMPROVANTES = 'APROVAR_COMPROVANTES'
PERMISSAO_ALTERAR_CAMISETAS = 'ALTERAR_CAMISETAS'
PERMISSAO_VENDA_PRESENCIAL = 'VENDA_PRESENCIAL'
PERMISSAO_GERAR_CRACHAS = 'GERAR_CRACHAS'
PERMISSAO_GERAR_LISTAS = 'GERAR_LISTAS'
PERMISSAO_SORTEAR = 'SORTEAR'


TIPO_PAGAMENTO_DEPOSITO_TRANSFERENCIA = 'Depósito/Transferência'
TIPO_PAGAMENTO_PAYPAL = 'PayPal'
TIPO_PAGAMENTO_CUPOM = 'Cupom'

STATUS_PAGAMENTO_APROVADO = 'Aprovado'
STATUS_PAGAMENTO_REJEITADO = 'Rejeitado'
STATUS_PAGAMENTO_AGUARDANDO_APROVACAO = 'Aguardando aprovação'


DATA_ULTIMA_ATUALIZACAO_ATIVIDADES = 'data_ultima_atualizacao_atividades'
DATA_ULTIMA_ATUALIZACAO_MINISTRANTES = 'data_ultima_atualizacao_ministrantes'
DATA_ULTIMA_ATUALIZACAO_PATROCINADORES = 'data_ultima_atualizacao_patrocinadores'

FCM_TOPIC = 'secomp2l17-teste' if settings.DEBUG else 'secomp2l17'
HAPPENING_NOW_HASHTAG = ' #NOW'

CRACHA_ROOT = settings.MEDIA_ROOT + "/crachas/secompufscar2017/"
CRACHA_FONT = CRACHA_ROOT + "fonte/"
CRACHA_ORGANIZACAO = CRACHA_ROOT + "organizacao/"
CRACHA_PARTICIPANTE = CRACHA_ROOT + "participante/"
CRACHA_PALESTRANTE = CRACHA_ROOT + "palestrante/"

TEMPLATE_INSCRICAO_ATIVIDADES_2018 = 'secompufscar2017/area_do_participante/inscricao_atividades_2018.html'
