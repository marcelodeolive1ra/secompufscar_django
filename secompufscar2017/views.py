# coding=utf-8
import requests
from django.db.models import Min
from django.db.models.signals import post_save, post_delete, m2m_changed
from django.dispatch import receiver
from django.http import HttpResponseRedirect
from django.shortcuts import render

from secompufscar2017.forms import FormularioDeContato, FormularioDeCadastroDeProjetos
from secompufscar2017.models import Projeto, CaraterProjeto, IntegranteProjeto
from secompufscar2017.utilities import *
from secompufscar_bd.models import *

evento = get_evento_atual()


def _ajustar_tamanho_de_nome_para_pagina_equipe(membros):
    for membro in membros:
        sobrenome_completo = membro.participante.usuario.last_name
        membro.participante.usuario.last_name = abreviar_sobrenome(sobrenome_completo=sobrenome_completo, min_len=20)
    return membros


def _get_diretorias():
    return [{'nome': diretoria.nome,
             'membros': _ajustar_tamanho_de_nome_para_pagina_equipe(evento.membros_equipe.filter(
                 diretoria=diretoria).order_by('-cargo__nome', 'participante__usuario__first_name',
                                               'participante__usuario__last_name'))}
            for diretoria in Diretoria.objects.all()]


def _get_perguntas_frequentes():
    return evento.perguntas_frequentes.order_by('prioridade')


def _get_patrocinadores_por_cota(nome_cota):
    return evento.patrocinadores.filter(ativo_site=True, cota__nome=nome_cota).order_by('ordem_site')


def _get_tipo_atividade(nome_tipo_atividade):
    try:
        return TipoAtividade.objects.get(nome=nome_tipo_atividade)
    except ObjectDoesNotExist:
        return None


def _get_atividades_por_tipo(nome_tipo_atividade):
    return evento.atividades.filter(ativa=True, tipo=_get_tipo_atividade(nome_tipo_atividade=nome_tipo_atividade)). \
        annotate(primeira_data=Min('horarios__data_hora_inicio')).order_by('primeira_data', 'titulo')


def _get_menus_site_ativos():
    return [menu.nome for menu in get_evento_atual().menus_site_ativos.filter(ativo=True)]


def _get_data_hora_inicio_evento():
    return evento.data_hora_inicio_evento


def _get_datas_inscricoes_por_atividade(tipo_atividade):
    try:
        tipo_atividade = evento.datas_inscricoes_atividades.get(tipo_atividade=tipo_atividade)
        return {
            "inicio": tipo_atividade.data_hora_inicio_inscricoes,
            "fim": tipo_atividade.data_hora_fim_inscricoes
        }
    except ObjectDoesNotExist:
        return {}


def _get_datas_inscricoes_desafio_programadores():
    if evento.desafio_programadores is not None:
        return {
            "inicio": evento.desafio_programadores.inicio_inscricoes_desafio_programadores,
            "fim": evento.desafio_programadores.fim_inscricoes_desafio_programadores
        }
    return {}


def _get_datas_inscricoes_atividades():
    return {
        MINICURSOS: _get_datas_inscricoes_por_atividade(tipo_atividade=_get_tipo_atividade('Minicurso')),
        WORKSHOPS: _get_datas_inscricoes_por_atividade(tipo_atividade=_get_tipo_atividade('Workshop')),
        PROCESSOS_SELETIVOS:
            _get_datas_inscricoes_por_atividade(tipo_atividade=_get_tipo_atividade('Processo Seletivo')),
        DESAFIO_PROGRAMADORES: _get_datas_inscricoes_desafio_programadores()
    }


def _get_tipos_atividades_com_cronograma_fechado():
    return [tipo.tipo_atividade.nome for tipo in evento.tipos_atividades.filter(cronograma_fechado=True)]


def _get_ministrantes():
    return Ministrante.objects.filter(atividade__evento=get_evento_atual()).order_by('nome')


def _get_atividades():
    atividades = {}
    for tipo_atividade in TipoAtividade.objects.all():
        atividades[converter_para_lowercase_e_sem_espacos(tipo_atividade.nome)] = get_evento_atual().atividades.filter(
            tipo=_get_tipo_atividade(nome_tipo_atividade=tipo_atividade.nome), ativa=True).order_by('titulo')
    return atividades


def _get_data_ultima_atualizacao_atividades():
    atividades = get_evento_atual().atividades.filter(ativa=True).order_by('-ultima_atualizacao_em')
    try:
        return atividades[0].ultima_atualizacao_em
    except Exception:
        return None


@receiver(post_save, sender=Atividade)
@receiver(post_delete, sender=Atividade)
def atualizar_data_ultima_atualizacao_atividades(sender, **kwargs):
    dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_ATIVIDADES] = _get_data_ultima_atualizacao_atividades()


def _get_data_ultima_atualizacao_ministrantes():
    ministrantes = Ministrante.objects.filter(atividade__evento=get_evento_atual()).order_by('-ultima_atualizacao_em')
    try:
        return ministrantes[0].ultima_atualizacao_em
    except Exception:
        return None


@receiver(post_save, sender=Ministrante)
@receiver(post_delete, sender=Ministrante)
def atualizar_data_ultima_atualizacao_ministrantes(sender, **kwargs):
    dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_MINISTRANTES] = _get_data_ultima_atualizacao_ministrantes()


def _get_data_ultima_atualizacao_patrocinadores():
    patrocinadores = evento.patrocinadores.filter(ativo_site=True).order_by('-ultima_atualizacao_em')
    try:
        return patrocinadores[0].ultima_atualizacao_em
    except Exception:
        return None


@receiver(post_save, sender=Patrocinador)
@receiver(post_delete, sender=Patrocinador)
def atualizar_data_ultima_atualizacao_patrocinadores(sender, **kwargs):
    dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_PATROCINADORES] = _get_data_ultima_atualizacao_patrocinadores()


def _get_dicionario_contexto():
    return {
        'producao': is_sistema_em_producao(),
        'google_analytics_key': get_google_analytics_key(),
        PATROCINADORES: {
            converter_para_lowercase_e_sem_espacos(COTA_DIAMANTE):
                _get_patrocinadores_por_cota(nome_cota=COTA_DIAMANTE),
            converter_para_lowercase_e_sem_espacos(COTA_OURO):
                _get_patrocinadores_por_cota(nome_cota=COTA_OURO),
            converter_para_lowercase_e_sem_espacos(COTA_PRATA):
                _get_patrocinadores_por_cota(nome_cota=COTA_PRATA),
            converter_para_lowercase_e_sem_espacos(COTA_DESAFIO_PROGRAMADORES):
                _get_patrocinadores_por_cota(nome_cota=COTA_DESAFIO_PROGRAMADORES),
            converter_para_lowercase_e_sem_espacos(COTA_APOIO):
                _get_patrocinadores_por_cota(nome_cota=COTA_APOIO),
        },
        'diretorias': _get_diretorias(),
        'perguntas_frequentes': _get_perguntas_frequentes(),
        PALESTRAS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PALESTRA),
        PALESTRAS_EMPRESARIAIS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PALESTRA_EMPRESARIAL),
        MINICURSOS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_MINICURSO),
        WORKSHOPS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_WORKSHOP),
        MESAS_REDONDAS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_MESA_REDONDA),
        PROCESSOS_SELETIVOS: _get_atividades_por_tipo(nome_tipo_atividade=TIPO_ATIVIDADE_PROCESSO_SELETIVO),
        'menus_ativos': _get_menus_site_ativos(),
        'data_hora_inicio_evento': _get_data_hora_inicio_evento(),
        'evento': get_evento_atual(),
        'data_hora_inscricoes_atividades': _get_datas_inscricoes_atividades(),
        'tipos_atividade_com_cronograma_fechado': _get_tipos_atividades_com_cronograma_fechado(),
        'ministrantes': _get_ministrantes(),
        'atividades': _get_atividades(),
        DATA_ULTIMA_ATUALIZACAO_ATIVIDADES: _get_data_ultima_atualizacao_atividades(),
        DATA_ULTIMA_ATUALIZACAO_MINISTRANTES: _get_data_ultima_atualizacao_ministrantes(),
        DATA_ULTIMA_ATUALIZACAO_PATROCINADORES: _get_data_ultima_atualizacao_patrocinadores(),
        'permissoes': {}
    }


dicionario_contexto = _get_dicionario_contexto()


def _atualizar_dicionario_contexto():
    dicionario_contexto.update(_get_dicionario_contexto())
    atualizar_dicionario_permissoes()


@receiver(post_save, sender=Evento)
@receiver(post_delete, sender=Evento)
def _atualizar_evento_dicionario_contexto(sender, **kwargs):
    _atualizar_dicionario_contexto()


m2m_changed.connect(_atualizar_evento_dicionario_contexto, sender=Evento.menus_site_ativos.through)
m2m_changed.connect(_atualizar_evento_dicionario_contexto, sender=Evento.tipos_atividades.through)


def atualizar_dicionario_permissoes():
    dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA'] = []
    for permissao in get_evento_atual().permissoes.all():
        dicionario_contexto['permissoes'][permissao.nome] = []
        for participante in permissao.usuarios_permitidos.all():
            dicionario_contexto['permissoes'][permissao.nome].append(participante.usuario)
            if participante.usuario not in dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA']:
                dicionario_contexto['permissoes']['AREA_ADMINISTRATIVA'].append(participante.usuario)


def index(request):
    dicionario_contexto[AGORA] = timezone.now()
    return render(request=request, template_name=TEMPLATE_INDEX, context=dicionario_contexto)


def a_secomp(request):
    return render(request=request, template_name=TEMPLATE_A_SECOMP, context=dicionario_contexto)


def apresentacao(request):
    return render(request=request, template_name=TEMPLATE_APRESENTACAO, context=dicionario_contexto)


def historia(request):
    # TODO implementar página História
    return HttpResponseRedirect('/2017/')


def sociocultural(request):
    return render(request=request, template_name=TEMPLATE_SOCIOCULTURAL, context=dicionario_contexto)


def programacao(request):
    return render(request=request, template_name=TEMPLATE_PROGRAMACAO, context=dicionario_contexto)


def palestras(request):
    dicionario_contexto[AGORA] = timezone.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[PALESTRAS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def palestras_empresariais(request):
    dicionario_contexto[AGORA] = timezone.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[PALESTRAS_EMPRESARIAIS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def minicursos(request):
    dicionario_contexto[AGORA] = timezone.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[MINICURSOS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def workshops(request):
    dicionario_contexto[AGORA] = timezone.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[WORKSHOPS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def mesas_redondas(request):
    dicionario_contexto[AGORA] = timezone.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[MESAS_REDONDAS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def game_night(request):
    return render(request=request, template_name=TEMPLATE_GAME_NIGHT, context=dicionario_contexto)


def desafio_de_programadores(request):
    return render(request=request, template_name=TEMPLATE_DESAFIO_DE_PROGRAMADORES, context=dicionario_contexto)


def capture_the_flag(request):
    return render(request=request, template_name=TEMPLATE_CTF, context=dicionario_contexto)


def futebol_de_robos(request):
    return render(request=request, template_name=TEMPLATE_FUTEBOL_DE_ROBOS, context=dicionario_contexto)


def cronograma(request):
    return render(request=request, template_name=TEMPLATE_CRONOGRAMA, context=dicionario_contexto)


def processos_seletivos(request):
    dicionario_contexto[AGORA] = datetime.datetime.now()
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[PROCESSOS_SELETIVOS]
    return render(request=request, template_name=TEMPLATE_ATIVIDADES, context=dicionario_contexto)


def enterprise_day(request):
    dicionario_contexto[ATIVIDADES] = dicionario_contexto[PALESTRAS_EMPRESARIAIS]
    dicionario_contexto['estandes'] = [
        get_evento_atual().patrocinadores.get(nome_empresa='The Climate Corporation'),
        get_evento_atual().patrocinadores.get(nome_empresa='Monitora Soluções Tecnológicas'),
        get_evento_atual().patrocinadores.get(nome_empresa='Guarani Sistemas | Innovaci'),
        get_evento_atual().patrocinadores.get(nome_empresa='Pagar.me'),
        get_evento_atual().patrocinadores.get(nome_empresa='Tokenlab'),
        get_evento_atual().patrocinadores.get(nome_empresa='Everis - an NTT DATA Company'),
        get_evento_atual().patrocinadores.get(nome_empresa='InovaBra'),
        get_evento_atual().patrocinadores.get(nome_empresa='Daitan Group'),
        get_evento_atual().patrocinadores.get(nome_empresa='Itaú'),
        get_evento_atual().patrocinadores.get(nome_empresa='InMetrics'),
        get_evento_atual().patrocinadores.get(nome_empresa='S2IT')
    ]
    return render(request=request, template_name=TEMPLATE_ENTERPRISE_DAY, context=dicionario_contexto)


def feira_de_projetos(request):
    if request.method == 'POST':
        form = FormularioDeCadastroDeProjetos(request.POST)
        if form.is_valid():
            try:
                integrantes = []
                for integrante in dict(request.POST)['nome_integrantes']:
                    if len(integrante) > 0:
                        integrantes.append(integrante)

            except Exception:
                integrantes = []

            if _cadastrar_projeto(form.cleaned_data, integrantes=integrantes):
                contexto_email = form.cleaned_data
                contexto_email['nome_integrantes'] = integrantes
                carater = []

                for carater_form in contexto_email['carater_projeto']:
                    carater.append(CaraterProjeto.objects.get(id=carater_form).carater)

                contexto_email['carater_projeto'] = carater
                _enviar_email_feira_projeto(contexto_email)
                return JsonResponse({'projeto_submetido': contexto_email['nome_projeto']})
            else:
                return JsonResponse({'erro': ''})
        else:
            return JsonResponse({'erro': str(form.errors)})
    else:
        dicionario_contexto['form'] = FormularioDeCadastroDeProjetos()
        dicionario_contexto['projetos_aprovados'] = Projeto.objects.filter(aprovado=True)

        return render(request=request, template_name=TEMPLATE_FEIRA_DE_PROJETOS, context=dicionario_contexto)


def _cadastrar_projeto(form_projeto, integrantes):
    try:
        novo_projeto = Projeto.objects.create(
            nome=form_projeto['nome_projeto'],
            areas=form_projeto['areas'],
            conteudo=form_projeto['conteudo'],
            descricao=form_projeto['descricao'],
            link_video=form_projeto['link_video'],
            relacao_computacao=form_projeto['relacao_computacao'],
            material_utilizado=form_projeto['material_utilizado'],
            objetivo=form_projeto['objetivo'],
            resultados=form_projeto['resultados'],
            facebook_responsavel=form_projeto['facebook_responsavel'],
            email_responsavel=form_projeto['email_responsavel'],
            telefone_responsavel=form_projeto['telefone_responsavel']
        )

        for carater_form in form_projeto['carater_projeto']:
            carater = CaraterProjeto.objects.get(id=carater_form)
            novo_projeto.carater.add(carater)

        responsavel = IntegranteProjeto.objects.create(
            nome=form_projeto['nome_responsavel'],
            projeto_participante=novo_projeto
        )

        novo_projeto.responsavel = responsavel

        novo_projeto.save()
        responsavel.save()

        for integrante_nome in integrantes:
            integrante = IntegranteProjeto.objects.create(
                nome=integrante_nome,
                projeto_participante=novo_projeto
            )
            integrante.save()
        return True

    except Exception:
        return False


def churrasco(request):
    return render(request=request, template_name=TEMPLATE_CHURRASCO_DE_INTEGRACAO, context=dicionario_contexto)


def parcerias(request):
    return render(request=request, template_name=TEMPLATE_PARCERIAS, context=dicionario_contexto)


def patrocinio(request):
    return render(request=request, template_name=TEMPLATE_PATROCINIO, context=dicionario_contexto)


def equipe(request):
    return render(request=request, template_name=TEMPLATE_EQUIPE, context=dicionario_contexto)


def pacotes(request):
    return render(request=request, template_name=TEMPLATE_PACOTES, context=dicionario_contexto)


def contato(request):
    dicionario_contexto['form'] = FormularioDeContato()
    return render(request=request, template_name=TEMPLATE_CONTATO, context=dicionario_contexto)


def enviar_email_contato(request):
    if request.method == 'POST':
        formulario_contato = FormularioDeContato(request.POST)

        if formulario_contato.is_valid():
            nome = formulario_contato.cleaned_data['nome']
            email = formulario_contato.cleaned_data['email']
            mensagem = formulario_contato.cleaned_data['mensagem']
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                return criar_resposta_json_sucesso('E-mail enviado com sucesso.')
            if _enviar_email_de_contato(nome=nome, email=email, mensagem=mensagem):
                return criar_resposta_json_sucesso('E-mail enviado com sucesso.')
            else:
                return criar_resposta_json_erro(mensagem='Erro no serviço de envio de e-mail.', status_code=503)
        else:
            return criar_resposta_json_erro(mensagem='Parâmetros inválidos.', status_code=400)
    else:
        return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)



def _enviar_email_de_contato(nome, email, mensagem):
    contexto = {'nome': nome, 'email': email, 'mensagem': mensagem}
    assunto = 'Contato via site'
    destinatario = EMAIL_CONTATO_SECOMP if is_sistema_em_producao() else EMAIL_TI_SECOMP
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=email,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_DE_CONTATO,
                                              contexto=contexto, nome_remetente=nome)
    return enviar_email(corpo_email=corpo_email)


def _enviar_email_feira_projeto(dados):
    # Envio para o Responsavel
    contexto = {'nome': 'SECOMP UFSCar', 'email': EMAIL_CONTATO_SECOMP,
                'nome_responsavel': dados['nome_responsavel'],
                'nome_projeto': dados['nome_projeto']}

    assunto = 'Submissão de projeto - SECOMP UFSCar'
    destinatario = dados['email_responsavel']
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_PROJETO_RESPONSAVEL,
                                              contexto=contexto)
    status = enviar_email(corpo_email=corpo_email)

    # Email para a organização
    dados['nome'] = dados['nome_responsavel']
    dados['email'] = dados['email_responsavel']
    dados['status'] = status
    destinatario = EMAIL_CONTEUDO
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_PROJETO_ORGANIZACAO,
                                              contexto=dados)

    return enviar_email(corpo_email=corpo_email)


def erro_400(request):
    response = render(request=request, template_name=TEMPLATE_ERRO_400, context=dicionario_contexto)
    response.status_code = 400
    return response


def erro_403(request):
    response = render(request=request, template_name=TEMPLATE_ERRO_403, context=dicionario_contexto)
    response.status_code = 403
    return response


def erro_404(request):
    response = render(request=request, template_name=TEMPLATE_ERRO_404, context=dicionario_contexto)
    response.status_code = 404
    return response


def erro_500(request):
    response = render(request=request, template_name=TEMPLATE_ERRO_500, context=dicionario_contexto)
    response.status_code = 500
    return response
