import os
from operator import itemgetter

from reportlab.pdfgen import canvas
from reportlab.lib.utils import ImageReader
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont, TTFError
from reportlab.lib.pagesizes import A4, A6


class Elemento:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0

    def set_position(self, x, y):
        self.x = x
        self.y = y

    def set_width(self, width):
        self.width = width

    def set_height(self, height):
        self.height = height

    def get_position(self):
        return self.get_x(), self.get_y()

    def get_x(self):
        return self.x

    def get_y(self):
        return self.height + self.y

    def get_size(self):
        return self.get_width(), self.get_height()

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height


class ElementoImagem(Elemento):
    def __init__(self, image_param):
        Elemento.__init__(self)
        self.image = image_param
        self.width = image_param.getSize()[0]
        self.height = image_param.getSize()[1]

    def set_width(self, width):
        if self.width > 0:
            ratio = self.height / self.width
            self.height = width * ratio

        self.width = width

    def set_height(self, height):
        if self.height > 0:
            ratio = self.width / self.height
            self.width = height * ratio

        self.height = height

    def set_center_position(self, x, y):
        self.x = x - self.get_width() / 2
        self.y = y - self.get_height() / 2

    def get_image(self):
        return self.image


class ElementoTexto(Elemento):
    DEFAULT_COLOR = (0, 0, 0)
    DEFAULT_FONT_SIZE = 12
    # TODO: Verificar esse valor
    DEFAULT_FONT_FAMILY = "Helvetica"

    def __init__(self, text):
        Elemento.__init__(self)
        self.text = text
        self.color = self.DEFAULT_COLOR
        self.size = self.DEFAULT_FONT_SIZE
        self.max_width = None
        self.font_family = self.DEFAULT_FONT_FAMILY
        self.draw_center = False

    def set_text(self, text):
        self.text = text

        if self.draw_center:
            self.set_center_position(self.x, self.y)

    def set_color(self, color):
        self.color = color

    def set_max_width(self, max_width):
        self.max_width = max_width

    def set_size(self, size):
        self.size = size

        if self.draw_center:
            self.set_center_position(self.x, self.y)

    def set_font_family(self, font):
        if font is not None:
            self.font_family = font

        if self.draw_center:
            self.set_center_position(self.x, self.y)

    def set_center_position(self, x, y):
        # No momento é necessário que esse método seja o ultimo chamado para não comprometer o resultado
        face = pdfmetrics.getFont(self.font_family).face
        alturaTexto = (self.get_size() * (face.ascent - face.descent) / 1000.0) / 2
        self.x = x
        self.y = y + alturaTexto / 2
        self.draw_center = True

    def get_text(self):
        return self.text

    def get_color(self):
        r, g, b = self.color
        return r / 256, g / 256, b / 256

    def get_size(self):
        fontsize = self.size

        if self.max_width is not None:
            while pdfmetrics.stringWidth(self.text, self.font_family, fontsize) > self.get_max_width():
                fontsize = fontsize - 1

        return fontsize

    def get_max_width(self):
        return self.max_width


class Pagina():
    def __init__(self):
        self.imagens = []
        self.textos = []

    def add_elemento(self, elemento):
        if isinstance(elemento, ElementoImagem):
            self.imagens.append(elemento)
        elif isinstance(elemento, ElementoTexto):
            self.textos.append(elemento)


class Gerador_PDF:
    def __init__(self, background=None):
        if background is not None:
            self.width = background.get_width()
            self.height = background.get_height()
        else:
            self.width = 0
            self.height = 0

        self.background = background

    def set_width(self, width):
        self.width = width

        if self.background is not None:
            self.background.set_width(width)

    def set_height(self, height):
        self.height = height

        if self.background is not None:
            self.background.set_height(height)

    def set_background(self, background):
        self.background = background
        self.set_width(background.get_width())
        self.set_height(background.get_height())

    def get_page_size(self):
        return self.get_width(), self.get_height()

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    @staticmethod
    def get_x_position(x):
        return x

    def get_y_position(self, y):
        return self.height - y

    def gera_pdf(self, file, paginas):

        document = canvas.Canvas(filename=file, pagesize=self.get_page_size())

        for pagina in paginas:

            if self.background is not None:
                document.drawImage(image=self.background.get_image(),
                                   x=self.get_x_position(self.background.get_x()),
                                   y=self.get_y_position(self.background.get_y()),
                                   width=self.get_width(), height=self.get_height(),
                                   mask=None, preserveAspectRatio=True)

            for imagem in pagina.imagens:
                document.drawImage(image=imagem.get_image(),
                                   x=self.get_x_position(imagem.get_x()),
                                   y=self.get_y_position(imagem.get_y()),
                                   width=imagem.get_width(), height=imagem.get_height(),
                                   mask=None, preserveAspectRatio=True)

            for texto in pagina.textos:

                r, g, b = texto.get_color()
                document.setFillColorRGB(r, g, b)

                document.setFont(texto.font_family, texto.get_size())

                if texto.draw_center:
                    document.drawCentredString(self.get_x_position(texto.get_x()),
                                               self.get_y_position(texto.get_y()), texto.text)
                else:
                    document.drawString(x=self.get_x_position(texto.get_x()),
                                        y=self.get_y_position(texto.get_y()),
                                        text=texto.text)

            document.showPage()

        document.save()
        return document


def gera_certificados(file, data, font, font_path, background_path):
    try:
        if os.path.exists(font_path):
            pdfmetrics.registerFont(TTFont(font, font_path))
        else:
            font = None

        background = ElementoImagem(ImageReader(background_path))

        gerador = Gerador_PDF(background)
        gerador.set_background(background)
        gerador.set_width(A4[1])
        gerador.set_height(A4[0])

        paginas = []

        for item in data:
            pagina = Pagina()
            nome = ElementoTexto(item[0])
            nome.set_size(40)
            nome.set_font_family(font)
            nome.set_max_width(gerador.get_width() - 10)
            nome.set_color((255, 255, 255))
            nome.set_center_position(gerador.get_width() / 2, gerador.get_height() / 2 - 82)

            pagina.add_elemento(nome)

            minicurso = ElementoTexto(item[1])
            minicurso.set_size(40)
            minicurso.set_font_family(font)
            minicurso.set_max_width(gerador.get_width() - 30)
            minicurso.set_color((255, 255, 255))
            minicurso.set_center_position(nome.get_x(), nome.get_y() + 75)

            pagina.add_elemento(minicurso)

            paginas.append(pagina)

        return gerador.gera_pdf(file=file, paginas=paginas)
    except IOError:
        return None


WIDTH_CONST_DEC = 20


def gera_back_crachas(file, template_back):
    try:
        back_cracha = ImageReader(template_back)

        gerador = Gerador_PDF()
        gerador.set_width(A4[0])
        gerador.set_height(A4[1])

        cracha_back = ElementoImagem(back_cracha)
        cracha_back.set_width(A6[0] - WIDTH_CONST_DEC)

        marginX = (A4[0] - 2 * cracha_back.get_width()) / 3
        marginY = (A4[1] - 2 * cracha_back.get_height()) / 3

        pagina = Pagina()

        for i in range(4):

            cracha_back = ElementoImagem(back_cracha)
            cracha_back.set_width(A6[0] - WIDTH_CONST_DEC)
            position_x = marginX + (i % 2) * (cracha_back.get_width() + marginX)

            if (i % 4) < 2:
                position_y = marginY
            else:
                position_y = 2 * marginY + cracha_back.get_height()

            cracha_back.set_position(position_x, position_y)
            pagina.add_elemento(cracha_back)

        return gerador.gera_pdf(file=file, paginas=[pagina])
    except IOError:
        return None


def gera_crachas(file, data, font, font_path, template_certificado_front):
    try:
        if os.path.exists(font_path):
            pdfmetrics.registerFont(TTFont(font, font_path))
        else:
            font = None

        # Parte da frente
        front_cracha = ImageReader(template_certificado_front)

        gerador = Gerador_PDF()
        gerador.set_width(A4[0])
        gerador.set_height(A4[1])

        cracha = ElementoImagem(front_cracha)
        cracha.set_width(A6[0] - WIDTH_CONST_DEC)

        marginX = (A4[0] - 2 * cracha.get_width()) / 3
        marginY = (A4[1] - 2 * cracha.get_height()) / 3

        i = 0

        paginas = []
        pagina = None

        for item in sorted(data, key=itemgetter(0, 1)):
            if i % 4 == 0:
                pagina = Pagina()
                paginas.append(pagina)

            cracha = ElementoImagem(front_cracha)
            cracha.set_width(A6[0] - WIDTH_CONST_DEC)
            position_x = marginX + (i % 2) * (cracha.get_width() + marginX)

            if (i % 4) < 2:
                position_y = marginY
            else:
                position_y = 2 * marginY + cracha.get_height()

            cracha.set_position(position_x, position_y)
            pagina.add_elemento(cracha)

            if item[2] is not None:

                primeiro_nome = ElementoTexto(str(item[0]).upper())
                primeiro_nome.set_size(22)
                primeiro_nome.set_font_family(font)
                primeiro_nome.set_max_width(cracha.get_width() - 20)
                primeiro_nome.set_color((0, 0, 0))
                primeiro_nome.set_center_position(position_x + cracha.get_width() / 2,
                                                  position_y + cracha.get_height() - 135)

                pagina.add_elemento(primeiro_nome)

                sobrenome = ElementoTexto(str(item[1]).upper())
                sobrenome.set_size(22)
                sobrenome.set_font_family(font)
                sobrenome.set_max_width(cracha.get_width() - 20)
                sobrenome.set_color((0, 0, 0))
                sobrenome.set_center_position(primeiro_nome.get_x(), primeiro_nome.get_y() + 20)

                pagina.add_elemento(sobrenome)

                codigo = ElementoImagem(ImageReader(item[2]))
                codigo.set_width(120)
                codigo.set_center_position(position_x + cracha.get_width() / 2, position_y + cracha.get_height() - 73)
                pagina.add_elemento(codigo)

            else:

                primeiro_nome = ElementoTexto(str(item[0]).upper())
                primeiro_nome.set_size(22)
                primeiro_nome.set_font_family(font)
                primeiro_nome.set_max_width(cracha.get_width() - 20)
                primeiro_nome.set_color((0, 0, 0))
                primeiro_nome.set_center_position(position_x + cracha.get_width() / 2,
                                                  position_y + cracha.get_height() - 110)

                pagina.add_elemento(primeiro_nome)

                sobrenome = ElementoTexto(str(item[1]).upper())
                sobrenome.set_size(22)
                sobrenome.set_font_family(font)
                sobrenome.set_max_width(cracha.get_width() - 20)
                sobrenome.set_color((0, 0, 0))
                sobrenome.set_center_position(primeiro_nome.get_x(), primeiro_nome.get_y() + 20)

                pagina.add_elemento(sobrenome)

            i = i + 1
        return gerador.gera_pdf(file=file, paginas=paginas)
    except IOError:
        return None
    except TTFError:
        return None
