import os

from io import BytesIO
from operator import itemgetter

from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from raven.utils import json
from django.db.models.functions import Lower

from secompufscar2017 import utilities
from secompufscar2017.gerador_pdfs import gera_crachas, gera_back_crachas, gera_certificados
from secompufscar2017.utilities import *
from secompufscar2017.views import dicionario_contexto, atualizar_dicionario_permissoes
from secompufscar_bd.models import *
from secompufscar2017.views_area_do_participante import _is_camiseta_disponivel, _dar_baixa_em_camiseta, \
    _devolver_camiseta_para_estoque, _get_objeto_camiseta, _trocar_pacote_inscricao_para_full, \
    _get_inscricao_participante, _get_status_pagamento_aprovado, _adicionar_pagamento_em_inscricao, \
    _get_valor_pagamentos_ja_realizados, _roolback_adicionar_pagamento, roolback_pacote_inscricao_para_free, \
    get_rank_by_day
from secompufscar2017.forms import FormularioNotificacao, FormularioGeracaoDeCrachas, FormularioEnvioEmail, \
    FormularioGerarCrachasUnitarios
from pyfcm import FCMNotification
import tweepy

from secompufscar_django.settings import API_PRESENCA_KEY
from secompufscar_inscricoes.pdf417.CandyBarPdf417 import CandyBarPdf417

from random import choice


def _get_permissao_admin():
    try:
        return get_evento_atual().permissoes.get(nome='ADMIN')
    except ObjectDoesNotExist:
        permissao_admin = Permissao(nome='ADMIN')
        permissao_admin.save()
        evento_atual = get_evento_atual()
        evento_atual.permissoes.add(permissao_admin)
        evento_atual.save()
        return permissao_admin


def _get_permissao_notificoes_app():
    try:
        return get_evento_atual().permissoes.get(nome='NOTIFICACOES_APP')
    except ObjectDoesNotExist:
        permissao_notificacoes_app = Permissao(nome='NOTIFICACOES_APP')
        permissao_notificacoes_app.save()
        evento_atual = get_evento_atual()
        evento_atual.permissoes.add(permissao_notificacoes_app)
        evento_atual.save()
        return permissao_notificacoes_app


def _get_permissao_venda_presencial():
    try:
        return get_evento_atual().permissoes.get(nome='VENDA_PRESENCIAL')
    except ObjectDoesNotExist:
        permissao_venda_presencial = Permissao(nome='VENDA_PRESENCIAL')
        permissao_venda_presencial.save()
        evento_atual = get_evento_atual()
        evento_atual.permissoes.add(permissao_venda_presencial)
        evento_atual.save()
        return permissao_venda_presencial


def _get_permissao_gera_listas():
    try:
        return get_evento_atual().permissoes.get(nome=PERMISSAO_GERAR_LISTAS)
    except ObjectDoesNotExist:
        permissao_gera_listas = Permissao(nome=PERMISSAO_GERAR_LISTAS)
        permissao_gera_listas.save()
        evento_atual = get_evento_atual()
        evento_atual.permissoes.add(permissao_gera_listas)
        evento_atual.save()
        return permissao_gera_listas


def _usuario_tem_permissao_area_administrativa(request):
    return usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_NOTIFICACOES_APP) or \
           usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES) or \
           usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ALTERAR_CAMISETAS) or \
           usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_CRACHAS) or \
           usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_VENDA_PRESENCIAL) or \
           usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_LISTAS)


@login_required(login_url=URL_LOGIN)
def area_administrativa(request):
    if _usuario_tem_permissao_area_administrativa(request=request):
        return HttpResponseRedirect(URL_DASHBOARD_AREA_ADMINISTRATIVA)
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)


@login_required(login_url=URL_LOGIN)
def dashboard(request):
    if _usuario_tem_permissao_area_administrativa(request=request):
        atualizar_dicionario_permissoes()
        dicionario_contexto['agora'] = timezone.now()
        dicionario_contexto['form_enviar_notificacao'] = FormularioNotificacao()
        dicionario_contexto['participantes'] = _get_participantes_com_camiseta()
        dicionario_contexto['camisetas'] = get_evento_atual().camisetas.all()
        dicionario_contexto['form_gerar_crachas'] = FormularioGeracaoDeCrachas()
        dicionario_contexto['form_enviar_email'] = FormularioEnvioEmail()
        dicionario_contexto['form_unique_crachas'] = FormularioGerarCrachasUnitarios()
        dicionario_contexto['atividades_do_evento'] = get_evento_atual().atividades.all()

        return render(request=request,
                      template_name=TEMPLATE_DASHBOARD_AREA_ADMINISTRATIVA, context=dicionario_contexto)
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)


def _get_participantes_com_comprovantes_pendentes_aprovacao():
    try:
        return get_evento_atual().inscricoes.filter(
            pagamentos__tipo_pagamento=get_tipo_pagamento(nome_tipo_pagamento=TIPO_PAGAMENTO_DEPOSITO_TRANSFERENCIA),
            pagamentos__status_atual=get_status_pagamento(nome_status_pagamento=STATUS_PAGAMENTO_AGUARDANDO_APROVACAO)) \
            .order_by(Lower('participante__usuario__first_name'), Lower('participante__usuario__last_name'))
    except Exception as e:
        print(e)
        return []


def _get_inscricoes_sem_compra_de_kit():
    try:
        return get_evento_atual().inscricoes.filter(pacote=get_pacote_free()). \
            order_by(Lower('participante__usuario__first_name'), Lower('participante__usuario__last_name'))
    except Exception as e:
        print('aqui')
        print(e)
        return []


@login_required(login_url=URL_LOGIN)
def api_get_inscricoes_pagamentos_pendentes(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES):
        return JsonResponse({
            "success": True,
            "results": [
                {
                    "name":
                        inscricao.participante.usuario.first_name + ' ' + inscricao.participante.usuario.last_name +
                        ' (' + inscricao.participante.usuario.email + ')',
                    "value": inscricao.id
                } for inscricao in _get_participantes_com_comprovantes_pendentes_aprovacao()
            ]
        })
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acesso a essa API.', status_code=403)


@login_required(login_url=URL_LOGIN)
def api_get_inscricoes_sem_kit(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_VENDA_PRESENCIAL):
        return JsonResponse({
            "success": True,
            "results": [
                {
                    "name":
                        inscricao.participante.usuario.first_name + ' ' + inscricao.participante.usuario.last_name +
                        ' (' + inscricao.participante.usuario.email + ')',
                    "value": inscricao.id
                } for inscricao in _get_inscricoes_sem_compra_de_kit()
            ]
        })
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acesso a essa API.', status_code=403)


@login_required(login_url=URL_LOGIN)
def aprovar_comprovante_transferencia_deposito(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES):
        if request.method == 'POST':
            id_inscricao = request.POST['inscricao']

            try:
                inscricao_participante = _get_inscricao_por_id(id_inscricao=id_inscricao)
                pagamento_aprovado = _trocar_status_comprovante_aprovado(usuario=request.user,
                                                                         inscricao_participante=inscricao_participante)
                _enviar_email_aprovacao_comprovante(participante=get_participante_por_inscricao(
                    inscricao_participante=inscricao_participante), pagamento=pagamento_aprovado)
                return criar_resposta_json_sucesso(mensagem='Comprovante aprovado com sucesso.')
            except ObjectDoesNotExist:
                return criar_resposta_json_erro(mensagem='Inscrição inexistente', status_code=404)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


@login_required(login_url=URL_LOGIN)
def rejeitar_comprovante_transferencia_deposito(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES):
        if request.method == 'POST':
            id_inscricao = request.POST['inscricao']

            try:
                inscricao_participante = _get_inscricao_por_id(id_inscricao=id_inscricao)
                pagamento_rejeitado = _trocar_status_comprovante_rejeitado(
                    usuario=request.user, inscricao_participante=inscricao_participante)
                roolback_pacote_inscricao_para_free(inscricao_participante=inscricao_participante)
                _enviar_email_rejeicao_comprovante(participante=get_participante_por_inscricao(
                    inscricao_participante=inscricao_participante), pagamento=pagamento_rejeitado)
                return criar_resposta_json_sucesso(mensagem='Comprovante aprovado com sucesso.')
            except ObjectDoesNotExist:
                return criar_resposta_json_erro(mensagem='Inscrição inexistente', status_code=404)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


def _trocar_status_comprovante_aprovado(usuario, inscricao_participante):
    return _trocar_status_comprovante(usuario=usuario, inscricao_participante=inscricao_participante,
                                      nome_novo_status=STATUS_PAGAMENTO_APROVADO)


def _trocar_status_comprovante_rejeitado(usuario, inscricao_participante):
    return _trocar_status_comprovante(usuario=usuario, inscricao_participante=inscricao_participante,
                                      nome_novo_status=STATUS_PAGAMENTO_REJEITADO)


def _trocar_status_comprovante(usuario, inscricao_participante, nome_novo_status):
    if inscricao_participante is not None:
        pagamento_comprovante = _get_pagamento_comprovante(inscricao_participante=inscricao_participante)
        pagamento_comprovante.status_atual = get_status_pagamento(nome_status_pagamento=nome_novo_status)
        pagamento_comprovante.data_hora_mudanca_status = get_data_hora_atual()
        pagamento_comprovante.autorizador = get_participante(usuario=usuario)
        pagamento_comprovante.save()
        return pagamento_comprovante
    else:
        raise ObjectDoesNotExist


def _enviar_email_aprovacao_comprovante(participante, pagamento):
    contexto = {
        'nome': participante.usuario.first_name,
        'valor': pagamento.valor
    }
    assunto = 'Comprovante de depósito/transferência aprovado - IX SECOMP UFSCar'
    destinatario = participante.usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_COMPROVANTE_APROVADO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


def _enviar_email_rejeicao_comprovante(participante, pagamento):
    contexto = {
        'nome': participante.usuario.first_name,
        'valor': pagamento.valor,
        'protocolo': 'https' if is_sistema_em_producao() else 'http',
        'dominio': 'secompufscar.com.br' if is_sistema_em_producao() else 'localhost:8002'
    }
    assunto = 'Comprovante de depósito/transferência rejeitado - IX SECOMP UFSCar'
    destinatario = participante.usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_COMPROVANTE_REJEITADO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


def _get_inscricao_por_id(id_inscricao):
    try:
        return get_evento_atual().inscricoes.get(id=id_inscricao)
    except ObjectDoesNotExist:
        return None


def _get_pagamento_comprovante(inscricao_participante):
    try:
        return inscricao_participante.pagamentos.get(tipo_pagamento=
                                                     get_tipo_pagamento(nome_tipo_pagamento=
                                                                        TIPO_PAGAMENTO_DEPOSITO_TRANSFERENCIA),
                                                     status_atual=get_status_pagamento(
                                                         nome_status_pagamento=STATUS_PAGAMENTO_AGUARDANDO_APROVACAO))
    except ObjectDoesNotExist:
        return None


@login_required(login_url=URL_LOGIN)
def api_get_detalhes_inscricao(request, id_inscricao):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES) or \
            usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_VENDA_PRESENCIAL) or \
            usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ALTERAR_CAMISETAS):
        try:
            return JsonResponse(_get_detalhes_inscricao_por_id(id_inscricao=id_inscricao))
        except ObjectDoesNotExist:
            return criar_resposta_json_erro(mensagem='Inscrição inexistente', status_code=404)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


def _get_detalhes_inscricao_por_id(id_inscricao):
    try:
        inscricao_participante = get_evento_atual().inscricoes.get(id=id_inscricao)

        detalhes_inscricao = {
            'nome': inscricao_participante.participante.usuario.first_name,
            'sobrenome': inscricao_participante.participante.usuario.last_name,
            'email': inscricao_participante.participante.usuario.email,
        }

        if inscricao_participante.pacote == get_pacote_full():
            pagamento_sem_aprovacao = inscricao_participante.pagamentos.get(
                status_atual=get_status_pagamento(STATUS_PAGAMENTO_AGUARDANDO_APROVACAO))
            detalhes_inscricao['camiseta'] = inscricao_participante.camiseta.tamanho
            detalhes_inscricao['pagamento_nao_aprovado'] = {
                'valor': ('%2.2f' % pagamento_sem_aprovacao.valor).replace('.', ','),
                'registro': pagamento_sem_aprovacao.registro,
                'enviado_em': pagamento_sem_aprovacao.data_hora_pagamento
            }

        detalhes_inscricao['pagamentos'] = []
        pagamentos_realizados = 0
        for pagamento in inscricao_participante.pagamentos.filter(
                status_atual=get_status_pagamento(STATUS_PAGAMENTO_APROVADO)):
            detalhes_inscricao['pagamentos'].append({
                'valor': ('%2.2f' % pagamento.valor).replace('.', ','),
                'registro': pagamento.registro,
                'data_hora_pagamento': pagamento.data_hora_pagamento
            })
            pagamentos_realizados += pagamento.valor

        detalhes_inscricao['valor_pendente_pagamento'] = \
            ('%2.2f' % (
                get_pacote_full().valor -
                _get_valor_pagamentos_ja_realizados(inscricao_participante=inscricao_participante))).replace('.', ',')

        return detalhes_inscricao
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist


def _enviar_notificacao(message):
    if _enviar_twitter(message):
        if _enviar_push(message):
            return True

    return False


def _enviar_twitter(message):
    auth = tweepy.OAuthHandler(
        consumer_key=settings.TWITTER_KEYS['consumer_key'],
        consumer_secret=settings.TWITTER_KEYS['consumer_secret'])
    auth.set_access_token(
        key=settings.TWITTER_KEYS['access_token'],
        secret=settings.TWITTER_KEYS['access_token_secret']
    )
    api = tweepy.API(auth)
    message = message + HAPPENING_NOW_HASHTAG
    try:
        status = api.update_status(message)
    except tweepy.TweepError:
        status = tweepy.Status
        status.id = None

    if status.id is not None:
        return True

    return False


def _enviar_push(message):
    push_service = FCMNotification(api_key=settings.FCM_KEY)
    result = push_service.notify_topic_subscribers(topic_name=FCM_TOPIC, message_body=message)
    if result['failure'] == 0:
        return True

    return False


@login_required(login_url=URL_LOGIN)
def enviar_notificacao(request):
    if usuario_tem_permissao(request.user, _get_permissao_notificoes_app()):
        if request.method == 'POST':
            formulario_notificacao = FormularioNotificacao(request.POST)
            if formulario_notificacao.is_valid():
                if _enviar_notificacao(formulario_notificacao.cleaned_data['mensagem']):
                    return criar_resposta_json_sucesso(mensagem='Notificação enviada com sucesso!')
                else:
                    return criar_resposta_json_erro(mensagem='Erro ao enviar a notificação.', status_code=500)
            else:
                return criar_resposta_json_erro(mensagem='Formulário inválido', status_code=400)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso', status_code=403)


def _get_participantes_com_camiseta():
    try:
        return get_evento_atual().inscricoes.filter(pacote=get_pacote_full()) \
            .order_by(Lower('participante__usuario__first_name'), Lower('participante__usuario__last_name'))
    except Exception as e:
        print(e)
        return []


def _get_detalhes_inscricao_por_id_all(id_inscricao):
    try:
        inscricao_participante = get_evento_atual().inscricoes.get(id=id_inscricao)

        return {
            'nome': inscricao_participante.participante.usuario.first_name,
            'sobrenome': inscricao_participante.participante.usuario.last_name,
            'email': inscricao_participante.participante.usuario.email,
            'camiseta': inscricao_participante.camiseta.tamanho,
            'camiseta_id': inscricao_participante.camiseta.id
        }
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist


@login_required(login_url=URL_LOGIN)
def api_get_detalhes_inscricao_all(request, id_inscricao):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_APROVAR_COMPROVANTES) or \
            usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ALTERAR_CAMISETAS):
        try:
            return JsonResponse(_get_detalhes_inscricao_por_id_all(id_inscricao=id_inscricao))
        except ObjectDoesNotExist:
            return criar_resposta_json_erro(mensagem='Inscrição inexistente', status_code=404)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


@login_required(login_url=URL_LOGIN)
def api_get_participantes_com_camiseta(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ALTERAR_CAMISETAS):
        return JsonResponse({
            "success": True,
            "results": [
                {
                    "name":
                        inscricao.participante.usuario.first_name + ' ' + inscricao.participante.usuario.last_name +
                        ' (' + inscricao.participante.usuario.email + ')' + ' Tam: ' + inscricao.camiseta.tamanho,
                    "value": inscricao.id
                } for inscricao in _get_participantes_com_camiseta()
            ]
        })
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acesso a essa API.', status_code=403)


def _alterar_tamanho_camiseta(id_inscricao, id_camiseta_nova):
    try:
        inscricao = _get_inscricao_por_id(id_inscricao=id_inscricao)
        if _is_camiseta_disponivel(id_camiseta_nova):
            try:
                _dar_baixa_em_camiseta(id_camiseta_nova)
                camiseta_nova = _get_objeto_camiseta(id_camiseta_nova)
                camiseta_devolvida = _get_objeto_camiseta(inscricao.camiseta.id)
                inscricao.camiseta = camiseta_nova
                inscricao.save()
                _devolver_camiseta_para_estoque(camiseta_devolvida)
                _enviar_email_troca_camiseta(inscricao.participante.usuario, camiseta_nova.tamanho)
                return 200
            except ObjectDoesNotExist as e:
                print(e)
                return 400
        else:
            return 412  # pre condicao de camiseta disponivel falhou
    except ObjectDoesNotExist as e:
        print(e)
        return 404


def _enviar_email_troca_camiseta(usuario, tamanho_novo):
    contexto = {
        'nome': usuario.first_name,
        'camiseta': tamanho_novo
    }
    assunto = 'Tamanho da camiseta alterado - IX SECOMP'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_TROCA_CAMISETA,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


@login_required(login_url=URL_LOGIN)
def atualizar_tamanho_camisetas(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ALTERAR_CAMISETAS):
        if request.method == 'POST':
            id_inscricao = request.POST['inscricao']
            try:
                id_camiseta = request.POST['camiseta']
            except Exception as e:
                print(e)
                return criar_resposta_json_erro(mensagem='Camiseta não selecionada.', status_code=412)
            status = _alterar_tamanho_camiseta(id_inscricao, id_camiseta)
            if status == 200:
                return criar_resposta_json_sucesso(mensagem='Camiseta alterada com sucesso.')
            elif status == 404:
                return criar_resposta_json_erro(mensagem='Inscrição inexistente', status_code=404)
            elif status == 400:
                return criar_resposta_json_erro(mensagem='Tamanho não disponível', status_code=404)
            else:
                return criar_resposta_json_erro(mensagem='Erro desconhecido.', status_code=500)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


@login_required(login_url=URL_LOGIN)
def realizar_venda_presencial(request):
    atualizar_dicionario_permissoes()
    usuario_usando_sistema = request.user
    if usuario_tem_permissao(usuario_usando_sistema, _get_permissao_venda_presencial()):
        if request.method == 'POST':
            id_inscricao = request.POST['codigo_inscricao_venda_presencial']

            inscricao_participante = None
            try:
                inscricao_participante = _get_inscricao_por_id(id_inscricao=id_inscricao)

                if inscricao_participante.pacote == get_pacote_full() and _get_valor_pagamentos_ja_realizados(
                        inscricao_participante) == get_pacote_full().valor:
                    return criar_resposta_json_erro(mensagem='Esta inscrição já comprou o kit.', status_code=412)
            except Exception as e:
                print(e)
                return criar_resposta_json_erro(mensagem='Inscrição não encontrada!', status_code=404)

            try:
                id_camiseta = request.POST['camiseta']
            except Exception as e:
                print(e)
                return criar_resposta_json_erro(mensagem='Camiseta não selecionada!', status_code=412)

            if _is_camiseta_disponivel(id_camiseta=id_camiseta):
                novo_pagamento = None
                try:
                    novo_pagamento = _registrar_pagamento_presencial(inscricao_participante, usuario_usando_sistema)
                    _trocar_pacote_inscricao_para_full(inscricao_participante=inscricao_participante,
                                                       id_camiseta=id_camiseta)
                    _enviar_email_venda_presencial(inscricao_participante.participante, novo_pagamento)
                except Exception as e:
                    print(e)
                    if novo_pagamento is not None:
                        _roolback_adicionar_pagamento(novo_pagamento)
                    roolback_pacote_inscricao_para_free(inscricao_participante)
                return criar_resposta_json_sucesso(mensagem='Venda realizada com sucesso.')
            else:
                return criar_resposta_json_erro(mensagem='Tamanho de camiseta não disponível.', status_code=404)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acessar este recurso.', status_code=403)


def _registrar_pagamento_presencial(inscricao_participante, usuario_usando_sistema):
    agora = timezone.now()
    valor_restante_para_pagamento = \
        get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante)

    novo_pagamento = Pagamento(
        tipo_pagamento=_get_tipo_pagamento_presencial(),
        data_hora_pagamento=agora,
        valor=valor_restante_para_pagamento,
        registro=str(uuid.uuid4())[0:12],
        status_atual=_get_status_pagamento_aprovado(),
        data_hora_mudanca_status=agora,
        autorizador=get_participante(usuario=usuario_usando_sistema)
    )
    novo_pagamento.save()
    _adicionar_pagamento_em_inscricao(inscricao_participante, novo_pagamento)
    return novo_pagamento


def _get_tipo_pagamento_presencial():
    try:
        return TipoPagamento.objects.get(nome='Presencial')
    except ObjectDoesNotExist:
        tipo_pagamento_presencial = TipoPagamento(nome='Presencial', deducoes_fixas=0, porcentagem_deducoes_variaveis=0)
        tipo_pagamento_presencial.save()
        return tipo_pagamento_presencial


def _enviar_email_venda_presencial(participante, novo_pagamento):
    usuario = participante.usuario
    inscricao_participante = _get_inscricao_participante(usuario)
    contexto = {
        'nome': usuario.first_name,
        'registro_pagamento': novo_pagamento.registro,
        'data_hora_pagamento': novo_pagamento.data_hora_pagamento,
        'valor_pagamento': ('%2.2f' % novo_pagamento.valor).replace('.', ','),
        'recebido_por': novo_pagamento.autorizador.usuario.first_name + ' ' +
                        novo_pagamento.autorizador.usuario.last_name,
        'camiseta': inscricao_participante.camiseta.tamanho
    }
    assunto = 'Comprovante de pagamento presencial - IX SECOMP UFSCar'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_COMPROVANTE_PAGAMENTO_PRESENCIAL,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


# Geracao de pdfs
def _gera_crachas_organizacao(buffer, intervalo):
    data_inicio, data_fim = intervalo
    inscricoes_organizacao = get_evento_atual().inscricoes.filter(
        participante__usuario__email__in=(
            [membro.participante.usuario.email for membro in get_evento_atual().membros_equipe.all()]))

    pdf417 = CandyBarPdf417()
    dados = []

    for inscricao in inscricoes_organizacao:
        if (data_inicio is None or data_inicio.date() <= inscricao.data_hora_inscricao.date()) and (
                        data_fim is None or data_fim.date() >= inscricao.data_hora_inscricao.date()):
            bs = pdf417.encode(inscricao.codigo)
            dados.append(
                [inscricao.participante.usuario.first_name,
                 utilities.abreviar_sobrenome(inscricao.participante.usuario.last_name, 24), BytesIO(bs)])

    return gera_crachas(file=buffer,
                        data=dados,
                        font='fonte',
                        font_path=CRACHA_ROOT + 'fonte/fonte.tff',
                        template_certificado_front=CRACHA_ORGANIZACAO + 'front.jpg')


def _gera_crachas_participantes_selecionados(buffer, inscricoes):
    inscr = []
    for i in inscricoes:
        inscr.append(_get_inscricao_por_id(i))

    pdf417 = CandyBarPdf417()
    inscricoes_sem_equipe = []
    for inscricao in inscr:
        bs = pdf417.encode(inscricao.codigo)
        inscricoes_sem_equipe.append([inscricao.participante.usuario.first_name,
                                      utilities.abreviar_sobrenome(inscricao.participante.usuario.last_name,
                                                                   24),
                                      BytesIO(bs)])

    return gera_crachas(file=buffer,
                        data=inscricoes_sem_equipe,
                        font='fonte',
                        font_path=CRACHA_FONT + 'fonte.tff',
                        template_certificado_front=CRACHA_PARTICIPANTE + 'front.jpg')


def _gera_crachas_participantes(buffer, intervalo):
    data_inicio, data_fim = intervalo

    emails_inscricoes_sem_membros_equipe = [inscricao.participante.usuario.email for inscricao in
                                            get_evento_atual().inscricoes.filter(
                                                participante__usuario__email__in=(
                                                    [membro.participante.usuario.email for membro in
                                                     get_evento_atual().membros_equipe.all()]))]

    pdf417 = CandyBarPdf417()
    inscricoes_sem_equipe = []

    for inscricao in get_evento_atual().inscricoes.all():
        if inscricao.participante.usuario.email not in emails_inscricoes_sem_membros_equipe:
            if (data_inicio is None or data_inicio.date() <= inscricao.data_hora_inscricao.date()) and (
                            data_fim is None or data_fim.date() >= inscricao.data_hora_inscricao.date()):
                bs = pdf417.encode(inscricao.codigo)
                inscricoes_sem_equipe.append([inscricao.participante.usuario.first_name,
                                              utilities.abreviar_sobrenome(inscricao.participante.usuario.last_name,
                                                                           24),
                                              BytesIO(bs)])

    return gera_crachas(file=buffer,
                        data=inscricoes_sem_equipe,
                        font='fonte',
                        font_path=CRACHA_FONT + 'fonte.tff',
                        template_certificado_front=CRACHA_PARTICIPANTE + 'front.jpg')


def _gera_crachas_palestrante(buffer):
    todos_ministrantes = []
    ministrantes_por_atividade = [m1 for m1 in
                                  ([ministrante for ministrante in atividade.ministrante.all()] for atividade in
                                   get_evento_atual().atividades.all())]

    for ministrantes in ministrantes_por_atividade:
        for ministrante in ministrantes:
            todos_ministrantes.append([ministrante.nome, utilities.abreviar_sobrenome(ministrante.sobrenome, 24), None])

    return gera_crachas(file=buffer,
                        data=todos_ministrantes,
                        font='fonte',
                        font_path=CRACHA_FONT + 'fonte.tff',
                        template_certificado_front=CRACHA_PALESTRANTE + 'front.jpg')


@login_required(login_url=URL_LOGIN)
def gerar_crachas(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_CRACHAS):
        if request.method == 'POST':
            formulario_geracao_crachas = FormularioGeracaoDeCrachas(request.POST)
            if formulario_geracao_crachas.is_valid():

                font = request.FILES.get('font_family')

                if font is not None:
                    if not os.path.exists(CRACHA_FONT):
                        os.makedirs(CRACHA_FONT)

                    with open(CRACHA_FONT + 'fonte.tff', 'wb+') as destination:
                        for chunk in font.chunks():
                            destination.write(chunk)

                template_back = request.FILES.get('cracha_back')
                if template_back is not None:
                    if not os.path.exists(CRACHA_ROOT):
                        os.makedirs(CRACHA_ROOT)

                    with open(CRACHA_ROOT + 'back.jpg', 'wb+') as destination:
                        for chunk in template_back.chunks():
                            destination.write(chunk)

                tipo = formulario_geracao_crachas.cleaned_data['tipo_cracha']

                # Create the HttpResponse object with the appropriate PDF headers.
                response = HttpResponse(content_type='application/pdf')

                buffer = BytesIO()

                if tipo == 'verso':
                    response['Content-Disposition'] = 'attachment; filename="crachas_verso.pdf"'

                    if gera_back_crachas(buffer, CRACHA_ROOT + 'back.jpg') is not None:
                        pdf = buffer.getvalue()
                        buffer.close()
                        response.write(pdf)
                        return response
                    else:
                        return HttpResponse("Houve um erro, tente atualizar os arquivos de template")

                else:
                    intervalo = (formulario_geracao_crachas.cleaned_data['datetime_inicio'],
                                 formulario_geracao_crachas.cleaned_data['datetime_fim'])
                    inicio_string = ''
                    fim_string = ''

                    if intervalo[0] is not None:
                        inicio_string = "_apartirDe_" + str(intervalo[0].date())

                    if intervalo[1] is not None:
                        fim_string = "_ate_" + str(intervalo[1].date())

                    response[
                        'Content-Disposition'] = 'attachment; filename="crachas_' + tipo + inicio_string + fim_string + '.pdf"'

                    if tipo == 'participante':
                        path = CRACHA_PARTICIPANTE
                    elif tipo == 'organizacao':
                        path = CRACHA_ORGANIZACAO
                    else:
                        path = CRACHA_PALESTRANTE

                    template_front = request.FILES.get('cracha_front')
                    if template_front is not None:
                        if not os.path.exists(path):
                            os.makedirs(path)
                        with open(path + 'front.jpg', 'wb+') as destination:
                            for chunk in template_front.chunks():
                                destination.write(chunk)
                    status_pdf = None
                    if tipo == 'participante':
                        status_pdf = _gera_crachas_participantes(buffer, intervalo)
                    elif tipo == 'organizacao':
                        status_pdf = _gera_crachas_organizacao(buffer, intervalo)
                    elif tipo == 'palestrante':
                        status_pdf = _gera_crachas_palestrante(buffer)

                    if status_pdf is not None:
                        pdf = buffer.getvalue()
                        buffer.close()
                        response.write(pdf)
                        return response
                    else:
                        return HttpResponse("Houve um erro, tente atualizar os arquivos de template e a fonte")
            else:
                return HttpResponse("Formulário Inválido")


# registrar_presenca
# entrada: post com as informações ['api_key', 'id_atividade', 'id_inscricao', 'timestamp']
# saida: json com as informações  ['nome', 'pacote']
#    - Se a operação ocorreu com sucesso, status_code é 200
#    - Se não existe participante com a chave especificada, status_code é 404
#    - Se houver algum erro desconhecido, status_code é 500
#    - Se a api_key não for a correta, status_code é 401
#    - Se o método não for POST, status_code é 405
@csrf_exempt
def registrar_presenca(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode('utf-8'))

            if data.get('api_key', 0) == API_PRESENCA_KEY:

                nro_atividade = int(data.get('id_atividade', -1))
                nro_inscricao = data.get('id_inscricao', None)

                if data.get('timestamp', False):
                    timestamp = data['timestamp']
                else:
                    timestamp = timezone.now()
                try:
                    inscricao = get_evento_atual().inscricoes.get(codigo=nro_inscricao)

                    if nro_atividade >= 0:
                        if nro_atividade != 0:
                            presenca = Presenca.objects.create(atividade=Atividade.objects.get(pk=nro_atividade),
                                                               data_hora_registro=timestamp)
                            presenca.save()
                            inscricao.presencas.add(presenca)

                        if not inscricao.credenciado:
                            inscricao.credenciado = True

                        inscricao.save()

                    response = JsonResponse(
                        {'nome': inscricao.participante.usuario.first_name + ' ' + abreviar_sobrenome(
                            inscricao.participante.usuario.last_name, 24),
                         'pacote': inscricao.pacote.nome})
                    response.status_code = 200

                except ObjectDoesNotExist:
                    response = JsonResponse({})
                    response.status_code = 404
                except Exception as e:
                    print(e)
                    print("ERRO PRESENCA: " + data)
                    response = JsonResponse({})
                    response.status_code = 500
            else:
                response = JsonResponse({})
                response.status_code = 401

            return response  # TODO Verificar qual foi o propósito desse response
        except Exception as e:
            print(e)
            response = JsonResponse({})
            response.status_code = 400
            return response
    else:
        response = JsonResponse({})
        response.status_code = 405
        return response


def _enviar_email_generico(assunto, titulo, conteudo, destinatario):
    contexto = {
        'nome': destinatario['nome'],
        'conteudo': conteudo,
        'titulo': titulo,
        'assunto': assunto,
    }
    destinatario = destinatario['email']
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_GENERICO,
                                              contexto=contexto)

    try:
        status = enviar_email(corpo_email=corpo_email)
        print(str(status) + ": No try")

    except Exception as e:
        print(e)
        status = False
        print(str(status) + ": No except")

    return status


@login_required(login_url=URL_LOGIN)
def realizar_teste_email(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ADMIN):
        if request.method == 'POST':
            formulario_email = FormularioEnvioEmail(request.POST)
            if formulario_email.is_valid():
                if _enviar_email_generico(
                        assunto=formulario_email.cleaned_data['assunto'],
                        titulo=formulario_email.cleaned_data['titulo'],
                        conteudo=formulario_email.cleaned_data['conteudo'],
                        destinatario={'nome': 'usuario teste', 'email': 'ti@secompufscar.com.br'}):
                    return criar_resposta_json_sucesso(mensagem='Email enviado com sucesso!')
                else:
                    return criar_resposta_json_erro(mensagem='Erro ao enviar o email.', status_code=500)
            else:
                return criar_resposta_json_erro(mensagem='Formulário inválido', status_code=400)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acesso a essa API.', status_code=403)


@login_required(login_url=URL_LOGIN)
def enviar_email_generico(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_ADMIN):
        if request.method == 'POST':
            formulario_email = FormularioEnvioEmail(request.POST)
            if formulario_email.is_valid():

                emails_error = []
                qtd_emails = 0

                for inscricao in get_evento_atual().inscricoes.all():

                    participante = {'nome': inscricao.participante.usuario.first_name,
                                    'email': inscricao.participante.usuario.email}

                    print(participante)

                    if not _enviar_email_generico(
                            assunto=formulario_email.cleaned_data['assunto'],
                            titulo=formulario_email.cleaned_data['titulo'],
                            conteudo=formulario_email.cleaned_data['conteudo'],
                            destinatario=participante):

                        emails_error.append(participante['email'])
                    else:
                        qtd_emails += 1

                    print("Emails enviados:" + str(qtd_emails))

                print(emails_error)

                if len(emails_error) == 0:
                    return criar_resposta_json_sucesso(mensagem=str(qtd_emails) + ' emails enviados com sucesso!')
                else:
                    msg = 'Erro ao enviar o email para: ['
                    for erro in emails_error:
                        msg += erro + ", "
                    msg += ']'
                    return criar_resposta_json_erro(mensagem=msg, status_code=500)
            else:
                return criar_resposta_json_erro(mensagem='Formulário inválido', status_code=400)
        else:
            return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)
    else:
        return criar_resposta_json_erro(mensagem='Usuário sem permissão para acesso a essa API.', status_code=403)


@login_required(login_url=URL_LOGIN)
def gerar_lista_inscricoes(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_LISTAS):
        start = request.GET.get('start', 'a')
        end = request.GET.get('end', 'z')

        tipo = request.GET.get('tipo', '0')

        if start == 'undefined' or len(start) == 0:
            start = 'a'

        if end == 'undefined' or len(end) == 0:
            end = 'z'

        if start > end:
            aux = start
            start = end
            end = aux

        header = ['Nome', 'KIT', 'Camiseta', 'Assinatura']
        dados = []

        intervalo_letras = [chr(letra) for letra in range(ord(start), ord(end) + 1)]

        inscricoes_pendentes_aprovacao = _get_participantes_com_comprovantes_pendentes_aprovacao()

        for letra in intervalo_letras:
            if tipo == '0':
                inscricoes = get_evento_atual().inscricoes.filter(
                    participante__usuario__first_name__istartswith=letra).order_by(
                    Lower('participante__usuario__first_name'),
                    Lower('participante__usuario__last_name'))
            else:
                inscricoes = get_evento_atual().inscricoes.filter(
                    participante__usuario__first_name__istartswith=letra, pacote__nome="FULL").order_by(
                    Lower('participante__usuario__first_name'),
                    Lower('participante__usuario__last_name'))

            for inscricao in inscricoes:

                if inscricao.camiseta is None:
                    camiseta = "-"
                else:
                    camiseta = inscricao.camiseta.tamanho

                if inscricao.pacote.nome == 'FULL':
                    kit = True
                else:
                    kit = False

                status_pagamento = True if inscricao in inscricoes_pendentes_aprovacao else False

                dados.append(
                    [inscricao.participante.usuario.first_name + ' ' + inscricao.participante.usuario.last_name,
                     kit,
                     camiseta, status_pagamento])

        contexto = {'titulo': 'Lista do Credenciamento',
                    'header': header,
                    'dados': dados}

        return render(request=request,
                      template_name="secompufscar2017/listas/lista_inscricoes.html", context=contexto)
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)


# TODO: Esse método tá voando aqui, temos que achar um lugar e adapta-lo
def gerar_certificado():
    palestras = get_evento_atual().atividades.filter(tipo__nome="Palestra Empresarial")

    dataSegunda = []
    for atividade in palestras:
        for ministrante in atividade.ministrante.all():
            print(ministrante)
            if "2017-09-19" == str(atividade.horarios.all()[0].data_hora_inicio.date()):
                dataSegunda.append(
                    [ministrante.nome.upper() + ' ' + ministrante.sobrenome.upper(), atividade.titulo.upper()])
                print(ministrante)
    gera_certificados('/home/felipe/palestraEmpresarial.pdf', dataSegunda, 'font', CRACHA_FONT + 'fonte.tff',
                      '/home/felipe/Downloads/template_cetificados/empresarial 19 - 09.png')


def api_get_atividades(request):
    return JsonResponse({
        "success": True,
        "results": [
            {
                "name":
                    atividade.titulo,
                "value": atividade.id
            } for atividade in get_evento_atual().atividades.all().order_by('titulo')
        ]
    })


@login_required(login_url=URL_LOGIN)
def gerar_lista_atividades(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_LISTAS):
        id_atividade = request.GET.get('id', '')

        try:
            atividade = get_evento_atual().atividades.get(id=id_atividade)

            # Se for lista de presenca
            if request.GET.get('tipo', '') == '0':
                header = ['Nome', 'Horário da presença']
                dados = []

                presentes_raw = get_evento_atual().inscricoes.filter(presencas__atividade=atividade)

                presentes = []
                for presente in presentes_raw:
                    if presente not in presentes:
                        presentes.append(presente)

                for presente in presentes:
                    dados.append(
                        [presente.participante.usuario.first_name + ' ' + presente.participante.usuario.last_name,
                         presente.presencas.filter(atividade=atividade)[0].data_hora_registro])

                dados = sorted(dados, key=itemgetter(1))

                contexto = {'titulo': 'IX Semana da Computação da UFSCar - Lista de presença de: ' + str(atividade),
                            'header': header,
                            'dados': dados}

                return render(request=request,
                              template_name="secompufscar2017/listas/lista_presenca.html", context=contexto)

            # Se for lista de inscritos
            elif request.GET.get('tipo', '') == '1':
                header = ['Nome', 'E-mail', 'CPF', 'Assinatura']
                dados = []

                for inscricao in get_evento_atual().inscricoes.all().order_by(
                        Lower('participante__usuario__first_name'),
                        Lower('participante__usuario__last_name')):
                    if atividade in inscricao.atividades_inscritas.all():
                        dados.append(
                            [inscricao.participante.usuario.first_name + ' ' + inscricao.participante.usuario.last_name,
                             inscricao.participante.usuario.email])

                contexto = {'titulo': 'Lista de inscritos em: ' + str(atividade),
                            'header': header,
                            'dados': dados}

                return render(request=request,
                              template_name="secompufscar2017/listas/lista_inscricoes_atividade.html", context=contexto)
        except Exception as e:
            print(e)
            return render(request=request,
                          template_name="secompufscar2017/erros/../secompufscar2018/templates/erros/erro_404.html", context={})
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)


def isPresente(inscricao, atividade):
    try:
        presenca = inscricao.presencas.filter(atividade=atividade)
    except ObjectDoesNotExist:
        return False
    if len(presenca) > 0:
        return True
    else:
        return False


@login_required(login_url=URL_LOGIN)
def sorteia_o_sortudo(request,atividade):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_SORTEAR):
        try:
            atividade = Atividade.objects.get(id=atividade)
        except ObjectDoesNotExist:
            return criar_resposta_json_erro(mensagem='Atividade não encontrada', status_code=404)

        evento = Evento.objects.get(edicao=9)
        inscricoes = evento.inscricoes.all()
        presentes = []
        if len(inscricoes) > 0 :
            for i in inscricoes:
                if isPresente(i,atividade):
                    presentes.append(i)
        if len(presentes) > 0:
            return criar_resposta_json_sucesso(mensagem=str(choice(presentes).participante.usuario.get_full_name()))
        else:
            return criar_resposta_json_erro(mensagem="Presentes não encontrados", status_code=404)
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)


@login_required(login_url=URL_LOGIN)
def gerar_cracha_unitario(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_GERAR_CRACHAS):
        if request.method == 'POST':
            formulario_geracao_crachas = FormularioGerarCrachasUnitarios(request.POST)
            if formulario_geracao_crachas.is_valid():

                font = request.FILES.get('font_family')

                if font is not None:
                    if not os.path.exists(CRACHA_FONT):
                        os.makedirs(CRACHA_FONT)

                    with open(CRACHA_FONT + 'fonte.tff', 'wb+') as destination:
                        for chunk in font.chunks():
                            destination.write(chunk)

                template_back = request.FILES.get('cracha_back')
                if template_back is not None:
                    if not os.path.exists(CRACHA_ROOT):
                        os.makedirs(CRACHA_ROOT)

                    with open(CRACHA_ROOT + 'back.jpg', 'wb+') as destination:
                        for chunk in template_back.chunks():
                            destination.write(chunk)

                tipo = formulario_geracao_crachas.cleaned_data['tipo_cracha']
                id_inscricoes = formulario_geracao_crachas.cleaned_data['participantes']

                # Create the HttpResponse object with the appropriate PDF headers.
                response = HttpResponse(content_type='application/pdf')

                buffer = BytesIO()

                if tipo == 'verso':
                    response['Content-Disposition'] = 'attachment; filename="crachas_verso.pdf"'

                    if gera_back_crachas(buffer, CRACHA_ROOT + 'back.jpg') is not None:
                        pdf = buffer.getvalue()
                        buffer.close()
                        response.write(pdf)
                        return response
                    else:
                        return HttpResponse("Houve um erro, tente atualizar os arquivos de template")

                else:

                    response[
                        'Content-Disposition'] = 'attachment; filename="crachas.pdf"'

                    if tipo == 'participante':
                        path = CRACHA_PARTICIPANTE
                    elif tipo == 'organizacao':
                        path = CRACHA_ORGANIZACAO
                    else:
                        path = CRACHA_PALESTRANTE

                    template_front = request.FILES.get('cracha_front')
                    if template_front is not None:
                        if not os.path.exists(path):
                            os.makedirs(path)
                        with open(path + 'front.jpg', 'wb+') as destination:
                            for chunk in template_front.chunks():
                                destination.write(chunk)
                    status_pdf = None
                    if tipo == 'participante':
                        status_pdf = _gera_crachas_participantes_selecionados(buffer, id_inscricoes)
                    elif tipo == 'organizacao':
                        status_pdf = _gera_crachas_participantes_selecionados(buffer, id_inscricoes)
                    elif tipo == 'palestrante':
                        status_pdf = _gera_crachas_participantes_selecionados(buffer,id_inscricoes)

                    if status_pdf is not None:
                        pdf = buffer.getvalue()
                        buffer.close()
                        response.write(pdf)
                        return response
                    else:
                        return HttpResponse("Houve um erro, tente atualizar os arquivos de template e a fonte")
            else:
                return HttpResponse("Formulário Inválido")


@login_required(login_url=URL_LOGIN)
def pegar_rank(request):
    if usuario_tem_permissao(usuario=request.user, nome_permissao=PERMISSAO_SORTEAR):
        result = "<center>"
        result += "<h1>Monday</h1>"
        aux = get_rank_by_day('Monday')
        result += "<h3>Sorteado:" + str(choice(aux)) +"</h3>"
        for m in aux:
            result += "<br>"+ str(m) +"<br>"

        result += "<h1>Tuesday</h1>"
        result += "<h3>Sorteado:" + str(choice(aux)) + "</h3>"
        aux = get_rank_by_day('Tuesday')
        for m in aux:
            result += "<br>" + str(m) + "<br>"

        result += "<h1>Wednesday</h1>"
        result += "<h3>Sorteado:" + str(choice(aux)) + "</h3>"
        aux = get_rank_by_day('Wednesday')
        for m in aux:
            result += "<br>" + str(m) + "<br>"

        result += "<h1>Geral</h1>"
        result += "<h3>Sorteado:" + str(choice(aux)) + "</h3>"
        aux = nesse_ponto_eu_ja_desisti_da_secomp()
        for m in aux:
            result += "<br>" + str(m) + "<br>"
        result += "</center>"
        return HttpResponse(result)
    else:
        return HttpResponseRedirect(URL_ACESSO_PROIBIDO)