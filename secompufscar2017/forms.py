from django import forms
from django.core.exceptions import ValidationError

from secompufscar2017.utilities import *
from secompufscar2017.models import CaraterProjeto
from secompufscar_bd.models import Curso, Instituicao


class FormularioDeContato(forms.Form):
    nome = forms.CharField(
        label='Seu nome',
        widget=forms.TextInput(attrs={'placeholder': 'Seu nome'}),
        error_messages={
            'required': 'Digite seu nome.'
        },
        help_text='Digite seu nome.'
    )
    email = forms.EmailField(
        label='Seu e-mail',
        widget=forms.EmailInput(attrs={'placeholder': 'Seu e-mail'}),
        error_messages={
            'required': 'Digite seu e-mail.',
            'invalid': 'Digite um e-mail válido.'
        },
        help_text='Digite seu e-mail.'
    )
    mensagem = forms.CharField(
        label='Sua mensagem',
        widget=forms.Textarea(attrs={'placeholder': 'Sua mensagem', 'rows': '5'}),
        error_messages={
            'required': 'Digite sua mensagem.'
        },
        help_text='Digite sua mensagem.'
    )


def instituicoes():
    return [('', 'Instituição')] + \
           [(instituicao.id, instituicao.nome) for instituicao in Instituicao.objects.all().order_by('nome')] + \
           [('0', 'Outra')]


def cursos():
    return [('', 'Curso')] + [(curso.id, curso.nome) for curso in Curso.objects.all().order_by('nome')] +\
           [('0', 'Outro')]


def ano_ingresso():
    return [(i, i) for i in range(2018, 2001, -1)]


class FormularioDeCadastro(forms.Form):
    email = forms.EmailField(
        label='E-mail',
        widget=forms.EmailInput(attrs={'placeholder': 'E-mail'}),
        error_messages={
            'required': 'Digite seu e-mail.',
            'invalid': 'Digite um e-mail válido.'
        },
        help_text='Digite seu e-mail.'
    )
    confirmacao_email = forms.EmailField(
        label='Confirmação do e-mail',
        widget=forms.EmailInput(attrs={'placeholder': 'Confirmação do e-mail'}),
        error_messages={
            'required': 'Digite novamente o seu e-mail.',
            'invalid': 'Digite um e-mail válido'
        },
        help_text='Digite novamente o seu e-mail.'
    )
    senha = forms.CharField(
        label='Senha',
        widget=forms.PasswordInput(attrs={'placeholder': 'Senha'}),
        error_messages={
            'required': 'Digite uma senha.'
        },
        help_text='Digite sua senha.'
    )
    confirmacao_senha = forms.CharField(
        label='Confirmação da senha',
        widget=forms.PasswordInput(attrs={'placeholder': 'Confirmação da senha'}),
        error_messages={
            'required': 'Digite novamente a sua senha.'
        },
        help_text='Digite novamente a sua senha.'
    )
    nome = forms.CharField(
        label='Nome',
        widget=forms.TextInput(attrs={'placeholder': 'Nome'}),
        error_messages={
            'required': 'Digite seu nome.'
        },
        help_text='Digite seu nome.'
    )
    sobrenome = forms.CharField(
        label='Sobrenome completo',
        widget=forms.TextInput(attrs={'placeholder': 'Sobrenome'}),
        error_messages={
            'required': 'Digite seu sobrenome.'
        },
        help_text='Digite seu sobrenome.'
    )
    instituicao = forms.MultipleChoiceField(
        label='Instituição',
        widget=forms.SelectMultiple(attrs={'placeholder': 'Curso'}),
        choices=instituicoes,
        required=False,
        error_messages={
            'invalid_choice': 'Opção escolhida inválida.'
        },
        help_text='Digite a instituição que estuda, se aplicável.'
    )
    outra_instituicao = forms.CharField(
        label='Outra instituição',
        widget=forms.TextInput(attrs={'placeholder': 'Qual?'}),
        required=False,
        help_text='Digite a instituição caso esta não esteja na lista.'
    )
    curso = forms.MultipleChoiceField(
        label='Curso',
        widget=forms.SelectMultiple(attrs={'placeholder': 'Curso'}),
        choices=cursos,
        required=False,
        error_messages={
            'invalid_choice': 'Opção escolhida inválida.'
        },
        help_text='Digite o curso que está cursando, se aplicável.'
    )
    outro_curso = forms.CharField(
        label='Outro curso',
        widget=forms.TextInput(attrs={'placeholder': 'Qual?'}),
        required=False,
        help_text='Digite o curso caso este não esteja na lista.'
    )
    ano_ingresso = forms.ChoiceField(
        label='Ano de ingresso',
        choices=ano_ingresso(),
        required=False,
        error_messages={
            'invalid_choice': 'Opção escolhida inválida.'
        },
        help_text='Digite o ano de ingresso no seu curso.'
    )


def _get_pacotes():
    return [(pacote.id, pacote.nome) for pacote in
            get_evento_atual().pacotes.filter(pacote_disponivel=True).order_by('ordem_site')]


def _get_camisetas_disponiveis(camisetas):
    camisetas_disponiveis = []
    for camiseta in camisetas:
        if camiseta.quantidade_restante > 0:
            camisetas_disponiveis.append(camiseta)
    return camisetas_disponiveis


def _get_camisetas():
    camisetas_disponiveis = _get_camisetas_disponiveis(get_evento_atual().camisetas.all().order_by('tamanho'))
    return [(camiseta.id, camiseta.tamanho) for camiseta in camisetas_disponiveis]


class FormularioDeInscricao(forms.Form):
    aceite_compartilhamento_email = forms.BooleanField(
        initial=True,
        label='Aceito receber e-mails de parceiros da IX SECOMP UFSCar.',
        widget=forms.CheckboxInput(),
        required=False,
        help_text='Nesta opção você autoriza ou não o compartilhamento do seu e-mail com os nossos patrocinadores.',
    )
    aceite_termos_inscricao = forms.BooleanField(
        initial=False,
        label='Aceito os termos de inscrição da IX SECOMP UFSCar.',
        widget=forms.CheckboxInput(),
        help_text='Para realizar a sua inscrição, você deve estar de acordo com os termos da IX SECOMP UFSCar.',
    )


class FormularioCamiseta(forms.Form):
    camiseta = forms.ChoiceField(
        label='Camiseta',
        widget=forms.RadioSelect,
        error_messages={
            'invalid_choice': 'A camiseta selecionada é inválida ou não está mais disponível.',
            'required': 'Você precisa selecionar uma camiseta.'
        },
        choices=_get_camisetas
    )


def _validar_cupom(cupom):
    cupons_validos = [cupom.codigo for cupom in get_evento_atual().cupons.filter(usado=False)]
    if cupom not in cupons_validos:
        raise ValidationError(u'Cupom de desconto inválido.')


class FormularioAplicarCupom(forms.Form):
    cupom = forms.CharField(
        label='Cupom',
        widget=forms.TextInput(attrs={'placeholder': 'Cupom de desconto'}),
        error_messages={
            'invalid_choice': 'Cupom inválido ou já utilizado.',
            'required': 'Você precisa inserir um cupom.'
        },
        validators=[_validar_cupom]
    )


def _validar_tipo_arquivo(arquivo):
    nome_arquivo = arquivo.name
    if nome_arquivo.endswith('.pdf') or nome_arquivo.endswith('.jpg') or nome_arquivo.endswith('.JPG') or \
            nome_arquivo.endswith('.jpeg') or nome_arquivo.endswith('.JPEG') or \
            nome_arquivo.endswith('.png') or nome_arquivo.endswith('.PNG'):
        return
    else:
        raise ValidationError(u'Tipo de arquivo não aceito.')


class FormularioEnvioComprovantePagamento(forms.Form):
    comprovante = forms.FileField(
        label='Comprovante',
        error_messages={
            'required': 'Você deve selecionar um arquivo para enviar.'
        },
        validators=[_validar_tipo_arquivo]
    )


def choices_carater_projeto():
    choices = [(carater.id, carater.carater) for carater in CaraterProjeto.objects.all().order_by('carater')]
    choices.append(('', 'Selecione uma ou mais opções'))
    return choices


class FormularioDeCadastroDeProjetos(forms.Form):
    nome_projeto = forms.CharField(
        label='Nome do projeto',
        widget=forms.TextInput(attrs={'placeholder': 'Nome do Projeto'}),
        error_messages={
            'required': 'Digite o nome do seu projeto.'
        },
        help_text='Digite o nome do seu projeto.'
    )
    carater_projeto = forms.MultipleChoiceField(
        label='Caráter do Projeto',
        widget=forms.SelectMultiple(attrs={'multiple': '', 'class': "ui dropdown"}),
        choices=choices_carater_projeto(),
        error_messages={
            'invalid_choice': 'Opção escolhida inválida.'
        },
        help_text='Selecione pelo menos uma opção que caracterize o projeto.'
    )

    conteudo = forms.CharField(
        label='Conteúdo abordado',
        widget=forms.Textarea(attrs={'placeholder': 'Conteúdo abordado pelo projeto', 'rows': '5'}),
        error_messages={
            'required': 'Digite o conteúdo abordado pelo seu projeto.'
        },
        help_text='Informe o conteúdo abordado pelo seu projeto.'
    )

    descricao = forms.CharField(
        label='Descrição do projeto',
        widget=forms.Textarea(attrs={'placeholder': 'Descrição do projeto', 'rows': '5'}),
        error_messages={
            'required': 'Digite a descrição do seu projeto.'
        },
        help_text='Descreva o seu projeto.'
    )

    link_video = forms.CharField(
        label='Link de vídeo',
        widget=forms.TextInput(attrs={'placeholder': 'Link para o vídeo do projeto'}),
        required=False,
        help_text='Digite um link para algum vídeo sobre seu projeto.'
    )

    relacao_computacao = forms.CharField(
        label='Relação com a computação',
        widget=forms.Textarea(attrs={'placeholder': 'Relação com a computação', 'rows': '5'}),
        error_messages={
            'required': 'Digite a relação do seu projeto com a computação.'
        },
        help_text='Informe a relação do seu projeto com a computação.'
    )

    material_utilizado = forms.CharField(
        label='Material utilizado para apresentação',
        widget=forms.Textarea(attrs={
            'placeholder': 'A SECOMP irá oferecer uma mesa para apresentação do projeto, caso seja necessário algo a mais nos informe nesse campo.',
            'rows': '5'}),
        error_messages={
            'required': 'Digite os materias utilizados para a apresentação do seu projeto.'
        },
        required=False,
        help_text='Informe os materiais que serão utilizados para a apresentação do seu projeto.'
    )

    objetivo = forms.CharField(
        label='Objetivo do projeto',
        widget=forms.Textarea(attrs={'placeholder': 'Objetivo do projeto', 'rows': '5'}),
        error_messages={
            'required': 'Informe o objetivo do seu projeto.'
        },
        help_text='Informe o objetivo do seu projeto.'
    )

    areas = forms.CharField(
        label='Áreas abordadas',
        widget=forms.TextInput(attrs={'placeholder': 'computação; linguística; IHC ...'}),
        error_messages={
            'required': 'Informe as áreas abordadas pelo seu projeto.'
        },
        help_text='Informe as áreas abordadas pelo seu projeto'
    )

    resultados = forms.CharField(
        label='Resultados alcançados',
        widget=forms.Textarea(attrs={'placeholder': 'Resultados alcançados pelo projeto', 'rows': '5'}),
        error_messages={
            'required': 'Digite os resultados alcançados pelo seu projeto.'
        },
        help_text='Informe os resultados já alcançados pelo seu projeto.'
    )

    # Campos sobre os integrantes do projeto
    nome_responsavel = forms.CharField(
        label='Nome do integrante responsável',
        widget=forms.TextInput(attrs={'placeholder': 'Nome do integrante responsável'}),
        error_messages={
            'required': 'Digite o nome do integrante responsável.'
        },
        help_text='Digite o nome do integrante responsável pelo projeto.'
    )

    email_responsavel = forms.EmailField(
        label='E-mail do integrante responsável',
        widget=forms.EmailInput(attrs={'placeholder': 'E-mail do integrante responsável'}),
        error_messages={
            'required': 'Digite o e-mail do responsável.',
            'invalid': 'Digite um e-mail válido.'
        },
        help_text='Digite o e-mail do integrante responsável pelo projeto.'
    )

    telefone_responsavel = forms.CharField(
        label='Telefone do responsável',
        widget=forms.TextInput(attrs={'placeholder': 'Telefone do responável'}),
        error_messages={
            'required': 'Digite o telefone do responsável.'
        },
        help_text='Digite o telefone do responsável.'
    )

    facebook_responsavel = forms.CharField(
        label='Perfil no facebook',
        widget=forms.TextInput(attrs={'placeholder': 'Perfil do facebook do responsável'}),
        required=False,
        help_text='Digite o link do perfil no facebook.'
    )


class FormularioNotificacao(forms.Form):
    mensagem = forms.CharField(
        label='Mensagem',
        widget=forms.Textarea(attrs={'placeholder': 'Conteúdo da notificação', 'rows': '3'}),
        help_text='Digite o conteúdo da notificação a ser enviada',
        max_length=135,
        error_messages={
            'required': 'Campo mensagem não deveria estar vazio',
            'invalid': 'A mensagem deve conter, no máximo, 135 caracteres'
        }
    )


class FormularioEditarCadastro(forms.Form):
    nome = forms.CharField(
        label='Nome',
        widget=forms.TextInput(attrs={'placeholder': 'Nome'}),
        error_messages={
            'required': 'Digite seu nome.'
        },
        help_text='Digite seu nome.'
    )
    sobrenome = forms.CharField(
        label='Sobrenome completo',
        widget=forms.TextInput(attrs={'placeholder': 'Sobrenome'}),
        error_messages={
            'required': 'Digite seu sobrenome.'
        },
        help_text='Digite seu sobrenome.'
    )


class FormularioGeracaoDeCrachas(forms.Form):
    cracha_front = forms.FileField(
        label='Atualiza template frente',
        required=False,
        validators=[_validar_tipo_arquivo]
    )

    datetime_inicio = forms.DateTimeField(
        label='Dados a partir de:',
        required=False
    )

    datetime_fim = forms.DateTimeField(
        label='Dados até:',
        required=False
    )

    cracha_back = forms.FileField(
        label='Atualizar template do verso',
        required=False,
        validators=[_validar_tipo_arquivo]
    )

    font_family = forms.FileField(
        label='Atualizar fonte',
        required=False
    )

    tipo_cracha = forms.ChoiceField(
        label='Tipo de crachá',
        widget=forms.Select(attrs={'class': "ui dropdown"}),
        choices=[('participante', 'Participante'), ('organizacao', 'Organização'), ('palestrante', 'Palestrante'),
                 ('verso', 'Apenas o verso')],
    )

def get_inscricoes_evento():
    result = []
    inscr = get_evento_atual().inscricoes.all()
    for i in inscr:
        result.append(( i.participante.id, str(i.participante.usuario.first_name)+" "+str(i.participante.usuario.last_name)  ))
    return result


class FormularioGerarCrachasUnitarios(forms.Form):
    cracha_front = forms.FileField(
        label='Atualiza template frente',
        required=False,
        validators=[_validar_tipo_arquivo]
    )

    cracha_back = forms.FileField(
        label='Atualizar template do verso',
        required=False,
        validators=[_validar_tipo_arquivo]
    )

    font_family = forms.FileField(
        label='Atualizar fonte',
        required=False
    )

    participantes = forms.MultipleChoiceField(
        label='Tipo de crachá',
        widget=forms.Select(attrs={'class': "ui search selection dropdown",'multiple':''}),
        choices= get_inscricoes_evento()
    )

    tipo_cracha = forms.ChoiceField(
        label='Tipo de crachá',
        widget=forms.Select(attrs={'class': "ui dropdown"}),
        choices=[('participante', 'Participante'), ('organizacao', 'Organização'), ('palestrante', 'Palestrante'),
                 ('verso', 'Apenas o verso')],
    )


class FormularioEnvioEmail(forms.Form):
    assunto = forms.CharField(
        label='Assunto',
        help_text='Digite o assunto do email',
        error_messages={
            'required': 'É necessário preencher o assunto do email',
        }
    )

    titulo = forms.CharField(
        label='Título',
        help_text='Digite o título do corpo do email',
        error_messages={
            'required': 'É necessário inserir um título para o email',
        }
    )

    conteudo = forms.CharField(
        label='Conteúdo',
        help_text='Digite o conteúdo do email',
        widget=forms.Textarea(attrs={'placeholder': 'Conteúdo do email', 'rows': '5'}),
        error_messages={
            'required': 'É necessário inserir o conteúdo do email',
        }
    )

class FormularioInscricaoEquipeDesafio(forms.Form):
    nomeEquipe = forms.CharField(
        label='Nome da Equipe',
        help_text='Digite o email do segundo integrante',
        error_messages={
            'required': 'É necessário inserir o nome da sua equipe',
        }
    )

    participante1 = forms.CharField(
        label='Integrante 2',
        required=False,
        help_text = 'Digite o email do terceiro integrante',
        error_messages={
            'required': 'É necessário inserir o email do integrante',
        }
    )

    participante2 = forms.CharField(
        label='Integrante 3',
        required=False,
        help_text='Digite o email do integrante',
        error_messages={
            'required': 'É necessário inserir o email do integrante',
        }
    )
