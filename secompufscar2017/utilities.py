from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from sendgrid.helpers.mail import *
from sendgrid.sendgrid import SendGridAPIClient

from secompufscar2017.constants import *
from secompufscar2018.constants import EDICAO_ATUAL
from secompufscar_bd.models import Evento, Permissao, TipoPagamento, StatusPagamento, Participante, Pacote, \
    TipoAtividade, Atividade
from django.conf import settings

# Constantes malucas... Nem rela a mão

palestra = TipoAtividade.objects.get(id=1)
mesa = TipoAtividade.objects.get(id=8)
workshop = TipoAtividade.objects.get(id=6)
minicurso = TipoAtividade.objects.get(id=2)
empresarial = TipoAtividade.objects.get(id=5)
abertura = Atividade.objects.get(id=142)
maddog = Atividade.objects.get(id=103)
surpresa = Atividade.objects.get(id=141)


def get_evento_atual():
    return Evento.objects.get(edicao=EDICAO_ATUAL)


def is_sistema_em_producao():
    return not settings.DEBUG


def get_url_dominio_corrente(request):
    return ('https' if request.is_secure() else 'http') + '://' + request.META['HTTP_HOST']


def get_data_hora_atual():
    return timezone.now()


def _status_envio_email(resposta_sendgrid):
    return str(resposta_sendgrid.status_code)[0] == '2'  # Convenção do SendGrid: 2xx == sucesso


def enviar_email(corpo_email):
    sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
    resposta_sendgrid = sg.client.mail.send.post(request_body=corpo_email)
    return _status_envio_email(resposta_sendgrid)


def get_email_pronto_para_envio(assunto, remetente, destinatario, template, contexto, nome_remetente=''):
    template_email_contato_renderizado = render_to_string(
        template_name=template, context=contexto)

    email_remetente = Email(email=remetente, name=(nome_remetente if nome_remetente != '' else 'SECOMP UFSCar'))
    conteudo_email = Content("text/html", template_email_contato_renderizado)
    email_pronto_para_envio = Mail(from_email=email_remetente, subject=assunto, to_email=Email(email=destinatario),
                                   content=conteudo_email)
    return email_pronto_para_envio.get()


def _get_tipo_permissao(nome_permissao):
    try:
        return get_evento_atual().permissoes.get(nome=nome_permissao)
    except ObjectDoesNotExist:
        nova_permissao = Permissao(nome=nome_permissao)
        nova_permissao.save()
        evento_atual = get_evento_atual()
        evento_atual.permissoes.add(nova_permissao)
        evento_atual.save()
        return nova_permissao


def usuario_tem_permissao(usuario, nome_permissao):
    tipo_permissao = None
    try:
        tipo_permissao = _get_tipo_permissao(nome_permissao=nome_permissao)
        tipo_permissao.usuarios_permitidos.get(usuario=usuario)
        return True
    except ObjectDoesNotExist:
        permissao_admin = _get_tipo_permissao(nome_permissao=PERMISSAO_ADMIN)
        if tipo_permissao != permissao_admin:
            try:
                permissao_admin.usuarios_permitidos.get(usuario=usuario)
                return True
            except ObjectDoesNotExist:
                return False
    return False


def get_tipo_pagamento(nome_tipo_pagamento):
    try:
        return TipoPagamento.objects.get(nome=nome_tipo_pagamento)
    except ObjectDoesNotExist:
        novo_tipo_pagamento = TipoPagamento(
            nome=nome_tipo_pagamento,
            deducoes_fixas=0.,
            porcentagem_deducoes_variaveis=0.
        )
        novo_tipo_pagamento.save()
        return novo_tipo_pagamento


def get_status_pagamento(nome_status_pagamento):
    try:
        return StatusPagamento.objects.get(nome=nome_status_pagamento)
    except ObjectDoesNotExist:
        novo_status_pagamento = StatusPagamento(
            nome=nome_status_pagamento,
        )
        novo_status_pagamento.save()
        return novo_status_pagamento


def get_participante(usuario):
    try:
        return Participante.objects.get(usuario=usuario)
    except ObjectDoesNotExist:
        return None


def get_participante_por_inscricao(inscricao_participante):
    try:
        return Participante.objects.get(inscricao=inscricao_participante)
    except ObjectDoesNotExist:
        return None


def criar_resposta_json_sucesso(mensagem):
    return JsonResponse({
        'status': 'ok',
        'mensagem': mensagem
    })


def criar_resposta_json_erro(mensagem, status_code):
    response = JsonResponse({
        'status': 'erro',
        'mensagem': mensagem
    })
    response.status_code = status_code
    return response


def abreviar_sobrenome(sobrenome_completo, min_len):
    if len(sobrenome_completo) > min_len:
        sobrenomes = sobrenome_completo.split(' ')

        if len(sobrenomes[-1]) <= 1:
            sobrenomes = sobrenomes[:-1]

        if len(sobrenomes) > 1:
            sobrenome_abreviado = ''
            for i in range(0, len(sobrenomes) - 1):
                if sobrenomes[i].lower() not in ['de', 'da'] and len(sobrenomes[i]) > 1:
                    sobrenome_abreviado += sobrenomes[i][0] + '. '
                else:
                    sobrenome_abreviado += sobrenomes[i] + ' '
            sobrenome_abreviado += sobrenomes[-1]
        else:
            sobrenome_abreviado = sobrenomes[0]
        return sobrenome_abreviado
    else:
        return sobrenome_completo


def get_google_analytics_key():
    return get_evento_atual().google_analytics_key if is_sistema_em_producao() else ''


def converter_para_lowercase_e_sem_espacos(nome_cota):
    return nome_cota.lower().replace(' ', '_')


def get_pacote_full():
    evento = get_evento_atual()
    try:
        return evento.pacotes.get(nome='FULL')
    except ObjectDoesNotExist:
        pacote_full = Pacote(nome='FULL',
                             descricao='FULL',
                             valor=35)
        pacote_full.save()
        evento.pacotes.add(pacote_full)
        evento.save()
        return pacote_full


def get_pacote_free():
    evento = get_evento_atual()
    try:
        return evento.pacotes.get(nome='FREE')
    except ObjectDoesNotExist:
        pacote_free = Pacote(nome='FREE',
                             descricao='FREE',
                             valor=0)
        pacote_free.save()
        evento.pacotes.add(pacote_free)
        evento.save()
        return pacote_free


def get_score(presenca):
    score = 0
    if presenca.atividade.tipo == palestra:
        score += 1
    elif presenca.atividade.tipo == mesa:
        score += 2
    elif presenca.atividade.tipo == workshop:
        score += 1
    elif presenca.atividade.tipo == empresarial:
        score += 3
    elif presenca.atividade == abertura:
        score += 2
    elif presenca.atividade == maddog:
        score += 3
    elif presenca.atividade == surpresa:
        score += 3
    elif presenca.atividade == minicurso:
        score += 2
    return score


def nesse_ponto_eu_ja_desisti_da_secomp():
    rank = []
    inscr = get_evento_atual().inscricoes.all()
    for i in inscr:
        score = 0
        presencas = i.presencas.all()
        for p in presencas:
            if p.atividade.tipo == palestra:
                score += 1
            elif p.atividade.tipo == mesa:
                score += 2
            elif p.atividade.tipo == workshop:
                score += 1
            elif p.atividade.tipo == empresarial:
                score += 3
            elif p.atividade == abertura:
                score += 2
            elif p.atividade == minicurso:
                score += 2
            elif p.atividade == surpresa:
                score += 3
            elif p.atividade == maddog:
                score += 3
        rank.append({"inscr":i.participante.usuario.first_name + " "+ i.participante.usuario.last_name,
                     "score":score})

    return rank