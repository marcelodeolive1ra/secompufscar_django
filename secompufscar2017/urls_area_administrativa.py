# coding: utf-8

from django.conf.urls import url
from secompufscar2017 import views_area_administrativa

urlpatterns = [
    url(r'^$', views_area_administrativa.area_administrativa, name='area_administrativa'),
    url(r'^dashboard/$', views_area_administrativa.dashboard, name='dashboard_area_administrativa'),
    url(r'^api/inscricoes-pagamentos-pendentes/$', views_area_administrativa.api_get_inscricoes_pagamentos_pendentes,
        name='api_get_inscricoes_pagamentos_pendentes'),
    url(r'^api/inscricoes-sem-kit/$', views_area_administrativa.api_get_inscricoes_sem_kit,
        name='api_get_inscricoes_sem_kit'),
    url(r'^api/inscricao/(?P<id_inscricao>[0-9]+)/$', views_area_administrativa.api_get_detalhes_inscricao,
        name='api_get_detalhes_inscricao'),
    url(r'^aprovar-comprovante/$', views_area_administrativa.aprovar_comprovante_transferencia_deposito,
        name='aprovar_comprovante'),
    url(r'^rejeitar-comprovante/$', views_area_administrativa.rejeitar_comprovante_transferencia_deposito,
        name='rejeitar_comprovante'),
    url(r'^gerar_crachas/$', views_area_administrativa.gerar_crachas, name='gerar_crachas'),
    url(r'^gerar_crachas_unitarios/$', views_area_administrativa.gerar_cracha_unitario, name='gerar_crachas_unitarios'),
    url(r'^api/registrar-presenca/$', views_area_administrativa.registrar_presenca, name='registrar_presenca'),
    url(r'^notificacoes/$', views_area_administrativa.enviar_notificacao, name='enviar_notificacao'),
    url(r'^api/participantes-com-kit/$', views_area_administrativa.api_get_participantes_com_camiseta,
        name='api_get_participantes_com_camiseta'),
    url(r'^api/camiseta/(?P<id_inscricao>[0-9]+)/$', views_area_administrativa.api_get_detalhes_inscricao_all,
        name='api_get_detalhes_inscricao_all'),
    url(r'^alterar-camisetas/$', views_area_administrativa.atualizar_tamanho_camisetas,
        name='atualizar_tamanho_camisetas'),
    url(r'^api/venda-presencial/$', views_area_administrativa.realizar_venda_presencial,
        name='realizar_venda_presencial'),
    url(r'notificacoes/$', views_area_administrativa.enviar_notificacao, name='enviar_notificacao'),
    url(r'gerar_crachas/$', views_area_administrativa.gerar_crachas, name='gerar_crachas'),
    url(r'api/registrar-presenca/$', views_area_administrativa.registrar_presenca, name='registrar_presenca'),
    url(r'realizar-teste-email/$', views_area_administrativa.realizar_teste_email, name='realizar_teste_email'),
    url(r'enviar-email-generico/$', views_area_administrativa.enviar_email_generico, name='enviar_email_generico'),
    url(r'gerar-lista-inscricoes/$', views_area_administrativa.gerar_lista_inscricoes, name='gerar_lista_inscricoes'),
    url(r'get-atividades/$', views_area_administrativa.api_get_atividades, name='api_get_atividades'),
    url(r'gerar-lista-atividades/$', views_area_administrativa.gerar_lista_atividades, name='gerar_lista_atividades'),
    url(r'sortear-presente/(?P<atividade>[0-9]+)/$', views_area_administrativa.sorteia_o_sortudo, name='sortear-presente'),
    url(r'pegar-rank/', views_area_administrativa.pegar_rank, name='pegar-rank'),
]
