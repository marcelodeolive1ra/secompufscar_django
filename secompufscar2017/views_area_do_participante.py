from random import choice

from django.contrib.auth.decorators import login_required
from django.contrib.auth.password_validation import validate_password, ValidationError
from django.db import IntegrityError
from django.shortcuts import render, HttpResponseRedirect
from secompufscar2017.forms import FormularioDeCadastro, FormularioDeInscricao, FormularioCamiseta, \
    FormularioEnvioComprovantePagamento, FormularioAplicarCupom, FormularioEditarCadastro, \
    FormularioInscricaoEquipeDesafio
from secompufscar2017.utilities import *
from secompufscar2018.utilities import get_evento_atual as evento_atual
from secompufscar2017.utilities import get_pacote_free
from secompufscar2017.views import dicionario_contexto, atualizar_dicionario_permissoes
from secompufscar2018.constants import TEMPLATE_RECAPTCHA
from secompufscar_bd.models import *
import requests
import os
import uuid
import paypalrestsdk


paypalrestsdk.configure({
    'mode': 'live',
    'client_id': settings.PAYPAL_CLIEND_ID,
    'client_secret': settings.PAYPAL_CLIENT_SECRET
})

# Usado para saber se o usuário já se inscreveu em um
# minicurso ou workshop
tipo_minicurso = TipoAtividade.objects.get(id=2)
tipo_workshop = TipoAtividade.objects.get(id=6)

def logout_realizado_com_sucesso(request):
    return render(request=request, template_name=TEMPLATE_LOGOUT_SUCESSO, context=dicionario_contexto)


@login_required(login_url=URL_LOGIN)
def area_do_participante(request):
    if _is_dashboard_disponivel():
        return HttpResponseRedirect(URL_DASHBOARD)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _is_inscricao_disponivel():
    evento_atual = get_evento_atual()
    return evento_atual.inicio_inscricoes_evento < timezone.now() < evento_atual.fim_inscricoes_evento


def _is_compra_kit_disponivel():
    evento_atual = get_evento_atual()
    return evento_atual.inicio_inscricoes_evento < timezone.now() < get_evento_atual().data_hora_fim_evento


def _is_dashboard_disponivel():
    return timezone.now() >= get_evento_atual().inicio_inscricoes_evento


def inscricoes_nao_disponiveis(request):
    if _is_inscricao_disponivel():
        return HttpResponseRedirect(URL_DASHBOARD)
    else:
        dicionario_contexto['agora'] = timezone.now()
        return render(request=request, template_name=TEMPLATE_INSCRICOES_NAO_DISPONIVEIS, context=dicionario_contexto)


def _get_inscricao_participante(usuario):
    try:
        return evento_atual().inscricoes.get(participante=Participante.objects.get(usuario=usuario))
    except ObjectDoesNotExist:
        return None


@login_required(login_url=URL_LOGIN)
def dashboard(request):
    if _is_dashboard_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            atualizar_dicionario_permissoes()
            if get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante) > 0:
                roolback_pacote_inscricao_para_free(inscricao_participante)
            dicionario_contexto['inscricao'] = inscricao_participante
            dicionario_contexto['participante'] = Participante.objects.get(usuario=request.user)
            dicionario_contexto['form_editar_cadastro'] = FormularioEditarCadastro()
            dicionario_contexto['score'] = get_user_score(inscricao_participante)
            dicionario_contexto['monday'] = get_user_score_day(inscricao_participante,1)
            dicionario_contexto['tuesday'] = get_user_score_day(inscricao_participante, 2)
            dicionario_contexto['wednesday'] = get_user_score_day(inscricao_participante, 3)

            return render(request=request, template_name=TEMPLATE_DASHBOARD, context=dicionario_contexto)
        elif _is_inscricao_disponivel():
            return HttpResponseRedirect(URL_INSCRICAO)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _is_camiseta_disponivel(id_camiseta):
    try:
        camiseta = get_evento_atual().camisetas.get(id=id_camiseta)
        return camiseta.quantidade_restante > 0
    except ObjectDoesNotExist:
        return None


def _get_objeto_camiseta(id_camiseta):
    try:
        return get_evento_atual().camisetas.get(id=int(id_camiseta))
    except Exception as e:
        print(e)
        return None


def _cadastrar_nova_inscricao(usuario, dados_inscricao):
    try:
        evento = get_evento_atual()
        inscr = _get_inscricao_participante(usuario)
        if inscr is None:
            nova_inscricao = Inscricao(
                codigo=uuid.uuid4(),
                participante=Participante.objects.get(usuario=usuario),
                data_hora_inscricao=timezone.now(),
                aceite_compartilhamento_email=dados_inscricao['aceite_compartilhamento_email'],
                pacote=get_pacote_free(),
            )
            nova_inscricao.save()
            evento.inscricoes.add(nova_inscricao)
            evento.save()
            return nova_inscricao
        else:
            return None
    except Exception as e:
        print(e)
        return None


@login_required(login_url=URL_LOGIN)
def inscricao(request):
    if _is_inscricao_disponivel():
        if request.method == 'POST':
            formulario_inscricao = FormularioDeInscricao(request.POST)
            if formulario_inscricao.is_valid():
                recaptcha_response = request.POST.get('g-recaptcha-response')
                data = {
                    'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                    'response': recaptcha_response
                }
                r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
                result = r.json()
                if result['success']:
                    dados_inscricao = formulario_inscricao.cleaned_data
                    nova_inscricao = _cadastrar_nova_inscricao(request.user, dados_inscricao)
                    if nova_inscricao is not None:
                        _enviar_email_inscricao_realizada(request)
                        return HttpResponseRedirect(URL_INSCRICAO_REALIZADA)
                    else:
                        return HttpResponseRedirect(URL_INTERNAL_SERVER_ERROR)
                else:
                    HttpResponseRedirect(TEMPLATE_RECAPTCHA)
            dicionario_contexto['form'] = formulario_inscricao
            return render(request=request, template_name=TEMPLATE_INSCRICAO, context=dicionario_contexto)
        else:
            if _get_inscricao_participante(request.user) is None:
                dicionario_contexto['form'] = FormularioDeInscricao()
                return render(request=request, template_name=TEMPLATE_INSCRICAO, context=dicionario_contexto)
            else:
                return HttpResponseRedirect(URL_DASHBOARD)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _enviar_email_inscricao_realizada(request):
    usuario = request.user
    contexto = {
        'nome': usuario.first_name,
        'link_comprar_kit': get_url_dominio_corrente(request) + URL_COMPRAR_KIT
    }
    assunto = 'Inscrição Realizada - IX SECOMP UFSCar'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_INSCRICAO_REALIZADA,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


def _enviar_email_pagamento_paypal_confirmado(request, pagamento):
    usuario = request.user
    inscricao_participante = _get_inscricao_participante(usuario)
    contexto = {
        'nome': usuario.first_name,
        'id_pagamento': pagamento.registro,
        'valor_pago': pagamento.valor,
        'camiseta': inscricao_participante.camiseta.tamanho
    }
    assunto = 'Pagamento do KIT via PayPal confirmado - IX SECOMP UFSCar'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_PAGAMENTO_PAYPAL_CONFIRMADO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


@login_required(login_url=URL_LOGIN)
def inscricao_realizada(request):
    if _is_inscricao_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            dicionario_contexto['inscricao'] = inscricao_participante
            return render(request=request, template_name=TEMPLATE_INSCRICAO_REALIZADA, context=dicionario_contexto)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _inscricao_tem_pagamentos(inscricao_participante):
    if inscricao_participante is not None:
        return inscricao_participante.pagamentos.count() > 0
    return False


@login_required(login_url=URL_LOGIN)
def comprar_kit(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            valor_pagamentos_ja_realizados = float(_get_valor_pagamentos_ja_realizados(inscricao_participante))
            valor_pacote_full = get_pacote_full().valor

            if inscricao_participante.pacote != get_pacote_full():
                if request.method == 'POST':
                    formulario_camiseta = FormularioCamiseta(request.POST)
                    dicionario_contexto['form_camiseta'] = formulario_camiseta
                else:
                    dicionario_contexto['form_camiseta'] = FormularioCamiseta()
                    dicionario_contexto['form_cupom'] = FormularioAplicarCupom()

                dicionario_contexto['inscricao'] = inscricao_participante
                dicionario_contexto['total_a_pagar'] = valor_pacote_full - valor_pagamentos_ja_realizados
                return render(request=request, template_name=TEMPLATE_COMPRAR_KIT, context=dicionario_contexto)
            else:
                return HttpResponseRedirect(URL_DASHBOARD)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


@login_required(login_url=URL_LOGIN)
def aplicar_cupom(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            if request.method == 'POST':
                formulario_cupom = FormularioAplicarCupom(request.POST)
                if formulario_cupom.is_valid():
                    cupom = formulario_cupom.cleaned_data['cupom']
                    _registrar_pagamento_com_cupom(inscricao_participante, cupom)
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return HttpResponseRedirect(URL_COMPRAR_KIT)
                else:
                    dicionario_contexto['total_a_pagar'] = \
                        get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante)
                    dicionario_contexto['form_cupom'] = formulario_cupom
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return render(request=request, template_name=TEMPLATE_COMPRAR_KIT, context=dicionario_contexto)
            else:
                dicionario_contexto['inscricao'] = inscricao_participante
                return HttpResponseRedirect(URL_COMPRAR_KIT)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


@login_required(login_url=URL_LOGIN)
def enviar_comprovante(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            if request.method == 'POST':
                formulario_camiseta = FormularioCamiseta(request.POST)
                if formulario_camiseta.is_valid():
                    dicionario_contexto['form_camiseta'] = formulario_camiseta
                    dicionario_contexto['form_envio_comprovante'] = FormularioEnvioComprovantePagamento()
                    dicionario_contexto['ultima_camiseta_selecionada'] = formulario_camiseta.cleaned_data['camiseta']
                    dicionario_contexto['valor_a_pagar'] = \
                        get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante)
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return render(request=request, template_name=TEMPLATE_ENVIAR_COMPROVANTE,
                                  context=dicionario_contexto)
                else:
                    dicionario_contexto['inscricao'] = inscricao_participante
                    dicionario_contexto['form_camiseta'] = formulario_camiseta
                    dicionario_contexto['form_cupom'] = FormularioAplicarCupom()
                    return comprar_kit(request)
            else:
                return HttpResponseRedirect(URL_COMPRAR_KIT)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


@login_required(login_url=URL_LOGIN)
def upload_comprovante(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            if request.method == 'POST':
                formulario_comprovante = FormularioEnvioComprovantePagamento(request.POST, request.FILES)
                if formulario_comprovante.is_valid():
                    novo_pagamento = None
                    try:
                        _trocar_pacote_inscricao_para_full(inscricao_participante=inscricao_participante,
                                                           id_camiseta=dicionario_contexto[
                                                               'ultima_camiseta_selecionada'])
                        nome_arquivo_comprovante = _salvar_arquivo_comprovante(request.user,
                                                                               request.FILES['comprovante'])
                        novo_pagamento = _registrar_pagamento_deposito_transferencia(inscricao_participante)
                        _enviar_email_comprovante_enviado_sucesso(request, novo_pagamento)
                        _enviar_email_submissao_comprovante_para_organizacao(request, novo_pagamento,
                                                                             nome_arquivo_comprovante)
                        dicionario_contexto['inscricao'] = inscricao_participante
                        return HttpResponseRedirect(URL_UPLOAD_COMPROVANTE_SUCESSO)
                    except Exception as e:
                        print(e)
                        if novo_pagamento is not None:
                            _roolback_adicionar_pagamento(novo_pagamento)
                        roolback_pacote_inscricao_para_free(inscricao_participante)
                        dicionario_contexto['inscricao'] = inscricao_participante
                        return HttpResponseRedirect(URL_INTERNAL_SERVER_ERROR)
                else:
                    dicionario_contexto['form_envio_comprovante'] = formulario_comprovante
                    return render(request=request, template_name=TEMPLATE_ENVIAR_COMPROVANTE,
                                  context=dicionario_contexto)
            else:
                return HttpResponseRedirect(URL_COMPRAR_KIT)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _get_tipo_pagamento_deposito_transferencia():
    try:
        return TipoPagamento.objects.get(nome='Depósito/Transferência')
    except ObjectDoesNotExist:
        tipo_pagamento_deposito_transferencia = TipoPagamento(nome='Depósito/Transferência', deducoes_fixas=0,
                                                              porcentagem_deducoes_variaveis=0)
        tipo_pagamento_deposito_transferencia.save()
        return tipo_pagamento_deposito_transferencia


def _registrar_pagamento_deposito_transferencia(inscricao_participante):
    agora = timezone.now()
    valor_restante_para_pagamento = \
        get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante)

    novo_pagamento = Pagamento(
        tipo_pagamento=_get_tipo_pagamento_deposito_transferencia(),
        data_hora_pagamento=agora,
        valor=valor_restante_para_pagamento,
        registro=str(uuid.uuid4())[0:12],
        status_atual=_get_status_pagamento_aguardando_aprovacao(),
        data_hora_mudanca_status=agora
    )
    novo_pagamento.save()
    _adicionar_pagamento_em_inscricao(inscricao_participante, novo_pagamento)
    return novo_pagamento


def _salvar_arquivo_comprovante(usuario, arquivo_comprovante):
    novo_nome_arquivo = usuario.email.replace('@', '.at.') + '_' + arquivo_comprovante.name.replace(' ', '_')
    path_arquivo = PATH_COMPROVANTES + novo_nome_arquivo

    diretorio = os.path.dirname(path_arquivo)
    if not os.path.exists(diretorio):
        os.makedirs(diretorio)
    with open(path_arquivo, 'wb+') as destino_comprovante:
        for chunk in arquivo_comprovante.chunks():
            destino_comprovante.write(chunk)

    return novo_nome_arquivo


@login_required(login_url=URL_LOGIN)
def upload_comprovante_sucesso(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            dicionario_contexto['inscricao'] = inscricao_participante
            return render(request=request, template_name=TEMPLATE_ENVIAR_COMPROVANTE_SUCESSO,
                          context=dicionario_contexto)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _enviar_email_comprovante_enviado_sucesso(request, pagamento):
    usuario = request.user
    contexto = {
        'nome': usuario.first_name,
        'valor_pago': pagamento.valor
    }
    assunto = 'Comprovante enviado com sucesso - IX SECOMP UFSCar'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_COMPROVANTE_ENVIADO_SUCESSO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


def _enviar_email_submissao_comprovante_para_organizacao(request, pagamento, nome_arquivo):
    usuario = request.user
    contexto = {
        'nome': usuario.first_name,
        'sobrenome': usuario.last_name,
        'email': usuario.email,
        'valor': pagamento.valor,
        'link_comprovante': get_url_dominio_corrente(request) + PATH_COMPROVANTES_RELATIVO + nome_arquivo
    }
    assunto = 'Novo comprovante recebido - IX SECOMP UFSCar'
    destinatario = EMAIL_PAGAMENTOS_SECOMP
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_TI_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_NOVO_COMPROVANTE_RECEBIDO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


@login_required(login_url=URL_LOGIN)
def reservar_kit(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            if request.method == 'POST':
                formulario_camiseta = FormularioCamiseta(request.POST)
                if formulario_camiseta.is_valid():
                    _trocar_pacote_inscricao_para_full(inscricao_participante=inscricao_participante,
                                                       id_camiseta=formulario_camiseta.cleaned_data['camiseta'])
                    _enviar_email_kit_reservado(request)
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return HttpResponseRedirect(URL_KIT_RESERVADO)
                else:
                    return comprar_kit(request)
            else:
                return HttpResponseRedirect(URL_COMPRAR_KIT)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _enviar_email_kit_reservado(request):
    usuario = request.user
    inscricao_participante = _get_inscricao_participante(usuario)
    contexto = {
        'nome': usuario.first_name,
        'camiseta': inscricao_participante.camiseta.tamanho
    }
    assunto = 'KIT Reservado - IX SECOMP UFSCar'
    destinatario = usuario.email
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_KIT_RESERVADO,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


@login_required(login_url=URL_LOGIN)
def kit_reservado(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            dicionario_contexto['inscricao'] = inscricao_participante
            return render(request=request, template_name=TEMPLATE_KIT_RESERVADO, context=dicionario_contexto)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _get_tipo_pagamento_cupom():
    try:
        return TipoPagamento.objects.get(nome='Cupom')
    except ObjectDoesNotExist:
        tipo_pagamento_cupom = TipoPagamento(nome='Cupom', deducoes_fixas=0, porcentagem_deducoes_variaveis=0)
        tipo_pagamento_cupom.save()
        return tipo_pagamento_cupom


def _registrar_pagamento_com_cupom(inscricao_participante, cupom):
    objeto_cupom = _get_objeto_cupom(cupom)
    if objeto_cupom is not None:
        try:
            _dar_baixa_em_cupom(objeto_cupom)
            agora = timezone.now()

            valor_restante_para_pagamento = \
                get_pacote_full().valor - _get_valor_pagamentos_ja_realizados(inscricao_participante)
            valor_pagamento = objeto_cupom.valor_desconto
            if valor_pagamento > valor_restante_para_pagamento:
                valor_pagamento = valor_restante_para_pagamento

            novo_pagamento = Pagamento(
                tipo_pagamento=_get_tipo_pagamento_cupom(),
                data_hora_pagamento=agora,
                valor=valor_pagamento,
                registro=objeto_cupom.codigo,
                status_atual=_get_status_pagamento_aprovado(),
                data_hora_mudanca_status=agora
            )
            novo_pagamento.save()
            _adicionar_pagamento_em_inscricao(inscricao_participante, novo_pagamento)
        except Exception as e:
            print(e)
            _roolback_cupom(objeto_cupom)


def _roolback_cupom(cupom):
    cupom.usado = False
    cupom.save()


def _dar_baixa_em_cupom(cupom):
    cupom.usado = True
    cupom.save()


def _get_objeto_cupom(codigo_cupom):
    try:
        return get_evento_atual().cupons.get(codigo=codigo_cupom)
    except Exception as e:
        print(e)
        return None


def _get_valor_pagamentos_ja_realizados(inscricao_participante):
    pagamentos_realizados = 0
    for pagamento in inscricao_participante.pagamentos.filter(
            status_atual__nome__in=[STATUS_PAGAMENTO_APROVADO, STATUS_PAGAMENTO_AGUARDANDO_APROVACAO]):
        pagamentos_realizados += pagamento.valor

    return pagamentos_realizados


@login_required(login_url=URL_LOGIN)
def realizar_pagamento_paypal(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            if request.method == 'POST':
                formulario_camiseta = FormularioCamiseta(request.POST)
                if formulario_camiseta.is_valid():
                    pagamentos_ja_realizados = _get_valor_pagamentos_ja_realizados(inscricao_participante)
                    valor_a_pagar = get_pacote_full().valor - pagamentos_ja_realizados

                    if valor_a_pagar > 0:
                        dominio_corrente = get_url_dominio_corrente(request)
                        url_retorno = URL_RETORNO_PAYPAL.replace('https://secompufscar.com.br', dominio_corrente)
                        url_cancelamento = URL_CANCELAMENTO_PAYPAL.replace('https://secompufscar.com.br',
                                                                           dominio_corrente)

                        pagamento = paypalrestsdk.Payment({
                            "intent": "sale",
                            "payer": {
                                "payment_method": "paypal"
                            },
                            "redirect_urls": {
                                "return_url": url_retorno,
                                "cancel_url": url_cancelamento

                            },
                            "transactions": [
                                {
                                    "item_list": {
                                        "items": [
                                            {
                                                "name": 'KIT IX SECOMP UFSCar',
                                                "price": valor_a_pagar,
                                                "currency": "BRL",
                                                "quantity": 1
                                            },
                                        ]
                                    },
                                    "amount": {
                                        "total": valor_a_pagar,
                                        "currency": "BRL"
                                    },
                                    "description": "KIT IX Semana da Computação da UFSCar"
                                }
                            ]
                        })

                        if pagamento.create():
                            _trocar_pacote_inscricao_para_full(inscricao_participante=inscricao_participante,
                                                               id_camiseta=formulario_camiseta.cleaned_data['camiseta'])
                            dicionario_contexto['inscricao'] = inscricao_participante
                            return HttpResponseRedirect(pagamento['links'][1]['href'])
                        else:
                            _enviar_email_erro(str(request.user)+"<br>"+str(pagamento))
                            return HttpResponseRedirect('/500/')
                    else:
                        # TODO Adicionar redirecionamento para Kit já pago
                        return HttpResponseRedirect(URL_DASHBOARD)
                else:
                    return comprar_kit(request)
            else:
                if float(_get_valor_pagamentos_ja_realizados(inscricao_participante)) < get_pacote_full().valor:
                    roolback_pacote_inscricao_para_free(inscricao_participante)
                return HttpResponseRedirect(URL_COMPRAR_KIT)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _get_status_pagamento_aprovado():
    try:
        return StatusPagamento.objects.get(nome='Aprovado')
    except ObjectDoesNotExist:
        status_aprovado = StatusPagamento(nome='Aprovado')
        status_aprovado.save()
        return status_aprovado


def _get_status_pagamento_aguardando_aprovacao():
    try:
        return StatusPagamento.objects.get(nome='Aguardando aprovação')
    except ObjectDoesNotExist:
        status_aguardando_aprovacao = StatusPagamento(nome='Aguardando aprovação')
        status_aguardando_aprovacao.save()
        return status_aguardando_aprovacao


def _criar_registro_pagamento(tipo_pagamento, valor, id_pagamento, status_atual=_get_status_pagamento_aprovado()):
    agora = timezone.now()
    novo_pagamento = Pagamento(
        tipo_pagamento=tipo_pagamento,
        data_hora_pagamento=timezone.now(),
        valor=valor,
        registro=id_pagamento,
        status_atual=status_atual,
        data_hora_mudanca_status=agora
    )
    novo_pagamento.save()
    return novo_pagamento


def _trocar_pacote_inscricao_para_full(inscricao_participante, id_camiseta):
    # TODO Adicionar tratamento de exceção? Verificar necessidade de validar quantidade de camisetas aqui
    inscricao_participante.pacote = get_pacote_full()
    inscricao_participante.camiseta = get_evento_atual().camisetas.get(id=id_camiseta)
    inscricao_participante.save()
    _dar_baixa_em_camiseta(id_camiseta)


def roolback_pacote_inscricao_para_free(inscricao_participante):
    if inscricao_participante.pacote == get_pacote_full():
        inscricao_participante.pacote = get_pacote_free()
        _devolver_camiseta_para_estoque(inscricao_participante.camiseta)
        inscricao_participante.camiseta = None
        inscricao_participante.save()


@transaction.atomic
def _dar_baixa_em_camiseta(id_camiseta):
    camiseta = get_evento_atual().camisetas.get(id=id_camiseta)
    camiseta.quantidade_restante -= 1
    camiseta.save()


@transaction.atomic
def _devolver_camiseta_para_estoque(camiseta):
    camiseta.quantidade_restante += 1
    camiseta.save()


def _adicionar_pagamento_em_inscricao(inscricao_participante, pagamento):
    inscricao_participante.pagamentos.add(pagamento)


@login_required(login_url=URL_LOGIN)
def pagamento_paypal_confirmado(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            id_pagamento = request.GET.get('paymentId', '')
            id_pagador = request.GET.get('PayerID', '')

            pagamento_paypal = paypalrestsdk.Payment.find(id_pagamento)
            pagamento = None

            try:
                if pagamento_paypal.execute({'payer_id': id_pagador}):
                    pagamento = _criar_registro_pagamento(tipo_pagamento=TipoPagamento.objects.get(nome='PayPal'),
                                                          valor=pagamento_paypal.transactions[0].amount.total,
                                                          id_pagamento=id_pagamento)
                    _adicionar_pagamento_em_inscricao(inscricao_participante=inscricao_participante,
                                                      pagamento=pagamento)
                    _enviar_email_pagamento_paypal_confirmado(request, pagamento)
                    dicionario_contexto['valor_pago'] = pagamento.valor
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return render(request=request, template_name=TEMPLATE_PAGAMENTO_PAYPAL_CONFIRMADO,
                                  context=dicionario_contexto)
                else:
                    roolback_pacote_inscricao_para_free(inscricao_participante)
                    dicionario_contexto['inscricao'] = inscricao_participante
                    return render(request=request, template_name=TEMPLATE_PAGAMENTO_PAYPAL_ERRO,
                                  context=dicionario_contexto)
            except Exception as e:
                print(e)
                _roolback_adicionar_pagamento(pagamento)
                roolback_pacote_inscricao_para_free(inscricao_participante)
                return render(request=request, template_name=TEMPLATE_PAGAMENTO_PAYPAL_ERRO,
                              context=dicionario_contexto)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _roolback_adicionar_pagamento(pagamento):
    if pagamento is not None:
        try:
            pagamento.delete()
        except Exception as e:
            print(e)
            return


def _get_tipo_pagamento_paypal():
    try:
        return TipoPagamento.objects.get(nome='PayPal')
    except ObjectDoesNotExist:
        tipo_pagamento_paypal = TipoPagamento(nome='PayPal',
                                              deducoes_fixas=0,
                                              porcentagem_deducoes_variaveis=0)
        tipo_pagamento_paypal.save()
        return tipo_pagamento_paypal


def _inscricao_tem_pagamento_paypal(inscricao_participante):
    pagamentos_paypal = inscricao_participante.pagamentos.filter(tipo_pagamento=_get_tipo_pagamento_paypal()).count()
    return pagamentos_paypal > 0


@login_required(login_url=URL_LOGIN)
def pagamento_paypal_cancelado(request):
    if _is_compra_kit_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            roolback_pacote_inscricao_para_free(inscricao_participante)
            dicionario_contexto['inscricao'] = inscricao_participante
            return render(request=request, template_name=TEMPLATE_PAGAMENTO_PAYPAL_CANCELADO,
                          context=dicionario_contexto)
        else:
            return HttpResponseRedirect(URL_INSCRICAO)
    else:
        return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def cadastro(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(URL_DASHBOARD)
    if _is_inscricao_disponivel():
        if request.method == 'POST':
            formulario_cadastro = FormularioDeCadastro(request.POST)
            if formulario_cadastro.is_valid():
                recaptcha_response = request.POST.get('g-recaptcha-response')
                data = {
                    'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                    'response': recaptcha_response
                }
                r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
                result = r.json()
                if result['success']:
                    if _email_e_senha_sao_validos(formulario_cadastro) and _cadastrar_novo_usuario(formulario_cadastro):
                        return HttpResponseRedirect(URL_CONFIRMACAO_EMAIL)
                else:
                    return HttpResponseRedirect("/recaptcha/")

        else:
            dicionario_contexto.pop('erro', None)
            formulario_cadastro = FormularioDeCadastro()
        dicionario_contexto['form'] = formulario_cadastro
        return render(request=request, template_name=TEMPLATE_CADASTRO, context=dicionario_contexto)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _get_instituicao_do_formulario(lista_instituicao):
    try:
        instituicao = lista_instituicao[0]
        return instituicao
    except Exception as e:
        print(e)
        return None


def _get_curso_do_formulario(lista_curso):
    try:
        curso = lista_curso[0]
        return curso
    except Exception as e:
        print(e)
        return None


def _obter_dados_formulario(formulario_cadastro):
    return (
        formulario_cadastro.cleaned_data['email'],
        formulario_cadastro.cleaned_data['senha'],
        formulario_cadastro.cleaned_data['nome'],
        formulario_cadastro.cleaned_data['sobrenome'],
        _get_instituicao_do_formulario(formulario_cadastro.cleaned_data['instituicao']),
        formulario_cadastro.cleaned_data['outra_instituicao'],
        _get_curso_do_formulario(formulario_cadastro.cleaned_data['curso']),
        formulario_cadastro.cleaned_data['outro_curso'],
        formulario_cadastro.cleaned_data['ano_ingresso'],
    )


def _obter_email_e_senha(formulario_cadastro):
    return (
        formulario_cadastro.cleaned_data['email'],
        formulario_cadastro.cleaned_data['confirmacao_email'],
        formulario_cadastro.cleaned_data['senha'],
        formulario_cadastro.cleaned_data['confirmacao_senha']
    )


def _email_e_senha_sao_validos(formulario_cadastro):
    email, confirmacao_email, senha, confirmacao_senha = _obter_email_e_senha(formulario_cadastro)

    erro = ''

    if not _email_bate_com_confirmacao(email, confirmacao_email):
        erro = 'E-mail e confirmação de e-mail não correspondem.'
    if not _senha_bate_com_confirmacao(senha, confirmacao_senha):
        erro = 'Senha e confirmação de senha não correspondem.'
    if _email_ja_cadastrado(email):
        erro = 'O e-mail informado já está cadastrado em nosso sistema.<br>' + \
               'Caso tenha esquecido a senha, você pode redefiní-la neste ' + \
               '<a href="/2017/area-do-participante/redefinir-senha/">link</a>.'

    try:
        validate_password(senha)
    except ValidationError:
        erro = 'A senha informada não atinge os requisitos mínimos (mínimo de 8 caracteres, ' \
               'não inteiramente numérica).'

    if erro != '':
        dicionario_contexto['erro'] = erro
        return False

    return True


def _email_bate_com_confirmacao(email, confirmacao_email):
    return email == confirmacao_email


def _senha_bate_com_confirmacao(senha, confirmacao_senha):
    return senha == confirmacao_senha


def _email_ja_cadastrado(email):
    try:
        User.objects.get(email=email)
        return True
    except ObjectDoesNotExist:
        return False


def _cadastrar_novo_usuario(formulario_cadastro):
    (email, senha, nome, sobrenome, instituicao, outra_instituicao, curso, outro_curso, ano_ingresso) = \
        _obter_dados_formulario(formulario_cadastro)

    novo_usuario = None
    try:
        novo_usuario = User.objects.create_user(
            username=email, email=email, password=senha, first_name=nome, last_name=sobrenome)
        novo_usuario.is_active = False
        novo_usuario.save()

        try:
            objeto_instituicao = _get_instituicao(instituicao, outra_instituicao)
            objeto_curso = _get_curso(curso, outro_curso)

            try:
                if ano_ingresso != '':
                    ano_ingresso = int(ano_ingresso)
                else:
                    ano_ingresso = None

                novo_participante = Participante(usuario=novo_usuario,
                                                 instituicao=objeto_instituicao,
                                                 curso=objeto_curso,
                                                 ano_ingresso=ano_ingresso,
                                                 token_confirmacao_email=uuid.uuid4())
                novo_participante.save()

                return _enviar_email_confirmacao_de_cadastro(novo_participante)
            except Exception as e:
                _enviar_email_erro(e)
                dicionario_contexto['erro'] = 'Parâmetros inválidos.'
                _roolback_cadastro(novo_usuario)
                return False
        except Exception as e:
            _enviar_email_erro(e)
            if instituicao == '0' and outra_instituicao != '':
                _roolback_cadastro_instituicao(novo_usuario.participante.instituicao)
            if curso == '0' and outro_curso != '':
                _roolback_cadastro_curso(novo_usuario.participante.curso)

            _roolback_cadastro(novo_usuario)
            dicionario_contexto['erro'] = 'Parâmetros inválidos.'
    except IntegrityError as e:
        return False
    except Exception as e:  # TODO especificar exception
        _enviar_email_erro(e)
        _roolback_cadastro(novo_usuario)
        dicionario_contexto['erro'] = 'Erro interno. Entre em contato com a organização.'
        return False


def _roolback_cadastro(novo_usuario):
    _roolback_cadastro_participante(novo_usuario)
    _roolback_cadastro_usuario(novo_usuario)


def _roolback_cadastro_participante(novo_usuario):
    try:
        novo_participante = Participante.objects.get(usuario=novo_usuario)
        novo_participante.delete()
    except Exception as e:
        print(e)
        return


def _roolback_cadastro_usuario(novo_usuario):
    try:
        usuario = User.objects.get(email=novo_usuario.email)
        usuario.delete()
    except ObjectDoesNotExist:
        return


def _roolback_cadastro_instituicao(outra_instituicao):
    if outra_instituicao is not None:
        outra_instituicao.delete()


def _roolback_cadastro_curso(outro_curso):
    if outro_curso is not None:
        outro_curso.delete()


def _get_curso(curso, outro_curso):
    if curso != '0':
        try:
            return Curso.objects.get(id=int(curso))
        except Exception as e:
            print(e)
            return None
    else:
        return _cadastrar_novo_curso(novo_curso=outro_curso)


def _cadastrar_novo_curso(novo_curso):
    try:
        if novo_curso != '':
            objeto_novo_curso = Curso(nome=novo_curso)
            objeto_novo_curso.save()
            return objeto_novo_curso
        else:
            return None
    except Exception as e:
        print(e)
        return None


def _get_instituicao(instituicao, outra_instituicao):
    if instituicao != '0':
        try:
            return Instituicao.objects.get(id=int(instituicao))
        except Exception as e:
            print(e)
            return None
    else:
        return _cadastrar_nova_instituicao(nova_instituicao=outra_instituicao)


def _cadastrar_nova_instituicao(nova_instituicao):
    try:
        if nova_instituicao != '':
            objeto_nova_instituicao = Instituicao(nome=nova_instituicao)
            objeto_nova_instituicao.save()
            return objeto_nova_instituicao
        else:
            return None
    except Exception as e:
        print(e)
        return None


def _enviar_email_confirmacao_de_cadastro(participante):
    contexto = {
        'nome': participante.usuario.first_name,
        'email': participante.usuario.email,
        'token_confirmacao_email': participante.token_confirmacao_email,
        'protocolo': 'https' if is_sistema_em_producao() else 'http',
        'dominio': 'secompufscar.com.br' if is_sistema_em_producao() else 'localhost:8002'
    }
    assunto = 'Confirmação de cadastro - IX SECOMP UFSCar'
    destinatario = contexto['email']
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario, template=TEMPLATE_EMAIL_CONFIRMACAO_EMAIL,
                                              contexto=contexto)
    return enviar_email(corpo_email=corpo_email)


def confirmar_email_via_link(request, token):
    if _confirmar_cadastro_via_token(token):
        return HttpResponseRedirect(URL_CADASTRO_CONFIRMADO)
    else:
        return HttpResponseRedirect(URL_LINK_INVALIDO)


def confirmar_email(request):
    if _is_inscricao_disponivel():
        return render(request=request, template_name=TEMPLATE_CONFIRMACAO_EMAIL, context=dicionario_contexto)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def reenviar_email_confirmacao(request):
    if _is_inscricao_disponivel():
        if request.method == 'POST':
            try:
                email = request.POST['email']
                participante = Participante.objects.get(usuario=User.objects.get(email=email))
                _resetar_token_confirmacao_email(participante=participante)
                _enviar_email_confirmacao_de_cadastro(participante=participante)
                return HttpResponseRedirect(URL_CONFIRMACAO_EMAIL)
            except ObjectDoesNotExist:
                return render(request=request, template_name=TEMPLATE_CONFIRMACAO_EMAIL, context=dicionario_contexto)
        return render(request=request, template_name=TEMPLATE_REENVIAR_EMAIL_CONFIRMACAO, context=dicionario_contexto)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def _confirmar_cadastro_via_token(token):
    try:
        participante = Participante.objects.get(token_confirmacao_email=token)
        participante.usuario.is_active = True
        participante.usuario.save()
    except ObjectDoesNotExist:
        return False
    return True


def _resetar_token_confirmacao_email(participante):
    participante.token_confirmacao_email = uuid.uuid4()
    participante.save()


def cadastro_confirmado(request):
    if _is_inscricao_disponivel():
        return render(request=request, template_name=TEMPLATE_CADASTRO_CONFIRMADO, context=dicionario_contexto)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def link_invalido(request):
    if _is_inscricao_disponivel():
        return render(request=request, template_name=TEMPLATE_LINK_INVALIDO, context=dicionario_contexto)
    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


@login_required(login_url=URL_LOGIN)
def editar_cadastro(request):
    if request.method == 'POST':
        formulario_editar_cadastro = FormularioEditarCadastro(request.POST)
        if formulario_editar_cadastro.is_valid():
            participante = Participante.objects.get(usuario=request.user)
            participante.usuario.first_name = formulario_editar_cadastro.cleaned_data['nome']
            participante.usuario.last_name = formulario_editar_cadastro.cleaned_data['sobrenome']
            participante.usuario.save()
            participante.save()
            return criar_resposta_json_sucesso(mensagem='Informações atualizadas com sucesso!')
        else:
            erros = str(formulario_editar_cadastro.errors) if formulario_editar_cadastro.errors else ' '
            return criar_resposta_json_erro(mensagem='Formulário inválido \n ' + erros, status_code=400)
    else:
        return criar_resposta_json_erro(mensagem='Métodos permitidos: [POST]', status_code=405)


@login_required(login_url=URL_LOGIN)
def api_detalhes_inscricao_atividades(request):

    inscricao_participante = Inscricao.objects.get(evento=get_evento_atual(),
                                                   participante__usuario=request.user)
    atividades_inscritas = [atividade.id for atividade in inscricao_participante.atividades_inscritas.all()]

    if request.method == 'POST':
        if int(request.POST['id_atividade']) not in atividades_inscritas:
            return api_inscricao_em_atividade(request)
        else:
            if get_evento_atual().atividades.get(
                    id=int(request.POST['id_atividade'])).titulo == 'Desafio de Programadores' and \
                            get_evento_atual().desafio_programadores.equipes.filter(
                                integrantes__in=[inscricao_participante.participante]).first() is not None:
                return criar_resposta_json_erro(
                    mensagem="Não é possível se desinscrever no momento, entre em contato com nossa equipe.",
                    status_code=403)
            else:
                return api_cancelar_inscricao_em_atividade(request)
    else:
        try:
            inscricoes_abertas = {}
            agora = timezone.now()
            inscricoes_minicursos = get_evento_atual().datas_inscricoes_atividades.get(
                tipo_atividade=TipoAtividade.objects.get(nome='Minicurso'))
            inscricoes_workshops = get_evento_atual().datas_inscricoes_atividades.get(
                tipo_atividade=TipoAtividade.objects.get(nome='Workshop'))
            inscricoes_processos_seletivos = get_evento_atual().datas_inscricoes_atividades.get(
                tipo_atividade=TipoAtividade.objects.get(nome='Processo Seletivo'))
            inscricoes_abertas['Minicurso'] = \
                inscricoes_minicursos.data_hora_inicio_inscricoes <= agora <= inscricoes_minicursos.data_hora_fim_inscricoes
            inscricoes_abertas['Workshop'] = \
                inscricoes_workshops.data_hora_inicio_inscricoes <= agora <= inscricoes_workshops.data_hora_fim_inscricoes
            inscricoes_abertas['Processo Seletivo'] = \
                inscricoes_processos_seletivos.data_hora_inicio_inscricoes <= agora <= inscricoes_processos_seletivos.data_hora_fim_inscricoes
            atividades = {'atividades': []}
            for atividade in Atividade.objects.filter(evento=get_evento_atual()):
                if atividade.tipo.nome == 'Minicurso' or atividade.tipo.nome == 'Workshop' \
                        or atividade.tipo.nome == 'Processo Seletivo' or atividade.titulo == 'Desafio de Programadores':
                    detalhes_atividade = {
                        'id': atividade.id,
                        'vagas_totais': atividade.vagas_totais,
                        'vagas_disponiveis': atividade.vagas_disponiveis,
                        'inscrito': atividade.id in atividades_inscritas,
                    }

                    if atividade.titulo != 'Desafio de Programadores':
                        detalhes_atividade['inscricao_disponivel'] = inscricoes_abertas[atividade.tipo.nome]
                    else:
                        detalhes_atividade['inscricao_disponivel'] = \
                            agora > get_evento_atual().desafio_programadores.inicio_inscricoes_desafio_programadores \
                            and inscricao_participante.pacote != get_pacote_free()

                    if atividade.tipo.nome == 'Minicurso':
                        detalhes_atividade['inscricao_encerrada'] = \
                            agora > inscricoes_minicursos.data_hora_fim_inscricoes
                        detalhes_atividade['minicurso'] = True
                    elif atividade.tipo.nome == 'Workshop':
                        detalhes_atividade['inscricao_encerrada'] = \
                            agora > inscricoes_workshops.data_hora_fim_inscricoes
                        detalhes_atividade['workshop'] = True
                    elif atividade.tipo.nome == 'Processo Seletivo':
                        detalhes_atividade['inscricao_encerrada'] = \
                            agora > atividade.horarios.first().data_hora_inicio
                        detalhes_atividade['processo_seletivo'] = True
                    elif atividade.titulo == 'Desafio de Programadores':
                        detalhes_atividade['inscricao_encerrada'] = \
                            agora > get_evento_atual().desafio_programadores.fim_inscricoes_desafio_programadores
                        detalhes_atividade['desafio_de_programadores'] = True

                    atividades['atividades'].append(detalhes_atividade)

            atividades['ultima_atualizacao'] = agora
            return JsonResponse(atividades)
        except ObjectDoesNotExist as e:
            print(e)
            return criar_resposta_json_erro(mensagem='Inscrição não encontrada', status_code=404)


@login_required(login_url=URL_LOGIN)
def api_inscricao_em_atividade(request):
    id_atividade = request.POST['id_atividade']

    try:
        atividade = get_evento_atual().atividades.get(id=id_atividade)
    except ObjectDoesNotExist:
        _enviar_email_erro(str(request.user.email) + "tentou se cadastrar em uma atividade inexistente")
        return criar_resposta_json_erro(mensagem='Atividade não encontrada.', status_code=404)

    try:
        inscricao_participante = _get_inscricao_participante(request.user)
    except ObjectDoesNotExist:
        print('INSCRIÇÃO NÃO ENCONTRADA - ' + request.user.email)
        return criar_resposta_json_erro(mensagem='Inscrição não encontrada.', status_code=404)

    try:
        for atividade_inscrita in inscricao_participante.atividades_inscritas.all():
            if atividade_inscrita == atividade:
                return criar_resposta_json_erro(mensagem='Usuário já inscrito nesta atividade.',
                                                status_code=412)
            if atividade_inscrita.tipo.nome == 'Minicurso' and atividade.tipo.nome == 'Minicurso':
                print('JÁ INSCRITO EM MINICURSO - ' + atividade.titulo + ' - ' + request.user.email)
                return criar_resposta_json_erro(
                    mensagem='É permitida a inscrição em apenas um minicurso por participante.',
                    status_code=412)

        atividade = get_evento_atual().atividades.get(id=id_atividade)

        if atividade.titulo == 'Desafio de Programadores':
            if len(get_evento_atual().desafio_programadores.equipes.filter(
                    integrantes__in=[get_participante(request.user)])) == 0:
                inscricao_participante.atividades_inscritas.add(atividade)
                inscricao_participante.save()
                #Evento.adicionar_log_inscricao_atividade(inscricao_participante, id_atividade)
                return criar_resposta_json_sucesso(
                    mensagem='Inscrição no Desafio de Programadores realizado com sucesso.')
            else:
                return criar_resposta_json_erro(
                    mensagem='Você está inscrito em uma equipe, e por isso não pode cancelar sua inscrição no Desafio' +
                             ' no momento. Solicite que o participante que inscreveu a equipe cancele a sua inscrição' +
                             ' na equipe.',
                    status_code=412)
        if atividade.tipo.nome == 'Processo Seletivo':
            inscricao_participante.atividades_inscritas.add(atividade)
            inscricao_participante.save()
            #Evento.adicionar_log_inscricao_atividade(inscricao_participante, id_atividade)
            return criar_resposta_json_sucesso(mensagem='Inscrição na atividade realizada com sucesso.')
        else:
            status = Atividade.inscrever_em_atividade(inscricao_participante.id, id_atividade)
            if status == 0:
                #Evento.adicionar_log_inscricao_atividade(inscricao_participante, id_atividade)

                return criar_resposta_json_sucesso(mensagem='Inscrição na atividade realizada com sucesso')
            elif status == 1:
                print('VAGAS ESGOTADAS - ' + atividade.titulo + ' - ' + request.user.email)
                return criar_resposta_json_erro(
                    mensagem='Alguém se inscreveu primeiro e as vagas estão esgotadas para esta atividade.',
                    status_code=412)
            else:
                print('USUÁRIO JÁ INSCRITO - ' + atividade.titulo + ' - ' + request.user.email)
                return criar_resposta_json_erro(mensagem='Usuário já inscrito nesta atividade.', status_code=412)

    except Exception as e:
        print('OUTRO ERRO - ' + request.user.email)
        print(e)
        return criar_resposta_json_erro(mensagem='Erro interno no servidor. Tente novamente.', status_code=500)


@login_required(login_url=URL_LOGIN)
def api_cancelar_inscricao_em_atividade(request):
    inscricao_participante = _get_inscricao_participante(request.user)
    id_inscricao = inscricao_participante.id
    id_atividade = request.POST['id_atividade']

    Atividade.cancelar_inscricao_em_atividade(id_inscricao=id_inscricao, id_atividade=id_atividade)
    #Evento.adicionar_log_cancelamento_atividade(inscricao_participante, id_atividade)

    return criar_resposta_json_sucesso(mensagem='Cancelamento de inscrição em atividade realizado com sucesso.')


def _enviar_email_generico(assunto, titulo, conteudo, destinatario):
    contexto = {
        'nome': destinatario['nome'],
        'conteudo': conteudo,
        'titulo': titulo,
        'assunto': assunto,
    }
    destinatario = destinatario['email']
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_GENERICO,
                                              contexto=contexto)

    try:
        status = enviar_email(corpo_email=corpo_email)
        print(str(status) + ": No try")

    except Exception as e:
        print(e)
        status = False
        print(str(status) + ": No except")

    return status


def _enviar_email_erro(erro):
    contexto = {
        'nome': "Mestre",
        'conteudo':erro,
        'titulo': "Erro",
        'assunto': "Erro",
    }
    destinatario = "ti@secompufscar.com.br"
    corpo_email = get_email_pronto_para_envio(assunto="Erro", remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_GENERICO,
                                              contexto=contexto)

    try:
        status = enviar_email(corpo_email=corpo_email)
        print(str(status) + ": No try")

    except Exception as e:
        print(e)
        status = False
        print(str(status) + ": No except")

    return status


@login_required(login_url=URL_LOGIN)
def inscrever_equipe_desafio(request):
    if request.method == 'POST':
        formulario_equipe = FormularioInscricaoEquipeDesafio(request.POST)
        if formulario_equipe.is_valid():

            nomeEquipe = formulario_equipe.cleaned_data['nomeEquipe']
            participante1_email = formulario_equipe.cleaned_data['participante1']
            participante2_email = formulario_equipe.cleaned_data['participante2']

            try:

                integrantes = [_get_inscricao_participante(request.user)]

                if len(participante1_email) > 0:
                    integrantes.append(get_evento_atual().inscricoes.get(
                        participante__usuario__email=participante1_email))

                if len(participante2_email) > 0:
                    integrantes.append(get_evento_atual().inscricoes.get(
                        participante__usuario__email=participante2_email))

                integrantes = set(integrantes)

                if len(get_evento_atual().desafio_programadores.equipes.filter(
                        integrantes__in=[inscricao.participante for inscricao in integrantes])) == 0:

                    atividade_desafio = get_evento_atual().atividades.get(titulo="Desafio de Programadores")

                    for inscricao in integrantes:
                        if atividade_desafio not in inscricao.atividades_inscritas.all():
                            return criar_resposta_json_erro(
                                mensagem="Todos os integrantes devem estar inscritos no Desafio de Programadores.",
                                status_code=412)

                    equipes_inscritas = [equipe.nome for equipe in
                                         get_evento_atual().desafio_programadores.equipes.all()]

                    if nomeEquipe not in equipes_inscritas:
                        nova_equipe = EquipeDesafioDeProgramadores(nome=nomeEquipe)
                        nova_equipe.data_hora_inscricao = timezone.now()
                        nova_equipe.save()

                        for inscricao in integrantes:
                            nova_equipe.integrantes.add(inscricao.participante)

                        nova_equipe.save()

                        get_evento_atual().desafio_programadores.equipes.add(nova_equipe)

                        conteudo_email = '<p>Você foi inscrito na equipe <b>' + nomeEquipe + '</b>.</p>' + \
                                         '<p>Em caso de divergência nesta mensagem, entre em contato conosco respondendo este e-mail.</p>'

                        for inscricao in integrantes:
                            _enviar_email_generico(
                                assunto='Desafio de Programadores: Inscrição em equipe - IX SECOMP UFSCar',
                                titulo='Desafio de Programadores: Inscrição em equipe',
                                conteudo=conteudo_email,
                                destinatario={'nome': inscricao.participante.usuario.first_name,
                                              'email': inscricao.participante.usuario.email})

                        return criar_resposta_json_sucesso("Equipe inscrita com sucesso")
                    else:
                        return criar_resposta_json_erro(mensagem="Já existe uma equipe com o mesmo nome.",
                                                        status_code=412)
                else:
                    return criar_resposta_json_erro(
                        mensagem="Um ou mais integrantes já estão cadastrados em outra equipe.",
                        status_code=412)

            except ObjectDoesNotExist:
                return criar_resposta_json_erro(
                    mensagem="Todos os integrantes devem estar inscritos na SECOMP, verifique os emails informados",
                    status_code=404)
        else:
            return criar_resposta_json_erro(mensagem="Formulário inválido",
                                            status_code=412)

    return criar_resposta_json_erro(mensagem="Algum erro interno ocorreu", status_code=500)


@login_required(login_url=URL_LOGIN)
def get_equipe_desafio(request):
    inscricao_participante_atual = _get_inscricao_participante(request.user)

    equipe = get_evento_atual().desafio_programadores.equipes.filter(
        integrantes__in=[inscricao_participante_atual.participante]).first()

    if equipe is not None:
        response = JsonResponse({'nome_equipe': equipe.nome})
        response.status_code = 200
    else:
        response = JsonResponse({})
        response.status_code = 404

    return response


@login_required(login_url=URL_LOGIN)
def pagina_inscricao_atividades(request):
    if _is_dashboard_disponivel():
        inscricao_participante = _get_inscricao_participante(request.user)
        if inscricao_participante is not None:
            dicionario_contexto.pop('alr_minicurso', None)
            dicionario_contexto.pop('alr_workshop', None)
            dicionario_contexto['nao_mostrar_workshop'] = False
            workshops = []
            dicionario_contexto['inscricao'] = inscricao_participante
            for atividade in inscricao_participante.atividades_inscritas.all():
                if(atividade.tipo==tipo_minicurso):
                    dicionario_contexto['alr_minicurso']= atividade
                if(atividade.tipo==tipo_workshop):
                    workshops.append(atividade)
            # Não rela a mão, pllmds kkkjkkkj!onze
            if len(workshops) == 4:
                dicionario_contexto['nao_mostrar_workshop'] = True
            dicionario_contexto['alr_workshop'] = workshops
            dicionario_contexto['participante'] = Participante.objects.get(usuario=request.user)
            return render(request=request, template_name=TEMPLATE_INSCRICAO_ATIVIDADES_2018, context=dicionario_contexto)

        elif _is_inscricao_disponivel():
            return HttpResponseRedirect(URL_INSCRICAO)

    return HttpResponseRedirect(URL_INSCRICOES_NAO_DISPONIVEIS)


def get_user_score(inscricao_participante):
    result = 0
    for p in inscricao_participante.presencas.all():
        result += get_score(p)
    return result


def get_user_score_day(inscricao,dia):
    score = 0
    presencas = inscricao.presencas.all()
    for p in presencas:
        try:
            if(p.atividade.horarios.first().data_hora_inicio.strftime('%w') == dia):
                score += get_score(p)
        except:
            pass
    return score


def get_rank_by_day(dia):
    inscr = evento_atual().inscricoes.all()
    rank = []
    max = 0
    for i in inscr:
        score = get_user_score_day(i,dia)
        rank.append({"inscr": str(i.participante.usuario.first_name)+" "+str(i.participante.usuario.last_name),
                     "score": score})
    max = 0
    maiores = []
    for r in rank:
        if(r['score']>=max):
            max = r['score']
            maiores.append(r)

    for m in maiores:
        if(m['score']<max):
            maiores.remove(m)

    return maiores


