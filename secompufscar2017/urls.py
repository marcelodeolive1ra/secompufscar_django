# coding: utf-8

from django.conf.urls import include, url
from secompufscar2017 import views

app_name = 'secompufscar2017'
urlpatterns = [
    url(r'^area-do-participante/', include('secompufscar2017.urls_area_do_participante')),
    url(r'^area-administrativa/', include('secompufscar2017.urls_area_administrativa')),
    url(r'^api/', include('secompufscar2017.urls_apis')),
]
