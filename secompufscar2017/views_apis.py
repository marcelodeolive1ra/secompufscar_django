import datetime
import requests

from secompufscar2017.utilities import *
from secompufscar2017.views import dicionario_contexto
from secompufscar_bd.models import Atividade, Ministrante, Camiseta, TipoAtividade


def _get_dicionario_horarios_atividade(atividade):
    return [
        {
            'data_hora_inicio': horario.data_hora_inicio,
            'data_hora_fim': horario.data_hora_fim
        }
        for horario in atividade.horarios.all()
    ]


def _get_local_atividade(atividade):
    return {
        'predio':
            atividade.local.predio if atividade.local is not None and atividade.local.predio is not None else None,
        'sala': atividade.local.sala if atividade.local is not None and atividade.local.predio is not None else None
    }


def _get_url_foto_ministrante(ministrante):
    if ministrante.foto:
        return ministrante.foto.url
    return None


def _get_resumo_dados_ministrante(request, ministrante):
    link_foto_relativo = _get_url_foto_ministrante(ministrante=ministrante)
    link_foto = None
    if link_foto_relativo is not None:
        link_foto = get_url_dominio_corrente(request) + _get_url_foto_ministrante(ministrante=ministrante)

    return {
        'id': ministrante.id,
        'nome': ministrante.nome,
        'sobrenome': ministrante.sobrenome,
        'profissao': ministrante.profissao,
        'empresa': ministrante.empresa,
        'link_foto': link_foto,
        'ultima_atualizacao': ministrante.ultima_atualizacao_em
    }


def _get_detalhes_dados_ministrante(ministrante):
    return {
        'descricao': ministrante.descricao,
        'contatos': _get_dicionario_contatos_ministrante(ministrante)
    }


def _get_dicionario_contato(contato_ministrante):
    return {
        'tipo_contato': contato_ministrante.tipo_contato.nome,
        'link': contato_ministrante.link
    }


def _get_dicionario_contatos_ministrante(ministrante):
    return [
        _get_dicionario_contato(contato_ministrante) for contato_ministrante in ministrante.contatos.all()
    ]


def _get_dicionario_ministrantes_atividade_resumido(request, atividade):
    return [
        _get_resumo_dados_ministrante(request, ministrante) for ministrante in atividade.ministrante.all()
    ]


def _get_dicionario_ministrante(request, ministrante):
    return dict(**_get_resumo_dados_ministrante(request, ministrante), **_get_detalhes_dados_ministrante(ministrante))


def _get_dicionario_ministrantes_atividade_completo(request, atividade):
    return [_get_dicionario_ministrante(request, ministrante) for ministrante in atividade.ministrante.all()]


def _get_dicionario_pks_ministrantes_atividade(atividade):
    return [{'id': ministrante.id} for ministrante in atividade.ministrante.all()]


def _get_dicionario_atividade(request, atividade, ministrantes_pk_only, ministrantes_resumo):
    dicionario_atividade = {
        'id': atividade.id,
        'tipo': atividade.tipo.nome.lower(),
        'titulo': atividade.titulo,
        'descricao': atividade.descricao,
        'horarios': _get_dicionario_horarios_atividade(atividade=atividade),
        'local': _get_local_atividade(atividade=atividade),
        'pre_requisitos': atividade.pre_requisitos,
        'pre_requisitos_recomendados': atividade.pre_requisitos_recomendados,
        'ministrantes': (_get_dicionario_pks_ministrantes_atividade(atividade) if ministrantes_pk_only else
            (_get_dicionario_ministrantes_atividade_resumido(request, atividade) if ministrantes_resumo else
             _get_dicionario_ministrantes_atividade_completo(request, atividade)))
            if atividade.ministrante.count() > 0 else [],
        'ultima_atualizacao': atividade.ultima_atualizacao_em
    }
    if atividade.vagas_totais is not None:
        dicionario_atividade['vagas_totais'] = atividade.vagas_totais
        dicionario_atividade['vagas_disponiveis'] = atividade.vagas_disponiveis

    return dicionario_atividade


def api_atividades(request, id_atividade):
    ministrantes_pk_only = True if request.GET.get('ministrantes_pk_only', '') == 'True' else False
    ministrantes_resumo = True if request.GET.get('ministrantes_resumo', '') == 'True' else False

    try:
        atividade = Atividade.objects.get(id=id_atividade)
        return JsonResponse(_get_dicionario_atividade(request, atividade, ministrantes_pk_only, ministrantes_resumo))
    except ObjectDoesNotExist:
        return criar_resposta_json_erro(mensagem='Atividade não encontrada.', status_code=404)


def _get_dicionario_atividades(request, atividades, ministrantes_pk_only, ministrantes_resumo):
    return [
        _get_dicionario_atividade(request, atividade, ministrantes_pk_only,
                                  ministrantes_resumo) for atividade in atividades
    ]


def api_todas_atividades(request):
    ministrantes_pk_only = True if request.GET.get('ministrantes_pk_only', '') == 'True' else False
    ministrantes_resumo = True if request.GET.get('ministrantes_resumo', '') == 'True' else False

    atividades = {}
    tipos_atividades = []

    for tipo in TipoAtividade.objects.all():
        nome_tipo_atividade = tipo.nome.lower()
        tipos_atividades.append(nome_tipo_atividade)

        atividades[nome_tipo_atividade] = _get_dicionario_atividades(request,
                                                                     get_evento_atual().atividades.filter(tipo=tipo),
                                                                     ministrantes_pk_only, ministrantes_resumo)
        for atividade in atividades[nome_tipo_atividade]:
            if 'tipo' in atividade:
                del atividade['tipo']

    return JsonResponse({
        'tipos_atividades': tipos_atividades,
        'atividades': atividades,
        'ultima_atualizacao': dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_ATIVIDADES]
    })


def _get_url_dominio_corrente(request):
    return ('https' if request.is_secure() else 'http') + '://' + request.META['HTTP_HOST']


def _get_url_logo_patrocinador(patrocinador):
    if patrocinador.logo:
        return patrocinador.logo.url
    return None


def _get_dicionario_patrocinador(request, patrocinador):
    url_logo_relativo = _get_url_logo_patrocinador(patrocinador=patrocinador)
    url_logo = None
    if url_logo_relativo is not None:
        url_logo = get_url_dominio_corrente(request) + url_logo_relativo

    return {
        'id': patrocinador.id,
        'nome': patrocinador.nome_empresa,
        'link_website': patrocinador.link_website,
        'url_logo': url_logo,
        'ultima_atualizacao': patrocinador.ultima_atualizacao_em
    }


def _get_dicionario_patrocinadores(request, patrocinadores):
    return [
        _get_dicionario_patrocinador(request, patrocinador) for patrocinador in patrocinadores
    ]


def api_todos_patrocinadores(request):
    patrocinadores = {}
    cotas = []

    for cota in dicionario_contexto[PATROCINADORES]:
        cotas.append(cota)
        patrocinadores[cota] = \
            _get_dicionario_patrocinadores(request, dicionario_contexto['patrocinadores'][cota])

    return JsonResponse({
        'cotas': cotas,
        'patrocinadores': patrocinadores,
        'ultima_atualizacao': dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_PATROCINADORES]
    })


def api_ministrantes(request, id_ministrante):
    try:
        ministrante = Ministrante.objects.get(id=id_ministrante)
        return JsonResponse(_get_dicionario_ministrante(request, ministrante))
    except ObjectDoesNotExist:
        response = JsonResponse({'erro': 'Ministrante não encontrado.'})
        response.status_code = 404
        return response


def api_todos_ministrantes(request):
    ministrantes = []
    for ministrante in dicionario_contexto['ministrantes']:
        ministrantes.append(_get_dicionario_ministrante(request, ministrante))

    return JsonResponse({
        'ministrantes': ministrantes,
        'ultima_atualizacao': dicionario_contexto[DATA_ULTIMA_ATUALIZACAO_MINISTRANTES]
    })


def api_camisetas(request, id_camiseta):
    try:
        camiseta = Camiseta.objects.get(id=id_camiseta)
        return JsonResponse({
            'id': camiseta.id,
            'quantidade_total': camiseta.quantidade,
            'quantidade_restante': camiseta.quantidade_restante
        })
    except ObjectDoesNotExist:
        response = JsonResponse({'erro': 'Camiseta não encontrada.'})
        response.status_code = 404
        return response


def api_facebook(request):
    #TODO: colocar como constante no arquivo constants.py
    LIMITE_POSTS = '10'
    PAGE_ID = '301932149195'
    payload = {
        'access_token' : settings.FACEBOOK,
            'fields': 'picture,name,posts.limit('+LIMITE_POSTS+'){full_picture, message, created_time, likes.summary(true), comments.summary(true), shares, picture.type(large)}',
    }
    request = requests.get(
        'https://graph.facebook.com/'+ PAGE_ID,
        params=payload
    )
    response = request.json()
    return JsonResponse(response)


def api_instagram(request):
    payload = {
        'access_token' : settings.INSTAGRAM,
        'count': '10'
    }
    request = requests.get(
        'https://api.instagram.com/v1/users/self/media/recent/',
        params=payload
    )
    dados = request.json()
    data = []
    for post in dados['data']:
        if post['caption'] is not None:
            post_info = {
                'id' : post['id'],
                'tags': post['tags'],
                'caption': {
                    'text' : post['caption']['text']
                },
                'images': post['images'],
                'created_time': datetime.datetime.fromtimestamp(int(post['created_time'])),
                'likes_count': post['likes']['count']
            }
        else:
            post_info = {
                'id': post['id'],
                'tags': post['tags'],
                'images': post['images'],
                'created_time': datetime.datetime.fromtimestamp(int(post['created_time'])),
                'likes_count': post['likes']['count']
            }
        data.append(post_info)
    return JsonResponse({
        'meta': {
            'code': 200
        },
        'user': dados['data'][0]['user'],
        'data': data
    })
