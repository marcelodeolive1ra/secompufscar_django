# coding: utf-8

from django.conf.urls import url
from django.contrib.auth import views as auth_views
from secompufscar2018.views import dicionario_contexto
from secompufscar2017 import views_area_do_participante

dicionario_contexto_login = dicionario_contexto.copy()

urlpatterns = [
    url(r'^$', views_area_do_participante.area_do_participante, name='area_do_participante'),
    url(r'^inscricoes-nao-disponiveis/$', views_area_do_participante.inscricoes_nao_disponiveis,
        name='inscricoes_nao_disponiveis'),
    url(r'^login/$',
        auth_views.LoginView.as_view(
            template_name='secompufscar2017/area_do_participante/login.html',
            extra_context=dicionario_contexto_login
        ),
        name='login'),
    url(r'^logout/$',
        auth_views.LogoutView.as_view(
            next_page='/2018/area-do-participante/logout/sucesso/',
            extra_context=dicionario_contexto
        ),
        name='logout'),
    url(r'^logout/sucesso/$', views_area_do_participante.logout_realizado_com_sucesso, name='logout_ok'),
    url(r'^redefinir-senha/$',
        auth_views.PasswordResetView.as_view(
            template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_form.html',
            email_template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_email.html',
            html_email_template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_email_html.html',
            subject_template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_subject.txt',
            success_url='/2018/area-do-participante/redefinir-senha/redefinida/',
            extra_context=dicionario_contexto),
        name='password_reset'),
    url(r'^redefinir-senha/redefinida/$',
        auth_views.PasswordResetDoneView.as_view(
            template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_done.html',
            extra_context=dicionario_contexto
        ),
        name='password_reset_done'),
    url(r'^redefinir-senha/criar-nova-senha/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_confirm.html',
            success_url='/2018/area-do-participante/redefinir-senha/criar-nova-senha/concluido/',
            extra_context=dicionario_contexto
        ),
        name='password_reset_confirm'),
    url(r'^redefinir-senha/criar-nova-senha/concluido/$',
        auth_views.PasswordResetCompleteView.as_view(
            template_name='secompufscar2017/area_do_participante/gerenciamento_senha/password_reset_complete.html',
            extra_context=dicionario_contexto
        ),
        name='password_reset_complete'),
    url(r'^cadastro/$', views_area_do_participante.cadastro, name='cadastro'),
    url(r'^cadastro/confirmar-email/$', views_area_do_participante.confirmar_email, name='confirmar_email'),
    url(r'^cadastro/confirmar-email/(?P<token>[0-9A-Fa-f-]+)',
        views_area_do_participante.confirmar_email_via_link, name='confirmar_email_via_link'),
    url(r'^cadastro/confirmado/$',
        views_area_do_participante.cadastro_confirmado, name='cadastro_confirmado'),
    url(r'^cadastro/link-invalido/$', views_area_do_participante.link_invalido, name='link_invalido'),
    url(r'^cadastro/reenviar-email-confirmacao/$', views_area_do_participante.reenviar_email_confirmacao,
        name='reenviar_email_confirmacao'),
    url(r'^dashboard/$', views_area_do_participante.dashboard, name='dashboard'),
    url(r'^inscricao/$', views_area_do_participante.inscricao, name='inscricao'),
    url(r'^inscricao/inscricao-realizada/$', views_area_do_participante.inscricao_realizada,
        name='inscricao_realizada'),
    url(r'^inscricao/comprar-kit/$', views_area_do_participante.comprar_kit, name='comprar_kit'),
    url(r'^inscricao/comprar-kit/enviar-comprovante/$', views_area_do_participante.enviar_comprovante,
        name='enviar_comprovante'),
    url(r'^inscricao/comprar-kit/enviar-comprovante/upload-comprovante/$',
        views_area_do_participante.upload_comprovante, name='upload_comprovante'),
    url(r'^inscricao/comprar-kit/enviar-comprovante/upload-comprovante/sucesso/$',
        views_area_do_participante.upload_comprovante_sucesso, name='upload_comprovante_sucesso'),
    url(r'^inscricao/comprar-kit/reservar-kit/$', views_area_do_participante.reservar_kit, name='reservar_kit'),
    url(r'^inscricao/comprar-kit/reservar-kit/reservado/$', views_area_do_participante.kit_reservado,
        name='kit_reservado'),
    url(r'^inscricao/aplicar-cupom/$', views_area_do_participante.aplicar_cupom, name='aplicar_cupom'),
    url(r'^inscricao/comprar-kit/pagamento-paypal/$', views_area_do_participante.realizar_pagamento_paypal,
        name='realizar_pagamento_paypal'),
    url(r'^inscricao/comprar-kit/pagamento-paypal/confirmado/$', views_area_do_participante.pagamento_paypal_confirmado,
        name='pagamento_paypal_confirmado'),
    url(r'^inscricao/comprar-kit/pagamento-paypal/cancelado/$', views_area_do_participante.pagamento_paypal_cancelado,
        name='pagamento_paypal_cancelado'),
    url(r'^inscricao/editar-cadastro/$', views_area_do_participante.editar_cadastro,
        name='editar_cadastro'),
    url(r'^api/atividades/inscricao/$', views_area_do_participante.api_detalhes_inscricao_atividades,
        name='api_vagas_atividades'),
    url(r'^pagina-inscrever-minicurso/$', views_area_do_participante.pagina_inscricao_atividades,
        name='pagina-inscrever-minicurso'),

]
