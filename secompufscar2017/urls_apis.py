# coding: utf-8

from django.conf.urls import url
from secompufscar2017 import views_apis

urlpatterns = [
    url(r'^atividades/$', views_apis.api_todas_atividades, name='api_todas_atividades'),
    url(r'^atividades/(?P<id_atividade>[0-9]+)/$', views_apis.api_atividades, name='api_atividades'),
    url(r'^ministrantes/$', views_apis.api_todos_ministrantes, name='api_todos_ministrantes'),
    url(r'^ministrantes/(?P<id_ministrante>[0-9]+)/$', views_apis.api_ministrantes, name='api_ministrantes'),
    url(r'^patrocinadores/$', views_apis.api_todos_patrocinadores, name='api_todos_patrocinadores'),
    url(r'^camisetas/(?P<id_camiseta>[0-9]+)/$', views_apis.api_camisetas, name='api_camisetas'),
    url(r'^facebook/$', views_apis.api_facebook, name='api_facebook'),
    url(r'^instagram/$', views_apis.api_instagram, name='api_instagram'),
]
