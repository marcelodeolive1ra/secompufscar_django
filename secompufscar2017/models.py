from django.db import models


class CaraterProjeto(models.Model):
    carater = models.CharField(verbose_name="Caráter de projeto", max_length=15, unique=True)

    def __str__(self):
        return str(self.carater)


class Projeto(models.Model):
    # Atributos da tabela projeto
    nome = models.CharField(verbose_name="Nome do projeto", max_length=100)
    areas = models.CharField(verbose_name="Áreas abordadas", max_length=200)
    carater = models.ManyToManyField(CaraterProjeto, verbose_name='Carater do projeto')
    conteudo = models.TextField(verbose_name="Conteúdo abordado")
    descricao = models.TextField(verbose_name="Descrição do projeto")
    link_video = models.CharField(verbose_name="Link para vídeo", max_length=100, null=True, blank=True)
    relacao_computacao = models.TextField(verbose_name="Relação com a computação")
    material_utilizado = models.TextField(verbose_name="Material utilizado", blank=True)
    objetivo = models.TextField(verbose_name="Objetivo do projeto", max_length=2000)
    resultados = models.TextField(verbose_name="Resultados já alcançados")
    # Responsavel pelo projeto: os seguintes campos estão inclusos na tabela projeto
    # e não integrante pois são relativos ao único responsável pelo projeto.
    responsavel = models.ForeignKey('IntegranteProjeto', on_delete=models.CASCADE, null=True)
    facebook_responsavel = models.CharField(verbose_name="Facebook do responsável", max_length=100, null=True,
                                            blank=True)
    email_responsavel = models.EmailField(verbose_name="Email do responsável")
    telefone_responsavel = models.CharField(verbose_name="Telefone do responsavel", max_length=15)

    aprovado = models.NullBooleanField(verbose_name="Status de aprovação", null=True)

    def __str__(self):
        return 'Projeto: ' + str(self.nome)

    class Meta:
        verbose_name = 'projeto'
        verbose_name_plural = 'projetos'


class IntegranteProjeto(models.Model):
    nome = models.CharField(verbose_name="Nome", max_length=100)
    projeto_participante = models.ForeignKey(Projeto, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.nome) + '[projeto: ' + self.projeto_participante.nome + ']'

    class Meta:
        verbose_name = 'integrante'
        verbose_name_plural = 'integrantes'
