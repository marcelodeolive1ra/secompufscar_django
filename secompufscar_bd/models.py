from django.db import models, transaction
from django.contrib.auth.models import User
from django.utils import timezone

import uuid
import datetime

PATH_FOTOS_EQUIPE = 'images/secompufscar' + str(datetime.datetime.now().year) + '/equipe/'
PATH_FOTOS_MINISTRANTES = 'images/secompufscar' + str(datetime.datetime.now().year) + '/ministrantes/'
PATH_LOGOS_PATROCINADORES = 'images/secompufscar' + str(datetime.datetime.now().year) + '/patrocinadores/'


class Curso(models.Model):
    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=100, verbose_name='curso')


class Instituicao(models.Model):
    class Meta:
        verbose_name = 'instituição'
        verbose_name_plural = 'instituições'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=100, verbose_name='instituição')


class Diretoria(models.Model):
    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=100, verbose_name='nome da diretoria')
    ordem = models.IntegerField(default=0, verbose_name='ordem no site')


class Cargo(models.Model):
    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=100, verbose_name='cargo')


class PerguntaFrequente(models.Model):
    class Meta:
        verbose_name = 'pergunta frequente'
        verbose_name_plural = 'perguntas frequentes'

    def __str__(self):
        return self.pergunta

    pergunta = models.TextField()
    resposta = models.TextField()
    prioridade = models.IntegerField(default=0)


class CotaDePatrocinio(models.Model):
    class Meta:
        verbose_name = 'cota de patrocínio'
        verbose_name_plural = 'cotas de patrocínio'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50, verbose_name='cota')
    prioridade = models.IntegerField(default=0)


class Patrocinador(models.Model):
    class Meta:
        verbose_name_plural = 'patrocinadores'
        ordering = ['cota__prioridade', 'nome_empresa']

    def __str__(self):
        return self.nome_empresa + ' [' + self.cota.nome + ']'

    def save(self, *args, **kwargs):
        self.ultima_atualizacao_em = timezone.now()
        return super(Patrocinador, self).save(*args, **kwargs)

    nome_empresa = models.CharField(max_length=100, verbose_name='nome da empresa')
    cota = models.ForeignKey(CotaDePatrocinio,on_delete=models.CASCADE,)
    logo = models.ImageField(null=True, blank=True, upload_to=PATH_LOGOS_PATROCINADORES)
    link_website = models.URLField(null=True, blank=True, verbose_name='link para o site')
    ativo_site = models.BooleanField(default=True, verbose_name='mostrar no site?')
    ordem_site = models.IntegerField(default=0, verbose_name='ordem no site')
    ultima_atualizacao_em = models.DateTimeField(null=True, blank=True, verbose_name='última atualização em')


class Camiseta(models.Model):
    class Meta:
        ordering = ['ordem_site']

    def __str__(self):
        return self.tamanho + ' [' + str(self.quantidade_restante) + '/' + str(self.quantidade) + ']'

    tamanho = models.CharField(max_length=30)
    quantidade = models.IntegerField(default=0)
    quantidade_restante = models.IntegerField(default=0)
    ordem_site = models.IntegerField(default=0, verbose_name='ordem no site')


class TipoContato(models.Model):
    class Meta:
        verbose_name = 'tipo de contato'
        verbose_name_plural = 'tipos de contato'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50, verbose_name='tipo de contato')


class Contato(models.Model):
    def __str__(self):
        return self.tipo_contato.nome + ': ' + self.link

    tipo_contato = models.ForeignKey(TipoContato, verbose_name='tipo de contato',on_delete=models.CASCADE,)
    link = models.CharField(max_length=255)


class Ministrante(models.Model):
    class Meta:
        ordering = ['nome', 'sobrenome']

    def __str__(self):
        return self.nome + ' ' + self.sobrenome

    def save(self, *args, **kwargs):
        self.ultima_atualizacao_em = timezone.now()
        return super(Ministrante, self).save(*args, **kwargs)

    nome = models.CharField(max_length=50)
    sobrenome = models.CharField(max_length=100)
    profissao = models.CharField(max_length=100, verbose_name='profissão')
    empresa = models.CharField(max_length=100)
    descricao = models.TextField(verbose_name='descrição')
    foto = models.ImageField(null=True, blank=True, upload_to=PATH_FOTOS_MINISTRANTES)
    contatos = models.ManyToManyField(Contato, blank=True, verbose_name='contato(s)')
    ultima_atualizacao_em = models.DateTimeField(null=True, blank=True, verbose_name='última atualização em')


class TipoPagamento(models.Model):
    class Meta:
        verbose_name = 'tipo de pagamento'
        verbose_name_plural = 'tipos de pagamento'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50, verbose_name='tipo de pagamento')
    deducoes_fixas = models.FloatField(verbose_name='valor de dedução fixa em R$')
    porcentagem_deducoes_variaveis = models.FloatField(verbose_name='valor de dedução proporcional em %')


class Pacote(models.Model):
    class Meta:
        ordering = ['ordem_site']

    def __str__(self):
        return self.nome + ' [R$ ' + str(self.valor) + '0]'

    nome = models.CharField(max_length=50, verbose_name='pacote')
    descricao = models.CharField(max_length=255, verbose_name='descrição')
    ordem_site = models.IntegerField(default=0, verbose_name='ordem no site')
    valor = models.FloatField()
    pacote_disponivel = models.BooleanField(default=True, verbose_name='pacote disponível?')


class LocalAtividade(models.Model):
    class Meta:
        verbose_name = 'local de atividade'
        verbose_name_plural = 'locais de atividades'

    def __str__(self):
        return self.predio + ((' - ' + self.sala) if (self.sala is not None) else '')

    predio = models.CharField(max_length=100, verbose_name='prédio')
    sala = models.CharField(max_length=100, null=True, blank=True)


class TipoAtividade(models.Model):
    class Meta:
        verbose_name = 'tipo de atividade'
        verbose_name_plural = 'tipos de atividades'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50, verbose_name='tipo de atividade')


class DatasInscricoesTipoAtividade(models.Model):
    class Meta:
        verbose_name = 'datas de inscrições por tipo de atividade'
        verbose_name_plural = 'datas de inscrições por tipo de atividade'

    def __str__(self):
        return self.tipo_atividade.nome

    tipo_atividade = models.ForeignKey(TipoAtividade, verbose_name='tipo de atividade',on_delete=models.CASCADE,)
    data_hora_inicio_inscricoes = models.DateTimeField(
        null=True, blank=True, verbose_name='data/hora de início das inscrições deste tipo de atividade')
    data_hora_fim_inscricoes = models.DateTimeField(
        null=True, blank=True, verbose_name='data/hora de fim das inscrições deste tipo de atividade')


class IntervalosDeHorario(models.Model):
    class Meta:
        verbose_name = 'intervalo de horário'
        verbose_name_plural = 'intervalos de horário'

    def __str__(self):
        return timezone.localtime(self.data_hora_inicio).strftime('%d/%m/%y %H:%M:%S') + ' - ' + \
               timezone.localtime(self.data_hora_fim).strftime('%d/%m/%y %H:%M:%S')

    def __contains__(self, item):
        if item.data_hora_inicio == self.data_hora_inicio and item.data_hora_fim == self.data_hora_fim:
            return True
        else:
            return False

    def __cmp__(self, other):
        if self.data_hora_fim == other.data_hora_fim and self.data_hora_inicio == other.data_hora_inicio:
            return 0
        elif self.data_hora_fim > other.data_hora_fim or (self.data_hora_fim == other.data_hora_fim and
                                                          self.data_hora_inicio > other.data_hora_inicio):
            return 1
        else:
            return -1

    data_hora_inicio = models.DateTimeField(verbose_name='data/hora de início')
    data_hora_fim = models.DateTimeField(verbose_name='data/hora de fim')


class Atividade(models.Model):
    class Meta:
        ordering = ['tipo__nome', 'titulo']

    def __str__(self):
        return '[' + self.tipo.nome + '] ' + self.titulo

    def save(self, *args, **kwargs):
        self.ultima_atualizacao_em = timezone.now()
        return super(Atividade, self).save(*args, **kwargs)

    @classmethod
    def inscrever_em_atividade(cls, id_inscricao, id_atividade):
        with transaction.atomic():

            atividade = (
                cls.objects.select_for_update().get(id=id_atividade)
            )
            inscricao = (
                Inscricao.objects.select_for_update().get(id=id_inscricao)
            )

            for atividade_inscrita in inscricao.atividades_inscritas.all():
                if atividade_inscrita.tipo.nome == 'Minicurso' and atividade.tipo.nome == 'Minicurso':
                    return 2

            if atividade.tipo.nome == 'Minicurso' or atividade.tipo.nome == 'Workshop':
                if atividade.vagas_disponiveis > 0:
                    atividade.vagas_disponiveis -= 1
                    atividade.save()
                    inscricao.atividades_inscritas.add(atividade)
                    inscricao.save()
                    return 0
                else:
                    return 1
            else:
                inscricao.atividades_inscritas.add(atividade)
                inscricao.save()
                return 0

    @classmethod
    def cancelar_inscricao_em_atividade(cls, id_inscricao, id_atividade):
        with transaction.atomic():

            atividade = (
                cls.objects.select_for_update().get(id=id_atividade)
            )
            inscricao = (
                Inscricao.objects.select_for_update().get(id=id_inscricao)
            )

            inscricao.atividades_inscritas.remove(atividade)
            inscricao.save()

            if atividade.tipo.nome == 'Minicurso' or atividade.tipo.nome == 'Workshop':
                atividade.vagas_disponiveis += 1
                atividade.save()

    titulo = models.CharField(max_length=255, verbose_name='atividade')
    tipo = models.ForeignKey(TipoAtividade, verbose_name='tipo de atividade',on_delete=models.CASCADE,)
    descricao = models.TextField(verbose_name='descrição')
    horarios = models.ManyToManyField(IntervalosDeHorario, blank=True, verbose_name='datas e horários')
    local = models.ForeignKey(LocalAtividade, null=True, blank=True, verbose_name='local da atividade',on_delete=models.CASCADE,)
    vagas_totais = models.IntegerField(null=True, blank=True, verbose_name='total de vagas')
    vagas_disponiveis = models.IntegerField(null=True, blank=True, verbose_name='vagas disponíveis')
    pre_requisitos = models.TextField(null=True, blank=True, verbose_name='pré-requisitos')
    pre_requisitos_recomendados = models.TextField(null=True, blank=True, verbose_name='pré-requisitos recomendados')
    ministrante = models.ManyToManyField(Ministrante, blank=True, verbose_name='ministrante(s)')
    empresa = models.ForeignKey(Patrocinador, null=True, blank=True,on_delete=models.CASCADE,)
    ultima_atualizacao_em = models.DateTimeField(null=True, blank=True, verbose_name='última atualização em')
    ativa = models.BooleanField(default=True, verbose_name='atividade ativa?')


class Presenca(models.Model):
    class Meta:
        verbose_name = 'presença'
        verbose_name_plural = 'presenças'

    def __str__(self):
        return self.atividade.titulo + \
               ' [' + timezone.localtime(self.data_hora_registro).strftime('%d/%m/%y %H:%M:%S') + ']'

    atividade = models.ForeignKey(Atividade,on_delete=models.CASCADE,)
    data_hora_registro = models.DateTimeField(verbose_name='data/hora de registro da presença',auto_now_add=True)
    inscrito = models.BooleanField(verbose_name='Está inscrito?', default=False)


class Participante(models.Model):
    class Meta:
        ordering = ['usuario']

    def __str__(self):
        return self.usuario.first_name + ' ' + self.usuario.last_name + ' [' + self.usuario.email + ']'

    usuario = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='usuário', related_name='user')
    instituicao = models.ForeignKey(Instituicao, null=True, blank=True, verbose_name='instituição',on_delete=models.CASCADE,)
    curso = models.ForeignKey(Curso, null=True, blank=True,on_delete=models.CASCADE,)
    ano_ingresso = models.IntegerField(null=True, blank=True, verbose_name='ano de ingresso')
    token_confirmacao_email = models.CharField(max_length=100, verbose_name='código de confirmação de e-mail')


class StatusPagamento(models.Model):
    class Meta:
        verbose_name = 'status de pagamento'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50)


class Pagamento(models.Model):
    def __str__(self):
        return self.tipo_pagamento.nome + ' - R$ ' + str(self.valor) + ' [' + self.registro + ']'

    tipo_pagamento = models.ForeignKey(TipoPagamento, verbose_name='tipo de pagamento',on_delete=models.CASCADE,)
    data_hora_pagamento = models.DateTimeField(verbose_name='data/hora de registro do pagamento')
    valor = models.FloatField()
    registro = models.CharField(max_length=255, verbose_name='código de registro do pagamento')
    autorizador = models.ForeignKey(Participante, null=True, blank=True, verbose_name='pagamento autorizado por',on_delete=models.CASCADE,)
    status_atual = models.ForeignKey(StatusPagamento, null=True, verbose_name='status do pagamento',on_delete=models.CASCADE,)
    data_hora_mudanca_status = models.DateTimeField(verbose_name='data/hora da última mudança de status do pagamento')


class Inscricao(models.Model):
    class Meta:
        verbose_name = 'inscrição'
        verbose_name_plural = 'inscrições'

    def __str__(self):
        return self.participante.usuario.first_name + ' ' + self.participante.usuario.last_name + \
               ' - ' + self.pacote.nome + ' - ' + \
               timezone.localtime(self.data_hora_inscricao).strftime('%d/%m/%y %H:%M:%S')

    codigo = models.CharField(max_length=100, default=str(uuid.uuid4()), unique=True,
                              verbose_name='código de inscrição')
    participante = models.ForeignKey(Participante,on_delete=models.CASCADE,)
    data_hora_inscricao = models.DateTimeField(verbose_name='data/hora de registro da inscrição')
    aceite_compartilhamento_email = models.BooleanField(verbose_name='aceita compartilhamento de e-mail?')
    pacote = models.ForeignKey(Pacote,on_delete=models.CASCADE,)
    camiseta = models.ForeignKey(Camiseta, null=True, blank=True,on_delete=models.CASCADE,)
    pagamentos = models.ManyToManyField(Pagamento, blank=True)
    credenciado = models.BooleanField(default=False, verbose_name='credenciado(a)?')
    atividades_inscritas = models.ManyToManyField(Atividade, blank=True, verbose_name='atividade(s) inscrita(s)')
    presencas = models.ManyToManyField(Presenca, blank=True, verbose_name='presença(s) registrada(s)')
    #eh_vegano = models.BooleanField(default=False,blank=True)


def gerar_cupom():
    return str(uuid.uuid4())[0:12]


class Cupom(models.Model):
    def __str__(self):
        return str(self.codigo) + ' - ' + self.descricao + ' - R$ ' + str(self.valor_desconto) + '0' + \
               (' - Usado' if self.usado else '')

    codigo = models.CharField(max_length=50, unique=True, default=gerar_cupom, verbose_name='código do cupom')
    descricao = models.CharField(max_length=100, verbose_name='descrição')
    valor_desconto = models.FloatField(default=0.0, verbose_name='valor de desconto em R$')
    percentual_desconto = models.FloatField(null=True, blank=True, verbose_name='desconto em %')
    gerado_por = models.ForeignKey(Participante, related_name='cupom_gerado_por',on_delete=models.CASCADE,)
    usado = models.BooleanField(default=False, verbose_name='cupom usado?')


class EquipeDesafioDeProgramadores(models.Model):
    class Meta:
        verbose_name = 'equipe do Desafio de Programadores'
        verbose_name_plural = 'equipes do Desafio de Programadores'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=255, verbose_name='nome da equipe')
    integrantes = models.ManyToManyField(Participante)
    data_hora_inscricao = models.DateTimeField(verbose_name='data/hora da inscrição da equipe')


class DesafioDeProgramadores(models.Model):
    class Meta:
        verbose_name = 'Desafio de Programadores'
        verbose_name_plural = 'Desafios de Programadores'

    def __str__(self):
        return str(self.edicao) + 'º Desafio de Programadores'

    edicao = models.IntegerField(verbose_name='edição do Desafio')
    equipes = models.ManyToManyField(EquipeDesafioDeProgramadores, blank=True)
    data_hora_aquecimento = models.ManyToManyField(IntervalosDeHorario, blank=True,
                                                   related_name='data_hora_aquecimento',
                                                   verbose_name='data/hora do Aquecimento')
    local_aquecimento = models.ForeignKey(LocalAtividade, null=True, blank=True, related_name='local_aquecimento',
                                          verbose_name='local do Aquecimento',on_delete=models.CASCADE,)
    data_hora_desafio = models.ManyToManyField(IntervalosDeHorario, blank=True,
                                               related_name='data_hora_desafio',
                                               verbose_name='data/hora do Desafio')
    local_desafio = models.ForeignKey(LocalAtividade, blank=True, null=True,
                                      related_name='local_desafio', verbose_name='local do Desafio',on_delete=models.CASCADE,)
    inicio_inscricoes_desafio_programadores = \
        models.DateTimeField(blank=True, null=True, verbose_name='data/hora de início das inscrições do Desafio')
    fim_inscricoes_desafio_programadores = \
        models.DateTimeField(blank=True, null=True, verbose_name='data/hora de fim das inscrições do Desafio')
    inicio_inscricoes_equipes_desafio_programadores = \
        models.DateTimeField(blank=True, null=True, verbose_name='data/hora de início das inscrições de equipes')
    fim_inscricoes_equipes_desafio_programadores = \
        models.DateTimeField(blank=True, null=True, verbose_name='data/hora de fim das inscrições de equipes')
    organizadores_prova = models.ManyToManyField(Participante, blank=True, verbose_name='organizadores da prova')
    premiacao = models.TextField(blank=True, null=True, verbose_name='premiação')
    texto_descricao = models.TextField(blank=True, null=True, verbose_name='texto descrição do Desafio')
    texto_informacoes_importantes = models.TextField(blank=True, null=True,
                                                     verbose_name='texto de informações importantes')
    edital = models.TextField(blank=True, null=True)


class MembroDeEquipe(models.Model):
    class Meta:
        verbose_name = 'membro de equipe'
        verbose_name_plural = 'membros de equipe'

    def __str__(self):
        return self.participante.usuario.first_name + ' ' + self.participante.usuario.last_name + \
               ((' - [' + self.cargo.nome + ']') if self.cargo is not None else '')

    participante = models.ForeignKey(Participante,on_delete=models.CASCADE,)
    foto = models.ImageField(null=True, blank=True, upload_to=PATH_FOTOS_EQUIPE)
    email_secomp = models.EmailField(null=True, blank=True, verbose_name='e-mail SECOMP')
    cargo = models.ForeignKey(Cargo, null=True, blank=True,on_delete=models.CASCADE,)
    diretoria = models.ForeignKey(Diretoria,on_delete=models.CASCADE,)


class Permissao(models.Model):
    class Meta:
        verbose_name = 'permissão'
        verbose_name_plural = 'permissões'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=100, verbose_name='descrição da permissão')
    usuarios_permitidos = models.ManyToManyField(Participante, blank=True, verbose_name='usuários permitidos')


class MenuDoSite(models.Model):
    class Meta:
        verbose_name = 'menu do site'
        verbose_name_plural = 'menus do site'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=50)
    ativo = models.BooleanField(default=True)


class TiposAtividadesEvento(models.Model):
    class Meta:
        verbose_name = 'tipos de atividade do evento'
        verbose_name_plural = 'tipos de atividade do evento'

    def __str__(self):
        return self.tipo_atividade.nome

    tipo_atividade = models.ForeignKey(TipoAtividade, verbose_name='tipo de atividade',on_delete=models.CASCADE,)
    cronograma_fechado = models.BooleanField()


class OperacaoInscricaoAtividade(models.Model):
    class Meta:
        verbose_name = 'operação de inscrição em atividade'
        verbose_name_plural = 'operações de inscrição em atividade'

    def __str__(self):
        return self.nome

    nome = models.CharField(max_length=20)


def _get_operacao_cancelar_inscricao():
    try:
        return OperacaoInscricaoAtividade.objects.get(nome='Cancelamento')
    except Exception as e:
        print(e)
        operacao_cancelar = OperacaoInscricaoAtividade(nome='Cancelamento')
        operacao_cancelar.save()
        return operacao_cancelar


def _get_operacao_inscricao():
    try:
        return OperacaoInscricaoAtividade.objects.get(nome='Inscrição')
    except Exception as e:
        print(e)
        operacao_inscricao = OperacaoInscricaoAtividade(nome='Inscrição')
        operacao_inscricao.save()
        return operacao_inscricao


class LogInscricoesAtividades(models.Model):
    class Meta:
        verbose_name = 'log de inscrição em atividades'
        verbose_name_plural = 'logs de inscrição em atividades'

    def __str__(self):
        return self.operacao.nome + ' - ' + self.atividade.titulo + ' - ' + \
               self.inscricao.participante.usuario.first_name + ' ' + self.inscricao.participante.usuario.last_name + \
               ' - ' + timezone.localtime(self.data_hora_registro).strftime('%d/%m/%y %H:%M:%S')

    inscricao = models.ForeignKey(Inscricao, verbose_name='inscrição',on_delete=models.CASCADE,)
    atividade = models.ForeignKey(Atividade,on_delete=models.CASCADE,)
    data_hora_registro = models.DateTimeField(verbose_name='data/hora de registro')
    operacao = models.ForeignKey(OperacaoInscricaoAtividade, verbose_name='operação',on_delete=models.CASCADE,)


class Evento(models.Model):
    def __str__(self):
        return str(self.edicao) + 'ª SECOMP UFSCar'

    edicao = models.IntegerField(verbose_name='edição da SECOMP')
    ano = models.IntegerField(verbose_name='ano da edição')
    data_hora_inicio_evento = models.DateTimeField(null=True, blank=True, verbose_name='data/hora de início do evento')
    data_hora_fim_evento = models.DateTimeField(null=True, blank=True, verbose_name='data/hora de fim do evento')
    membros_equipe = models.ManyToManyField(MembroDeEquipe, blank=True, verbose_name='membros da equipe')
    tipos_atividades = models.ManyToManyField(TiposAtividadesEvento, verbose_name='tipos de atividades', blank=True)
    atividades = models.ManyToManyField(Atividade, blank=True)
    patrocinadores = models.ManyToManyField(Patrocinador, blank=True)
    perguntas_frequentes = models.ManyToManyField(PerguntaFrequente, blank=True)
    permissoes = models.ManyToManyField(Permissao, blank=True, verbose_name='permissões')
    camisetas = models.ManyToManyField(Camiseta, blank=True)
    cupons = models.ManyToManyField(Cupom, blank=True)
    inicio_inscricoes_evento = models.DateTimeField(null=True, blank=True,
                                                    verbose_name='data/hora de início das inscrições do evento')
    fim_inscricoes_evento = models.DateTimeField(null=True, blank=True,
                                                 verbose_name='data/hora de fim das inscrições do evento')
    datas_inscricoes_atividades = models.ManyToManyField(DatasInscricoesTipoAtividade,
                                                         verbose_name='datas de inscrições de atividades', blank=True)
    pacotes = models.ManyToManyField(Pacote)
    desafio_programadores = models.ForeignKey(DesafioDeProgramadores, null=True, blank=True,
                                              verbose_name='Desafio de Programadores',on_delete=models.CASCADE,)
    email_destino_formulario_contato = models.EmailField(verbose_name='e-mail destino do formulário de contato do site')
    nome_servidor = models.CharField(null=True, blank=True,
                                     max_length=100, verbose_name='nome do servidor de hospedagem')
    google_analytics_key = models.CharField(max_length=50, null=True, blank=True,
                                            verbose_name='chave do Google Analytics')
    menus_site_ativos = models.ManyToManyField(MenuDoSite, blank=True, verbose_name='menus ativos no site')
    inscricoes = models.ManyToManyField(Inscricao, blank=True, verbose_name='inscrições do evento')
    logs_inscricoes_atividades = models.ManyToManyField(LogInscricoesAtividades, blank=True,
                                                        verbose_name='logs de inscrições em atividades')

    @classmethod
    def adicionar_log_cancelamento_atividade(cls, inscricao_participante, id_atividade):
        with transaction.atomic():
            evento = (
                cls.objects.select_for_update().get(edicao=8)
            )

            log_cancelamento = LogInscricoesAtividades(inscricao=inscricao_participante,
                                                       atividade=Atividade.objects.get(id=id_atividade),
                                                       data_hora_registro=timezone.now(),
                                                       operacao=_get_operacao_cancelar_inscricao())
            log_cancelamento.save()
            evento.logs_inscricoes_atividades.add(log_cancelamento)
            evento.save()

    @classmethod
    def adicionar_log_inscricao_atividade(cls, inscricao_participante, id_atividade):
        with transaction.atomic():
            evento = (
                cls.objects.select_for_update().get(edicao=8)
            )

            log_inscricao = LogInscricoesAtividades(inscricao=inscricao_participante,
                                                    atividade=Atividade.objects.get(id=id_atividade),
                                                    data_hora_registro=timezone.now(),
                                                    operacao=_get_operacao_inscricao())
            log_inscricao.save()
            evento.logs_inscricoes_atividades.add(log_inscricao)
            evento.save()
