from django.contrib import admin
from .models import *

admin.site.register(Evento)
admin.site.register(MenuDoSite)
admin.site.register(TiposAtividadesEvento)
admin.site.register(DatasInscricoesTipoAtividade)

admin.site.register(Diretoria)
admin.site.register(Curso)
admin.site.register(Cargo)
admin.site.register(MembroDeEquipe)

admin.site.register(CotaDePatrocinio)
admin.site.register(Patrocinador)

admin.site.register(PerguntaFrequente)

admin.site.register(TipoContato)
admin.site.register(Contato)
admin.site.register(Ministrante)

admin.site.register(TipoAtividade)
admin.site.register(LocalAtividade)
admin.site.register(IntervalosDeHorario)
admin.site.register(Atividade)

admin.site.register(DesafioDeProgramadores)
admin.site.register(EquipeDesafioDeProgramadores)

admin.site.register(Pacote)
admin.site.register(Camiseta)
admin.site.register(Cupom)
admin.site.register(TipoPagamento)
admin.site.register(Pagamento)
admin.site.register(StatusPagamento)
admin.site.register(Instituicao)
admin.site.register(Participante)
admin.site.register(Inscricao)
admin.site.register(Presenca)

admin.site.register(Permissao)

admin.site.register(LogInscricoesAtividades)
admin.site.register(OperacaoInscricaoAtividade)
