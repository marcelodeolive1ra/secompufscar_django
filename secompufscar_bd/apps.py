from django.apps import AppConfig


class SecompufscarBdConfig(AppConfig):
    name = 'secompufscar_bd'
