import requests
from django import template
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from secompufscar2018.constants import *
from secompufscar2018.forms import LoginForm, ConteudoMesaRedondaForm, ConteudoMiniCursoForm, ConteudoPalestraForm, ConteudoMaddogForm
from secompufscar2018.forms import ProcessoSeletivoForm
from secompufscar2018.models import ProcessoSeletivo
from secompufscar2018.views import _get_dicionario_contexto
from secompufscar2018.views_apis import _get_basic_infos_from_ministrantes
from secompufscar_django import settings
from secompufscar2017.views_area_do_participante import _enviar_email_generico, criar_resposta_json_erro
from secompufscar2017.utilities import get_email_pronto_para_envio, enviar_email


register = template.Library()


def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()

def login_conteudo_required(function):
    def wrapper(request, *args, **kw):
        user = request.user
        if (user.is_authenticated and has_group(user, GROUP_CONTEUDO)):
            return function(request, *args, **kw)
        else:
            HttpResponse('403 Acesso Proibido')

    return wrapper


dicionario_contexto = _get_dicionario_contexto()


def cadastra_processo_seletivo(request):
    if request.method == 'POST':
        form = ProcessoSeletivoForm(data=request.POST)
        if form.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                form.save()
                form = form.cleaned_data
                destinatario = {
                    'nome':form['nome'],
                    'email':form['email']
                }
                assunto = "Processo Seletivo - IX SECOMP"
                titulo = "Bem vindo ao processo seletivo da melhor semana da computação de todas!"
                conteudo = 'Sua inscrição foi feita com sucesso! Entraremos em contato em breve.' \
                           'Caso possua alguma dúvida entre em contato conosco.'
                _enviar_email_generico(assunto=assunto,conteudo=conteudo,titulo=titulo,destinatario=destinatario)
                response = JsonResponse({'mensagem': 'sucesso'})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({'mensagem': 'reCaptcha inválido!'})
                response.status_code = 501
                return response
        else:
            response = JsonResponse({'mensagem': form.errors})
            response.status_code = 502
            return response
    else:
        return render(request=request, template_name=TEMPLATE_400, context=dicionario_contexto)


def conteudo_formularios(request,tipo):
    if(not(tipo == TIPO_MESA_REDONDA or tipo == TIPO_MINICURSO or tipo == TIPO_PALESTRA)):
        return render(request=request, template_name=TEMPLATE_403, context={})
    else:
        context = {}
        if(tipo==TIPO_MESA_REDONDA):
            context = {'form': ConteudoMesaRedondaForm,
                       'tipo': TIPO_MESA_REDONDA}
        if (tipo == TIPO_MINICURSO):
            context = {'form': ConteudoMiniCursoForm,
                       'tipo': TIPO_MINICURSO}
        if (tipo == TIPO_PALESTRA):
            context = {'form': ConteudoPalestraForm,
                       'tipo': TIPO_PALESTRA}
        return render(request=request, template_name=TEMPLATE_FORM, context=context)


def _get_info_context(tipo):
    return {'form': _get_basic_infos_from_ministrantes(tipo),
            'tipo': tipo}


@login_conteudo_required
def area_conteudo_info(request,tipo):
    if (not (tipo == TIPO_MESA_REDONDA or tipo == TIPO_MINICURSO or tipo == TIPO_PALESTRA)):
        return render(request=request, template_name=TEMPLATE_403, context={})
    context = _get_info_context(tipo)
    return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO_INFO, context=context)


def area_conteudo_logout(request):
    logout(request)


def area_conteudo_login(request):
    if request.user.is_authenticated and has_group(request.user, GROUP_CONTEUDO):
        return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO, context=dicionario_contexto)
    form = LoginForm()
    context = {'form': form}
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            account = authenticate(username=username, password=password)
            if account is not None:
                login(request, account)
                # here is redirecting to dashboard
                return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO, context=dicionario_contexto)
            else:
                return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO_LOGIN, context=context)
        else:
            return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO_LOGIN, context=context)
    else:
        return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO_LOGIN, context=context)


def _get_processo_seletivo_counts():
    objetos = ProcessoSeletivo.objects

    primeira_ti = objetos.filter(primeira_opcao='TI').count()
    segunda_ti = objetos.filter(segunda_opcao='TI').count()

    primeira_socio = objetos.filter(primeira_opcao='SC').count()
    segunda_socio = objetos.filter(segunda_opcao='SC').count()

    primeira_dm = objetos.filter(primeira_opcao='DM').count()
    segunda_dm = objetos.filter(segunda_opcao='DM').count()

    primeira_jf = objetos.filter(primeira_opcao='JF').count()
    segunda_jf = objetos.filter(segunda_opcao='JF').count()

    primeira_pa = objetos.filter(primeira_opcao='PA').count()
    segunda_pa = objetos.filter(segunda_opcao='PA').count()

    primeira_co = objetos.filter(primeira_opcao='CO').count()
    segunda_co = objetos.filter(segunda_opcao='CO').count()

    return {'primeira_ti': primeira_ti,
            'segunda_ti': segunda_ti,
            'primeira_socio': primeira_socio,
            'segunda_socio': segunda_socio,
            'primeira_dm': primeira_dm,
            'segunda_dm': segunda_dm,
            'primeira_jf': primeira_jf,
            'segunda_jf': segunda_jf,
            'primeira_pa': primeira_pa,
            'segunda_pa': segunda_pa,
            'primeira_co': primeira_co,
            'segunda_co': segunda_co}


def _get_processo_seletivo():
    objetos = ProcessoSeletivo.objects.all()
    return [{'nome': pessoa.nome, 'email': pessoa.email, 'primeira_opcao': pessoa.primeira_opcao,
             'segunda_opcao': pessoa.segunda_opcao, 'tel': pessoa.tel, 'curso': pessoa.curso}
            for pessoa in objetos]


@login_conteudo_required
def area_conteudo_processo_seletivo(request):
    context = {'form': _get_processo_seletivo,
               'numeros': _get_processo_seletivo_counts}
    return render(request=request, template_name=TEMPLATE_AREA_CONTEUDO_PROCESSO_SELETIVO, context=context)



def enviar_email_processo_seletivo_informativo():
    destinatario = ''
    objects = ProcessoSeletivo.objects.all()
    assunto = "Processo Seletivo - IX SECOMP"
    titulo = "Resultado - Processo Seletivo"
    conteudo = ''
    #for pessoa in objects:
    destinatario = {
        'nome': '',
        'email': ''
    }
    _enviar_email_generico(assunto=assunto, conteudo=conteudo, titulo=titulo, destinatario=destinatario)



def cadastra_ministrante(request,tipo):
    if (not (tipo == TIPO_MESA_REDONDA or tipo == TIPO_MINICURSO or tipo == TIPO_PALESTRA)):
        return render(request=request, template_name=TEMPLATE_400, context=dicionario_contexto)
    if request.method == 'POST':
        form = ""
        if(tipo==TIPO_MESA_REDONDA):
            form = ConteudoMesaRedondaForm(data=request.POST, files=request.FILES)
        if(tipo==TIPO_MINICURSO):
            form = ConteudoMiniCursoForm(data=request.POST, files=request.FILES)
        if(tipo==TIPO_PALESTRA):
            form = ConteudoPalestraForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                form.save()
                form = form.cleaned_data
                destinatario = {
                    'nome': form['nome'],
                    'email': form['email']
                }
                assunto = "Semana da Computação da UFSCar - IX SECOMP"
                titulo = "Bem vindo a melhor semana da computação de todas!"
                conteudo = 'Sua inscrição foi feita com sucesso! Entraremos em contato em breve.' \
                           'Caso possua alguma dúvida entre em contato conosco.'
                _enviar_email_generico(assunto=assunto, conteudo=conteudo, titulo=titulo, destinatario=destinatario)
                response = JsonResponse({'mensagem': 'sucesso'})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({'mensagem': 'reCaptcha inválido!'})
                response.status_code = 501
                return response
        else:
            response = JsonResponse({'mensagem': form.errors})
            response.status_code = 502
            return response
    else:
        return render(request=request, template_name=TEMPLATE_400, context=dicionario_contexto)


def _enviar_email_generico_maddog(assunto, titulo, conteudo, destinatario):
    contexto = {
        'nome': destinatario['nome'],
        'conteudo': conteudo,
        'titulo': titulo,
        'assunto': assunto,
    }
    destinatario = destinatario['email']
    corpo_email = get_email_pronto_para_envio(assunto=assunto, remetente=EMAIL_CONTATO_SECOMP,
                                              destinatario=destinatario,
                                              template=TEMPLATE_EMAIL_GENERICO_MADDOG,
                                              contexto=contexto)

    try:
        status = enviar_email(corpo_email=corpo_email)
        print(str(status) + ": No try")

    except Exception as e:
        print(e)
        status = False
        print(str(status) + ": No except")

    return status


def maddog(request):
    dicionario_contexto['form'] = ConteudoMaddogForm
    if request.method == 'POST':
        # TODO Criar este form
        formulario_contato = ConteudoMaddogForm(request.POST, request.FILES)
        if formulario_contato.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                formulario_contato.save()
                formulario_contato = formulario_contato.cleaned_data
                destinatario = {
                    'nome': formulario_contato['nome'],
                    'email': formulario_contato['email']
                }
                assunto = "Semana da Computação da UFSCar - IX SECOMP"
                titulo = "Welcome to the best computer science week ever!"
                conteudo = 'Your registration was successful! We will contact you soon.' \
                           'If you have any questions, please contact us.'
                _enviar_email_generico_maddog(assunto=assunto, conteudo=conteudo, titulo=titulo, destinatario=destinatario)
                response = JsonResponse({'mensagem': 'sucesso'})
                response.status_code = 200
                return response
            else:
                return criar_resposta_json_erro(mensagem='ReCaptcha inválido.', status_code=503)
        else:
            for field in formulario_contato:
                print(field.errors)
            return criar_resposta_json_erro(mensagem='Parâmetros inválidos.', status_code=400)
    else:
        return render(request=request, template_name=TEMPLATE_MADDDOG, context=dicionario_contexto)
