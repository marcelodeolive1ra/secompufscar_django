import locale

from django.conf.urls import include
from django.urls import path

from secompufscar2018 import views

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
app_name = 'secompufscar2018'

urlpatterns = [
    path('', views.index, name='index'),
    path('faq', views.faq, name='faq'),
    path('caravanas', views.caravanas, name='caravanas'),
    path('patrocinio', views.patrocinio, name='patrocinio'),
    path('equipe', views.equipe, name='equipe'),
    path('a-secomp/', views.a_secomp, name='asecomp'),
    path('a-secomp/apresentacao/', views.apresentacao, name='apresentacao'),
    path('a-secomp/sociocultural/', views.sociocultural, name='sociocultural'),
    path('recaptcha/', views.erro_recaptcha, name='recaptcha'),
    path('area-conteudo/',include('secompufscar2018.urls_area_conteudo')),
    path('api/',include('secompufscar2018.urls_apis')),
    path('processo-seletivo',views.processo_seletivo,name='processo-seletivo'),
    path('contato/enviar-email/', views.enviar_email_contato, name='enviar_email_contato'),
    path('caravanas/enviar-email/', views.enviar_email_caravana, name='enviar_email_caravana'),
    path('programacao/', views.programacao, name='programacao'),
    path('programacao/capture-the-flag/', views.capture_the_flag, name='ctf'),
    path('programacao/ministrantes/', views.ministrantes, name='ministrantes'),
    path('programacao/palestras/', views.palestras, name='palestras'),
    path('programacao/palestras-empresariais/', views.palestras_empresariais, name='palestras_empresariais'),
    path('programacao/minicursos/', views.minicursos, name='minicursos'),
    path('programacao/workshops/', views.workshops, name='workshops'),
    path('programacao/mesas-redondas/', views.mesas_redondas, name='mesasredondas'),
    path('programacao/cronograma/', views.cronograma, name='cronograma'),
    path('404/', views.erro_404, name='404'),
    path('500/', views.erro_500, name='400')
]
