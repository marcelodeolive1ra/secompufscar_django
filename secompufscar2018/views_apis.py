from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from secompufscar2018.constants import TIPO_MINICURSO, TIPO_MESA_REDONDA, TIPO_PALESTRA, TIPO_MADDOG, GROUP_CONTEUDO
from django import template
from operator import attrgetter

from secompufscar2018.utilities import criar_resposta_json_erro, get_evento_atual
from secompufscar_bd.models import Atividade, Inscricao, Presenca, Participante, IntervalosDeHorario
from secompufscar2018.models import ConteudoPalestra, ConteudoMesaRedonda, ConteudoMiniCurso, MadDog
import datetime
from django.utils import timezone
register = template.Library()

def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()


def login_conteudo_required(function):
    def wrapper(request, *args, **kw):
        user=request.user
        if not (user.is_authenticated and has_group(user, GROUP_CONTEUDO)):
            HttpResponseRedirect(reverse('secompufscar2018:area_conteudo_login'))
        else:
            return function(request, *args, **kw)
    return wrapper


def api_todas_atividades(request):
    atividades = Atividade.objects.all()
    atividades = atividades.objects.filter(date__year='2018',ativa=True)
    at = []
    for a in atividades:
        at.append({
            'titulo': a.titulo,
            'tipo':a.tipo,
            'descricao': a.descricao,
            'horarios': a.horarios,
            'local': a.local,
            'vagas_totais':a.vagas_totais,
            'vagas_disponiveis':a.vagas_disponiveis,
            'pre_requisitos':a.pre_requisitos,
            'pre_requisitos_recomendados':a.pre_requisitos_recomendados,
            'ministrantes':a.ministrante,
            'empresa':a.empresa,
        })

    return JsonResponse({
        'atividades': at,
    })


def api_single_atividades(request,atividade):
    atividades = Atividade.objects.get(id=atividade)
    atividades = atividades.objects.filter(date__year='2018')
    at = []
    for a in atividades:
        at.append({
            'titulo': a.titulo,
            'tipo':a.tipo,
            'descricao': a.descricao,
            'horarios': a.horarios,
            'local': a.local,
            'vagas_totais':a.vagas_totais,
            'vagas_disponiveis':a.vagas_disponiveis,
            'pre_requisitos':a.pre_requisitos,
            'pre_requisitos_recomendados':a.pre_requisitos_recomendados,
            'ministrantes':a.ministrante,
            'empresa':a.empresa,
        })

    return JsonResponse({
        'atividades': at,
    })


def api_todos_ministrantes(request):
    minicurso = ConteudoMiniCurso.objects.all()
    minicurso = minicurso.filter(valido=True)
    minicurso = minicurso.values_list('empresa', 'id', 'nome', 'profissao', 'bio', 'github', 'twitter', 'linkedin', 'face', 'foto')

    palestrantes = ConteudoPalestra.objects.all()
    palestrantes = palestrantes.filter(valido=True)
    palestrantes = palestrantes.values_list('empresa', 'id', 'nome', 'profissao', 'bio', 'github', 'twitter', 'linkedin', 'face', 'foto')

    mesaredonda = ConteudoMesaRedonda.objects.all()
    mesaredonda = mesaredonda.filter(valido=True)
    mesaredonda = mesaredonda.values_list('empresa', 'id', 'nome', 'profissao', 'bio', 'github', 'twitter', 'linkedin', 'face', 'foto')

    ministrantes = []
    for m in minicurso:
        ministrantes.append(
            {'nome': m.nome,
             'profissao': m.profissao,
             'bio': m.bio,
             'github': m.github,
             'twitter': m.twitter,
             'linkedin': m.linkedin,
             'face': m.face,
             'id': str(m.id),
             'tipo':TIPO_MINICURSO,
             'empresa': m.empresa,
             "foto":str(m.foto)}
        )

    for m in mesaredonda:
        ministrantes.append(
            {'nome': m.nome,
             'profissao': m.profissao,
             'bio': m.bio,
             'github': m.github,
             'twitter': m.twitter,
             'linkedin': m.linkedin,
             'face': m.face,
             'id': str(m.id),
             'tipo': TIPO_MESA_REDONDA,
             'empresa': m.empresa,
             "foto": str(m.foto)}
        )

    for m in palestrantes:
        ministrantes.append(
            {'nome':m.nome,
             'profissao':m.profissao,
             'bio':m.bio,
             'github':m.github,
             'twitter':m.twitter,
             'linkedin':m.linkedin,
             'face':m.face,
             'id':str(m.id),
             'tipo': TIPO_PALESTRA,
             'empresa':m.empresa,
             "foto": str(m.foto)}
        )
    try:
        return JsonResponse({
            'ministrantes': ministrantes,
        })
    except:
        criar_resposta_json_erro("inválido", 404)



def api_single_ministrantes(request,ministrante,tipo):

    mt = 0
    try:
        if tipo == TIPO_MINICURSO:
            mt = ConteudoMiniCurso.objects.get(id=ministrante)
        elif tipo == TIPO_PALESTRA:
            mt = ConteudoPalestra.objects.get(id=ministrante)
        elif tipo == TIPO_MESA_REDONDA:
            mt = ConteudoMesaRedonda.objects.get(id=ministrante)
        elif tipo == TIPO_MADDOG:
            mt = MadDog.objects.get(id=ministrante)
        elif mt.valido == False:
            return criar_resposta_json_erro("Ministrante não validado, contate o administrador para validá-lo",403)

        return JsonResponse({
            'ministrante': {'nome': mt.nome,
                            'profissao': mt.profissao,
                            'bio': mt.bio,
                            'github': mt.github,
                            'twitter': mt.twitter,
                            'linkedin': mt.linkedin,
                            'face': mt.face,
                            'id': str(mt.id),
                            'empresa': mt.empresa,
                            'foto': str(mt.foto)}
        })
    except:
        return criar_resposta_json_erro("Ministrante não encontrado", 404)


def _get_basic_infos_from_ministrantes(tipo):
    objetos = ''
    if tipo == TIPO_MINICURSO:
        objetos = ConteudoMiniCurso.objects.all()
    if tipo == TIPO_PALESTRA:
        objetos = ConteudoPalestra.objects.all()
    if tipo == TIPO_MESA_REDONDA:
        objetos = ConteudoMesaRedonda.objects.all()
    return [{'nome': mesa.nome, 'profissao': mesa.profissao, 'empresa': mesa.empresa,
             'foto': mesa.foto, 'tel': mesa.tel}
            for mesa in objetos]


def _get_ministrante_mini_cursos(ministrante):
    return {'nome': str(ministrante.nome),
            'tel': str(ministrante.tel),
            'profissao': str(ministrante.profissao),
            'empresa': str(ministrante.empresa),
            'bio': str(ministrante.bio),
            'foto': str(ministrante.foto),
            'github': str(ministrante.github),
            'face': str(ministrante.face),
            'twitter': str(ministrante.twitter),
            'linkedin': str(ministrante.linkedin),
            'tamanho_camiseta': str(ministrante.tamanho_camiseta),
            'cidade': str(ministrante.cidade),
            'locomocao': bool(ministrante.locomocao),
            'locomocao_tipo': str(ministrante.locomocao_tipo),
            'data_chegada': str(ministrante.data_chegada),
            'data_saida': str(ministrante.data_saida),
            'acomodacao': bool(ministrante.acomodacao),
            'vaga_acomodacao': bool(ministrante.vaga_acomodacao),
            'obs': str(ministrante.obs),
            'criado_em': str(ministrante.criado_em),
            'curso_titulo': str(ministrante.curso_titulo),
            'curso_descricao': str(ministrante.curso_descricao),
            'tipo_dinamica': str(ministrante.tipo_dinamica),
            'prereqs': str(ministrante.prereqs),
            'lab_hw': str(ministrante.lab_hw),
            'lab_sw': str(ministrante.lab_sw),
            'obs_lab': str(ministrante.obs_lab)}



def _get_ministrante_palestra(ministrante):
    return {'nome': str(ministrante.nome),
            'tel': str(ministrante.tel),
            'profissao': str(ministrante.profissao),
            'empresa': str(ministrante.empresa),
            'bio': str(ministrante.bio),
            'foto': str(ministrante.foto),
            'github': str(ministrante.github),
            'face': str(ministrante.face),
            'twitter': str(ministrante.twitter),
            'linkedin': str(ministrante.linkedin),
            'tamanho_camiseta': str(ministrante.tamanho_camiseta),
            'cidade': str(ministrante.cidade),
            'locomocao': bool(ministrante.locomocao),
            'locomocao_tipo': str(ministrante.locomocao_tipo),
            'data_chegada': str(ministrante.data_chegada),
            'data_saida': str(ministrante.data_saida),
            'acomodacao': bool(ministrante.acomodacao),
            'obs':str(ministrante.obs),
            'vaga_acomodacao': bool(ministrante.vaga_acomodacao),
            'palestra_titulo': str(ministrante.palestra_titulo),
            'palestra_descricao': str(ministrante.palestra_descricao),
            'recursos': str(ministrante.recursos),
            'obs_palestra': str(ministrante.obs_palestra)}



def _get_ministrante_mesa_redonda(ministrante):
    return {'nome': str(ministrante.nome),
            'tel': str(ministrante.tel),
            'profissao': str(ministrante.profissao),
            'empresa': str(ministrante.empresa),
            'bio': str(ministrante.bio),
            'foto': str(ministrante.foto),
            'github': str(ministrante.github),
            'face': str(ministrante.face),
            'twitter': str(ministrante.twitter),
            'linkedin': str(ministrante.linkedin),
            'tamanho_camiseta': str(ministrante.tamanho_camiseta),
            'cidade': str(ministrante.cidade),
            'locomocao': str(ministrante.locomocao),
            'locomocao_tipo': str(ministrante.locomocao_tipo),
            'data_chegada': str(ministrante.data_chegada),
            'data_saida': str(ministrante.data_saida),
            'acomodacao': str(ministrante.acomodacao),
            'vaga_acomodacao': bool(ministrante.vaga_acomodacao),
            'obs': str(ministrante.obs),
            'criado_em': str(ministrante.criado_em)}


@login_conteudo_required
def api_area_conteudo_ministrantes(request):
    if request.GET:
        json = JsonResponse({'erro': 'Ministrante não encontrado.'})
        tipo = request.GET.get('tipo',1)
        nomezera = request.GET.get('tel_ministrante', 'nada')
        try:
            tipo = int(tipo)
            if tipo == TIPO_MINICURSO:
                ministrante = ConteudoMiniCurso.objects.get(tel=nomezera)
                json = JsonResponse(_get_ministrante_mini_cursos(ministrante))
            elif tipo == TIPO_MESA_REDONDA:
                ministrante = ConteudoMesaRedonda.objects.get(tel=nomezera)
                json = JsonResponse(_get_ministrante_mesa_redonda(ministrante))
            elif tipo == TIPO_PALESTRA:
                ministrante = ConteudoPalestra.objects.get(tel=nomezera)
                json = JsonResponse(_get_ministrante_palestra(ministrante))
            response = json
            response.status_code = 200
            return response
        except ObjectDoesNotExist:
            response = json
            response.status_code = 404
            return response
    else:
        return HttpResponseRedirect(reverse('secompufscar2018:400'))


def nearest_date(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))


api_atividades = get_evento_atual().atividades.all()

def atividades(request):
    response = []
    for a in api_atividades:
        try:
            response.append(
                {
                    'id':a.id,
                    'titulo':a.titulo,
                    'tipo':a.tipo.nome,
                    'data_hora_fim':a.horarios.all()[0].data_hora_fim.replace(tzinfo=None),
                    'data_hora_inicio':a.horarios.all()[0].data_hora_inicio.replace(tzinfo=None)

                }
            )
        except:
            response.append(
                {
                    'id': a.id,
                    'titulo': a.titulo,
                    'tipo': a.tipo.nome,
                    'data_hora_fim': "Nada cadastrado",
                    'data_hora_inicio': "Nada cadastrado"

                }
            )
    return JsonResponse(response,safe=False)


def registra_presenca(request,codigo,atividade_id):
    try:
        pessoa = Inscricao.objects.get(codigo=codigo)
    except ObjectDoesNotExist:
        return criar_resposta_json_erro(mensagem='Inscrição não encontrada', status_code=404)

    if atividade_id == 107:
        pessoa.credenciado = True
        pessoa.save()
    
    try:
        atividade = Atividade.objects.get(id=atividade_id)
    except ObjectDoesNotExist:
        return criar_resposta_json_erro(mensagem='Atividade não encontrada', status_code=404)

    if atividade in list(pessoa.atividades_inscritas.all()):
        inscrito = True
    else:
        inscrito = False

    presencas_pessoa = pessoa.presencas.all()
    if presencas_pessoa:
        for presenca in  presencas_pessoa:
            if presenca.atividade == atividade:
                resposta = JsonResponse({
                    'status': 'success',
                    'mensagem': 'Tudo certo rogerinho, mas está presença já foi registrada antes!'
                })
                resposta.status_code = 202
                return resposta

    try:
        presenca = Presenca()
        presenca.atividade = atividade
        presenca.inscrito = inscrito
        presenca.save()
    except Exception as e:
        return criar_resposta_json_erro(mensagem=str(e), status_code=500)

    pessoa.presencas.add(presenca)
    pessoa.save()
    response = {
            'status': 'success',
            'mensagem': 'Tudo certo rogerinho'
    }
    resposta = JsonResponse(response)
    resposta.status_code = 200
    return resposta


def aux_nude(label, data):
    return "<b>"+label+": </b>"+str(data)+"<br>"


def manda_nude_from_inscricao(request,codigo):
    try:
        inscricao = Inscricao.objects.get(codigo=codigo)
    except ObjectDoesNotExist:
        return criar_resposta_json_erro(mensagem='Inscrição não encontrada', status_code=404)

    try:
        instituicao = inscricao.participante.instituicao.nome
    except Exception:
        instituicao = None

    try:
        curso = inscricao.participante.curso.nome
    except Exception:
        curso = None

    try:
        ano_ingresso = inscricao.participante.ano_ingresso
    except Exception:
        ano_ingresso = None

    nude = {
        'nome_completo': inscricao.participante.usuario.get_full_name(),
        'email': inscricao.participante.usuario.email,
        'instituicao': instituicao,
        'curso': curso,
        'ano_ingresso': ano_ingresso,
        'codigo_inscricao': inscricao.codigo,
        'data_hora_inscricao': str(inscricao.data_hora_inscricao),
        'aceite_compartilhamento_email': inscricao.aceite_compartilhamento_email,
        'pacote': str(inscricao.pacote),
        'camiseta': str(inscricao.camiseta),
        'pagamentos': [str(pagamento) for pagamento in inscricao.pagamentos.all()],
        'credenciado': inscricao.credenciado,
        'atividades_inscritas': [str(atividades) for atividades in inscricao.atividades_inscritas.all()],
        'presencas': [str(presenca) for presenca in inscricao.presencas.all()],
    }

    retorno = {
        'mensagem': nude['nome_completo'] + "#" + aux_nude('Email',nude['email']) + \
               aux_nude('Instituição', nude['instituicao']) + aux_nude('Curso',nude['curso']) + \
               aux_nude('Pacote', nude['pacote']) + aux_nude('Camiseta',nude['camiseta']) + \
               aux_nude('Credenciado', nude['credenciado']) + aux_nude('Pagamentos',nude['pagamentos']) + \
               aux_nude('Atividades Inscritas', nude['atividades_inscritas']) + aux_nude('Presenças', nude['presencas'])
    }
    retorno = JsonResponse(retorno)
    retorno.status_code = 200
    return retorno


def funcao_nojenta(a,b):
    if a >= b.data_hora_inicio and a <= b.data_hora_fim:
        return True
    else:
        return False


intervalos = IntervalosDeHorario.objects.all()
intervalos = list(intervalos)
intervalos.sort(key=attrgetter('data_hora_fim'))
comprimento_intervalo = len(intervalos)
todas_atividades = Atividade.objects.all()


def api_dificil_do_app(request):
    agora = None

    for intervalo in intervalos:
        if funcao_nojenta(timezone.now(),intervalo):
            agora = intervalo

    if agora:
        index = intervalos.index(agora)
        if not (index+1 == comprimento_intervalo):
            proximo = intervalos[index + 1]
        else:
            proximo = None

        if index - 1 < 0:
            anterior = None
        else:
            anterior = intervalos[index-1]

        atividades_agora = []
        for atividade in todas_atividades:
            if agora in atividade.horarios.all():
                atividades_agora.append(str(atividade))

        atividades_anterior = []
        if anterior:
            for atividade in todas_atividades:
                if anterior in atividade.horarios.all():
                    atividades_anterior.append(str(atividade))
        else:
            atividades_anterior = None

        atividades_proximo = []
        if proximo:
            for atividade in todas_atividades:
                if proximo in atividade.horarios.all():
                    atividades_proximo.append(str(atividade))
        else:
            atividades_proximo = None
    else:
        flag_maior = True
        anterior = None
        proximo = None
        count = 0
        while flag_maior and count < comprimento_intervalo:
            if timezone.now() < intervalos[count].data_hora_fim:
                if count == 0:
                    anterior = None
                else:
                    anterior = intervalos[count-1]
                proximo = intervalos[count]
                flag_maior = False
            count = count + 1
        atividades_agora = None

        atividades_anterior = []
        if anterior is None:
            atividades_anterior = None
        else:
            for atividade in todas_atividades:
                if anterior in atividade.horarios.all():
                    atividades_anterior.append(str(atividade))

        atividades_proximo = []
        if proximo is None:
            atividades_proximo = None
        else:
            for atividade in todas_atividades:
                if proximo in atividade.horarios.all():
                    atividades_proximo.append(str(atividade))

    if anterior is not None:
        anterior = timezone.localtime(anterior.data_hora_inicio).strftime('%H:%M') + ' - ' + \
                   timezone.localtime(anterior.data_hora_fim).strftime('%H:%M')

    if agora is not None:
        agora = timezone.localtime(agora.data_hora_inicio).strftime('%H:%M') + ' - ' + \
                timezone.localtime(agora.data_hora_fim).strftime('%H:%M')

    if proximo is not None:
        proximo = timezone.localtime(proximo.data_hora_inicio).strftime('%H:%M') + ' - ' + \
                  timezone.localtime(proximo.data_hora_fim).strftime('%H:%M')

    retorno = {
        'anterior': {
            'atividades': atividades_anterior,
            'horario': anterior

        },
        'agora': {
            'atividades': atividades_agora,
            'horario': agora
        },
        'proximo': {
            'atividades': atividades_proximo,
            'horario': proximo
        }
    }

    resposta = JsonResponse(retorno)
    resposta.status_code = 200
    return resposta