allowSubmit = false;
captcha_warning = $('#captcha_warning');
captcha_warning.hide();
sucesso = $('#sucesso');
sucesso.hide();
erro = $('#erro');
erro.hide();
botao_enviar = $("#botao_enviar");
form = $('.ui.form');
campo_tel = $('#id_tel');
formulario_post = $('#post-form');
checkbox_locomocao = $( "#id_locomocao" );
checkbox_vaga_na_garagem = $( "#id_acomodacao" );
field_vaga = $("#vaga_garagem");
field_locomocao = $("#locomocao_tipo_modo");



//Remove o required default do HTML para adicionar nossos próprios via js
$('input').removeAttr('required');
$('textarea').removeAttr('required');
$('select').removeAttr('required');

$(function() {
    formulario_post.submit(upload);
});
function show_errors(data)
{
    msg = '';
    dict = JSON.parse(data);
    dict = dict['mensagem'];
    if(dict['nome'] !== undefined)
        msg += '<br>Nome:'+dict['nome'];
    if(dict['email'])
        msg += '<br>Nome:'+dict['email'];
    if(dict['tel'] !== undefined)
        msg += '<br>Telefone:'+dict['tel'];
    if(dict['profissao'] !== undefined)
        msg += '<br>Profissão:'+dict['profissao'];
    if(dict['empresa'] !== undefined)
        msg += '<br>Empresa:'+dict['empresa'];
    if(dict['bio'] !== undefined)
        msg += '<br>Biografia:'+dict['bio'];
    if(dict['foto'] !== undefined)
        msg += '<br>Foto:'+dict['foto'];
    if(dict['github'] !== undefined)
        msg += '<br>Github:'+dict['github'];
    if(dict['face'] !== undefined)
        msg += '<br>Facebook:'+dict['face'];
    if(dict['twitter'] !== undefined)
        msg += '<br>Twitter:'+dict['twitter'];
    if(dict['linkedin'] !== undefined)
        msg += '<br>LinkedIn:'+dict['linkedin'];
    if(dict['tamanho_camiseta'] !== undefined)
        msg += '<br>Tamanho Da Camiseta:'+dict['tamanho_camiseta'];
    if(dict['cidade'] !== undefined)
        msg += '<br>Cidade:'+dict['cidade'];
    if(dict['locomocao'] !== undefined)
        msg += '<br>Locomoção:'+dict['locomocao'];
    if(dict['locomocao_tipo'] !== undefined)
        msg += '<br>Tipo Da Locomoção:'+dict['locomocao_tipo'];
    if(dict['data_chegada'] !== undefined)
        msg += '<br>Data chegada:'+dict['data_chegada'];
    if(dict['data_saida'] !== undefined)
        msg += '<br>Data saída:'+dict['data_saida'];
    if(dict['acomodacao'] !== undefined)
        msg += '<br>Acomodação:'+dict['acomodacao'];
    if(dict['vaga_acomodacao'] !== undefined)
        msg += '<br>Vaga na garagem:'+dict['vaga_acomodacao'];
    if(dict['obs'] !== undefined)
        msg += '<br>Observação:'+dict['obs'];
    if(dict['palestra_titulo'] !== undefined)
        msg += '<br>Título Da Palestra:'+dict['palestra_titulo'];
    if(dict['palestra_descricao'] !== undefined)
        msg += '<br>Descrição Da Palestra:'+dict['palestra_descricao'];
    if(dict['recursos'] !== undefined)
        msg += '<br>Recursos:'+dict['recursos'];
    if(dict['obs_palestra'] !== undefined)
        msg += '<br>Observações da Palestra:'+dict['obs_palestra'];
    if(dict['curso_titulo'] !== undefined)
        msg += '<br>Títúlo do Minicurso:'+dict['curso_titulo'];
    if(dict['curso_descricao'] !== undefined)
        msg += '<br>Descrição do Minicurso:'+dict['curso_descricao'];
    if(dict['tipo_dinamica'] !== undefined)
        msg += '<br>Tipo da Dinâmica:'+dict['tipo_dinamica'];
    if(dict['prereqs'] !== undefined)
        msg += '<br>Pré-req:'+dict['prereqs'];
    if(dict['lab_hw'] !== undefined)
        msg += '<br>Req. Hardware:'+dict['lab_hw'];
    if(dict['lab_sw'] !== undefined)
        msg += '<br>Req. Software:'+dict['lab_sw'];
    if(dict['obs_lab'] !== undefined)
        msg += '<br>Obs. Lab:'+dict['obs_lab'];
    $("#msgs").html(msg)
}

function upload(event) {
    if(allowSubmit) {
        campo_tel.unmask();
        event.preventDefault();
        var data = new FormData(formulario_post.get(0));
        botao_enviar.addClass('loading');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                grecaptcha.reset();
                form.hide();
                erro.hide();
                sucesso.show(100);
                botao_enviar.removeClass('loading');
            },
            error: function (data) {
                grecaptcha.reset();
                erro.show(100);
                sucesso.hide();
                botao_enviar.removeClass('loading');
                show_errors(data.responseText);
            }
        });
    }
    else {
        captcha_warning.show();
    }
    return false;
}

$(document).on('keydown', '#id_tel', function (e) {
    var digit = e.key.replace(/\D/g, '');
    var value = $(this).val().replace(/\D/g, '');
    var size = value.concat(digit).length;
    $(this).mask('(00)00000-0000');
});


field_vaga.toggle(checkbox_vaga_na_garagem.checked);
field_locomocao.toggle(checkbox_locomocao.checked);
checkbox_locomocao.click(function() {
    field_locomocao.toggle(this.checked);
});

checkbox_vaga_na_garagem.click(function() {
    field_vaga.toggle(this.checked);
});

//Campos required criado por nós
form.form({
    fields: {
        nome: {
            identifier: 'nome',
            rules: [{
                type: 'empty',
                prompt: 'Informe seu nome'
            }
            ]
        },
        tel: {
            identifier: 'tel',
            rules: [{
                type: 'empty',
                prompt: 'Informe seu telefone'
            }]
        },
        email: {
            identifier: 'email',
            rules: [{
                type: 'empty',
                prompt: 'Informe seu email'
            }]
        },
        profissao:{
            identifier: 'profissao',
            rules: [{
                type: 'empty',
                prompt: 'Informe sua profissão'
            }]
        },
        empresa:{
            identifier: 'empresa',
            rules: [{
                type: 'empty',
                prompt: 'Informe a empresa/universidade'
            }]
        },
        bio:{
            identifier: 'bio',
            rules: [{
                type: 'empty',
                prompt: 'Informe a biografia'
            }]
        },
        tamanho_camiseta:{
            identifier: 'tamanho_camiseta',
            rules: [{
                type: 'empty',
                prompt: 'Informe o tamanho da camiseta'
            }]
        },
        curso_titulo:{
            identifier: 'curso_titulo',
            rules: [{
                type: 'empty',
                prompt: 'Informe o título do minicurso'
            }]
        },
        curso_descricao:{
            identifier: 'curso_descricao',
            rules: [{
                type: 'empty',
                prompt: 'Informe a descrição do minicurso'
            }]
        },
        tipo_dinamica:{
            identifier: 'tipo_dinamica',
            rules: [{
                type: 'empty',
                prompt: 'Informe o tipo de dinâmica'
            }]
        },
        lab_hw:{
            identifier: 'lab_hw',
            rules: [{
                type: 'empty',
                prompt: 'Informe os requisitos de hardware'
            }]
        },
        lab_sw:{
            identifier: 'lab_sw',
            rules: [{
                type: 'empty',
                prompt: 'Informe os requisitos de software'
            }]
        },
        cidade:{
            identifier: 'cidade',
            rules: [{
                type: 'empty',
                prompt: 'Informe a cidade'
            }]
        },
        locomocao_tipo:{
            identifier: 'locomocao_tipo',
            depends: 'locomocao',
            rules: [{
                type: 'empty',
                prompt: 'Informe o tipo de locomoção'
            }]
        },
        data_chegada:{
            identifier: 'data_chegada',
            rules: [{
                type: 'empty',
                prompt: 'Informe a data de chegada'
            }]
        },
        data_saida:{
            identifier: 'data_saida',
            rules: [{
                type: 'empty',
                prompt: 'Informe a data de saída'
            }]
        },
        palestra_titulo:{
            identifier: 'palestra_titulo',
            rules: [{
                type: 'empty',
                prompt: 'Informe o título da palestra'
            }]
        },
        palestra_descricao:{
            identifier: 'palestra_descricao',
            rules: [{
                type: 'empty',
                prompt: 'Informe a descrição da palestra'
            }]
        },
        recursos:{
            identifier: 'recursos',
            rules: [{
                type: 'empty',
                prompt: 'Informe os recursos necessários'
            }]
        },
        termos:{
            identifier: 'termos',
            rules: [{
                type: 'checked',
                prompt: 'Você precisa aceitar os termos de uso'
            }]
        }
    },inline: true,
    on: 'blur',
    onSuccess: function () {
        form.removeClass('success').removeClass('error');
    },
    onFailure: function () {
        window.scrollTo(0, 0);
        form.removeClass('success').removeClass('error');
    }
}).on('submit', function (e) {
    e.preventDefault();
});

function captcha_filled () {
    allowSubmit = true;
    captcha_warning.hide();
}
function captcha_expired () {
    allowSubmit = false;
    captcha_warning.show();
}

pessoal = $("#info-pessoais");
minicurso = $("#info-minicurso");
palestra = $("#info-palestra");
locomocao = $("#info-locomocao");
step1 = $("#one");
step2 = $("#two");
step3 = $("#three");

minicurso.hide();
palestra.hide();
locomocao.hide();

function setValidation1(){
    form.form({
        fields: {
            nome: {
                identifier: 'nome',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe seu nome'
                }
                ]
            },
            tel: {
                identifier: 'tel',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe seu telefone'
                }]
            },
            email: {
                identifier: 'email',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe seu email'
                }]
            },
            profissao: {
                identifier: 'profissao',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe sua profissão'
                }]
            },
            empresa: {
                identifier: 'empresa',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a empresa/universidade'
                }]
            },
            bio: {
                identifier: 'bio',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a biografia'
                }]
            },
            tamanho_camiseta: {
                identifier: 'tamanho_camiseta',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe o tamanho da camiseta'
                }]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function setValidation2_1(){
    form.form({
        fields:{
            curso_titulo:{
                identifier: 'curso_titulo',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe o título do minicurso'
                }]
            },
            curso_descricao:{
                identifier: 'curso_descricao',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a descrição do minicurso'
                }]
            },
            tipo_dinamica:{
                identifier: 'tipo_dinamica',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe o tipo de dinâmica'
                }]
            },
            lab_hw:{
                identifier: 'lab_hw',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe os requisitos de hardware'
                }]
            },
            lab_sw:{
                identifier: 'lab_sw',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe os requisitos de software'
                }]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function setValidation2_2(){
    form.form({
        fields: {
            palestra_titulo: {
                identifier: 'palestra_titulo',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe o título da palestra'
                }]
            },
            palestra_descricao: {
                identifier: 'palestra_descricao',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a descrição da palestra'
                }]
            },
            recursos: {
                identifier: 'recursos',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe os recursos necessários'
                }]
            }
        }
        ,inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function setValidation3(){
    form.form({
        fields:{
            cidade:{
                identifier: 'cidade',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a cidade'
                }]
            },
            locomocao_tipo:{
                identifier: 'locomocao_tipo',
                depends: 'locomocao',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe o tipo de locomoção'
                }]
            },
            data_chegada:{
                identifier: 'data_chegada',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a data de chegada'
                }]
            },
            data_saida:{
                identifier: 'data_saida',
                rules: [{
                    type: 'empty',
                    prompt: 'Informe a data de saída'
                }]
            },termos:{
                identifier: 'termos',
                rules: [{
                    type: 'checked',
                    prompt: 'Você precisa aceitar os termos de uso'
                }]
            }
        },inline: true,
        on: 'blur',
        onSuccess: function () {
            form.removeClass('success').removeClass('error');
        },
        onFailure: function () {
            window.scrollTo(0, 0);
            form.removeClass('success').removeClass('error');
        }
    }).on('submit', function (e) {
        e.preventDefault();
    });
}

function proxPessoal(a) {
    setValidation1();
    if(form.form('is valid')){
        pessoal.hide(500);
        if(a === 1){
            minicurso.show(500);
            step1.removeClass("active");
            step1.addClass("completed");
            step2.addClass("active");
            setValidation2_1();
        }
        if(a === 2){
            palestra.show(500);
            step1.removeClass("active");
            step1.addClass("completed");
            step2.addClass("active");
            setValidation2_2();
        }
        if(a === 0){
            locomocao.show(500);
            step1.removeClass("active");
            step1.addClass("completed");
            step3.addClass("active");
            setValidation3();
        }
    }
}

function anterior2(a){
    if(a === 1)
        minicurso.hide(500);
    if(a === 2)
        palestra.hide(500);

    pessoal.show(500);
    step2.removeClass("active");
    step1.removeClass("completed");
    step1.addClass("active");
    setValidation1();
}

function proximo2(a){
    if(form.form('is valid')) {
        if(a === 1)
            minicurso.hide(500);
        if(a === 2)
            palestra.hide(500);

        locomocao.show(500);
        step2.removeClass("active");
        step2.addClass("completed");
        step3.addClass("active");
        setValidation3();
    }
}

function anterior3(a){
    locomocao.hide(500);
    if(a === 1){
        minicurso.show(500);
        step3.removeClass("active");
        step2.removeClass("completed");
        step2.addClass("active");
        setValidation2_1();
    }
    if(a === 2){
        palestra.show(500);
        step3.removeClass("active");
        step2.removeClass("completed");
        step2.addClass("active");
        setValidation2_2();
    }
    if(a === 0){
        pessoal.show(500);
        step3.removeClass("active");
        step1.removeClass("completed");
        step1.addClass("active");
        setValidation1();
    }

}

function isValid(){
    form.form('is valid');
}

var modal_edital = $('.ui.large.modal');
modal_edital.modal();
modal_edital.modal('attach events', '#botao_modal_edital');