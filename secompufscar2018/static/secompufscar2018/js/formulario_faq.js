allowSubmit = false;
formulario = $(".ui.form");
captcha_warning = $("#captcha_warning");
captcha_warning.hide();
botao_enviar = $('#botao_enviar_email');

function isFormularioValido(formulario) {
    return formulario.form('is valid', ['name', 'email', 'message']);
}

function captcha_filled () {
    allowSubmit = true;
    captcha_warning.hide();
}

function captcha_expired () {
    allowSubmit = false;
    captcha_warning.show();
}

botao_enviar.on('click', function () {
        if (allowSubmit) {
            formulario.removeClass('success').removeClass('error');

            if (isFormularioValido(formulario)) {
                botao_enviar.addClass('loading');
                $.post("contato/enviar-email/", formulario.serialize())
                    .done(function () {
                        formulario.addClass('success');
                        botao_enviar.removeClass('loading');
                    }).fail(function () {
                    formulario.addClass('error');
                    botao_enviar.removeClass('loading');
                })
            }
        }
        else {
            captcha_warning.show();
        }
    }
);

$('.message .close').on('click', function () {
    formulario.removeClass('success').removeClass('error');
});

$('.ui.accordion').accordion();

formulario.form({
    fields: {
        name: {
            identifier: 'id_nome',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Digite o seu nome.'
                }
            ]
        },
        email: {
            identifier: 'id_email',
            rules: [
                {
                    type: 'email',
                    prompt: 'Digite um endereço de email válido.'
                }
            ]
        },
        message: {
            identifier: 'id_mensagem',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Digite a mensagem.'
                }
            ]
        }
    },
    inline: true,
    on: 'blur',
    onSuccess: function () {
        formulario.removeClass('success').removeClass('error');
    },
    onFailure: function () {
        formulario.removeClass('success').removeClass('error');
    }
}).on('submit', function (e) {
    e.preventDefault();
    return false
});
