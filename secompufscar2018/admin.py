from django.contrib import admin

# Register your models here.
from secompufscar2018.models import ConteudoMesaRedonda, ConteudoMiniCurso, ConteudoPalestra, ProcessoSeletivo, MadDog

admin.site.register(ConteudoMesaRedonda)
admin.site.register(ConteudoPalestra)
admin.site.register(ConteudoMiniCurso)
admin.site.register(ProcessoSeletivo)
admin.site.register(MadDog)
