from django.contrib.auth.views import logout
from django.urls import path
from secompufscar2018 import views_area_conteudo as views

urlpatterns = [
    path('', views.area_conteudo_login, name='area-conteudo'),
    # Exibe as informações dos palestrantes cadastrados
    path('egENjVYBKIDgOF4tQTJ8Eg/<int:tipo>/',views.area_conteudo_info,name='area_conteudo_info'),
    # Views usadas para o cadastro dos forms
    path('7lr69Ec09KnZ2u3CvF7dfA/<int:tipo>/', views.cadastra_ministrante, name='cadastra_ministrante'),
    # Views com os forms visuais
    path('oG1_hoQcfgvTSMdK2UDwmA/<int:tipo>/', views.conteudo_formularios, name='conteudo_formularios'),
    path('logout/', logout, name='area_conteudo_logout'),
    path('login/', views.area_conteudo_login, name='area_conteudo_login'),
    # Processo Seletivo
    path('kxamcSJcI4zm6rL8OsE62A/', views.cadastra_processo_seletivo, name='cadastra_processo_seletivo'),
    path('ps/', views.area_conteudo_processo_seletivo, name='area_conteudo_ps'),
    path('ⓜⓐⓓⓓⓞⓖ/',views.maddog,name='maddog')

]
