import datetime
import os

from django.db import models
from secompufscar2018.secrets import token_urlsafe
from secompufscar_bd.models import Camiseta, Inscricao, Atividade

# Create your models here.

# Não foi possível importar a classe Diretoria, pois possui também os valores Voluntários/Coordenação Geral
DIRETORIAS =(
    ('SC','Sociocultural'),
    ('DM','Design e Marketing'),
    ('TI','Tecnologia de Informação'),
    ('JF','Jurídico-Financeira'),
    ('PA','Patrocínio'),
    ('CO','Conteúdo')
)
CURSOS =(
    ('BCC','Bacharelado em Ciência da Computação'),
    ('ENC','Eng. de Computação'),
    ('PPG','Pós Graduação'),
)
LOCOMOCAO = (
    ('Próprio', 'CARRO PRÓPRIO (COMBUSTÍVEL + PEDÁGIOS, CALCULADOS PELO JURÍDICO E FINANCEIRO)'),
    ('Ônibus', 'PASSAGEM DE ÔNIBUS (IDA E VOLTA)'),
    ('Alugado', 'CARRO ALUGADO (APENAS O VALOR DO ALUGUEL DO CARRO)')
)
LOCOMOCAO_MADDOG = (
    ('Fuel', 'Car fuel'),
    ('Bus', 'Bus ticket'),
    ('Other', 'Other')
)
CAMISETA_MADDOG = (
    ('PP', 'XS'),
    ('P','S'),
    ('M','M'),
    ('G','L'),
    ('GG','XL')
)



def picfolder():
    return 'images/secompufscar'+str(datetime.datetime.now().year)+'/ministrantes'


def path_and_rename(instance,filename):
    upload_to = picfolder()
    ext = filename.split('.')[-1]
    filename = token_urlsafe(8)+'.{}'.format(ext)
    return os.path.join(upload_to, filename)


class ConteudoInformacoesComuns(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField('Nome completo', max_length=50, blank=False, default="")
    email = models.EmailField('Email', blank=False, default="")
    tel = models.CharField('Telefone', max_length=15, blank=False, unique=True, default="")
    profissao = models.CharField('Profissão', max_length=100, blank=False, default="")
    empresa = models.CharField('Empresa', max_length=100, blank=False, default="")
    bio = models.TextField('Breve biografia', blank=True, default="")
    foto = models.FileField('Foto', upload_to=path_and_rename, blank=True)
    github = models.URLField('Github', max_length=255, blank=True)
    face = models.URLField('Facebook', max_length=255, blank=True)
    twitter = models.URLField('Twitter', max_length=255, blank=True)
    linkedin = models.URLField('LinkedIn', max_length=255, blank=True)
    tamanho_camiseta = models.ForeignKey(Camiseta,on_delete=models.CASCADE)
    cidade = models.CharField('Cidade', max_length=30, blank=False, default="")
    locomocao = models.BooleanField('Precisa de locomoção até a SECOMP?', blank=False, default=False)
    locomocao_tipo = models.CharField('Qual o tipo de locomoção?', blank=True, choices=LOCOMOCAO, max_length=10)
    data_chegada = models.DateField('Data de chegada em São Carlos', blank=False)
    data_saida = models.DateField('Data de saída de São Carlos', blank=False)
    acomodacao = models.BooleanField('Necessidade de acomodação (estadia em hotel)?', blank=False, default=False)
    vaga_acomodacao = models.BooleanField('Caso seja necessário uma estadia em hotel, é necessário1 uma vaga na garagem?', blank=True, default=False)
    obs = models.TextField('Observações', blank=True, default="")
    valido = models.BooleanField(default=False)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)


    class Meta:
        abstract = True


class ConteudoMesaRedonda(ConteudoInformacoesComuns):
    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'Mesas Redonda')
        verbose_name_plural = (u'Mesas Redondas')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome

class ConteudoMiniCurso(ConteudoInformacoesComuns):
    curso_titulo = models.CharField('Título do minicurso',max_length=80,blank=False,unique=True)
    curso_descricao = models.TextField('Descrição do minicurso')
    tipo_dinamica = models.TextField('Como se daria a dinâmica de participantes?')
    prereqs = models.TextField('Pré-requisitos:',blank=True)
    lab_hw = models.TextField('Recursos Hardware:',blank=False)
    lab_sw = models.TextField('Recursos Software:',blank=False)
    obs_lab = models.TextField('Observações:',blank=True)
    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'Mini Curso')
        verbose_name_plural = (u'Mini Cursos')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome

class ConteudoPalestra(ConteudoInformacoesComuns):
    palestra_titulo = models.CharField('Título da palestra',blank=False,max_length=255,unique=True)
    palestra_descricao = models.TextField('Descrição da palestra')
    recursos = models.TextField('Recursos:',blank=False)
    obs_palestra = models.TextField('Observações:', blank=True)
    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'Palestra')
        verbose_name_plural = (u'Palestras')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome

class MadDog(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField('Nome completo', max_length=50, blank=False, default="")
    email = models.EmailField('Email', blank=False, default="")
    tel = models.CharField('Telefone', max_length=15, blank=False, unique=True, default="")
    profissao = models.CharField('Profissão', max_length=100, blank=False, default="")
    empresa = models.CharField('Empresa', max_length=100, blank=False, default="")
    bio = models.TextField('Breve biografia', blank=True, default="")
    foto = models.FileField('Foto', upload_to=path_and_rename, blank=True)
    github = models.URLField('Github', max_length=255, blank=True)
    face = models.URLField('Facebook', max_length=255, blank=True)
    twitter = models.URLField('Twitter', max_length=255, blank=True)
    linkedin = models.URLField('LinkedIn', max_length=255, blank=True)
    tamanho_camiseta = models.CharField('Tamanho camiseta', choices=CAMISETA_MADDOG, blank=True, max_length=10)
    cidade = models.CharField('Cidade', max_length=30, blank=False, default="")
    locomocao = models.BooleanField('Will SECOMP UFSCar be in charge of your travel expenses?', blank=False, default=False)
    locomocao_tipo = models.CharField('Qual o tipo de locomoção?', blank=True, choices=LOCOMOCAO_MADDOG, max_length=10)
    data_chegada = models.DateField('Data de chegada em São Carlos', blank=False)
    data_saida = models.DateField('Data de saída de São Carlos', blank=False)
    acomodacao = models.BooleanField('Necessidade de acomodação (estadia em hotel)?', blank=False, default=False)
    vaga_acomodacao = models.BooleanField(
        'Caso seja necessário uma estadia em hotel, é necessário1 uma vaga na garagem?', blank=True, default=False)
    obs = models.TextField('Observações', blank=True, default="")
    valido = models.BooleanField(default=False)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)
    palestra_titulo = models.CharField('Título da palestra', blank=False, max_length=255, unique=True)
    palestra_descricao = models.TextField('Descrição da palestra')
    recursos = models.TextField('Recursos:', blank=False)
    obs_palestra = models.TextField('Observações:', blank=True)

    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'MadDog')
        verbose_name_plural = (u'MadDogs')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome


class ProcessoSeletivo(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=100, blank=False, default="")
    email = models.EmailField(max_length=100, blank=False, default="",unique=True)
    tel = models.CharField(max_length=20, blank=False, unique=True, default="")
    aprovado = models.BooleanField(default=False)
    primeira_opcao = models.CharField(blank=False, choices=DIRETORIAS, max_length=2)
    segunda_opcao = models.CharField(blank=False, choices=DIRETORIAS, max_length=2 )
    curso = models.CharField(max_length=3,choices=CURSOS, blank=False)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)

    class Meta:
        ordering = ['criado_em']
        verbose_name = (u'Processo Seletivo')
        verbose_name_plural = (u'Processo Seletivo')

    def __unicode__(self):
        return self.nome

    def __str__(self):
        return self.nome
