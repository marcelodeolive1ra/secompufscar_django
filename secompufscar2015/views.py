from django.shortcuts import render


def index(request):
    return render(request, 'secompufscar2015/index.html')


def apresentacao(request):
    return render(request, 'secompufscar2015/apresentacao.html')


def programacao(request):
    return render(request, 'secompufscar2015/programacao.html')


def cronograma(request):
    return render(request, 'secompufscar2015/cronograma.html')


def patrocinadores(request):
    return render(request, 'secompufscar2015/patrocinadores.html')


def equipe(request):
    return render(request, 'secompufscar2015/equipe.html')


def contato(request):
    return render(request, 'secompufscar2015/contato.html')