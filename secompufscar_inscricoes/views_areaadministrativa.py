import uuid

import sendgrid
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from sendgrid.helpers.mail import *

from secompufscar2016 import views
from .models import Inscricao, Pagamento, Voucher
from .views import SUPER_ADMINS, AUTORIZADORES_PAGAMENTO_PRESENCIAL

from django.http import JsonResponse

from secompufscar_inscricoes.views import ANO_ATUAL

from secompufscar_inscricoes.pdf417.CandyBarPdf417 import CandyBarPdf417

import socket


def enviar_email_confirmacao_pagamento(email, first_name, last_name, tipo_pagamento, valor_pago, pacote, tamanho_da_camiseta, desafio_de_programadores, contato_com_parceiros):
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
    subject = tipo_pagamento + ' - VII SECOMP UFSCar'
    to_email = Email(email)

    # desafio = 'Não'
    #
    # if desafio_de_programadores:
    #     desafio = 'Sim'

    conteudo_do_email = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <meta charset="utf-8"/>
            <title>Confirmação de Pagamento - VII SECOMP UFSCar</title>
        </head>
        <body>
            <h1>Confirmação de #TIPOPAGAMENTO - VII SECOMP UFSCar</h1>
            <p>Olá #NOME!</p>
            <p>Seu pagamento de R$ #VALOR0 foi confirmado com sucesso. Confira os dados de sua inscrição:</p>
            <p><ul>
            <li><b>Nome Completo:</b> #NOME #SOBRENOME</li>
            <li><b>Pacote:</b> #PACOTE</li>
            <li><b>Camiseta:</b> #CAMISETA</li>
            <li><b>Desafio de Programadores:</b> #DESAFIODEPROGRAMADORES</li>
            <li><b>Contato com parceiros da SECOMP:</b> #CONTATOCOMPARCEIROS</li>
            <li><b>Valor pago:</b> R$ #VALOR0</li>
            </ul>
            </p>

            <p>Em caso de problemas, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">
            contato@secompufscar.com.br</a> informando o e-mail que você utilizou na inscrição.</p>
            <p>Nos vemos em breve!<br>
            Equipe VII SECOMP UFSCar</p>
        </body>
        </html>
        """
    # valor_a_pagar = '30,00'
    #
    # if pacote == 'STUDY':
    #     valor_a_pagar = '20,00'
    # elif pacote == 'FREE':
    #     valor_a_pagar = '0,00'

    conteudo_do_email = conteudo_do_email.replace('#TIPOPAGAMENTO', tipo_pagamento)
    conteudo_do_email = conteudo_do_email.replace('#NOME', first_name)
    conteudo_do_email = conteudo_do_email.replace('#SOBRENOME', last_name)
    conteudo_do_email = conteudo_do_email.replace('#PACOTE', pacote)
    conteudo_do_email = conteudo_do_email.replace('#CAMISETA', tamanho_da_camiseta)
    conteudo_do_email = conteudo_do_email.replace('#DESAFIODEPROGRAMADORES', desafio_de_programadores)
    conteudo_do_email = conteudo_do_email.replace('#CONTATOCOMPARCEIROS', contato_com_parceiros)
    conteudo_do_email = conteudo_do_email.replace('#VALOR', str(valor_pago))

    content = Content("text/html", conteudo_do_email)
    ready_mail = Mail(from_email, subject, to_email, content)

    sg.client.mail.send.post(request_body=ready_mail.get())

############################################################################
# Área administrativa
############################################################################

def write_file(file_name, bs):
    of = open(file_name, 'wb')
    of.write(bs)
    of.close()


@login_required(login_url='/2016/areadoparticipante/login/')
def area_administrativa(request):
    if request.user.email in SUPER_ADMINS or AUTORIZADORES_PAGAMENTO_PRESENCIAL:
        context_dictionary = views.context_dictionary.copy()
        if request.user.email in SUPER_ADMINS:
            context_dictionary['super_admin'] = True
        else:
            context_dictionary['autorizador_pagamento_presencial'] = True

        if request.method == 'POST':
            context_dictionary['pagamento_confirmado'] = False

            try:
                participante = User.objects.get(email=request.POST['email'])
                context_dictionary['participante'] = participante

                inscricao = participante.inscricao_set.last()
                context_dictionary['ja_pago'] = False
                # ja_pago = False
                # for pagamento in inscricao.pagamentos.all():
                #     if pagamento.referencia_pagamento == 'Inscrição pacote FULL' or pagamento.referencia_pagamento == 'Inscrição pacote STUDY':
                #         # ja_pago = True
                #         context_dictionary['ja_pago'] = True

                pacote = participante.inscricao_set.last().pacote  # qual o pacote do usuário?

                valor_a_pagar = 0.

                if pacote == 'FULL':
                    valor_a_pagar = 30.
                elif pacote == 'STUDY':
                    valor_a_pagar = 20.

                valor_ja_pago = 0.

                pagamentos = participante.inscricao_set.last().pagamentos.all()

                for pagamento in pagamentos:
                    valor_ja_pago += pagamento.valor_pagamento

                valor_a_pagar -= valor_ja_pago

                if pacote == 'FULL' and valor_ja_pago == 30. or pacote == 'STUDY' and valor_ja_pago == 20 or pacote == 'FREE':
                    context_dictionary['ja_pago'] = True
                else:
                    context_dictionary['ja_pago'] = False

                context_dictionary['valor_a_pagar'] = valor_a_pagar
                print('ok até aqui')

                pdf417 = CandyBarPdf417()
                bs = pdf417.encode(inscricao.numero_inscricao)
                print('still ok')


                CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/'
                if socket.gethostname() != 'secompufscar-lamp-ubuntu':
                    CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/'

                # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')

                write_file(CODES_URL + inscricao.numero_inscricao + '.png', bs)

                context_dictionary['pdf417'] = 'secompufscar_inscricoes/codes/' + inscricao.numero_inscricao + '.png'
                print('ok')


            except:
                context_dictionary = views.context_dictionary.copy()
                context_dictionary['participante'] = False
                context_dictionary['email_nao_encontrado'] = request.POST['email']
            return render(request, 'secompufscar_inscricoes/areaadministrativa/areaadministrativa.html',
                          context_dictionary)
        else:
            context_dictionary = views.context_dictionary.copy()
            context_dictionary['email_nao_encontrado'] = ''
            context_dictionary['pagamento_confirmado'] = False


            inscricoes_full = Inscricao.objects.filter(pacote='FULL').count()
            inscricoes_study = Inscricao.objects.filter(pacote='STUDY').count()
            inscricoes_free = Inscricao.objects.filter(pacote='FREE').count()
            camisetas_p = Inscricao.objects.filter(camiseta='P').count()
            camisetas_m = Inscricao.objects.filter(camiseta='M').count()
            camisetas_g = Inscricao.objects.filter(camiseta='G').count()
            camisetas_gg = Inscricao.objects.filter(camiseta='GG').count()
            camisetas_54 = Inscricao.objects.filter(camiseta='54').count()
            camisetas_babylook_p = Inscricao.objects.filter(camiseta='BabyLook P').count()
            camisetas_babylook_m = Inscricao.objects.filter(camiseta='BabyLook M').count()
            camisetas_babylook_g = Inscricao.objects.filter(camiseta='BabyLook G').count()
            camisetas_babylook_gg = Inscricao.objects.filter(camiseta='BabyLook GG').count()



            inscricoes_organizacao = Voucher.objects.filter(referencia='Organização').filter(usado=True).count()


            context_dictionary['pacote_free'] = inscricoes_free
            context_dictionary['pacote_study'] = inscricoes_study
            context_dictionary['pacote_full'] = inscricoes_full - inscricoes_organizacao
            context_dictionary['organizacao'] = inscricoes_organizacao
            context_dictionary['total_inscricoes'] = inscricoes_free + inscricoes_full + inscricoes_study
            context_dictionary['total_inscricoes_sem_organizacao'] = inscricoes_free + inscricoes_full + inscricoes_study - inscricoes_organizacao
            context_dictionary['camisetas_p'] = [camisetas_p, camisetas_p / inscricoes_full * 100]
            context_dictionary['camisetas_m'] = [camisetas_m, camisetas_m / inscricoes_full * 100]
            context_dictionary['camisetas_g'] = [camisetas_g, camisetas_g / inscricoes_full * 100]
            context_dictionary['camisetas_gg'] = [camisetas_gg, camisetas_gg / inscricoes_full * 100]
            context_dictionary['camisetas_54'] = [camisetas_54, camisetas_54 / inscricoes_full * 100]
            context_dictionary['camisetas_babylook_p'] = [camisetas_babylook_p, camisetas_babylook_p / inscricoes_full * 100]
            context_dictionary['camisetas_babylook_m'] = [camisetas_babylook_m, camisetas_babylook_m / inscricoes_full * 100]
            context_dictionary['camisetas_babylook_g'] = [camisetas_babylook_g, camisetas_babylook_g / inscricoes_full * 100]
            context_dictionary['camisetas_babylook_gg'] = [camisetas_babylook_gg, camisetas_babylook_gg / inscricoes_full * 100]

            pagamentos_paypal = 0.0
            pagamentos_presencial = 0.0
            pagamentos_deposito = 0.0
            pagamentos_cupom = 0.0

            for inscricao in Inscricao.objects.all():
                for pagamento in inscricao.pagamentos.all():
                    if pagamento.metodo_pagamento == 'PayPal':
                        pagamentos_paypal += pagamento.valor_pagamento * 0.95 - 0.6
                    elif pagamento.metodo_pagamento == 'Pagamento Presencial':
                        pagamentos_presencial += pagamento.valor_pagamento
                    elif pagamento.metodo_pagamento == 'Transferência/Depósito':
                        pagamentos_deposito += pagamento.valor_pagamento
                    else:
                        pagamentos_cupom += pagamento.valor_pagamento

            context_dictionary['pagamentos_paypal'] = pagamentos_paypal
            context_dictionary['pagamentos_presencial'] = pagamentos_presencial
            context_dictionary['pagamentos_deposito'] = pagamentos_deposito
            context_dictionary['pagamentos_cupom'] = pagamentos_cupom
            context_dictionary['pagamentos_totais'] = pagamentos_paypal + pagamentos_presencial + pagamentos_deposito


            inscricoes_full_pagas = 0
            inscricoes_full_parciais = 0
            inscricoes_full_pendentes = 0
            inscricoes_study_pagas = 0
            inscricoes_study_parciais = 0
            inscricoes_study_pendentes = 0

            for inscricao in Inscricao.objects.all():
                valor_pago = 0.0
                for pagamento in inscricao.pagamentos.all():
                    valor_pago += pagamento.valor_pagamento

                if inscricao.pacote == 'FULL':
                    if valor_pago == 30.0:
                        inscricoes_full_pagas += 1
                    elif valor_pago > 0:
                        inscricoes_full_parciais += 1
                    else:
                        inscricoes_full_pendentes += 1
                elif inscricao.pacote == 'STUDY':
                    if valor_pago == 20.0:
                        inscricoes_study_pagas += 1
                    elif valor_pago > 0:
                        inscricoes_study_parciais += 1
                    else:
                        inscricoes_study_pendentes += 1

            context_dictionary['inscricoes_full_pagas'] = inscricoes_full_pagas
            context_dictionary['inscricoes_full_parciais'] = inscricoes_full_parciais
            context_dictionary['inscricoes_full_pendentes'] = inscricoes_full_pendentes
            context_dictionary['inscricoes_study_pagas'] = inscricoes_study_pagas
            context_dictionary['inscricoes_study_parciais'] = inscricoes_study_parciais
            context_dictionary['inscricoes_study_pendentes'] = inscricoes_study_pendentes

            organizacao = Voucher.objects.filter(referencia='Organização').filter(usado=True)

            membros = []

            # P, M, G, GG, 56, BabyLook P, BabyLook M, BabyLook G, BabyLook GG
            tamanhos_camisetas_organizacao = [0, 0, 0, 0, 0, 0, 0, 0, 0]

            for i in range(0, len(organizacao)):
                print(organizacao[i].codigo)
                try:
                    p = Pagamento.objects.get(registro_pagamento=organizacao[i].codigo)
                    i = Inscricao.objects.filter(pagamentos=p)
                    membros.append(i)
                    if i.last().camiseta == 'P':
                        tamanhos_camisetas_organizacao[0] += 1
                    elif i.last().camiseta == 'M':
                        tamanhos_camisetas_organizacao[1] += 1
                    elif i.last().camiseta == 'G':
                        tamanhos_camisetas_organizacao[2] += 1
                    elif i.last().camiseta == 'GG':
                        tamanhos_camisetas_organizacao[3] += 1
                    elif i.last().camiseta == '54':
                        tamanhos_camisetas_organizacao[4] += 1
                    elif i.last().camiseta == 'BabyLook P':
                        tamanhos_camisetas_organizacao[5] += 1
                    elif i.last().camiseta == 'BabyLook M':
                        tamanhos_camisetas_organizacao[6] += 1
                    elif i.last().camiseta == 'BabyLook G':
                        tamanhos_camisetas_organizacao[7] += 1
                    elif i.last().camiseta == 'BabyLook GG':
                        tamanhos_camisetas_organizacao[8] += 1


                except:
                    print('Erro na inscrição: ' + organizacao[i].codigo)

            context_dictionary['membros'] = membros
            context_dictionary['tamanhos_camisetas_organizacao'] = tamanhos_camisetas_organizacao
            usuarios_cadastrados_nao_inscritos = []

            for user in User.objects.all():
                if user.inscricao_set.count() == 0:
                    usuarios_cadastrados_nao_inscritos.append(user)

            usuarios_cadastrados_nao_inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))

            # usuarios_cadastrados_nao_inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))

            context_dictionary['usuarios_cadastrados_nao_inscritos'] = usuarios_cadastrados_nao_inscritos

            return render(request, 'secompufscar_inscricoes/areaadministrativa/areaadministrativa.html',
                          context_dictionary)
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')


@login_required(login_url='/2016/areadoparticipante/login/')
def admin_confirmar_pagamento_presencial(request):
    if request.user.email in SUPER_ADMINS or AUTORIZADORES_PAGAMENTO_PRESENCIAL:
        context_dictionary = views.context_dictionary.copy()
        if request.method == 'POST':
            user = request.user

            email = request.POST['email']
            participante = User.objects.get(email=email)

            pacote = participante.inscricao_set.last().pacote  # qual o pacote do usuário?

            valor_a_pagar = 0.

            if pacote == 'FULL':
                valor_a_pagar = 30.
            elif pacote == 'STUDY':
                valor_a_pagar = 20.

            valor_ja_pago = 0.

            pagamentos = participante.inscricao_set.last().pagamentos.all()
            for pagamento in pagamentos:
                valor_ja_pago += pagamento.valor_pagamento

            valor_a_pagar -= valor_ja_pago

            print('Valor a pagar ' + str(valor_a_pagar))

            ja_pago = False

            if pacote == 'FULL' and valor_ja_pago == 30. or pacote == 'STUDY' and valor_ja_pago == 20 or pacote == 'FREE':
                ja_pago = True

            if not ja_pago:
                print('not ja pago')
                pagamento_presencial = Pagamento(metodo_pagamento='Pagamento Presencial',
                                                 autorizacao_pagamento=user.username + ' (' + user.first_name + ' ' + user.last_name + ')',
                                                 registro_pagamento=str(uuid.uuid4()),
                                                 valor_pagamento=valor_a_pagar,
                                                 referencia_pagamento='Inscrição pacote ' + pacote)
                pagamento_presencial.save()

                inscricao = participante.inscricao_set.last()
                if inscricao.desafio_de_programadores:
                    desafio = 'Sim'
                else:
                    desafio = 'Não'

                if inscricao.aceite_compartilhamento_email:
                    contato = 'Sim'
                else:
                    contato = 'Não'

                inscricao.pagamentos.add(pagamento_presencial)
                inscricao.save()

                enviar_email_confirmacao_pagamento(email, participante.first_name, participante.last_name,
                                                   pagamento_presencial.metodo_pagamento,
                                                   valor_a_pagar, pacote, inscricao.camiseta,
                                                   desafio,
                                                   contato)

                context_dictionary['pagamento_confirmado'] = True
                context_dictionary['metodo_pagamento'] = pagamento_presencial.metodo_pagamento
            else:
                print('ja pago')
                context_dictionary['ja_pago'] = True
                context_dictionary['pagamento_confirmado'] = False

        return render(request, 'secompufscar_inscricoes/areaadministrativa/areaadministrativa.html', context_dictionary)

    return HttpResponseRedirect('/2016/areadoparticipante/areaadministrativa/')


@login_required(login_url='/2016/areadoparticipante/login/')
def admin_confirmar_pagamento_transferencia(request):
    context_dictionary = views.context_dictionary.copy()
    if request.user.email in ['marcelodeoliveira@outlook.com']:

        if request.method == 'POST':

            user = request.user

            email = request.POST['email']
            participante = User.objects.get(email=email)

            pacote = participante.inscricao_set.last().pacote  # qual o pacote do usuário?

            valor_a_pagar = 0.

            if pacote == 'FULL':
                valor_a_pagar = 30.
            elif pacote == 'STUDY':
                valor_a_pagar = 20.

            valor_ja_pago = 0.

            pagamentos = participante.inscricao_set.last().pagamentos.all()
            for pagamento in pagamentos:
                valor_ja_pago += pagamento.valor_pagamento

            valor_a_pagar -= valor_ja_pago

            print('Valor a pagar ' + str(valor_a_pagar))

            ja_pago = False

            if pacote == 'FULL' and valor_ja_pago == 30. or pacote == 'STUDY' and valor_ja_pago == 20 or pacote == 'FREE':
                ja_pago = True

            if not ja_pago:
                print('not ja pago')
                pagamento_presencial = Pagamento(metodo_pagamento='Transferência/Depósito',
                                                 autorizacao_pagamento=user.username + ' (' + user.first_name + ' ' + user.last_name + ')',
                                                 registro_pagamento=str(uuid.uuid4()),
                                                 valor_pagamento=valor_a_pagar,
                                                 referencia_pagamento='Inscrição pacote ' + pacote)
                pagamento_presencial.save()

                inscricao = participante.inscricao_set.last()

                inscricao.pagamentos.add(pagamento_presencial)
                inscricao.save()

                if inscricao.desafio_de_programadores:
                    desafio = 'Sim'
                else:
                    desafio = 'Não'

                if inscricao.aceite_compartilhamento_email:
                    contato = 'Sim'
                else:
                    contato = 'Não'

                enviar_email_confirmacao_pagamento(email, participante.first_name, participante.last_name,
                                                   pagamento_presencial.metodo_pagamento,
                                                   valor_a_pagar, pacote, inscricao.camiseta,
                                                   desafio,
                                                   contato)

                context_dictionary['pagamento_confirmado'] = True
                context_dictionary['metodo_pagamento'] = pagamento_presencial.metodo_pagamento

            else:
                print('ja pago')
                context_dictionary['ja_pago'] = True
                context_dictionary['pagamento_confirmado'] = False

        return render(request, 'secompufscar_inscricoes/areaadministrativa/areaadministrativa.html', context_dictionary)

    return HttpResponseRedirect('/2016/areadoparticipante/areaadministrativa/')


@login_required(login_url='/2016/areadoparticipante/login/')
def gerar_cupons(request):
    if request.user.email in SUPER_ADMINS:
        if request.method == 'POST':
            quantidade = request.POST['quantidade']
            desconto = request.POST['desconto']
            referencia = request.POST['referencia']

            # context_dictionary = views.context_dictionary.copy()

            cupons_gerados = []

            for _ in range(0, int(quantidade)):
                codigo = str(uuid.uuid4())[0:12]
                v = Voucher(codigo=codigo, referencia=referencia, desconto=desconto, usado=False)
                v.save()
                cupons_gerados.append(codigo)

            views.context_dictionary['desconto'] = desconto
            views.context_dictionary['referencia'] = referencia
            views.context_dictionary['cupons_gerados'] = cupons_gerados

    return HttpResponseRedirect('/2016/areadoparticipante/areaadministrativa/')


# @login_required(login_url='/2016/areadoparticipante/login/')
# def verificar_inscricoes_nao_concluidas(request):
#
#     context_dictionary = views.get_context_dictionary().copy()
#
#     usuarios_cadastrados_nao_inscritos = []
#
#     for user in User.objects.all():
#         if user.inscricao_set.count() == 0:
#             usuarios_cadastrados_nao_inscritos.append(user)
#
#     usuarios_cadastrados_nao_inscritos['usuarios_cadastrados_nao_inscritos'] = usuarios_cadastrados_nao_inscritos

@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_cadastrados_nao_inscritos(request):
    # usuarios_cadastrados_nao_inscritos = [User.objects.get(username='marcelodeoliveira@outlook.com')]
    usuarios_cadastrados_nao_inscritos = []

    for user in User.objects.all():
        if user.inscricao_set.count() == 0:
            usuarios_cadastrados_nao_inscritos.append(user)

    usuarios_cadastrados_nao_inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = usuarios_cadastrados_nao_inscritos

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in usuarios_cadastrados_nao_inscritos:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Conclua sua inscrição - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>Conclua sua inscrição na VII SECOMP UFSCar</h1>
                   <p>Olá #NOME!</p>
                   <p>Percebemos que você se cadastrou no sistema da SECOMP UFSCar, mas não realizou sua inscrição.</p>

                   <p>Conclua sua inscrição agora mesmo! Basta logar na <a href="https://secompufscar.com.br/2016/areadoparticipante/">Área do Participante (https://secompufscar.com.br/2016/areadoparticipante/)</a> e seguir as instruções.</p>

                   <p>Não perca esta que vai ser a nossa melhor SECOMP de todas!<br>As inscrições em minicursos abrem hoje às 19h!<br>
                   <br>Nos vemos em breve :-)<br><br>
                   Equipe VII SECOMP UFSCar
                   </p>

                   <p>Qualquer dúvida, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">contato@secompufscar.com.br</a>.</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "Conclua sua inscrição na SECOMP UFSCar", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)





# from unidecode import unidecode


def gerar_codigos(request):
    # full = Inscricao.objects.filter(pacote='FULL')
    #
    # for inscricao in full:
    #
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/full/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/full/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)
    #
    #
    # study = Inscricao.objects.filter(pacote='STUDY')
    #
    # for inscricao in study:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/full/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/study/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)
    #
    # free = Inscricao.objects.filter(pacote='FREE')
    #
    # for inscricao in free:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/full/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/free/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)

    # palestrantes = [
    # 'EDUARDO_BARASAL_MORALES'
    # ,'REGINALDO_GOTARDO'
    #     , 'SILVIO_LUIZ_STANZANI'
    #     , 'RAPHAEL_MENDES_DE_OLIVEIRA_COBE'
    #     , 'ROGERIO_LUIZ_IOPE'
    #     , 'FERNANDO_MAGNAGHI'
    #     , 'RAFAEL_LACHI'
    #     , 'FELIPE_MINEY_GONCALVES_DA_COSTA'
    #     , 'ALEX_FERNANDO_ORLANDO'
    #     , 'ROBSON_BARTOLOMEU_GOMES_DIAS'
    #     , 'LUIS_GONZALEZ'
    #     , 'ANDRE_CARLOS_PONCE_DE_LEON_FERREIRA_DE_CARVALHO'
    #     , 'RAFAEL_BASTOS'
    #     , 'VANIA_PAULA_DE_ALMEIDA_NERIS'
    #     , 'WILLIAN_DAS_NEVES_GRILLO'
    #     , 'ALEXANDRE_ZOLLINGER_CHOHFI'
    #     , 'LUIS_FELIPE_TOMAZINI'
    #     , 'LEONARDO_TAVARES_OLIVEIRA'
    #     , 'ALEXANDRE_MARTINS_MOREIRA_NOGUEIRA'
    #     , 'JOSE_PACHECO'
    #     , 'KELEN_VIVALDINI'
    #     , 'JESSICA_CAROLINE_DIAS_NASCIMENTO'
    #     , 'JULIANA_KAROLINE_DE_SOUSA'
    #     , 'RAFAEL_BRINGEL'
    #     , 'PEDRO_E_CUNHA_BRIGATTO'
    #     , 'FERNANDO_MASANORI_ASHIKAGA'
    #     , 'WAGNER_ROSADO'
    #     , 'ELIS_CRISTINA_MONTORO_HERNANDES_ERVOLINO'
    #     , 'DANIEL_LUCREDIO'
    #     , 'MARCOS_DANILO_CHIODI_MARTINS'
    #     , 'ROBERTO_FERRARI_JUNIOR'
    #     , 'THAIS_DE_MOURA_CARDOSO'
    #     , 'HELIO_CRESTANA_GUARDIA'
    #     , 'FABIO_PEDRUCI'
    #     , 'MARCO_TULIO_KEHDI'
    #     , 'HELOISA_DE_ARRUDA_CAMARGO'
    #     , 'RICARDO_CERRI'
    #     , 'PAULO_EDUARDO_SANTOS'
    #     , 'ESTEVAM_R_HRUSCHKA_JR'
    #
    # ]
    #
    # palestrantes = [
    #     'JULIO_DOS_SANTOS', 'CESAR_MARCONDES'
    # ]
    #
    # for inscricao in palestrantes:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/palestrantes/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/palestrantes/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao
    #     # filename = filename.join(filename.split())
    #     # filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)


    # full = []
    # study = []
    # free = []
    # for i in Inscricao.objects.all():
    #     if i.id > 516:
    #         if i.pacote == 'FULL':
    #             full.append(i)
    #         elif i.pacote == 'STUDY':
    #             study.append(i)
    #         elif i.pacote == 'FREE':
    #             free.append(i)
    #
    # print('FULL')
    # for i in full:
    #     print(i.user.first_name.upper() + ';' + i.user.last_name.upper())
    #
    # print('STUDY')
    # for i in study:
    #     print(i.user.first_name.upper() + ';' + i.user.last_name.upper())
    #
    # print('FREE')
    # for i in free:
    #     print(i.user.first_name.upper() + ';' + i.user.last_name.upper())
    #
    #
    # for inscricao in full:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/palestrantes/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/ultimos/full/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     # filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)
    #
    # for inscricao in study:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/palestrantes/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/ultimos/study/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     # filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)
    #
    # for inscricao in free:
    #     pdf417 = CandyBarPdf417()
    #     bs = pdf417.encode(inscricao.numero_inscricao)
    #     print('still ok')
    #
    #     CODES_URL = '/home/marcelo/secompufscar_env/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/palestrantes/'
    #     if socket.gethostname() != 'secompufscar-lamp-ubuntu':
    #         CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/ultimos/free/'
    #
    #     # CODES_URL = os.path.join(settings.BASE_DIR, '/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/')
    #
    #     filename = inscricao.user.first_name + ' ' + inscricao.user.last_name
    #     # filename = filename.join(filename.split())
    #     # filename.replace(' ', '_')
    #     write_file(CODES_URL + filename + '.png', bs)

    pdf417 = CandyBarPdf417()
    bs = pdf417.encode('GUILHERME_PEDROSO')
    CODES_URL = '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/ultimos/FULL/'
    write_file(CODES_URL + 'GUILHERME_PEDROSO' + '.png', bs)

    return HttpResponseRedirect('/2016/areadoparticipante/areaadministrativa/')
    # return render_to_response('secompufscar_inscricoes/inscricao/cracha.html')


@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_rede_wifi(request):
    fora_da_ufscar = []

    # fora_da_ufscar.append(User.objects.get(username='marcelodeoliveira@outlook.com'))

    for i in Inscricao.objects.all():
        fora_da_ufscar.append(i.user)

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = fora_da_ufscar

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in fora_da_ufscar:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Cadastro na rede WIFI-UFSCAR-VISITANTE - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>Cadastro na rede WIFI-UFSCAR-VISITANTE - VII SECOMP UFSCar</h1>
                   <p>Olá #NOME!</p>
                   <p>É aluno da UFSCar? Então você provavelmente já tem acesso à rede Wi-Fi da UFSCar e nem precisa ler esse e-mail.</p>
                   <p>Não é aluno da UFSCar? Pois saiba que a UFSCar tem rede Wi-Fi em boa parte do campus e você poderá utilizá-la durante a SECOMP :-)
                   </p>
                   <p>Para utilizar a rede, é necessário que façamos um cadastro que necessita de seu CPF. Caso tenha interesse em se cadastrar na rede, preencha o formulário disponível em <a href="https://secompufscar.com.br/2016/cadastronaredewifi/">https://secompufscar.com.br/2016/cadastronaredewifi/</a>.
                   </p>
                   <p>Após preencher o formulário, nós faremos o cadastro e você receberá um e-mail com mais informações.</p>

                   <p>Qualquer dúvida, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">contato@secompufscar.com.br</a>.</p>

                   <p>Até breve!<br>
                   Equipe VII SECOMP UFSCar</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "Cadastro na rede WIFI-UFSCAR-VISITANTE - VII SECOMP UFSCar", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)

@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_churrasco(request):

    # todas_inscricoes = Inscricao.objects.all()
    inscritos = []
    # inscritos.append(User.objects.get(username='jlanssarini@gmail.com'))
    # inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
    for i in Inscricao.objects.all():
        inscritos.append(i.user)

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = inscritos

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in inscritos:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Churrasco de Integração - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>Churrasco de Integração - VII SECOMP UFSCar</h1>
                   <p>Olá #NOME!</p>
                   <p>Você sabia que na SECOMP também temos um <b>Churrasco de Integração</b>?</p>
                   <p>Quinta-feira, dia 29/09, a partir das 19h. Open de carne (e opções vegetarianas), cerveja, refrigerante e suco por apenas R$ 25,00 para inscritos no pacote FULL ou R$ 30,00 para os demais.</p>
                   <p>Interessado? Preencha o formulário disponível em nosso site: <a href="https://secompufscar.com.br/2016/programacao/churrascodeintegracao/">https://secompufscar.com.br/2016/programacao/churrascodeintegracao/</a>. <b>As vagas são LIMITADAS</b>.</p>
                    <p>Confirme presença em nosso <a href="https://www.facebook.com/events/1108914425811160/">evento no Facebook</a>.
                   <p>Qualquer dúvida, entre em contato conosco pelo e-mail <a href="mailto:churrasco@secompufscar.com.br">churrasco@secompufscar.com.br</a>.</p>

                   <p>Até breve!<br>
                   Equipe VII SECOMP UFSCar</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "Churrasco de Integração - VII SECOMP UFSCar", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)

@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_desafio(request):
    if request.user.email == 'marcelodeoliveira@outlook.com':
        desafio = []

        # fora_da_ufscar.append(User.objects.get(username='marcelodeoliveira@outlook.com'))

        for i in Inscricao.objects.filter(desafio_de_programadores=True):
            desafio.append(i.user)

        desafio.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
        desafio.append(User.objects.get(username='caio.herrera@outlook.com'))

        context_dictionary = views.get_context_dictionary().copy()

        context_dictionary['usuarios_cadastrados_nao_inscritos'] = desafio

        emails_enviados = []
        emails_nao_enviados = []
        sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

        for usuario in desafio:

            from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
            to_email = Email(usuario.email)

            # desafio = 'Não'
            #
            # if desafio_de_programadores:
            #     desafio = 'Sim'

            conteudo_do_email = """
                   <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                   <html>
                   <head>
                       <meta charset="utf-8"/>
                       <title>Aquecimento para o Desafio de Programadores - VII SECOMP UFSCar</title>
                   </head>
                   <body>
                       <h1>Aquecimento para o Desafio de Programadores - VII SECOMP UFSCar</h1>
                       <p>Olá #NOME!</p>
                       <p>Ansioso para disputar os R$ 1000,00 do III Desafio de Programadores da UFSCar?</p>
                       <p>Lembramos que <b>hoje às 19h</b> teremos o aquecimento para o Desafio de Programadores, no Departamento de Computação.</p>
                       <p>Ainda não inscreveu a sua equipe? Faça a inscrição da equipe até <b>amanhã (29/09), às 15h</b>.</p>

                       <p>Qualquer dúvida, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">contato@secompufscar.com.br</a>.</p>

                       <p>Até breve!<br>
                       Equipe VII SECOMP UFSCar</p>
                   </body>
                   </html>
                   """
            # valor_a_pagar = '30,00'
            #
            # if pacote == 'STUDY':
            #     valor_a_pagar = '20,00'
            # elif pacote == 'FREE':
            #     valor_a_pagar = '0,00'

            conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

            content = Content("text/html", conteudo_do_email)
            ready_mail = Mail(from_email, "O aquecimento para o Desafio de Programadores é hoje - VII SECOMP UFSCar", to_email, content)

            response = sg.client.mail.send.post(request_body=ready_mail.get())

            print(response.status_code)

            if response.status_code >= 200 and response.status_code <= 300:
                emails_enviados.append(usuario)
            else:
                emails_nao_enviados.append(usuario)

            context_dictionary['emails_enviados'] = emails_enviados
            context_dictionary['emails_nao_enviados'] = emails_nao_enviados

        return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/')

@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_inscricao_workshops(request):

    # todas_inscricoes = Inscricao.objects.all()
    inscritos = []
    # inscritos.append(User.objects.get(username='jlanssarini@gmail.com'))
    # inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
    for i in Inscricao.objects.all():
        inscritos.append(i.user)

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = inscritos

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in inscritos:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Inscrição em workshops - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>Inscrição em workshops a partir de 25/09 às 19h - VII SECOMP UFSCar</h1>
                   <p>Olá #NOME!</p>
                   <p>Amanhã, dia 25/09 às 19h, abriremos as inscrições para os workshops da VII SECOMP. As inscrições devem ser feitas através da Área do Participante em nosso site.</p>
                   <p>Esse ano teremos 3 workshops:
                   <ul>
                   <li><b>Programação Paralela e Otimização para Arquitetura Intel</b>, com 18 vagas, oferecido pela Intel.<br></li>
                   <b>ATENÇÃO: Este workshop tem como pré-requisito assistir a palestra da Intel, de mesmo título, que será na segunda-feira, às 16h.</b><br><br>
                   <li><b>SCRUM & JIRA na prática - umas das muitas formas de gerenciar atividades de desevolvimento</b>, com 18 vagas, oferecido pela Monitora.</li><br>
                   <li><b>Extraindo o Valor dos Dados</b>, com 180 vagas (isso mesmo, você leu corretamente, são 180 vagas pois este workshop não precisa de computadores), oferecido pela Amdocs.</li>
                   </ul>
                   <p>Lembrando que, para cada workshop que você se inscrever, <b>você deverá trazer 1 kg de alimento não-perecível</b>, que será doado a instituições de caridade de São Carlos.</p>

                   <p>Veja mais detalhes em nosso site e em caso de dúvidas, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">contato@secompufscar.com.br</a>.</p>

                   <p>Até breve!<br>
                   Equipe VII SECOMP UFSCar</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "Aviso sobre a inscrição em workshops - VII SECOMP UFSCar", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)

@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_credenciamento(request):

    # todas_inscricoes = Inscricao.objects.all()
    inscritos = []
    # inscritos.append(User.objects.get(username='jlanssarini@gmail.com'))
    # inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
    # inscritos.append(User.objects.get(username='felipe.pa.i@hotmail.com'))
    for i in Inscricao.objects.all():
        if i.user.is_active:
            inscritos.append(i.user)

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = inscritos

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in inscritos:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Já é amanhã - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>A VII SECOMP UFSCar começa amanhã!</h1>
                   <p>Olá #NOME!</p>
                   <p>A espera acabou! Amanhã, dia 26/09, a partir das 8h estaremos no Anfiteatro Bento Prado Jr. realizando o credenciamento para a 7ª edição da Semana da Computação da UFSCar.</p>
                   <p>Chegue cedo para evitar filas! Este ano tivemos recorde de inscrições e estamos certos de que o evento será um sucesso!</p>
                   <p>Confira a programação do primeiro dia:</p><b>
                   <ul>
                   <li>A partir das 8h: credenciamento</li>
                   <li>9h30: Abertura oficial da VII SECOMP UFSCar</li>
                   <li>10h - 10h50: Palestra Conhecendo o IPv6</li>
                   <li>11h - 11h50: Palestra <b>Inovação: vá além da apresentação do PowerPoint</b></li>
                   <li>12h - 12h15: Lightning Talk: Escola Piloto de Computação</li>
                   <li>12h45 - 14h: Workshop Extraindo o Valor dos Dados</li>
                    <li>14h - 15h: Mesa-redonda de Ex-alunos</li>
                    <li>16h - 17h15: Palestra Programação Paralela e Otimização para Arquitetura Intel</li>
                    </ul>
                    </b>
                    <p>Todas as atividades acima serão realizadas no Anfiteatro Bento Prado Jr. A seguir, às 17h45 temos o workshop <b>Programação Paralela e Otimização para Arquitetura Intel</b>, no Departamento de Computação.</p>
                    <p>E pra fechar o primeiro dia com chave de ouro: a partir das 19h temos a <b>Game Night v3.0.2 by CAEnC</b>.</p>
                    <p>É muita coisa boa, não dá pra perder!

                    <p><b>Observação para os inscritos nos workshops</b>: não se esqueçam de trazer o quilo de alimento não-perecível para cada workshop que você estiver inscrito.</p>

                   <p>Nos vemos amanhã :-)<br>
                   Equipe VII SECOMP UFSCar</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "A VII SECOMP UFSCar já vai começar", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)


@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_programacao(request):

    if request.user.email == 'marcelodeoliveira@outlook.com':

        # todas_inscricoes = Inscricao.objects.all()
        inscritos = []
        # inscritos.append(User.objects.get(username='jlanssarini@gmail.com'))
        inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
        inscritos.append(User.objects.get(username='caio.herrera@outlook.com'))
        # inscritos.append(User.objects.get(username='phdmauad@gmail.com'))
        # inscritos.append(User.objects.get(username='victorcora98@gmail.com'))


        # inscritos.append(User.objects.get(username='pedro.migliatti@gmail.com'))

        # a = Atividade.objects.get(id=19)
        # for i in Inscricao.objects.filter(credenciado=True):
        for i in Inscricao.objects.filter(minicurso1=19):
            if i.user.is_active:
                inscritos.append(i.user)

        context_dictionary = views.get_context_dictionary().copy()

        context_dictionary['usuarios_cadastrados_nao_inscritos'] = inscritos

        emails_enviados = []
        emails_nao_enviados = []
        sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

        for usuario in inscritos:

            from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
            to_email = Email(usuario.email)

            # desafio = 'Não'
            #
            # if desafio_de_programadores:
            #     desafio = 'Sim'

            # conteudo_do_email = """
            #        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            #        <html>
            #        <head>
            #            <meta charset="utf-8"/>
            #            <title>Confira a programação desta terça-feira - VII SECOMP UFSCar</title>
            #        </head>
            #        <body>
            #            <h1>Confira a programação do segundo dia da SECOMP</h1>
            #            <p>Olá #NOME!</p>
            #            <p>Esperamos que tenha gostado deste primeiro dia de SECOMP!</p>
            #            <p>Pois a gente preparou muito mais. Confira a programação para esta terça-feira.</p>
            #
            #            <h2>Enterprise Day e Palestras Empresariais</h2>
            #            <p>
            #            <b>
            #
            #            <ul>
            #            <li>9h - Amdocs: Tendências das comunicações e seu mercado de trabalho</li>
            #            <li>9h50 - Conheça a Tempest! Por Segurança...</li>
            #            <li>10h40 - Daitan Group: 5 Tendências em nossos clientes do Silicon Valley</li>
            #            <li>11h30 - Oportunidades na Monitora!</li>
            #            <li>14h - ASRock e o mercado de TI e Gamer no Brasil</li>
            #            <li>15h - KonkerLabs - Prototipação de IoT com baixo custo</li>
            #             </b>
            #            </ul>
            #
            #            <p>
            #            Além disso, teremos os estandes das empresas no <b>Saguão da Biblioteca Comunitária da UFSCar</b>, das <b>13h às 18h</b>.
            #            </p>
            #
            #            <p><b>Atenção para os locais:</b> as palestras do período da manhã serão no <b>Auditório Mauro Biajiz, no Departamento de Computação</b>, e as palestras do período da tarde serão no <b>Auditório 2 da Biblioteca Comunitária</b>.
            #
            #            </p>
            #
            #            <h2>Processos Seletivos</h2>
            #            <p>Amanhã também teremos dois processos seletivos:
            #            <b>
            #            <ul>
            #            <li>Daitan Group: às 12h45 na sala PPG-CC1 do Departamento de Computação</li>
            #            <li>Monitora: às 17h no Laboratório de Ensino 4 do Departamento de Computação</li>
            #            </ul>
            #            </b>
            #            </p>
            #
            #            <p>Até logo mais :-)<br>
            #            Equipe VII SECOMP UFSCar</p>
            #        </body>
            #        </html>
            #        """

            # conteudo_do_email = """
            #                    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            #                    <html>
            #                    <head>
            #                        <meta charset="utf-8"/>
            #                        <title>Confira a programação desta quarta-feira - VII SECOMP UFSCar</title>
            #                    </head>
            #                    <body>
            #                        <h1>Confira a programação desta quarta-feira na VII SECOMP UFSCar</h1>
            #                        <p>Olá #NOME!</p>
            #                        <p>Aproveitou bastante as atividades de hoje? Pois amanhã tem muito mais. Confira a programação do terceiro dia de SECOMP:</p>
            #
            #                        <h2>Processos Seletivos</h2>
            #                        <p>
            #                        <b>
            #
            #                        <ul>
            #                        <li>9h às 10h: Tempest Security Intelligence</li>
            #                        <li>10h às 12h: Ícaro Technologies</li>
            #                        </ul>
            #                        </b>
            #                        <b>Local</b>: Sala PPG-CC1 do Departamento de Computação.
            #
            #                        <p>
            #
            #                        <h2>Palestras e mesas-redondas</h2>
            #                       <p><b>
            #                         <ul>
            #                        <li>14h: Big Data e Ciência de Dados</li>
            #                        <li>15h: Mesa-Redonda - MesaTech: O dia-a-dia do profissional</li>
            #                        <li>16h30: Criação, detalhamento e execução de projeto de jogo</li>
            #                        <li>17h30: Reflexões sobre Interação Humano-Computador na próxima década</li>
            #
            #                        </ul>
            #                        </b>
            #                        </p>
            #
            #                        <h2>Venda de livros da Novatec com desconto</h2>
            #                        <p>A <b>Editora Novatec</b> estará no período da tarde no Anexo do Anfiteatro Bento Prado Jr. vendendo diversos livros de computação com desconto exclusivo para os participantes da SECOMP. Eles vão aceitar cartão de crédito também o/</p>
            #
            #                        <h2>Aquecimento para o Desafio de Programadores</h2>
            #                        <p>Já fez a inscrição da sua equipe?<br>
            #                        Participe do aquecimento para o desafio, às 19h, no Departamento de Computação.</p>
            #
            #                        <p>Até logo mais :-)<br>
            #                        Equipe VII SECOMP UFSCar</p>
            #                    </body>
            #                    </html>
            #                    """

            # conteudo_do_email = """
            #                                <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            #                                <html>
            #                                <head>
            #                                    <meta charset="utf-8"/>
            #                                    <title>Processo Seletivo Amdocs - VII SECOMP UFSCar</title>
            #                                </head>
            #                                <body>
            #                                    <h1>Processo Seletivo Amdocs na VII SECOMP UFSCar</h1>
            #                                    <p>Olá #NOME!</p>
            #                                    <p>Lembramos que <b>hoje, às 12h30</b>, ocorrerá o Processo Seletivo da Amdocs na Sala PPG-CC1 do Departamento de Computação. Logo em seguida à prova, a Amdocs também já fará entrevistas com os candidatos.</p>
            #
            #                                    <p>Não percam!</p>
            #
            #                                    <p><b>Observação</b>: tragam canetas.</p>
            #
            #                                    <p>Até logo mais :-)<br>
            #                                    Equipe VII SECOMP UFSCar</p>
            #                                </body>
            #                                </html>
            #                                """

            # conteudo_do_email = """
            #                                           <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            #                                           <html>
            #                                           <head>
            #                                               <meta charset="utf-8"/>
            #                                               <title>Workshop de SCRUM & Jira na prática - VII SECOMP UFSCar</title>
            #                                           </head>
            #                                           <body>
            #                                               <h1>Workshop de SCRUM & Jira na prática - VII SECOMP UFSCar</h1>
            #                                               <p>Olá #NOME!</p>
            #                                               <p>Daqui a pouco, <b>às 12h45</b>, começa o workshop de <b>SCRUM & Jira na prática</b>. Será no Laboratório de Ensino 4 do Departamento de Computação.</p>
            #                                               <p>Não se esqueça de trazer 1kg de alimento :-)</p>
            #
            #                                               <p>Até logo mais!<br>
            #                                               Equipe VII SECOMP UFSCar</p>
            #                                           </body>
            #                                           </html>
            #                                           """

            # conteudo_do_email = """
            #                                                       <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            #                                                       <html>
            #                                                       <head>
            #                                                           <meta charset="utf-8"/>
            #                                                           <title>Dê o seu feedback - VII SECOMP UFSCar</title>
            #                                                       </head>
            #                                                       <body>
            #                                                           <h1>Dê o seu feedback - VII SECOMP UFSCar</h1>
            #                                                           <p>Olá #NOME!</p>
            #                                                           <p>Primeiramente, agradecemos a sua participação na sétima edição da SECOMP UFSCar. Sem você, o nosso evento não seria o mesmo!</p>
            #                                                           <p>E é por ser um evento pensado nos participantes que viemos pedir a sua ajuda para tornar o evento ainda melhor! Preencha o nosso formulário de feedback, é rapidinho! É só clicar no link a seguir e deixar sua avaliação, sugestões e críticas.</p>
            #
            #                                                           <p><a href="https://goo.gl/forms/SRxCreOkux5EOVEt2">https://goo.gl/forms/SRxCreOkux5EOVEt2</a>.</p>
            #
            #                                                           <p>Obrigado,<br>
            #                                                           Equipe VII SECOMP UFSCar</p>
            #                                                       </body>
            #                                                       </html>
            #                                                       """

            conteudo_do_email = """
                                                                              <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                                                              <html>
                                                                              <head>
                                                                                  <meta charset="utf-8"/>
                                                                                  <title>Material do minicurso de DevOps - VII SECOMP UFSCar</title>
                                                                              </head>
                                                                              <body>
                                                                                  <h1>Material do minicurso de DevOps - VII SECOMP UFSCar</h1>
                                                                                  <p>Olá #NOME!</p>
                                                                                  <p>A você que participou do minicurso <b>Um dia de DevOps: da concepção à entrega contínua de projetos</b>, segue o material que o ministrante Pedro Brigatto pediu para compartilhar:</p>
                                                                                  <p><a href="https://github.com/pedrobrigatto/UFSCar-SeComp2016">https://github.com/pedrobrigatto/UFSCar-SeComp2016</a>

                                                                                  <p>Atenciosamente,<br>
                                                                                  Equipe VII SECOMP UFSCar</p>
                                                                              </body>
                                                                              </html>
                                                                              """


            # valor_a_pagar = '30,00'
            #
            # if pacote == 'STUDY':
            #     valor_a_pagar = '20,00'
            # elif pacote == 'FREE':
            #     valor_a_pagar = '0,00'

            if usuario.email == 'caio.herrera@outlook.com':
                conteudo_do_email = conteudo_do_email.replace('#NOME', '#ficaCaio')
            else:
                conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

            content = Content("text/html", conteudo_do_email)
            # ready_mail = Mail(from_email, "Confira a programação do segundo dia da SECOMP", to_email, content)
            # ready_mail = Mail(from_email, "Dê o seu feedback - VII SECOMP UFSCar", to_email, content)
            ready_mail = Mail(from_email, "Material do minicurso de DevOps - VII SECOMP UFSCar", to_email, content)

            response = sg.client.mail.send.post(request_body=ready_mail.get())

            print(response.status_code)

            if response.status_code >= 200 and response.status_code <= 300:
                emails_enviados.append(usuario)
            else:
                emails_nao_enviados.append(usuario)

            context_dictionary['emails_enviados'] = emails_enviados
            context_dictionary['emails_nao_enviados'] = emails_nao_enviados

        return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/')


@login_required(login_url='/2016/areadoparticipante/login/')
def enviar_email_minicurso_swift(request):

    # todas_inscricoes = Inscricao.objects.all()
    inscritos = []
    # inscritos.append(User.objects.get(username='jlanssarini@gmail.com'))
    inscritos.append(User.objects.get(username='marcelodeoliveira@outlook.com'))
    # inscritos.append(User.objects.get(username='felipe.pa.i@hotmail.com'))
    # for i in Inscricao.objects.all():
    #     if i.user.is_active:
    #         inscritos.append(i.user)

    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['usuarios_cadastrados_nao_inscritos'] = inscritos

    emails_enviados = []
    emails_nao_enviados = []
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    for usuario in inscritos:

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        to_email = Email(usuario.email)

        # desafio = 'Não'
        #
        # if desafio_de_programadores:
        #     desafio = 'Sim'

        conteudo_do_email = """
               <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
               <html>
               <head>
                   <meta charset="utf-8"/>
                   <title>Mudança de local do minicurso de iOS - VII SECOMP UFSCar</title>
               </head>
               <body>
                   <h1>Mudança de local do minicurso de iOS</h1>
                   <p>Olá #NOME!</p>
                   <p>O minicurso <b>Cr<br>
                   Equipe VII SECOMP UFSCar</p>
               </body>
               </html>
               """
        # valor_a_pagar = '30,00'
        #
        # if pacote == 'STUDY':
        #     valor_a_pagar = '20,00'
        # elif pacote == 'FREE':
        #     valor_a_pagar = '0,00'

        conteudo_do_email = conteudo_do_email.replace('#NOME', usuario.first_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, "Confira a programação do segundo dia da SECOMP", to_email, content)

        response = sg.client.mail.send.post(request_body=ready_mail.get())

        print(response.status_code)

        if response.status_code >= 200 and response.status_code <= 300:
            emails_enviados.append(usuario)
        else:
            emails_nao_enviados.append(usuario)

        context_dictionary['emails_enviados'] = emails_enviados
        context_dictionary['emails_nao_enviados'] = emails_nao_enviados

    return render(request, 'secompufscar_inscricoes/areaadministrativa/enviodemailsresultado.html', context_dictionary)


