from django.apps import AppConfig


class SecompufscarInscricoesConfig(AppConfig):
    name = 'secompufscar_inscricoes'
