from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import uuid
from secompufscar2016.models import Atividade

ANO_ATUAL = '2016'


class Participante(models.Model):
    class Meta:
        verbose_name = 'Participante'
        verbose_name_plural = 'Participantes'
        ordering = ['user']

    def __str__(self):  # __unicode__ on Python 2
        return self.user.first_name + ' ' + self.user.last_name + ' [' + self.user.email + ']'

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Usuário')
    instituicao = models.CharField(max_length=100, verbose_name='Instituição')
    curso = models.CharField(max_length=100, verbose_name='Curso')
    anodeingresso = models.CharField(max_length=4, verbose_name='Ano de ingresso')
    # genero = models.CharField(max_length=1)
    codigodeconfirmacaoemail = models.CharField(max_length=100, verbose_name='Código de confirmação de e-mail')


class Pagamento(models.Model):
    class Meta:
        verbose_name = 'Pagamento'
        verbose_name_plural = 'Pagamentos'

    def __str__(self):  # __unicode__ on Python 2
        return self.metodo_pagamento + ' ' + str(self.valor_pagamento)

    METODO_PAGAMENTO = (
        ('Presencial', 'Presencial'),
        ('Depósito/Transferência', 'Depósito/Transferência'),
        ('PayPal', 'PayPal'),
    )

    # user = models.ManyToManyField(User)
    # ano_pagamento = models.CharField(max_length=4)
    metodo_pagamento = models.CharField(max_length=20, verbose_name='Método de pagamento', choices=METODO_PAGAMENTO)
    autorizacao_pagamento = models.CharField(max_length=50, verbose_name='Autorização do pagamento')  # quem fez a autorização do pagamento
    registro_pagamento = models.CharField(max_length=100, verbose_name='Registro do pagamento')  # código de confirmação do pagamento do PayPal
    valor_pagamento = models.FloatField(verbose_name='Valor do Pagamento')
    referencia_pagamento = models.CharField(max_length=50, verbose_name='Referência do pagamento')  # este pagamento é referente ao que?
    data_hora_pagamento = models.DateTimeField(default=timezone.now, verbose_name='Data/hora do pagamento')


class Inscricao(models.Model):
    class Meta:
        verbose_name = 'Inscrição'
        verbose_name_plural = 'Inscrições'

    def __str__(self):  # __unicode__ on Python 2
        return self.pacote + ' - ' + self.user.first_name + ' ' + self.user.last_name

    PACOTES = (
        ('FULL', 'FULL'),
        ('STUDY', 'STUDY'),
        ('FREE', 'FREE'),
    )

    CAMISETAS = (
        ('P', 'P'),
        ('M', 'M'),
        ('G', 'G'),
        ('GG', 'GG'),
        ('54', '54'),
        ('BabyLook P', 'BabyLook P'),
        ('BabyLook M', 'BabyLook M'),
        ('BabyLook G', 'BabyLook G'),
        ('BabyLook GG', 'BabyLook GG'),
    )

    user = models.ForeignKey(User, verbose_name='Usuário',on_delete=models.CASCADE,)
    ano = models.CharField(max_length=4, default=ANO_ATUAL, verbose_name='Ano da inscrição')
    numero_inscricao = models.CharField(max_length=50, unique=True, verbose_name='Número da inscrição')
    data_hora_inscricao = models.DateTimeField(default=timezone.now, verbose_name='Data/hora da inscrição')
    aceite_compartilhamento_email = models.BooleanField(verbose_name='Aceite de compartilhamento de e-mail')
    pacote = models.CharField(max_length=10, choices=PACOTES, verbose_name='Pacote')
    camiseta = models.CharField(max_length=20, choices=CAMISETAS, verbose_name='Camiseta')
    desafio_de_programadores = models.BooleanField(verbose_name='Desafio de Programadores')
    inscrito_em_equipe = models.BooleanField(verbose_name='Inscrito em equipe')
    churrasco_de_integracao = models.BooleanField(verbose_name='Churrasco de Integração')
    pagamentos = models.ManyToManyField(Pagamento, verbose_name='Pagamentos', blank=True)  # é possível mais de um pagamento na mesma inscrição, por exemplo, quando
    # muda de pacote ou adiciona churrasco
    cracha_emitido = models.BooleanField(verbose_name='Crachá emitido')
    credenciado = models.BooleanField(verbose_name='Credenciado')
    minicurso1 = models.IntegerField(verbose_name='Id do minicurso 1', null=True, blank=True)
    minicurso2 = models.IntegerField(verbose_name='Id do minicurso 2', null=True, blank=True)
    workshops = models.ManyToManyField(Atividade, blank=True)


def voucher():
    return str(uuid.uuid4())[0:12]


class Voucher(models.Model):
    class Meta:
        verbose_name = 'Voucher'
        verbose_name_plural = 'Vouchers'

    def __str__(self):
        return self.referencia + ' ' + str(self.desconto) + '%'

    codigo = models.CharField(max_length=100, unique=True, default=voucher, verbose_name='Código do voucher')
    referencia = models.CharField(max_length=100, verbose_name='Referência')
    desconto = models.FloatField(verbose_name='Porcentagem de desconto')
    usado = models.BooleanField(verbose_name='Usado')


class EquipeDesafio(models.Model):
    class Meta:
        verbose_name = 'Equipe do Desafio de Programadores'
        verbose_name_plural = 'Equipes do Desafio de Programadores'

    def __str__(self):
        return self.nome_equipe

    ano = models.CharField(max_length=4, default=ANO_ATUAL, verbose_name='Ano')
    nome_equipe = models.CharField(max_length=100, verbose_name='Nome da equipe')
    inscrita_por = models.EmailField(verbose_name='Inscrita por')
    integrante1 = models.EmailField(verbose_name='Integrante 1')
    integrante2 = models.EmailField(verbose_name='Integrante 2')
    integrante3 = models.EmailField(verbose_name='Integrante 3')
    data_hora_inscricao = models.DateTimeField(default=timezone.now, verbose_name='Data/hora da inscrição')


class Presenca(models.Model):
    id_atividade = models.ForeignKey(Atividade, verbose_name='Atividade',on_delete=models.CASCADE,)
    inscricao = models.ForeignKey(Inscricao, verbose_name='Usuário',on_delete=models.CASCADE,)
    data_hora_local = models.DateTimeField(default=timezone.now)
    data_hora_remota = models.DateTimeField()