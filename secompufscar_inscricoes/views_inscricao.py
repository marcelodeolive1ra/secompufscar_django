import paypalrestsdk
import uuid
import sendgrid
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from sendgrid.helpers.mail import *
import datetime

from .models import Inscricao, Pagamento

from secompufscar_inscricoes.views import ANO_ATUAL
from secompufscar2016 import views
from secompufscar_inscricoes.views_areaadministrativa import enviar_email_confirmacao_pagamento

# paypalrestsdk.configure({
#     'mode': 'sandbox',
#     'client_id': 'AXGM6JpPWzLIJVBCX98Clu8llg3ftI_uiu9gW1uDylTb5-O6X0L-jW_m8rMcBCisTtbpOjvfUplM3cZB',
#     'client_secret': 'EOKEaMOirbYYV0pJT3ZVRgcTQOV41mqVRO4Rr4PaR06zdSpouMVqMMuxZrjPl9j-HUbDeiC_PWLpDScu'
# })

paypalrestsdk.configure({
    'mode': 'live',
    'client_id': settings.PAYPAL_CLIEND_ID,
    'client_secret': settings.PAYPAL_CLIENT_SECRET
})

# CAMISETAS = ['P', 'M', 'G', 'GG', '54', 'BabyLook P', 'BabyLook M', 'BabyLook G', 'BabyLook GG']
CAMISETAS = ['P', 'M', 'G', 'GG', '54']
PACOTES = ['FREE', 'STUDY', 'FULL']


@login_required(login_url='/2016/areadoparticipante/login/')
def inscricao(request):
    participante_inscrito = False

    try:
        inscricao = request.user.inscricao_set.all()

        if len(inscricao) > 0:
            participante_inscrito = True

        if not participante_inscrito:
            if request.method == 'POST':
                churrasco_de_integracao = False
                desafio_de_programadores = False
                contato_com_parceiros = False

                if request.POST.getlist('desafio_de_programadores'):
                    desafio_de_programadores = True
                if request.POST.getlist('contato_com_parceiros'):
                    contato_com_parceiros = True

                pacote = request.POST['pacote']
                tamanho_da_camiseta = request.POST['tamanho_camiseta']
                numero_inscricao = uuid.uuid4().__str__()

                usuario = request.user

                if pacote not in PACOTES or tamanho_da_camiseta not in CAMISETAS:
                    enviar_email_inscricao_parametros_invalidos(request.user.email, request.user.first_name,
                                                                request.user.last_name, datetime.datetime.now(),
                                                                tamanho_da_camiseta, pacote, desafio_de_programadores)
                    return render(request, 'secompufscar_inscricoes/inscricao/inscricaocomparametrosinvalidos.html',
                                  views.get_context_dictionary())

                if pacote != 'FULL':
                    tamanho_da_camiseta = ''
                    desafio_de_programadores = False

                nova_inscricao = Inscricao(user=usuario, ano=ANO_ATUAL, numero_inscricao=numero_inscricao,
                                           pacote=pacote, desafio_de_programadores=desafio_de_programadores,
                                           churrasco_de_integracao=churrasco_de_integracao,
                                           aceite_compartilhamento_email=contato_com_parceiros,
                                           camiseta=tamanho_da_camiseta,
                                           cracha_emitido=False,
                                           credenciado=False,
                                           inscrito_em_equipe=False)

                nova_inscricao.save()

                valor_a_pagar = 0.

                if pacote == 'FULL':
                    valor_a_pagar = 30.
                elif pacote == 'STUDY':
                    valor_a_pagar = 20.

                valor_ja_pago = 0.

                try:
                    for pagamento in request.user.inscricao_set.filter(ano=ANO_ATUAL).pagamentos.all():
                        valor_ja_pago += pagamento.valor_pagamento
                except:
                    valor_ja_pago = 0.

                valor_a_pagar -= valor_ja_pago

                context_dictionary = views.get_context_dictionary().copy()
                context_dictionary['valor_a_pagar'] = valor_a_pagar

                enviar_email_inscricao_realizada(request.user.email, request.user.first_name, request.user.last_name,
                                                 pacote, desafio_de_programadores, contato_com_parceiros,
                                                 tamanho_da_camiseta)

                return render(request, 'secompufscar_inscricoes/inscricao/inscricaorealizada.html', context_dictionary)

            else:
                context_dictionary = views.get_context_dictionary().copy()
                context_dictionary['nomedousuario'] = request.user.first_name

                return render(request, 'secompufscar_inscricoes/inscricao/inscricao.html', context_dictionary)
        else:  # usuário já está inscrito

            if request.method == 'POST':
                try:
                    pagamentos = request.user.inscricao_set.pagamentos.all()
                    if len(pagamentos) > 0:
                        return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')
                except:
                    pass

                desafio_de_programadores = False
                contato_com_parceiros = False

                if request.POST.getlist('desafio_de_programadores'):
                    desafio_de_programadores = True

                if request.POST.getlist('contato_com_parceiros'):
                    contato_com_parceiros = True

                inscricao_a_ser_alterada = request.user.inscricao_set.get(ano=ANO_ATUAL)
                pacote = request.POST['pacote']
                inscricao_a_ser_alterada.pacote = pacote
                tamanho_da_camiseta = request.POST['tamanho_camiseta']

                if pacote not in PACOTES or tamanho_da_camiseta not in CAMISETAS:
                    print('Identificada inscrição inválida')
                    enviar_email_inscricao_parametros_invalidos(request.user.email, request.user.first_name,
                                                                request.user.last_name, datetime.datetime.now(),
                                                                tamanho_da_camiseta, pacote, desafio_de_programadores)
                    print('Should have sent the email')
                    return render(request, 'secompufscar_inscricoes/inscricao/inscricaocomparametrosinvalidos.html',
                                  views.get_context_dictionary())

                if pacote != 'FULL':
                    print('Verificacao da camiseta. Pacote = ' + pacote)
                    tamanho_da_camiseta = ''
                    desafio_de_programadores = False

                inscricao_a_ser_alterada.camiseta = tamanho_da_camiseta
                inscricao_a_ser_alterada.desafio_de_programadores = desafio_de_programadores
                inscricao_a_ser_alterada.aceite_compartilhamento_email = contato_com_parceiros
                inscricao_a_ser_alterada.save()

                valor_a_pagar = 0.

                if pacote == 'FULL':
                    valor_a_pagar = 30.
                elif pacote == 'STUDY':
                    valor_a_pagar = 20.

                valor_ja_pago = 0.

                try:
                    for pagamento in request.user.inscricao_set.filter(ano=ANO_ATUAL).pagamentos.all():
                        valor_ja_pago += pagamento.valor_pagamento
                except:
                    valor_ja_pago = 0.

                valor_a_pagar -= valor_ja_pago

                context_dictionary = views.get_context_dictionary().copy()
                context_dictionary['valor_a_pagar'] = valor_a_pagar

                enviar_email_inscricao_realizada(request.user.email, request.user.first_name, request.user.last_name,
                                                 pacote, desafio_de_programadores, contato_com_parceiros,
                                                 tamanho_da_camiseta)

                return render(request, 'secompufscar_inscricoes/inscricao/inscricaorealizada.html',
                              context_dictionary)
            else:
                # return
                try:
                    pagamentos = request.user.inscricao_set.last().pagamentos.all()
                    print(len(pagamentos))
                    if len(pagamentos) > 0:
                        return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')
                    # else:
                        # return HttpResponseRedirect('/2016/areadoparticipante/inscricao/')
                        # render(request, 'secompufscar_inscricoes/inscricao/inscricao.html',
                        #               views.get_context_dictionary().copy())
                except:
                    print('Except')
                # return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')

    except:
        return render(request, 'secompufscar_inscricoes/inscricao/inscricao.html', views.get_context_dictionary())
    return render(request, 'secompufscar_inscricoes/inscricao/inscricao.html', views.get_context_dictionary())


@login_required(login_url='/2016/areadoparticipante/login/')
def inscricao_realizada(request):
    return render(request, 'secompufscar_inscricoes/inscricao/inscricaorealizada.html', views.context_dictionary)


@login_required(login_url='/2016/areadoparticipante/login/')
def editar_inscricao(request):
    return render(request, 'secompufscar_inscricoes/inscricao.html', views.context_dictionary)


def enviar_email_inscricao_parametros_invalidos(email, nome, sobrenome, horario, camiseta, pacote, desafio):
    print('Inscricao invalida')
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    from_email = Email('SECOMP UFSCar <ti@secompufscar.com.br>')
    subject = 'Tentativa de Inscrição Inválida'
    to_email = Email('Marcelo <marcelo@secompufscar.com.br>')
    print('OK até aqui')

    desafio_de_programadores = 'Não'

    if desafio:
        desafio_de_programadores = 'Sim'

    conteudo_do_email = """
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta charset="utf-8"/>
                <title>Tentativa de Inscrição Inválida</title>
            </head>
            <body>
                <h1>Tentativa de Inscrição Inválida</h1>

                <p><b>Usuário:</b> #USUARIO<br>
                <b>Nome:</b> #NOME #SOBRENOME<br>
                <b>Horário:</b> #HORARIO<br>
                <b>Pacote:</b> #PACOTE<br>
                <b>Camiseta:</b> #CAMISETA<br>
                <b>Desafio:</b> #DESAFIODEPROGRAMADORES<br>
                </p>
            </body>
            </html>
            """
    conteudo_do_email = conteudo_do_email.replace('#USUARIO', email)
    conteudo_do_email = conteudo_do_email.replace('#NOME', nome)
    conteudo_do_email = conteudo_do_email.replace('#SOBRENOME', sobrenome)
    conteudo_do_email = conteudo_do_email.replace('#PACOTE', pacote)
    conteudo_do_email = conteudo_do_email.replace('#CAMISETA', camiseta)
    conteudo_do_email = conteudo_do_email.replace('#DESAFIODEPROGRAMADORES', desafio_de_programadores)
    conteudo_do_email = conteudo_do_email.replace('#HORARIO', str(horario))

    content = Content("text/html", conteudo_do_email)
    ready_mail = Mail(from_email, subject, to_email, content)

    print('Quase done')
    response = sg.client.mail.send.post(request_body=ready_mail.get())
    print('UHULL')
    print(response)


def enviar_email_inscricao_realizada(usuario, nome, sobrenome, pacote, desafio_de_programadores, contato_com_parceiros,
                                     tamanho_da_camiseta):
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
    subject = 'Inscrição Realizada - VII SECOMP UFSCar'
    to_email = Email(usuario)

    desafio = 'Não'

    if desafio_de_programadores:
        desafio = 'Sim'

    if pacote != 'FULL':
        tamanho_da_camiseta = 'O pacote ' + pacote + ' não inclui camiseta.'
        desafio = 'O pacote ' + pacote + ' não inclui o Desafio de Programadores.'

    if contato_com_parceiros:
        contato_com_parceiros = 'Sim'
    else:
        contato_com_parceiros = 'Não'

    conteudo_do_email = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <meta charset="utf-8"/>
            <title>Inscrição Realizada - VII SECOMP UFSCar</title>
        </head>
        <body>
            <h1>Inscrição Realizada - VII SECOMP UFSCar</h1>
            <p>Olá #NOME!</p>
            <p>Sua inscrição na VII SECOMP UFSCar foi realizada com sucesso. Confira os dados de sua inscrição:</p>
            <p><ul>
            <li><b>Nome Completo:</b> #NOME #SOBRENOME</li>
            <li><b>Pacote:</b> #PACOTE</li>
            <li><b>Camiseta:</b> #CAMISETA</li>
            <li><b>Desafio de Programadores:</b> #DESAFIODEPROGRAMADORES</li>
            <li><b>Contato com parceiros da SECOMP:</b> #CONTATOCOMPARCEIROS</li>
            <li><b>Valor a pagar:</b> R$ #VALOR</li>
            </ul>
            </p>

            <p>Em caso de problemas, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">
            contato@secompufscar.com.br</a> informando o e-mail que você utilizou na inscrição.</p>
            <p>Nos vemos em breve!<br>
            Equipe VII SECOMP UFSCar</p>
        </body>
        </html>
        """
    valor_a_pagar = '30,00'

    if pacote == 'STUDY':
        valor_a_pagar = '20,00'
    elif pacote == 'FREE':
        valor_a_pagar = '0,00'

    conteudo_do_email = conteudo_do_email.replace('#NOME', nome)
    conteudo_do_email = conteudo_do_email.replace('#SOBRENOME', sobrenome)
    conteudo_do_email = conteudo_do_email.replace('#PACOTE', pacote)
    conteudo_do_email = conteudo_do_email.replace('#CAMISETA', tamanho_da_camiseta)
    conteudo_do_email = conteudo_do_email.replace('#DESAFIODEPROGRAMADORES', desafio)
    conteudo_do_email = conteudo_do_email.replace('#CONTATOCOMPARCEIROS', contato_com_parceiros)
    conteudo_do_email = conteudo_do_email.replace('#VALOR', valor_a_pagar)

    content = Content("text/html", conteudo_do_email)
    ready_mail = Mail(from_email, subject, to_email, content)

    sg.client.mail.send.post(request_body=ready_mail.get())


############################################################################
# Pagamento
############################################################################

@login_required(login_url='/2016/areadoparticipante/login/')
def pagamento(request):
    pacote = request.user.inscricao_set.last().pacote

    valor_a_pagar = 0.

    if pacote == 'FULL':
        valor_a_pagar = 30.
    elif pacote == 'STUDY':
        valor_a_pagar = 20.

    valor_ja_pago = 0.

    for pagamento in request.user.inscricao_set.last().pagamentos.all():
        valor_ja_pago += pagamento.valor_pagamento

    valor_a_pagar -= valor_ja_pago

    context_dictionary = views.get_context_dictionary().copy()
    context_dictionary['valor_a_pagar'] = valor_a_pagar

    context_dictionary['aplicar_cupom'] = False

    return render(request, 'secompufscar_inscricoes/inscricao/pagamento.html', context_dictionary)


@login_required(login_url='/2016/areadoparticipante/login/')
def redirecionar_pagamento(request):

    pacote = request.user.inscricao_set.last().pacote  # qual o pacote do usuário?

    valor_a_pagar = 0.

    if pacote == 'FULL':
        valor_a_pagar = 30.
    elif pacote == 'STUDY':
        valor_a_pagar = 20.

    valor_ja_pago = 0.

    for pagamento in request.user.inscricao_set.last().pagamentos.all():
        valor_ja_pago += pagamento.valor_pagamento

    valor_a_pagar -= valor_ja_pago
    #
    # print(settings.PAYPAL_CLIEND_ID)
    # print(settings.PAYPAL_CLIENT_SECRET)

    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"},
        "redirect_urls": {
            "return_url": "https://secompufscar.com.br/2016/areadoparticipante/inscricao/confirmacao",
            "cancel_url": "https://secompufscar.com.br/2016/areadoparticipante/inscricao/pagamento/pagamentocancelado"},
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": 'Pacote ' + pacote + ' VII SECOMP UFSCar',
                    # "sku": "item",
                    "price": valor_a_pagar,
                    "currency": "BRL",
                    "quantity": 1},
                ]},
            "amount": {
                "total": valor_a_pagar,
                "currency": "BRL"},
            "description": "Inscrição VII Semana da Computação da UFSCar"}]})

    if payment.create():
        print("Payment created successfully" + str(payment))
        print(payment['links'][1]['href'])

        # Redireciona
        return HttpResponseRedirect(payment['links'][1]['href'])

    else:
        print(payment.error)

    return HttpResponseRedirect('/2016/areadoparticipante/inscricao/pagamento/')


def pagamento_cancelado_paypal(request):
    context_dictionary = views.get_context_dictionary().copy()
    pacote = request.user.inscricao_set.last().pacote

    valor_a_pagar = 0.

    if pacote == 'FULL':
        valor_a_pagar = 30.
    elif pacote == 'STUDY':
        valor_a_pagar = 20.

    valor_ja_pago = 0.

    for pagamento in request.user.inscricao_set.last().pagamentos.all():
        valor_ja_pago += pagamento.valor_pagamento

    valor_a_pagar -= valor_ja_pago

    context_dictionary['valor_a_pagar'] = valor_a_pagar

    if valor_a_pagar != 0:
        return render(request, 'secompufscar_inscricoes/inscricao/pagamentocancelado.html', context_dictionary)
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/inscricao/pagamento/')


@login_required(login_url='/2016/areadoparticipante/login/')
def confirmacao_de_pagamento_paypal(request):
    url = request.get_raw_uri()
    url_splitted = url.split('?')
    # url_splitted2 = url_splitted.split('&')
    print(url)
    print(url_splitted)
    data_paypal = url_splitted[1]
    data_paypal_splitted = data_paypal.split('&')
    print(data_paypal_splitted)
    paymentId = data_paypal_splitted[0].replace('paymentId=', '')
    print('PaymentId = ' + paymentId)
    payerId = data_paypal_splitted[2]
    print('PayerId = ' + payerId)
    payerIdSplitted = payerId.split('=')
    print(payerIdSplitted)

    payment = paypalrestsdk.Payment.find(paymentId)
    #
    if payment.execute({"payer_id": payerIdSplitted[1]}):
        print("Payment execute successfully")
        print(payment)
        pagamento_paypal = Pagamento(metodo_pagamento='PayPal', autorizacao_pagamento='Sistema',
                                     registro_pagamento=paymentId,
                                     valor_pagamento=float(payment.transactions[0].amount.total),
                                     referencia_pagamento='Inscrição pacote ' + request.user.inscricao_set.last().pacote)
        pagamento_paypal.save()

        inscricao = request.user.inscricao_set.last()
        inscricao.pagamentos.add(pagamento_paypal)
        inscricao.save()

        if inscricao.desafio_de_programadores:
            desafio = 'Sim'
        else:
            desafio = 'Não'

        if inscricao.aceite_compartilhamento_email:
            contato = 'Sim'
        else:
            contato = 'Não'

        enviar_email_confirmacao_pagamento(request.user.email, request.user.first_name, request.user.last_name, 'Pagamento via PayPal', pagamento_paypal.valor_pagamento, inscricao.pacote, inscricao.camiseta, desafio, contato)

    else:
        print(payment.error)  # Error Hash

    return render(request, 'secompufscar_inscricoes/inscricao/confirmacaodepagamentopaypal.html',
                  views.context_dictionary)


from secompufscar_inscricoes.models import Voucher


@login_required(login_url='/2016/areadoparticipante/login/')
def aplicar_cupom_de_desconto(request):
    if request.method == 'POST':
        cupom = request.POST['cupom_de_desconto']

        try:
            cupom_de_desconto = Voucher.objects.get(codigo=cupom)  # o cupom existe?
            print('Achou o cupom')

            pacote = request.user.inscricao_set.last().pacote  # qual o pacote do usuário?

            valor = 30.
            if pacote == 'STUDY':
                valor = 20.
            elif pacote == 'FREE':
                valor = 0.

            valor = valor * (cupom_de_desconto.desconto / 100)

            if not cupom_de_desconto.usado:  # cupom é válido e ainda não foi utilizado
                print('Cupom não usado ainda')
                pagamento_cupom = Pagamento(metodo_pagamento='Cupom', autorizacao_pagamento='Sistema',
                                            registro_pagamento=cupom, valor_pagamento=valor,
                                            referencia_pagamento='Inscrição pacote ' + pacote)

                pagamento_cupom.save()

                inscricao = request.user.inscricao_set.last()

                inscricao.pagamentos.add(pagamento_cupom)
                inscricao.save()
                cupom_de_desconto.usado = True
                cupom_de_desconto.save()

                context_dictionary = views.context_dictionary.copy()
                context_dictionary['cupom_valido'] = True
                context_dictionary['cupom_ja_utilizado'] = False
                context_dictionary['cupom'] = cupom_de_desconto
                context_dictionary['aplicar_cupom'] = True

                pacote = request.user.inscricao_set.last().pacote

                valor_a_pagar = 0.

                if pacote == 'FULL':
                    valor_a_pagar = 30.
                elif pacote == 'STUDY':
                    valor_a_pagar = 20.

                valor_ja_pago = 0.

                for pagamento in request.user.inscricao_set.last().pagamentos.all():
                    valor_ja_pago += pagamento.valor_pagamento

                valor_a_pagar -= valor_ja_pago

                context_dictionary['valor_a_pagar'] = valor_a_pagar

                return render(request, 'secompufscar_inscricoes/inscricao/pagamento.html', context_dictionary)
            else:  # cupom já foi utilizado anteriormente
                context_dictionary = views.context_dictionary.copy()
                context_dictionary['cupom_valido'] = True
                context_dictionary['cupom_ja_utilizado'] = True
                context_dictionary['aplicar_cupom'] = True

                pacote = request.user.inscricao_set.last().pacote
                valor_a_pagar = 0.

                if pacote == 'FULL':
                    valor_a_pagar = 30.
                elif pacote == 'STUDY':
                    valor_a_pagar = 20.

                valor_ja_pago = 0.

                pagamentos = request.user.inscricao_set.last().pagamentos.all()
                for pagamento in pagamentos:
                    valor_ja_pago += pagamento.valor_pagamento

                valor_a_pagar -= valor_ja_pago

                context_dictionary['valor_a_pagar'] = valor_a_pagar

                return render(request, 'secompufscar_inscricoes/inscricao/pagamento.html', context_dictionary)


        except:

            pacote = request.user.inscricao_set.last().pacote  # qual o pacote do usuário?

            valor_a_pagar = 0.

            if pacote == 'FULL':
                valor_a_pagar = 30.
            elif pacote == 'STUDY':
                valor_a_pagar = 20.

            valor_ja_pago = 0.

            for pagamento in request.user.inscricao_set.last().pagamentos.all():
                valor_ja_pago += pagamento.valor_pagamento

            valor_a_pagar -= valor_ja_pago

            context_dictionary = views.context_dictionary.copy()
            context_dictionary['valor_a_pagar'] = valor_a_pagar
            context_dictionary['aplicar_cupom'] = True
            context_dictionary['cupom_invalido'] = True
            return render(request, 'secompufscar_inscricoes/inscricao/pagamento.html', context_dictionary)