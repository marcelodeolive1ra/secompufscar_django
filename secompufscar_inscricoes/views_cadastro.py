import uuid
import sendgrid
from django.conf import settings
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from sendgrid.helpers.mail import *

from secompufscar2016 import views
from .models import Participante
from secompufscar_inscricoes.views import ANO_ATUAL


############################################################################
# Cadastro de contas
############################################################################

def cadastro(request):
    if request.method == 'POST':
        context_dictionary = views.get_context_dictionary().copy()

        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        instituicao = request.POST['instituicao']
        curso = request.POST['curso']
        anodeingresso = request.POST['anodeingresso']

        if anodeingresso != '' and (anodeingresso < '2001' or anodeingresso > ANO_ATUAL):
            print('Ano invalido')
            return render(request, 'secompufscar_inscricoes/cadastro/cadastrocomparametrosinvalidos.html',
                          views.get_context_dictionary())

        if instituicao == 'Outra':
            instituicao = request.POST['outrainstituicao']
        else:
            if instituicao != 'UFSCar - campus São Carlos' and instituicao != 'UFSCar - campus Sorocaba' and instituicao != 'UFSCar - campus Araras' and instituicao != 'UFSCar - campus Lagoa do Sino' and instituicao != 'USP - São Carlos' and instituicao != 'UNICEP' and instituicao != 'IFSP - São Carlos':
                return render(request, 'secompufscar_inscricoes/cadastro/cadastrocomparametrosinvalidos.html',
                              views.get_context_dictionary())

        if curso == 'Outro':
            curso = request.POST['outrocurso']
        else:
            if curso != 'Ciência da Computação' and curso != 'Engenharia de Computação' and curso != 'Análise e Desenvolvimento de Sistemas' and curso != 'Sistemas de Informação':
                print('Curso inválido')
                return render(request, 'secompufscar_inscricoes/cadastro/cadastrocomparametrosinvalidos.html',
                              views.get_context_dictionary())

        codigo_de_confirmacao = uuid.uuid4()
        print(codigo_de_confirmacao)

        try:
            # username, requerido pelo User model do Django, está sendo usado como o próprio e-mail, para evitar a
            # necessidade de criar um Authentication Backend específico
            novo_usuario = User.objects.create_user(username=email, email=email, password=password,
                                                    first_name=first_name,
                                                    last_name=last_name)
            novo_usuario.is_active = False
            novo_usuario.save()
            novo_usuario_dadosadicionais = Participante(user=novo_usuario, instituicao=instituicao, curso=curso,
                                                        anodeingresso=anodeingresso,
                                                        codigodeconfirmacaoemail=codigo_de_confirmacao)
            novo_usuario_dadosadicionais.save()
            enviar_email_de_confirmacao_de_cadastro(first_name, email, codigo_de_confirmacao)

            context_dictionary['email_cadastrado'] = email

            # redirect to a new URL:
            return HttpResponseRedirect('/2016/areadoparticipante/cadastro/confirmaremail/')

        except IntegrityError:  # E-mail já está cadastrado
            print(IntegrityError)
            context_dictionary['integrityerror'] = True
            return render(request, 'secompufscar_inscricoes/cadastro/cadastro.html', context_dictionary)

    return render(request, 'secompufscar_inscricoes/cadastro/cadastro.html', views.get_context_dictionary())


def reenviar_email_de_confirmacao_de_cadastro(request):
    if request.method == 'POST':
        email = request.POST['email']

        try:
            user = User.objects.get(username=email)
            codigo_de_confirmacao = user.participante.codigodeconfirmacaoemail
            enviar_email_de_confirmacao_de_cadastro(user.first_name, email, codigo_de_confirmacao)
            context_dictionary = views.get_context_dictionary().copy()
            context_dictionary['email_reenviado'] = True
            return render(request, 'secompufscar_inscricoes/cadastro/confirmacaodeemail.html',
                          context_dictionary)
        except:
            return render(request, 'secompufscar_inscricoes/cadastro/confirmacaodeemail.html',
                          views.get_context_dictionary())

    return render(request, 'secompufscar_inscricoes/cadastro/confirmacaodeemail.html', views.get_context_dictionary())


def enviar_email_de_confirmacao_de_cadastro(first_name, email, codigo_de_confirmacao):
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

    from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
    subject = 'Confirme seu cadastro na SECOMP UFSCar'
    to_email = Email(email)

    conteudo_do_email = """
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
        <meta charset="utf-8"/>
        <title>Confirmação de cadastro na SECOMP UFSCar</title>
    </head>
    <body>
        <h1>Código de confirmação</h1>
        <p>Olá #NOME!</p>
        <p>Seu cadastro na SECOMP UFSCar foi realizado com sucesso.</p>

        <p>Para ativá-lo, utilize o seguinte código em nosso site:
        <br><b>#CODIGODECONFIRMACAO</b></p>

        <p>Ou então, clique no link a seguir:</p>
        <a href="https://secompufscar.com.br/2016/areadoparticipante/cadastro/confirmarcadastro/?#CODIGODECONFIRMACAO">
        https://secompufscar.com.br/2016/areadoparticipante/cadastro/confirmarcadastro/?#CODIGODECONFIRMACAO</a>
        </p>

        <p>Obrigado,<br>
        Equipe VII SECOMP UFSCar</p>
    </body>
    </html>
    """

    conteudo_do_email = conteudo_do_email.replace('#NOME', first_name)
    conteudo_do_email = conteudo_do_email.replace('#CODIGODECONFIRMACAO', str(codigo_de_confirmacao))

    content = Content("text/html", conteudo_do_email)
    ready_mail = Mail(from_email, subject, to_email, content)

    sg.client.mail.send.post(request_body=ready_mail.get())

    # Adicionar verificação do código de resposta. Se código for de erro, enviar e-mail para administrador


def confirmar_cadastro(request):
    url = request.get_raw_uri()
    url_splitted = url.split('?')

    codigo_de_confirmacao = url_splitted[1]
    participante = Participante.objects.filter(codigodeconfirmacaoemail=codigo_de_confirmacao)

    context_dictionary = views.get_context_dictionary()

    if participante.count() > 0:
        user = User.objects.get(participante=participante)
        user.is_active = True
        user.save()
        context_dictionary['email_confirmado'] = True
        context_dictionary['usuario_inativo'] = False
    else:
        context_dictionary['codigo_invalido'] = True

    return render(request, 'secompufscar_inscricoes/cadastro/confirmacaodeemail.html', context_dictionary)


def confirmar_email(request):
    context_dictionary = views.get_context_dictionary().copy()
    context_dictionary['email_confirmado'] = False
    context_dictionary['codigo_invalido'] = False
    if request.method == 'POST':
        codigodeconfirmacao = request.POST['confirmacaodeemail']
        participante = Participante.objects.filter(codigodeconfirmacaoemail=codigodeconfirmacao)

        if participante.count() > 0:
            user = User.objects.get(participante=participante)
            user.is_active = True
            user.save()
            context_dictionary['email_confirmado'] = True
            context_dictionary['usuario_inativo'] = False
        else:
            context_dictionary['codigo_invalido'] = True

    return render(request, 'secompufscar_inscricoes/cadastro/confirmacaodeemail.html', context_dictionary)
