from django.contrib import admin
from .models import Participante, Voucher, Inscricao, Pagamento, EquipeDesafio, Presenca

# Register your models here.
admin.site.register(Participante)
admin.site.register(Voucher)
admin.site.register(Inscricao)
admin.site.register(Pagamento)
admin.site.register(EquipeDesafio)
admin.site.register(Presenca)