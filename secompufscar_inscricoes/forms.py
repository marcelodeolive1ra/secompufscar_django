from django import forms

class FormularioDeCadastro(forms.Form):
    email = forms.EmailField(label='Seu e-mail')
    confirmacaodeemail = forms.EmailField()
    first_name = forms.CharField(label='Primeiro nome')
    last_name = forms.CharField(label='Sobrenome')
    password = forms.PasswordInput()
    confirmacaodesenha = forms.PasswordInput()
    instituicao = forms.CharField(label='Instituição', required=False)
    curso = forms.CharField(label='Curso', required=False)
    outrocurso = forms.CharField(required=False)
    outrainstituicao = forms.CharField(required=False)
    # aceite = forms.BooleanField()
    anodeingresso = forms.CharField(required=False)

class FormularioDeLogin(forms.Form):
    email = forms.EmailField(label='Seu e-mail')
    password = forms.PasswordInput()

class FormularioDeInscricao2016(forms.Form):
    pacote = forms.CharField(required=True)
    tamanho_camiseta = forms.CharField(required=False)
    desafio_de_programadores = forms.BooleanField(required=False)
    churrasco_de_integracao = forms.BooleanField(required=False)
    contato_com_parceiros = forms.BooleanField(required=True)

