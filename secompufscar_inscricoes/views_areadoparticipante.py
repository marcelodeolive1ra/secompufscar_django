from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from secompufscar2016 import views
from .models import EquipeDesafio, Presenca
from secompufscar_inscricoes.views import ANO_ATUAL, SUPER_ADMINS, AUTORIZADORES_PAGAMENTO_PRESENCIAL
from secompufscar2016.models import Atividade

import sendgrid
from sendgrid.helpers.mail import *
from django.conf import settings


############################################################################
# Autenticação
############################################################################

def login(request):
    context_dictionary = views.get_context_dictionary().copy()
    context_dictionary['disabled_account'] = False
    context_dictionary['invalid_login'] = False
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']

        try:
            user = User.objects.get(username=username)
            if user.is_active:
                authenticated_user = authenticate(username=username, password=password)

                if authenticated_user is not None:
                    django_login(request, user)
                    return HttpResponseRedirect('/2016/areadoparticipante/')  # usuário autenticado e ativo
                else:
                    context_dictionary['invalid_login'] = True

            else:
                context_dictionary['disabled_account'] = True
                return render(request, 'secompufscar_inscricoes/areadoparticipante/login.html', context_dictionary)
        except:
            context_dictionary['invalid_login'] = True
    else:
        if request.user.is_authenticated and request.user.is_active:
            return HttpResponseRedirect('/2016/areadoparticipante/')

    return render(request, 'secompufscar_inscricoes/areadoparticipante/login.html', context_dictionary)


def logout(request):
    if request.user.is_authenticated:
        context_dictionary = views.get_context_dictionary().copy()
        context_dictionary['nomedousuario'] = request.user.first_name
        django_logout(request)
        return render(request, 'secompufscar_inscricoes/areadoparticipante/logout.html', context_dictionary)

    return HttpResponseRedirect('/2016/areadoparticipante/login/')


############################################################################
# Área do participante
############################################################################

@login_required(login_url='/2016/areadoparticipante/login/')
def areadoparticipante(request):
    try:
        if request.user.inscricao_set.count() > 0:
            inscricao = request.user.inscricao_set.last()

            if inscricao.ano == ANO_ATUAL:
                return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')
    except:
        return HttpResponseRedirect('/2016/areadoparticipante/inscricao/')

    return HttpResponseRedirect('/2016/areadoparticipante/inscricao/')


def write_file(file_name, bs):
    of = open(file_name, 'wb')
    of.write(bs)
    of.close()

    # pdf417 = CandyBarPdf417()
    # bs = pdf417.encode(request.user.inscricao_set.last().numero_inscricao)
    # write_file(
    #     '/Users/marcelodeoliveiradasilva/PycharmProjects/secompufscar_django/secompufscar_inscricoes/static/secompufscar_inscricoes/codes/' + request.user.inscricao_set.last().numero_inscricao + '.png',
    #     bs)
    #
    # context_dictionary[
    #     'pdf147'] = 'secompufscar_inscricoes/codes/' + request.user.inscricao_set.last().numero_inscricao + '.png'

import datetime

#REVISAR ESTA FUNÇÃO
@login_required(login_url='/2016/areadoparticipante/login/')
def dashboard(request):
    context_dictionary = views.get_context_dictionary().copy()

    context_dictionary['minicursos'] = Atividade.objects.filter(tipo_atividade='Minicurso').filter(
        esta_ativa_atividade=True)
    context_dictionary['workshops'] = Atividade.objects.filter(tipo_atividade='Workshop').filter(
        esta_ativa_atividade=True)
    context_dictionary['palestras'] = Atividade.objects.filter(tipo_atividade='Palestra').filter(
        esta_ativa_atividade=True)
    context_dictionary['mesasredondas'] = Atividade.objects.filter(tipo_atividade='Mesa-redonda').filter(esta_ativa_atividade=True)

    context_dictionary['horario'] = datetime.datetime.now()

    if request.user.email in SUPER_ADMINS:
        context_dictionary['admin'] = True

    if request.user.email in AUTORIZADORES_PAGAMENTO_PRESENCIAL:
        context_dictionary['autorizador_pagamento_presencial'] = True

    if request.user.inscricao_set.last().inscrito_em_equipe:
        try:
            e = EquipeDesafio.objects.get(integrante1=request.user.email)
            context_dictionary['equipe'] = e.nome_equipe
            context_dictionary['inscrita_por'] = User.objects.get(
                email=e.inscrita_por).first_name + ' ' + User.objects.get(email=e.inscrita_por).last_name
            integrante1 = User.objects.get(username=e.integrante1)
            integrante2 = User.objects.get(username=e.integrante2)
            integrante3 = User.objects.get(username=e.integrante3)
            context_dictionary['integrante1'] = integrante1.first_name + ' ' + integrante1.last_name + ' <' + e.integrante1 + '>'
            context_dictionary['integrante2'] = integrante2.first_name + ' ' + integrante2.last_name + ' <' + e.integrante2 + '>'
            context_dictionary['integrante3'] = integrante3.first_name + ' ' + integrante3.last_name + ' <' + e.integrante3 + '>'
        except:
            pass

        try:
            e = EquipeDesafio.objects.get(integrante2=request.user.email)
            context_dictionary['equipe'] = e.nome_equipe
            context_dictionary['inscrita_por'] = User.objects.get(
                email=e.inscrita_por).first_name + ' ' + User.objects.get(email=e.inscrita_por).last_name
            integrante1 = User.objects.get(username=e.integrante1)
            integrante2 = User.objects.get(username=e.integrante2)
            integrante3 = User.objects.get(username=e.integrante3)
            context_dictionary[
                'integrante1'] = integrante1.first_name + ' ' + integrante1.last_name + ' <' + e.integrante1 + '>'
            context_dictionary[
                'integrante2'] = integrante2.first_name + ' ' + integrante2.last_name + ' <' + e.integrante2 + '>'
            context_dictionary[
                'integrante3'] = integrante3.first_name + ' ' + integrante3.last_name + ' <' + e.integrante3 + '>'

        except:
            pass

        try:
            e = EquipeDesafio.objects.get(integrante3=request.user.email)
            context_dictionary['equipe'] = e.nome_equipe
            context_dictionary['inscrita_por'] = User.objects.get(
                email=e.inscrita_por).first_name + ' ' + User.objects.get(email=e.inscrita_por).last_name
            integrante1 = User.objects.get(username=e.integrante1)
            integrante2 = User.objects.get(username=e.integrante2)
            integrante3 = User.objects.get(username=e.integrante3)
            context_dictionary[
                'integrante1'] = integrante1.first_name + ' ' + integrante1.last_name + ' <' + e.integrante1 + '>'
            context_dictionary[
                'integrante2'] = integrante2.first_name + ' ' + integrante2.last_name + ' <' + e.integrante2 + '>'
            context_dictionary[
                'integrante3'] = integrante3.first_name + ' ' + integrante3.last_name + ' <' + e.integrante3 + '>'

        except:
            pass

    pacote = request.user.inscricao_set.last().pacote

    valor_a_pagar = 0.

    if pacote == 'FULL':
        valor_a_pagar = 30.
    elif pacote == 'STUDY':
        valor_a_pagar = 20.

    valor_ja_pago = 0.
    for pagamento in request.user.inscricao_set.last().pagamentos.all():
        valor_ja_pago += pagamento.valor_pagamento

    valor_a_pagar -= valor_ja_pago

    inscricao = request.user.inscricao_set.last()
    if inscricao.minicurso1 is not None:
        context_dictionary['minicurso_inscrito'] = inscricao.minicurso1

    context_dictionary['workshops_inscritos'] = []
    for w in inscricao.workshops.all():
        print(str(w.id))
        context_dictionary['workshops_inscritos'].append(w.id)

    p = Presenca.objects.filter(inscricao=inscricao)
    presencas = []
    print(p)
    for p1 in p:
        print(p1.id_atividade.id)
        presencas.append(p1.id_atividade.id)

    context_dictionary['presencas'] = presencas

    if pacote == 'FULL' and valor_ja_pago == 30. or pacote == 'STUDY' and valor_ja_pago == 20 or pacote == 'FREE':
        return render(request, 'secompufscar_inscricoes/areadoparticipante/dashboard.html', context_dictionary)
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/inscricao/pagamento/')


@login_required(login_url='/2016/areadoparticipante/login/')
def editar_desafio(request):
    if not request.user.inscricao_set.last().inscrito_em_equipe:
        if request.method == 'POST':
            inscricao = request.user.inscricao_set.last()
            if inscricao.pacote == 'FULL':
                inscricao.desafio_de_programadores = not inscricao.desafio_de_programadores
                inscricao.save()
    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')


@login_required(login_url='/2016/areadoparticipante/login/')
def editar_churrasco(request):
    if request.method == 'POST':
        inscricao = request.user.inscricao_set.last()
        print('Editando churrasco = ' + str(inscricao.churrasco_de_integracao))
        inscricao.churrasco_de_integracao = not inscricao.churrasco_de_integracao
        inscricao.save()
        print('Editado = ' + str(inscricao.churrasco_de_integracao))
        return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')

    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')


@login_required(login_url='/2016/areadoparticipante/login/')
def editar_cadastro(request):
    if request.method == 'POST':
        user = request.user

        if request.POST['first_name'] != '':
            print('first_name')
            user.first_name = request.POST['first_name']
            user.save()

        if request.POST['last_name'] != '':
            print('last_name')
            user.last_name = request.POST['last_name']
            user.save()

        participante = request.user.participante
        if request.POST['anodeingresso'] != '':
            print('anodeingresso')
            participante.anodeingresso = request.POST['anodeingresso']
            participante.save()

        try:
            if request.POST['instituicao'] != '':
                print('instituicao')
                if request.POST['instituicao'] == 'Outra':
                    if request.POST['outrainstituicao'] != '':
                        print('outra instituicao')
                        participante.instituicao = request.POST['outrainstituicao']
                        participante.save()
                else:
                    participante.instituicao = request.POST['instituicao']
                    participante.save()
        except:
            pass

        try:
            if request.POST['curso'] != '':
                print('curso')
                if request.POST['curso'] == 'Outro':
                    if request.POST['outrocurso'] != '':
                        participante.curso = request.POST['outrocurso']
                        participante.save()
                else:
                    participante.curso = request.POST['curso']
                    participante.save()
        except:
            pass

        return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')

    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')


@login_required(login_url='/2016/areadoparticipante/login/')
def alterar_senha(request):
    if request.method == 'POST':
        user = request.user

        senha = request.POST['password']
        user.set_password(senha)
        user.save()
        django_logout(request)
        authenticated_user = authenticate(username=user.username, password=senha)
        django_login(request, authenticated_user)

        context_dictionary = views.get_context_dictionary().copy()
        context_dictionary['minicursos'] = Atividade.objects.filter(tipo_atividade='Minicurso').filter(
            esta_ativa_atividade=True)
        context_dictionary['workshops'] = Atividade.objects.filter(tipo_atividade='Workshop').filter(
            esta_ativa_atividade=True)
        context_dictionary['palestras'] = Atividade.objects.filter(tipo_atividade='Palestra').filter(
            esta_ativa_atividade=True)
        context_dictionary['senha_alterada'] = True

        return render(request, 'secompufscar_inscricoes/areadoparticipante/dashboard.html', context_dictionary)

    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/')


@login_required(login_url='/2016/areadoparticipante/login/')
def inscricao_equipe_desafio(request):
    if request.user.inscricao_set.last().desafio_de_programadores and not request.user.inscricao_set.last().inscrito_em_equipe:
        if request.method == 'POST':
            context_dictionary = views.get_context_dictionary().copy()
            nome_da_equipe = request.POST['nome_da_equipe']
            # nome_da_equipe = nome_da_equipe.lower()

            if EquipeDesafio.objects.filter(nome_equipe__iexact=nome_da_equipe).count() > 0:
                context_dictionary['nome_de_equipe_ja_cadastrado'] = True
                context_dictionary['nome_ja_cadastrado'] = nome_da_equipe
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            integrante2 = request.POST['integrante2']
            integrante3 = request.POST['integrante3']

            usuarios_com_pacote_invalido = []
            usuarios_com_desafio_desabilitado = []
            usuarios_nao_cadastrados = []
            usuarios_pendentes_de_pagamento = []
            usuarios_ja_inscritos_em_equipe = []
            usuarios_nao_inscritos_no_evento = []

            if nome_da_equipe == '' or integrante2 == '' or integrante3 == '':
                context_dictionary['formulario_invalido'] = True
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            if request.user.email == integrante2 or request.user.email == integrante3 or integrante2 == integrante3:
                context_dictionary['usuarios_repetidos'] = True
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            try:
                user2 = User.objects.get(username=integrante2)
            except:
                usuarios_nao_cadastrados.append(integrante2)

            try:
                user3 = User.objects.get(username=integrante3)
            except:
                usuarios_nao_cadastrados.append(integrante3)

            if len(usuarios_nao_cadastrados) > 0:
                context_dictionary['usuarios_nao_cadastrados'] = usuarios_nao_cadastrados
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            user1 = request.user
            user2 = User.objects.get(username=integrante2)
            user3 = User.objects.get(username=integrante3)

            try:
                inscricao1 = user1.inscricao_set.get(ano=ANO_ATUAL)
            except:
                usuarios_nao_inscritos_no_evento.append(user1.email)

            try:
                inscricao2 = user2.inscricao_set.get(ano=ANO_ATUAL)
            except:
                usuarios_nao_inscritos_no_evento.append(user2.email)

            try:
                inscricao3 = user3.inscricao_set.get(ano=ANO_ATUAL)
            except:
                usuarios_nao_inscritos_no_evento.get(ano=ANO_ATUAL)

            if len(usuarios_nao_inscritos_no_evento) > 0:
                context_dictionary['usuarios_nao_inscritos_no_evento'] = usuarios_nao_inscritos_no_evento
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            if inscricao1.pacote != 'FULL':
                usuarios_com_pacote_invalido.append(user1.email)
            if inscricao2.pacote != 'FULL':
                usuarios_com_pacote_invalido.append(user2.email)
            if inscricao3.pacote != 'FULL':
                usuarios_com_pacote_invalido.append(user3.email)

            if len(usuarios_com_pacote_invalido) > 0:
                context_dictionary['usuarios_com_pacote_invalido'] = usuarios_com_pacote_invalido
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            valor_a_pagar = 30.
            valor_ja_pago = 0.

            pagamentos = user1.inscricao_set.last().pagamentos.all()

            for pagamento in pagamentos:
                valor_ja_pago += pagamento.valor_pagamento

            if valor_ja_pago < valor_a_pagar:
                usuarios_pendentes_de_pagamento.append(user1.email)

            valor_a_pagar = 30.
            valor_ja_pago = 0.

            pagamentos = user2.inscricao_set.last().pagamentos.all()

            for pagamento in pagamentos:
                valor_ja_pago += pagamento.valor_pagamento

            if valor_ja_pago < valor_a_pagar:
                usuarios_pendentes_de_pagamento.append(user2.email)

            valor_a_pagar = 30.
            valor_ja_pago = 0.

            pagamentos = user3.inscricao_set.last().pagamentos.all()

            for pagamento in pagamentos:
                valor_ja_pago += pagamento.valor_pagamento

            if valor_ja_pago < valor_a_pagar:
                usuarios_pendentes_de_pagamento.append(user3.email)

            if len(usuarios_pendentes_de_pagamento) > 0:
                context_dictionary['usuarios_pendentes_de_pagamento'] = usuarios_pendentes_de_pagamento
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)


            if not user1.inscricao_set.last().desafio_de_programadores:
                usuarios_com_desafio_desabilitado.append(user1.email)
            if not user2.inscricao_set.last().desafio_de_programadores:
                usuarios_com_desafio_desabilitado.append(user2.email)
            if not user3.inscricao_set.last().desafio_de_programadores:
                usuarios_com_desafio_desabilitado.append(user3.email)

            if len(usuarios_com_desafio_desabilitado) > 0:
                context_dictionary['usuarios_com_desafio_desabilitado'] = usuarios_com_desafio_desabilitado
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                          context_dictionary)

            if user1.inscricao_set.last().inscrito_em_equipe:
                usuarios_ja_inscritos_em_equipe.append(user1.email)
            if user2.inscricao_set.last().inscrito_em_equipe:
                usuarios_ja_inscritos_em_equipe.append(user2.email)
            if user3.inscricao_set.last().inscrito_em_equipe:
                usuarios_ja_inscritos_em_equipe.append(user3.email)

            if len(usuarios_ja_inscritos_em_equipe) > 0:
                context_dictionary['usuarios_ja_inscritos_em_equipe'] = usuarios_ja_inscritos_em_equipe
                return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                              context_dictionary)

            e = EquipeDesafio(ano='2016', inscrita_por=user1.email, nome_equipe=nome_da_equipe,
                              integrante1=user1.email, integrante2=integrante2, integrante3=integrante3)
            e.save()

            inscricao1 = user1.inscricao_set.last()
            inscricao1.inscrito_em_equipe = True
            inscricao1.save()
            inscricao2 = user2.inscricao_set.last()
            inscricao2.inscrito_em_equipe = True
            inscricao2.save()
            inscricao3 = user3.inscricao_set.last()
            inscricao3.inscrito_em_equipe = True
            inscricao3.save()

            enviar_email_inscricao_equipe_desafio(e)

            context_dictionary['equipe_inscrita'] = True
            context_dictionary['equipe'] = e.nome_equipe
            context_dictionary['integrante1'] = user1.first_name + ' ' + user1.last_name + ' <' + user1.email + '>'
            context_dictionary['integrante2'] = user2.first_name + ' ' + user2.last_name + ' <' + user2.email + '>'
            context_dictionary['integrante3'] = user3.first_name + ' ' + user3.last_name + ' <' + user3.email + '>'

            return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                          context_dictionary)
        else:
            return render(request, 'secompufscar_inscricoes/areadoparticipante/inscricaodeequipenodesafio.html',
                          views.get_context_dictionary())
    else:
        return HttpResponseRedirect('/2016/areadoparticipante/')


def enviar_email_inscricao_equipe_desafio(equipe):

    i1 = User.objects.get(email=equipe.integrante1)
    i2 = User.objects.get(email=equipe.integrante2)
    i3 = User.objects.get(email=equipe.integrante3)

    for i in range(0, 3):
        sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

        from_email = Email('SECOMP UFSCar <contato@secompufscar.com.br>')
        subject = 'Inscrição de equipe no Desafio de Programadores - VII SECOMP UFSCar'

        conteudo_do_email = """
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta charset="utf-8"/>
                <title>Inscrição de equipe no Desafio de Programadores - VII SECOMP UFSCar</title>
            </head>
            <body>
                <h1>Inscrição de equipe no Desafio de Programadores - VII SECOMP UFSCar</h1>
                <p>Olá #NOME!</p>
                <p>A inscrição de sua equipe no III Desafio de Programadores da UFSCar foi confirmada com sucesso.
                </p>
                <p><b>Nome da sua equipe: </b>#EQUIPE<br>
                <b>Integrantes:</b><br>
                - #INTEGRANTE1<br>
                - #INTEGRANTE2<br>
                - #INTEGRANTE3<br>
                </p>

                <h3>Algumas informações importantes:</h3>
                <p>
                - O Desafio será realizado na sexta-feira, dia 30/09, das 8h às 12h. Chegue com antecedência, para que não haja atrasos no início do Desafio.<br>
                - Algum membro não poderá participar? Solicite alteração o quanto antes! A sua equipe só poderá participar do desafio se estiver completa, com os 3 integrantes.<br>
                - Haverá um Aquecimento para o Desafio de Programadores na quarta-feira, dia 28/09, das 19h às 21h. A presença no Aquecimento não é obrigatória.<br>
                - Leve um comprovante de que você está cursando no máximo a graduação. Pode ser a carteirinha da universidade ou atestado de matrícula, por exemplo.
                </p>

                <p>Em caso de problemas, entre em contato conosco pelo e-mail <a href="mailto:contato@secompufscar.com.br">
                contato@secompufscar.com.br</a> informando sua equipe.</p>
                <p>Nos vemos em breve!<br>
                Equipe VII SECOMP UFSCar</p>
            </body>
            </html>
            """

        if i == 0:
            to_email = Email(equipe.integrante1)
            conteudo_do_email = conteudo_do_email.replace('#NOME', i1.first_name)
        elif i == 1:
            to_email = Email(equipe.integrante2)
            conteudo_do_email = conteudo_do_email.replace('#NOME', i2.first_name)
        elif i == 2:
            to_email = Email(equipe.integrante3)
            conteudo_do_email = conteudo_do_email.replace('#NOME', i3.first_name)

        conteudo_do_email = conteudo_do_email.replace('#EQUIPE', equipe.nome_equipe)
        conteudo_do_email = conteudo_do_email.replace('#INTEGRANTE1', i1.first_name + ' ' + i1.last_name)
        conteudo_do_email = conteudo_do_email.replace('#INTEGRANTE2', i2.first_name + ' ' + i2.last_name)
        conteudo_do_email = conteudo_do_email.replace('#INTEGRANTE3', i3.first_name + ' ' + i3.last_name)

        content = Content("text/html", conteudo_do_email)
        ready_mail = Mail(from_email, subject, to_email, content)

        sg.client.mail.send.post(request_body=ready_mail.get())


def inscricao_minicurso(request):

    MINICURSOS = []

    minicursos = Atividade.objects.filter(tipo_atividade='Minicurso')

    for m in minicursos:
        MINICURSOS.append(str(m.id))

    if request.method == 'POST':

        inscricao = request.user.inscricao_set.last()
        if inscricao.pacote == 'FREE':
            return HttpResponseRedirect('/2016/areadoparticipante/')

        id_minicurso = request.POST['id_minicurso']

        if id_minicurso not in MINICURSOS:
            return HttpResponseRedirect('/2016/areadoparticipante/')

        if Atividade.objects.get(id=id_minicurso).vagas_disponiveis_atividade > 0:

            print(type(id_minicurso))



            if inscricao.minicurso1 is not None:
                print('minicurso 1 = ' + str(inscricao.minicurso1))
            else:
                print('Entrei aqui')
                print(MINICURSOS)

                # if id_minicurso in MINICURSOS:
                print('Entrei denovo')
                inscricao.minicurso1 = int(id_minicurso)
                print('so close')
                inscricao.save()
                minicurso_inscrito = Atividade.objects.get(id=id_minicurso)
                minicurso_inscrito.vagas_disponiveis_atividade -= 1
                minicurso_inscrito.save()
                print('inscricao salva')

            return HttpResponseRedirect('/2016/areadoparticipante/dashboard/#secao_minicursos')
        else:
            context_dictionary = views.get_context_dictionary().copy()
            context_dictionary['vagas_esgotadas'] = True
            context_dictionary['minicurso_vagas_esgotadas'] = Atividade.objects.get(id=id_minicurso).nome_atividade
            context_dictionary['minicursos'] = Atividade.objects.filter(tipo_atividade='Minicurso').filter(
                esta_ativa_atividade=True)
            context_dictionary['workshops'] = Atividade.objects.filter(tipo_atividade='Workshop').filter(
                esta_ativa_atividade=True)
            context_dictionary['palestras'] = Atividade.objects.filter(tipo_atividade='Palestra').filter(
                esta_ativa_atividade=True)
            context_dictionary['mesasredondas'] = Atividade.objects.filter(tipo_atividade='Mesa-redonda').filter(
                esta_ativa_atividade=True)
            return render(request, 'secompufscar_inscricoes/areadoparticipante/dashboard.html', context_dictionary)


    else:
        return HttpResponseRedirect('/2016/areadoparticipante/')


def cancelar_inscricao_minicurso(request):
    if request.method == 'POST':
        inscricao = request.user.inscricao_set.last()
        inscricao.minicurso1 = None
        inscricao.save()

        try:
            minicurso_cancelado = Atividade.objects.get(id=request.POST['id_minicurso'])
            minicurso_cancelado.vagas_disponiveis_atividade += 1
            minicurso_cancelado.save()
        except:
            pass

    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/#secao_minicursos')


def inscricao_workshop(request):
    WORKSHOPS = []

    workshops = Atividade.objects.filter(tipo_atividade='Workshop')

    for w in workshops:
        WORKSHOPS.append(str(w.id))

    if request.method == 'POST':

        inscricao = request.user.inscricao_set.last()

        id_workshop = request.POST['id_workshop']

        if id_workshop not in WORKSHOPS:
            return HttpResponseRedirect('/2016/areadoparticipante/')

        if Atividade.objects.get(id=id_workshop).vagas_disponiveis_atividade > 0:

            ja_inscrito = False

            workshops_inscritos = inscricao.workshops

            for w in workshops_inscritos.all():
                if w.id == id_workshop:
                    ja_inscrito = True

            if not ja_inscrito:
                workshop = Atividade.objects.get(id=id_workshop)
                inscricao.workshops.add(workshop)
                inscricao.save()
                workshop.vagas_disponiveis_atividade -= 1
                workshop.save()

            return HttpResponseRedirect('/2016/areadoparticipante/dashboard/#secao_workshops')
        else:
            context_dictionary = views.get_context_dictionary().copy()
            context_dictionary['vagas_esgotadas_workshops'] = True
            context_dictionary['workshop_vagas_esgotadas'] = Atividade.objects.get(id=id_workshop).nome_atividade
            context_dictionary['minicursos'] = Atividade.objects.filter(tipo_atividade='Minicurso').filter(
                esta_ativa_atividade=True)
            context_dictionary['workshops'] = Atividade.objects.filter(tipo_atividade='Workshop').filter(
                esta_ativa_atividade=True)
            context_dictionary['palestras'] = Atividade.objects.filter(tipo_atividade='Palestra').filter(
                esta_ativa_atividade=True)
            context_dictionary['mesasredondas'] = Atividade.objects.filter(tipo_atividade='Mesa-redonda').filter(
                esta_ativa_atividade=True)
            return render(request, 'secompufscar_inscricoes/areadoparticipante/dashboard.html', context_dictionary)

    else:
        return HttpResponseRedirect('/2016/areadoparticipante/')


def cancelar_inscricao_workshop(request):
    if request.method == 'POST':
        inscricao = request.user.inscricao_set.last()
        id_workshop = request.POST['id_workshop']

        for w in inscricao.workshops.all():
            if str(w.id) == id_workshop:
                inscricao.workshops.remove(w)
                inscricao.save()
                workshop_a_aumentar_vaga = Atividade.objects.get(id=id_workshop)
                workshop_a_aumentar_vaga.vagas_disponiveis_atividade += 1
                workshop_a_aumentar_vaga.save()

        # workshops = inscricao.workshops
        # workshops.remove(workshop_a_ser_removido)
        # inscricao.save()


    return HttpResponseRedirect('/2016/areadoparticipante/dashboard/#secao_workshops')
