from django.db import models
from secompufscar_inscricoes.views import ANO_ATUAL

# Create your models here.

class Ministrante(models.Model):
    class Meta:
        verbose_name = 'Ministrante'
        verbose_name_plural = 'Ministrantes'

    def __str__(self):  # __unicode__ on Python 2
        return self.nome_ministrante

    nome_ministrante = models.CharField(max_length=255, verbose_name='Nome do ministrante')
    profissao_ministrante = models.CharField(max_length=255, verbose_name='Profissão do ministrante')
    empresa_ministrante = models.CharField(max_length=255, verbose_name='Empresa')
    facebook_ministrante = models.CharField(max_length=255, blank=True, verbose_name='Facebook')
    twitter_ministrante = models.CharField(max_length=255, blank=True, verbose_name='Twitter')
    github_ministrante = models.CharField(max_length=255, blank=True, verbose_name='GitHub')
    linkedin_ministrante = models.CharField(max_length=255, blank=True, verbose_name='LinkedIn')
    aboutme_ministrante = models.CharField(max_length=255, blank=True, verbose_name='AboutMe ou Site')
    descricao_ministrante = models.TextField(verbose_name='Descrição do ministrante')
    foto_ministrante = models.CharField(max_length=255, verbose_name='Nome do arquivo da foto')

class Atividade(models.Model):
    class Meta:
        verbose_name = 'Atividade'
        verbose_name_plural = 'Atividades'
        ordering = ['tipo_atividade', 'data_inicio_atividade', 'hora_inicio_atividade', 'nome_atividade']

    def __str__(self):  # __unicode__ on Python 2
        return self.tipo_atividade + ': ' + self.nome_atividade

    TIPO_ATIVIDADE = (
        ('Palestra', 'Palestra'),
        ('Minicurso', 'Minicurso'),
        ('Workshop', 'Workshop'),
        ('Mesa-redonda', 'Mesa-redonda'),
        ('Mesa de abertura', 'Mesa de abertura')
    )

    LOCAIS = (
        ('', ''),
        ('Anfiteatro Bento Prado Jr.', 'Anfiteatro Bento Prado Jr.'),
        ('Departamento de Computação', 'Departamento de Computação'),
        ('Departamento de Computação, Auditório Mauro Biajiz', 'Departamento de Computação, Auditório Mauro Biajiz'),
        ('Departamento de Computação, LE 2', 'Departamento de Computação, LE 2'),
        ('Departamento de Computação, LE 3', 'Departamento de Computação, LE 3'),
        ('Departamento de Computação, LE 4', 'Departamento de Computação, LE 4'),
        ('Departamento de Computação, LE 5', 'Departamento de Computação, LE 5'),
        ('Departamento de Computação, LE 6', 'Departamento de Computação, LE 6'),
        ('Departamento de Computação, Sala PPG-CC 1', 'Departamento de Computação, Sala PPG-CC 1'),
        ('Saguão da Biblioteca Comunitária', 'Saguão da Biblioteca Comunitária'),
        ('Auditório 2 da Biblioteca Comunitária', 'Auditório 2 da Biblioteca Comunitária')
    )

    tipo_atividade = models.CharField(max_length=20, choices=TIPO_ATIVIDADE, verbose_name='Tipo de atividade')
    nome_atividade = models.CharField(max_length=255, verbose_name='Nome da atividade')
    descricao_atividade = models.TextField(verbose_name='Descrição da atividade')
    data_inicio_atividade = models.DateField(verbose_name='Data de início da atividade')
    hora_inicio_atividade = models.TimeField(verbose_name='Horário de início da atividade')
    hora_fim_atividade = models.TimeField(verbose_name='Horário de fim da atividade')
    hora_retorno_atividade = models.TimeField(verbose_name='Horário de retorno da atividade', null=True, blank=True)
    hora_fim_retorno_atividade = models.TimeField(verbose_name='Horário de fim do retorno da atividade', null=True, blank=True)
    local_atividade = models.CharField(max_length=255, verbose_name='Local da atividade', choices=LOCAIS)
    vagas_atividade = models.IntegerField(blank=True, null=True, verbose_name='Número de vagas')
    vagas_disponiveis_atividade = models.IntegerField(blank=True, null=True, verbose_name='Vagas disponíveis')
    pre_requisitos_atividade = models.CharField(max_length=255, blank=True, verbose_name='Pré-requisitos')
    pre_requisitos_recomendados_atividade = models.CharField(max_length=255, blank=True, verbose_name='Pré-requisitos recomendados')
    ministrante_atividade = models.ManyToManyField(Ministrante, verbose_name='Ministrante da atividade')
    ano_atividade = models.CharField(max_length=4, default=ANO_ATUAL, verbose_name='Ano da atividade')
    esta_ativa_atividade = models.BooleanField(verbose_name='Atividade ativa')


class Patrocinador(models.Model):
    class Meta:
        verbose_name = 'Patrocinador'
        verbose_name_plural = 'Patrocinadores'

    def __str__(self):  # __unicode__ on Python 2
        return self.nome_patrocinador

    TIPO_PATROCINADOR = (
        ('Diamante', 'Diamante'),
        ('Ouro', 'Ouro'),
        ('Prata', 'Prata'),
        ('Apoio', 'Apoio'),
    )

    nome_patrocinador = models.CharField(max_length=50, verbose_name='Patrocinador')
    tipo_patrocinador = models.CharField(max_length=10, choices=TIPO_PATROCINADOR, verbose_name='Cota de patrocínio')
    ano_patrocinador = models.CharField(max_length=4, default='2016', verbose_name='Ano')
    website_patrocinador = models.CharField(max_length=100, blank=True, null=True, verbose_name='Site do patrocinador')
    tempo_animacao_patrocinador = models.CharField(max_length=5, blank=True, null=True, verbose_name='Tempo de animação no site')
    logo_patrocinador = models.CharField(max_length=50, verbose_name='Nome do arquivo do logo do patrocinador')
    ordem_patrocinador = models.IntegerField(default=0, verbose_name='Ordem do patrocinador no site')
    esta_ativo_patrocinador = models.BooleanField(default=False, verbose_name='Ativo')



class MembroDaEquipe(models.Model):
    class Meta:
        verbose_name = 'Membro da Equipe'
        verbose_name_plural = 'Membros da Equipe'

    def __str__(self):  # __unicode__ on Python 2
        return self.nome_membro_equipe

    DIRETORIAS = (
        ('Coordenação Geral', 'Coordenação Geral'),
        ('TI', 'Diretoria de TI'),
        ('Design & Marketing', 'Diretoria de Design & Marketing'),
        ('Patrocínio', 'Diretoria de Patrocínio'),
        ('Conteúdo', 'Diretoria de Conteúdo'),
        ('Jurídico-Financeira', 'Diretoria Jurídico-Financeira'),
        ('Comercial', 'Diretoria Comercial'),
        ('Sociocultural', 'Diretoria Sociocultural'),
        ('Voluntários', 'Voluntários'),
    )

    CURSOS = (
        ('BCC', 'Ciência da Computação'),
        ('EnC', 'Engenharia de Computação')
    )

    ANOS = (
        (2016, '2016'),
        (2015, '2015'),
        (2014, '2014'),
        (2013, '2013'),
        (2012, '2012'),
        (2011, '2011'),
    )

    nome_membro_equipe = models.CharField(max_length=255, verbose_name='Nome do membro da equipe')
    ano_membro_equipe = models.CharField(max_length=4, default='2016', verbose_name='Ano')
    diretoria_membro_equipe = models.CharField(max_length=255, choices=DIRETORIAS, verbose_name='Diretoria')
    curso_membro_equipe = models.CharField(max_length=3, choices=CURSOS, verbose_name='Curso')
    ano_ingresso_membro_equipe = models.IntegerField(choices=ANOS, verbose_name='Ano de ingresso')
    foto_membro_equipe = models.CharField(max_length=255, verbose_name='Nome do arquivo da foto')
    cargo_membro_equipe = models.CharField(max_length=255, blank=True, verbose_name='Cargo')
    email_membro_equipe = models.CharField(max_length=255, blank=True, verbose_name='E-mail')

class PerguntaFrequente(models.Model):
    class Meta:
        verbose_name = 'Pergunta Frequente'
        verbose_name_plural = 'Perguntas Frequentes'

    def __str__(self):
        return self.pergunta

    pergunta = models.CharField(max_length=255, verbose_name='Pergunta')
    resposta = models.TextField(verbose_name='Resposta')
    ano_pergunta_frequente = models.CharField(max_length=4, default='2016', verbose_name='Ano')
    ordem_pergunta_frequente = models.IntegerField(default=0, verbose_name='Ordem da pergunta no site')