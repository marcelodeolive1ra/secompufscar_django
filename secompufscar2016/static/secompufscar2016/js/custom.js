/* jQuery Pre loader
 -----------------------------------------------*/
$(window).on("load", function () {
    $(".preloader").fadeOut(1000); // set duration in brackets
});

/* HTML document is loaded. DOM is ready. 
 -------------------------------------------*/
$(document).ready(function () {

    /* Parallax section
     -----------------------------------------------*/
    function initParallax() {
        $("#intro").parallax("100%", 0.1);
        $("#novaidentidade").parallax("100%", 0.3);
    }

    // Parallax só é ativado na versão desktop
    var $window = $(window);
    if ($window.width() > 1024) {
        initParallax();
    }

    /* Back to top
     -----------------------------------------------*/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $(".go-top").fadeIn(200);
        } else {
            $(".go-top").fadeOut(200);
        }
    });
    // Animate the scroll to top
    $(".go-top").click(function (event) {
        event.preventDefault();
        $("html, body").animate({scrollTop: 0}, 300);
    });

    /* wow
     -------------------------------*/
    var wow = new WOW({mobile: true});
    wow.init();

});

