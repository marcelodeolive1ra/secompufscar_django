from django import forms


class FormularioDeContato(forms.Form):
    name = forms.CharField(label='Seu nome', widget=forms.TextInput(attrs={'placeholder': 'Seu nome'}))
    email = forms.EmailField(label='Seu e-mail', widget=forms.EmailInput(attrs={'placeholder': 'Seu e-mail'}))
    message = forms.CharField(label='Sua mensagem', widget=forms.Textarea(attrs={'placeholder': 'Sua mensagem', 'rows': '5'}))

