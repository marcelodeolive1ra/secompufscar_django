from django.contrib import admin
from .models import Ministrante, Atividade, Patrocinador, MembroDaEquipe, PerguntaFrequente

# Register your models here.
admin.site.register(Ministrante)
admin.site.register(Atividade)
admin.site.register(Patrocinador)
admin.site.register(MembroDaEquipe)
admin.site.register(PerguntaFrequente)

