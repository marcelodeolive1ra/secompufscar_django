# coding: utf-8

from django.conf.urls import include, url
from secompufscar2018 import views
app_name = 'secompufscar2016'


urlpatterns = [
    #  url(r'^$', views.index, name='index'),
    # url(r'^asecomp/$', views.asecomp, name='asecomp'),
    # url(r'^asecomp/apresentacao/$', views.apresentacao, name='apresentacao'),
    # url(r'^asecomp/sociocultural/$', views.sociocultural, name='sociocultural'),
    # url(r'^programacao/$', views.programacao, name='programacao'),
    # url(r'^programacao/palestras/$', views.palestras, name='palestras'),
    # url(r'^programacao/minicursos/$', views.minicursos, name='minicursos'),
    # url(r'^programacao/workshops/$', views.workshops, name='workshops'),
    # url(r'^programacao/enterpriseday/$', views.enterpriseday, name='enterpriseday'),
    # url(r'^programacao/processosseletivos/$', views.processosseletivos, name='processosseletivos'),
    # url(r'^programacao/mesasredondas/$', views.mesasredondas, name='mesasredondas'),
    # url(r'^programacao/gamenight/$', views.gamenight, name='gamenight'),
    # url(r'^programacao/desafiodeprogramadores/$', views.desafiodeprogramadores, name='desafiodeprogramadores'),
    # url(r'^patrocinio/$', views.patrocinio, name='patrocinio'),
    # url(r'^equipe/$', views.equipe, name='equipe'),
    # url(r'^contato/$', views.contato, name='contatonovo'),
    # url(r'^contato/mensagemenviada/$', views.mensagem_enviada, name='mensagemenviada'),
    # url(r'^400/$', views.bad_request, name='400'),
    # url(r'^403/$', views.permission_denied, name='403'),
    # url(r'^404/$', views.page_not_found, name='404'),
    # url(r'^500/$', views.server_error, name='500'),
    # url(r'^areadoparticipante/', include('secompufscar_inscricoes.urls')),
    # url(r'^pacotes/$', views.pacotes, name='pacotes'),
    # url(r'^programacao/cronograma/$', views.cronograma, name='cronograma'),
    # # url(r'^programacao/parcerias/$', views.parcerias, name='parcerias'),
    # url(r'^termos/$', views.termos, name='termos'),
    #
    # url(r'^app/', views.app, name='app'),
    # url(r'^cadastronaredewifi', views.cadastro_rede_wifi, name='cadastroredewifi'),
    # url(r'^programacao/churrascodeintegracao/', views.churrasco, name='churrascodeintegracao'),
]
