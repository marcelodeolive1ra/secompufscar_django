from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from secompufscar_inscricoes.models import *
from django.views.decorators.csrf import csrf_exempt

import socket
import datetime
import sendgrid
from sendgrid.helpers.mail import *

from django.http import JsonResponse

from .forms import FormularioDeContato
from .models import Atividade, Patrocinador, MembroDaEquipe, PerguntaFrequente

if socket.gethostname() == settings.SERVER_NAME:
    GOOGLE_ANALYTICS_KEY = 'UA-80285902-1'
else:
    GOOGLE_ANALYTICS_KEY = 'UA-80174314-1'


def confirm(request):
    return HttpResponse('typs6hpLPg1kHZvlVstgwWeqpzcv4JwC6l4Yooa2OlU.bHlrAvSn2IM4RnYh4x-81uWhP2__r7oSf-xH0HWD1Sg')


def get_context_dictionary():
    context_dictionary = {'patrocinadores_diamante': Patrocinador.objects.filter(tipo_patrocinador='Diamante').filter(
        esta_ativo_patrocinador=True).order_by('ordem_patrocinador'),
                          'patrocinadores_ouro': Patrocinador.objects.filter(tipo_patrocinador='Ouro').filter(
                              esta_ativo_patrocinador=True).order_by('ordem_patrocinador'),
                          'patrocinadores_prata': Patrocinador.objects.filter(tipo_patrocinador='Prata').filter(
                              esta_ativo_patrocinador=True).order_by('ordem_patrocinador'),
                          'apoiadores': Patrocinador.objects.filter(tipo_patrocinador='Apoio').filter(
                              esta_ativo_patrocinador=True).order_by('ordem_patrocinador'),
                          'google_analytics_key': GOOGLE_ANALYTICS_KEY,
                          'horario': datetime.datetime.now().hour,
                          'pagina': '',
                          'perguntas_frequentes': PerguntaFrequente.objects.filter(
                              ano_pergunta_frequente='2016').order_by('ordem_pergunta_frequente')}
    return context_dictionary


context_dictionary = get_context_dictionary()

diretorias = [{'nome': 'Coordenação Geral',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Coordenação Geral')},
              {'nome': 'Diretoria de TI', 'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='TI')},
              {'nome': 'Diretoria de Design & Marketing',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Design & Marketing')},
              {'nome': 'Diretoria de Patrocínio',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Patrocínio')},
              {'nome': 'Diretoria de Conteúdo',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Conteúdo')},
              {'nome': 'Diretoria Jurídico-Financeira',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Jurídico-Financeira')},
              {'nome': 'Diretoria Comercial',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Comercial')},
              {'nome': 'Diretoria Sociocultural',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Sociocultural')},
              {'nome': 'Voluntários',
               'membros': MembroDaEquipe.objects.filter(diretoria_membro_equipe='Voluntários').order_by('nome_membro_equipe')},
              ]


def termos(request):
    return render(request, 'secompufscar2016/termos.html', get_context_dictionary())


def criar_arquivo(file_name, contents):
    save_path = '/static/secompufscar2016/app/'
    of = open(save_path + file_name, 'w')
    of.write(contents)
    of.close()


@csrf_exempt
def app(request):

    if request.method == 'POST':
        print('Requisição post')
        numero_de_inscricao = request.POST['id_usuario']
        id_atividade = request.POST['id_atividade']
        hora = request.POST['horas']
        dia = request.POST['dia']
        mes = request.POST['mes']
        segundos = request.POST['segundos']
        minutos = request.POST['minutos']

        print(numero_de_inscricao)
        print(id_atividade)
        print(hora)
        print(dia)
        print(mes)
        print(segundos)
        print(minutos)

        dicionario = {}
        try:
            inscricao = Inscricao.objects.get(numero_inscricao=numero_de_inscricao)
            user = inscricao.user
            dicionario['status'] = 'OK'
            dicionario['nome_usuario'] = user.first_name + ' ' + user.last_name
            dicionario['nome_usuario'] = dicionario['nome_usuario'].replace('  ', ' ')

            if id_atividade == '0': #credenciamento
                inscricao.credenciado = True
                inscricao.save()
            elif id_atividade != '-1':
                atividade = Atividade.objects.get(id=id_atividade)
                print('atividade okay')
                d = datetime.datetime(day=int(dia), month=int(mes), year=2016, hour=int(hora), minute=int(minutos), second=int(segundos))
                print(d)
                p = Presenca(inscricao=inscricao, id_atividade=atividade, data_hora_remota=datetime.datetime(day=int(dia), month=int(mes), year=2016, hour=int(hora), minute=int(minutos), second=int(segundos)))
                p.save()
                print('teste')
            print(dicionario)
        except ObjectDoesNotExist:
            print('Exceção')
            dicionario['status'] = 'NOT_FOUND'

        return JsonResponse(dicionario)

    else:
        print('Requisição get')

        palestras = Atividade.objects.filter(tipo_atividade='Palestra').filter(esta_ativa_atividade=True)
        dicionario = {}
        todas_palestras = []

        for palestra in palestras:
            dict_palestras = {}
            dict_palestras['id_atividade'] = palestra.id
            dict_palestras['nome_atividade'] = palestra.nome_atividade
            dict_palestras['descricao_atividade'] = palestra.descricao_atividade.replace('<p>', '').replace('</p>', '').replace('<b>', '').replace('</b>', '')
            dict_palestras['data_inicio_atividade'] = str(palestra.data_inicio_atividade.day) + '/' + str(
                palestra.data_inicio_atividade.month)

            if str(palestra.hora_inicio_atividade.minute) == '0':
                dict_palestras['hora_inicio_atividade'] = str(palestra.hora_inicio_atividade.hour) + 'h'
            else:
                dict_palestras['hora_inicio_atividade'] = str(palestra.hora_inicio_atividade.hour) + 'h' + str(
                    palestra.hora_inicio_atividade.minute)

            dict_palestras['hora_fim_atividade'] = palestra.hora_fim_atividade
            dict_palestras['local_atividade'] = palestra.local_atividade
            dict_palestras['ministrante_atividade'] = palestra.ministrante_atividade.last().nome_ministrante
            dict_palestras['foto_atividade'] = 'https://secompufscar.com.br/static/secompufscar2016/images/palestrantes/palestras/' + palestra.ministrante_atividade.last().foto_ministrante

            todas_palestras.append(dict_palestras)

        dicionario['palestras'] = todas_palestras

        minicursos = Atividade.objects.filter(tipo_atividade='Minicurso').filter(esta_ativa_atividade=True)
        todos_minicursos = []

        for minicurso in minicursos:
            dict_minicursos = {}
            dict_minicursos['id_atividade'] = minicurso.id
            dict_minicursos['nome_atividade'] = minicurso.nome_atividade
            dict_minicursos['descricao_atividade'] = minicurso.descricao_atividade.replace('<p>', '').replace('</p>', '').replace('<b>', '').replace('</b>', '')
            dict_minicursos['data_inicio_atividade'] = str(minicurso.data_inicio_atividade.day) + '/' + str(minicurso.data_inicio_atividade.month)

            if str(minicurso.hora_inicio_atividade.minute) == '0':
                dict_minicursos['hora_inicio_atividade'] = str(minicurso.hora_inicio_atividade.hour) + 'h'
            else:
                dict_minicursos['hora_inicio_atividade'] = str(minicurso.hora_inicio_atividade.hour) + 'h' + str(minicurso.hora_inicio_atividade.minute)

            dict_minicursos['hora_fim_atividade'] = minicurso.hora_fim_atividade
            dict_minicursos['local_atividade'] = minicurso.local_atividade
            dict_minicursos['ministrante_atividade'] = minicurso.ministrante_atividade.last().nome_ministrante
            dict_minicursos['foto_atividade'] = 'https://secompufscar.com.br/static/secompufscar2016/images/palestrantes/minicursos/' + minicurso.ministrante_atividade.last().foto_ministrante
            todos_minicursos.append(dict_minicursos)

        dicionario['minicursos'] = todos_minicursos

        workshops = Atividade.objects.filter(tipo_atividade='Workshop').filter(esta_ativa_atividade=True)
        todos_workshops = []

        for workshop in workshops:
            dict_workshops = {}
            dict_workshops['id_atividade'] = workshop.id
            dict_workshops['nome_atividade'] = workshop.nome_atividade
            dict_workshops['descricao_atividade'] = workshop.descricao_atividade.replace('<p>', '').replace('</p>', '').replace('<b>', '').replace('</b>', '')

            dict_workshops['data_inicio_atividade'] = str(workshop.data_inicio_atividade.day) + '/' + str(
                workshop.data_inicio_atividade.month)

            if str(workshop.hora_inicio_atividade.minute) == '0':
                dict_workshops['hora_inicio_atividade'] = str(workshop.hora_inicio_atividade.hour) + 'h'
            else:
                dict_workshops['hora_inicio_atividade'] = str(workshop.hora_inicio_atividade.hour) + 'h' + str(
                    workshop.hora_inicio_atividade.minute)

            dict_workshops['hora_fim_atividade'] = workshop.hora_fim_atividade
            dict_workshops['local_atividade'] = workshop.local_atividade
            dict_workshops['ministrante_atividade'] = workshop.ministrante_atividade.last().nome_ministrante
            dict_workshops['foto_atividade'] = 'https://secompufscar.com.br/static/secompufscar2016/images/palestrantes/workshops/' + workshop.ministrante_atividade.last().foto_ministrante
            todos_workshops.append(dict_workshops)

        dicionario['workshops'] = todos_workshops

        mesasredondas = Atividade.objects.filter(tipo_atividade='Mesa-redonda').filter(esta_ativa_atividade=True)
        todas_mesas_redondas = []

        for mesaredonda in mesasredondas:
            dict_mesasredondas = {}
            dict_mesasredondas['id_atividade'] = mesaredonda.id
            dict_mesasredondas['nome_atividade'] = mesaredonda.nome_atividade
            dict_mesasredondas['descricao_atividade'] = mesaredonda.descricao_atividade.replace('<p>', '').replace('</p>', '').replace('<b>', '').replace('</b>', '')

            dict_mesasredondas['data_inicio_atividade'] = str(mesaredonda.data_inicio_atividade.day) + '/' + str(
                mesaredonda.data_inicio_atividade.month)

            if str(mesaredonda.hora_inicio_atividade.minute) == '0':
                dict_mesasredondas['hora_inicio_atividade'] = str(mesaredonda.hora_inicio_atividade.hour) + 'h'
            else:
                dict_mesasredondas['hora_inicio_atividade'] = str(mesaredonda.hora_inicio_atividade.hour) + 'h' + str(
                    mesaredonda.hora_inicio_atividade.minute)

            dict_mesasredondas['hora_fim_atividade'] = mesaredonda.hora_fim_atividade
            dict_mesasredondas['local_atividade'] = mesaredonda.local_atividade
            todas_mesas_redondas.append(dict_mesasredondas)

        dicionario['workshops'] = todos_workshops
        dicionario['mesasredondas'] = todas_mesas_redondas

        return JsonResponse(dicionario)


def index(request):
    return render(request, 'secompufscar2016/index.html', get_context_dictionary())


def asecomp(request):
    return render(request, 'secompufscar2016/asecomp.html', get_context_dictionary())


def apresentacao(request):
    return render(request, 'secompufscar2016/apresentacao.html', get_context_dictionary())


def sociocultural(request):
    return render(request, 'secompufscar2016/sociocultural.html', get_context_dictionary())


def programacao(request):
    return render(request, 'secompufscar2016/programacao.html', get_context_dictionary())


def palestras(request):
    context_dictionary = get_context_dictionary()
    context_dictionary['atividades'] = Atividade.objects.filter(tipo_atividade='Palestra').filter(
        esta_ativa_atividade=True)
    return render(request, 'secompufscar2016/palestras.html', context_dictionary)


def mesasredondas(request):
    context_dictionary = get_context_dictionary()
    context_dictionary['atividades'] = Atividade.objects.filter(tipo_atividade='Mesa-redonda').filter(esta_ativa_atividade=True)
    return render(request, 'secompufscar2016/mesasredondas.html', context_dictionary)


def gamenight(request):
    return render(request, 'secompufscar2016/gamenight.html', get_context_dictionary())


def desafiodeprogramadores(request):
    return render(request, 'secompufscar2016/desafio.html', get_context_dictionary())


def minicursos(request):
    context_dictionary = get_context_dictionary()
    context_dictionary['atividades'] = Atividade.objects.filter(tipo_atividade='Minicurso').filter(
        esta_ativa_atividade=True)
    return render(request, 'secompufscar2016/minicursos.html', context_dictionary)


def workshops(request):
    context_dictionary = get_context_dictionary()
    context_dictionary['atividades'] = Atividade.objects.filter(tipo_atividade='Workshop').filter(
        esta_ativa_atividade=True)
    return render(request, 'secompufscar2016/workshops.html', context_dictionary)


def cronograma(request):
    return render(request, 'secompufscar2016/cronograma.html', get_context_dictionary())


def processosseletivos(request):
    return render(request, 'secompufscar2016/processosseletivos.html', get_context_dictionary())


def enterpriseday(request):
    context_dictionary = get_context_dictionary().copy()
    context_dictionary['palestras'] = Atividade.objects.filter(tipo_atividade='Palestra', data_inicio_atividade=datetime.date(2016, 9, 27))
    return render(request, 'secompufscar2016/enterpriseday.html', context_dictionary)


def parcerias(request):
    return render(request, 'secompufscar2016/parcerias.html', get_context_dictionary())


def patrocinio(request):
    context_dictionary_patrocinio = get_context_dictionary().copy()
    context_dictionary_patrocinio['pagina'] = 'Patrocínio'
    return render(request, 'secompufscar2016/patrocinio.html', context_dictionary_patrocinio)


def equipe(request):
    context_dictionary = get_context_dictionary()
    context_dictionary['diretorias'] = diretorias
    return render(request, 'secompufscar2016/equipe.html', context_dictionary)


def pacotes(request):
    context_dictionary_pacotes = get_context_dictionary().copy()
    context_dictionary_pacotes['pagina'] = 'Pacotes'
    return render(request, 'secompufscar2016/pacotes.html', context_dictionary_pacotes)


def contato(request):
    if request.method == 'POST':
        form = FormularioDeContato(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data['name']
            user_email = form.cleaned_data['email']
            user_message = form.cleaned_data['message']

            rendered_email_template = render_to_string('secompufscar2016/helpers/emaildecontatosimples.html',
                                                       {'user_name': user_name, 'user_email': user_email,
                                                        'user_message': user_message})

            sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

            from_email = Email(user_name + '<' + user_email + '>')
            subject = 'Contato via site'
            to_email = Email('contato@secompufscar.com.br')
            if socket.gethostname() != settings.SERVER_NAME:
                to_email = Email('marcelo@secompufscar.com.br')

            content = Content("text/html", rendered_email_template)
            ready_mail = Mail(from_email, subject, to_email, content)

            sg.client.mail.send.post(request_body=ready_mail.get())

            return HttpResponseRedirect('/2016/contato/mensagemenviada/')

    else:
        form = FormularioDeContato()

    context_dictionary = get_context_dictionary().copy()
    context_dictionary['form'] = form
    return render(request, 'secompufscar2016/contato.html', context_dictionary)


def mensagem_enviada(request):
    return render(request, 'secompufscar2016/helpers/mensagemenviada.html', get_context_dictionary())


def bad_request(request):
    response = render(request, 'secompufscar2016/400.html', get_context_dictionary())
    response.status_code = 400
    return response


def permission_denied(request):
    response = render(request, 'secompufscar2016/403.html', get_context_dictionary())
    response.status_code = 403
    return response


def page_not_found(request):
    response = render(request, 'secompufscar2016/404.html', get_context_dictionary())
    response.status_code = 404
    return response


def server_error(request):
    response = render(request, 'secompufscar2016/500.html', get_context_dictionary())
    response.status_code = 500
    return response


def cadastro_rede_wifi(request):
    return render(request, 'secompufscar2016/cadastronaredewifi.html', get_context_dictionary())


def churrasco(request):
    return render(request, 'secompufscar2016/churrasco.html', get_context_dictionary())

