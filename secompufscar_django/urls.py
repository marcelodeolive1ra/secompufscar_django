"""secompufscar_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include , handler404
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

app_name = "secompufscar_django"


urlpatterns = [
    path('robots.txt', include('robots.urls')),
    path('admin/', admin.site.urls),
    path('2018/', include('secompufscar2018.urls', namespace='secompufscar2018')),
    path('2018/', include('secompufscar2017.urls', namespace='secompufscar2017')),
    path('2016/', include('secompufscar2016.urls', namespace='secompufscar2016')),
    path('2015/', include('secompufscar2015.urls', namespace='secompufscar2015')),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('', include('secompufscar2019.urls', namespace='root')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#handler404 = 'secompufscar2018.views.erro_404'
handler500 = 'secompufscar2018.views.erro_500'
